(function (console) { "use strict";
var $hxClasses = {},$estr = function() { return js_Boot.__string_rec(this,''); };
function $extend(from, fields) {
	function Inherit() {} Inherit.prototype = from; var proto = new Inherit();
	for (var name in fields) proto[name] = fields[name];
	if( fields.toString !== Object.prototype.toString ) proto.toString = fields.toString;
	return proto;
}
var Communicator = function(roomId,callback,err,cls) {
	this.distance = 0.0;
	this.radian = 0.0;
	this.shottime = 0.0;
	this.receiveFunc = null;
	var _g = this;
	this.ws = new WebSocket("ws://" + Std.string(Reflect.field(window,"WS_SERVER_HOST")) + "/shooting_websocket_servlet");
	this.ws.onopen = function() {
		_g.receiveFunc = function(msgs) {
			if(msgs.length == 2 && msgs[0] == "CONN") {
				if(msgs[1] == "success") callback(true); else if(msgs[1] == "failure") callback(false);
			}
			_g.receiveFunc = null;
		};
		_g.ws.send("CONN,child," + roomId);
	};
	this.ws.onclose = err;
	this.ws.onerror = cls;
	this.ws.onmessage = $bind(this,this.onMessage);
	window.setInterval($bind(this,this.move2),50);
};
$hxClasses["Communicator"] = Communicator;
Communicator.__name__ = true;
Communicator.prototype = {
	onMessage: function(event) {
		var data = event.data;
		var msgs = data.split(",");
		console.log(msgs);
		if(this.receiveFunc != null) this.receiveFunc(msgs);
	}
	,enter: function(name,callback) {
		var _g = this;
		this.receiveFunc = function(msgs) {
			if(msgs.length == 2 && msgs[0] == "ENTR") callback(msgs[1]);
			_g.receiveFunc = null;
		};
		this.ws.send("ENTR," + name);
	}
	,shot: function() {
		var time = new Date().getTime();
		if(this.shottime + 250 >= time) return false;
		this.ws.send("SHOT");
		this.shottime = time;
		return true;
	}
	,move: function(radian,distance) {
		distance *= 10;
		if(radian == 0.0 || this.radian == 0.0) this.ws.send("MOVE," + radian + "," + distance);
		this.radian = radian;
		this.distance = distance;
	}
	,move2: function() {
		if(this.radian != 0.0) this.ws.send("MOVE," + this.radian + "," + this.distance);
	}
	,playerReady: function(on) {
		var s;
		if(on) s = "on"; else s = "off";
		this.ws.send("PRDY," + s);
	}
	,allReady: function(callback) {
		this.receiveFunc = function(msgs) {
			if(msgs.length == 2 && msgs[0] == "ARDY") callback(msgs[1] == "on"?true:false);
		};
	}
	,__class__: Communicator
};
var DateTools = function() { };
$hxClasses["DateTools"] = DateTools;
DateTools.__name__ = true;
DateTools.delta = function(d,t) {
	var t1 = d.getTime() + t;
	var d1 = new Date();
	d1.setTime(t1);
	return d1;
};
var EControllerScene = { __ename__ : true, __constructs__ : ["TitleScene","ControllerScene","ConfigScene","InfoScene"] };
EControllerScene.TitleScene = ["TitleScene",0];
EControllerScene.TitleScene.toString = $estr;
EControllerScene.TitleScene.__enum__ = EControllerScene;
EControllerScene.ControllerScene = ["ControllerScene",1];
EControllerScene.ControllerScene.toString = $estr;
EControllerScene.ControllerScene.__enum__ = EControllerScene;
EControllerScene.ConfigScene = ["ConfigScene",2];
EControllerScene.ConfigScene.toString = $estr;
EControllerScene.ConfigScene.__enum__ = EControllerScene;
EControllerScene.InfoScene = ["InfoScene",3];
EControllerScene.InfoScene.toString = $estr;
EControllerScene.InfoScene.__enum__ = EControllerScene;
EControllerScene.__empty_constructs__ = [EControllerScene.TitleScene,EControllerScene.ControllerScene,EControllerScene.ConfigScene,EControllerScene.InfoScene];
var EReg = function(r,opt) {
	opt = opt.split("u").join("");
	this.r = new RegExp(r,opt);
};
$hxClasses["EReg"] = EReg;
EReg.__name__ = true;
EReg.prototype = {
	replace: function(s,by) {
		return s.replace(this.r,by);
	}
	,__class__: EReg
};
var GameMgr = function() { };
$hxClasses["GameMgr"] = GameMgr;
GameMgr.__name__ = true;
GameMgr.init = function() {
	GameMgr.setWidthHeight();
	GameMgr.ce = js_Boot.__cast(window.document.createElement("canvas") , HTMLCanvasElement);
	GameMgr.ce.setAttribute("width","" + GameMgr.WIDTH);
	GameMgr.ce.setAttribute("height","" + GameMgr.HEIGHT);
	GameMgr.ce.style.position = "absolute";
	GameMgr.ce.style.zIndex = "60";
	window.document.body.appendChild(GameMgr.ce);
	GameMgr.div = js_Boot.__cast(window.document.createElement("div") , HTMLDivElement);
	GameMgr.div.style.width = GameMgr.WIDTH + "px";
	GameMgr.div.style.height = GameMgr.HEIGHT + "px";
	GameMgr.div.style.position = "absolute";
	GameMgr.div.style.zIndex = "70";
	GameMgr.div.addEventListener("touchstart",function(event) {
		console.log("touchstart");
		GameMgr.scene.onTouchStart(event);
	});
	GameMgr.div.addEventListener("touchmove",function(event1) {
		GameMgr.scene.onTouchMove(event1);
	});
	GameMgr.div.addEventListener("touchend",function(event2) {
		GameMgr.scene.onTouchEnd(event2);
	});
	GameMgr.div.addEventListener("touchcancel",function(event3) {
		GameMgr.scene.onTouchEnd(event3);
	});
	window.document.body.appendChild(GameMgr.div);
	var _g = 0;
	var _g1 = Type.allEnums(EControllerScene);
	while(_g < _g1.length) {
		var s = _g1[_g];
		++_g;
		var inst = Type.createInstance(Type.resolveClass("scene." + Std.string(s)),[]);
		GameMgr.sceneMap.set(s,inst);
	}
	var url = window.location.href.split("/");
	var roomId = url[url.length - 1];
	GameMgr.com = new Communicator(roomId,function(result) {
		if(result) GameMgr.changeScene(EControllerScene.TitleScene,[]); else GameMgr.changeScene(EControllerScene.InfoScene,[1]);
	},function() {
		GameMgr.changeScene(EControllerScene.InfoScene,[3]);
	},function() {
		GameMgr.changeScene(EControllerScene.InfoScene,[2]);
	});
	GameMgr.nowScene = EControllerScene.InfoScene;
	GameMgr.scene = GameMgr.sceneMap.get(GameMgr.nowScene);
	GameMgr.scene.initialize(null,[4]);
	window.onorientationchange = GameMgr.repaint;
	window.requestAnimationFrame(GameMgr.repaint);
};
GameMgr.repaint = function(time) {
	if(GameMgr.nxtScene != null) {
		GameMgr.scene.finalize();
		GameMgr.scene = GameMgr.sceneMap.get(GameMgr.nxtScene);
		GameMgr.scene.initialize(GameMgr.nowScene,GameMgr.nxtSceneArgs);
		GameMgr.nowScene = GameMgr.nxtScene;
		GameMgr.nxtScene = null;
		GameMgr.nxtSceneArgs = null;
	}
	var context = GameMgr.ce.getContext("2d",null);
	GameMgr.setWidthHeight();
	context.canvas.width = GameMgr.WIDTH;
	context.canvas.height = GameMgr.HEIGHT;
	context.clearRect(0,0,GameMgr.WIDTH,GameMgr.HEIGHT);
	GameMgr.scene.update(context);
	window.requestAnimationFrame(GameMgr.repaint);
};
GameMgr.changeScene = function(nextScene,args) {
	GameMgr.nxtScene = nextScene;
	if(args != null) GameMgr.nxtSceneArgs = args; else GameMgr.nxtSceneArgs = [];
};
GameMgr.setWidthHeight = function() {
	GameMgr.WIDTH = window.screen.width;
	GameMgr.HEIGHT = window.screen.height;
	if(GameMgr.WIDTH < GameMgr.HEIGHT) {
		console.log(1);
		var tmp = GameMgr.WIDTH;
		GameMgr.WIDTH = GameMgr.HEIGHT;
		GameMgr.HEIGHT = tmp;
	}
};
var HxOverrides = function() { };
$hxClasses["HxOverrides"] = HxOverrides;
HxOverrides.__name__ = true;
HxOverrides.cca = function(s,index) {
	var x = s.charCodeAt(index);
	if(x != x) return undefined;
	return x;
};
HxOverrides.substr = function(s,pos,len) {
	if(pos != null && pos != 0 && len != null && len < 0) return "";
	if(len == null) len = s.length;
	if(pos < 0) {
		pos = s.length + pos;
		if(pos < 0) pos = 0;
	} else if(len < 0) len = s.length + len - pos;
	return s.substr(pos,len);
};
var Main = function() { };
$hxClasses["Main"] = Main;
Main.__name__ = true;
Main.main = function() {
	window.onload = GameMgr.init;
};
Math.__name__ = true;
var Reflect = function() { };
$hxClasses["Reflect"] = Reflect;
Reflect.__name__ = true;
Reflect.field = function(o,field) {
	try {
		return o[field];
	} catch( e ) {
		if (e instanceof js__$Boot_HaxeError) e = e.val;
		return null;
	}
};
Reflect.compare = function(a,b) {
	if(a == b) return 0; else if(a > b) return 1; else return -1;
};
Reflect.isEnumValue = function(v) {
	return v != null && v.__enum__ != null;
};
var Std = function() { };
$hxClasses["Std"] = Std;
Std.__name__ = true;
Std.string = function(s) {
	return js_Boot.__string_rec(s,"");
};
Std["int"] = function(x) {
	return x | 0;
};
Std.parseInt = function(x) {
	var v = parseInt(x,10);
	if(v == 0 && (HxOverrides.cca(x,1) == 120 || HxOverrides.cca(x,1) == 88)) v = parseInt(x);
	if(isNaN(v)) return null;
	return v;
};
var StringTools = function() { };
$hxClasses["StringTools"] = StringTools;
StringTools.__name__ = true;
StringTools.isSpace = function(s,pos) {
	var c = HxOverrides.cca(s,pos);
	return c > 8 && c < 14 || c == 32;
};
StringTools.ltrim = function(s) {
	var l = s.length;
	var r = 0;
	while(r < l && StringTools.isSpace(s,r)) r++;
	if(r > 0) return HxOverrides.substr(s,r,l - r); else return s;
};
var Type = function() { };
$hxClasses["Type"] = Type;
Type.__name__ = true;
Type.resolveClass = function(name) {
	var cl = $hxClasses[name];
	if(cl == null || !cl.__name__) return null;
	return cl;
};
Type.createInstance = function(cl,args) {
	var _g = args.length;
	switch(_g) {
	case 0:
		return new cl();
	case 1:
		return new cl(args[0]);
	case 2:
		return new cl(args[0],args[1]);
	case 3:
		return new cl(args[0],args[1],args[2]);
	case 4:
		return new cl(args[0],args[1],args[2],args[3]);
	case 5:
		return new cl(args[0],args[1],args[2],args[3],args[4]);
	case 6:
		return new cl(args[0],args[1],args[2],args[3],args[4],args[5]);
	case 7:
		return new cl(args[0],args[1],args[2],args[3],args[4],args[5],args[6]);
	case 8:
		return new cl(args[0],args[1],args[2],args[3],args[4],args[5],args[6],args[7]);
	default:
		throw new js__$Boot_HaxeError("Too many arguments");
	}
	return null;
};
Type.allEnums = function(e) {
	return e.__empty_constructs__;
};
var haxe_IMap = function() { };
$hxClasses["haxe.IMap"] = haxe_IMap;
haxe_IMap.__name__ = true;
var haxe_ds_BalancedTree = function() {
};
$hxClasses["haxe.ds.BalancedTree"] = haxe_ds_BalancedTree;
haxe_ds_BalancedTree.__name__ = true;
haxe_ds_BalancedTree.prototype = {
	set: function(key,value) {
		this.root = this.setLoop(key,value,this.root);
	}
	,get: function(key) {
		var node = this.root;
		while(node != null) {
			var c = this.compare(key,node.key);
			if(c == 0) return node.value;
			if(c < 0) node = node.left; else node = node.right;
		}
		return null;
	}
	,setLoop: function(k,v,node) {
		if(node == null) return new haxe_ds_TreeNode(null,k,v,null);
		var c = this.compare(k,node.key);
		if(c == 0) return new haxe_ds_TreeNode(node.left,k,v,node.right,node == null?0:node._height); else if(c < 0) {
			var nl = this.setLoop(k,v,node.left);
			return this.balance(nl,node.key,node.value,node.right);
		} else {
			var nr = this.setLoop(k,v,node.right);
			return this.balance(node.left,node.key,node.value,nr);
		}
	}
	,balance: function(l,k,v,r) {
		var hl;
		if(l == null) hl = 0; else hl = l._height;
		var hr;
		if(r == null) hr = 0; else hr = r._height;
		if(hl > hr + 2) {
			if((function($this) {
				var $r;
				var _this = l.left;
				$r = _this == null?0:_this._height;
				return $r;
			}(this)) >= (function($this) {
				var $r;
				var _this1 = l.right;
				$r = _this1 == null?0:_this1._height;
				return $r;
			}(this))) return new haxe_ds_TreeNode(l.left,l.key,l.value,new haxe_ds_TreeNode(l.right,k,v,r)); else return new haxe_ds_TreeNode(new haxe_ds_TreeNode(l.left,l.key,l.value,l.right.left),l.right.key,l.right.value,new haxe_ds_TreeNode(l.right.right,k,v,r));
		} else if(hr > hl + 2) {
			if((function($this) {
				var $r;
				var _this2 = r.right;
				$r = _this2 == null?0:_this2._height;
				return $r;
			}(this)) > (function($this) {
				var $r;
				var _this3 = r.left;
				$r = _this3 == null?0:_this3._height;
				return $r;
			}(this))) return new haxe_ds_TreeNode(new haxe_ds_TreeNode(l,k,v,r.left),r.key,r.value,r.right); else return new haxe_ds_TreeNode(new haxe_ds_TreeNode(l,k,v,r.left.left),r.left.key,r.left.value,new haxe_ds_TreeNode(r.left.right,r.key,r.value,r.right));
		} else return new haxe_ds_TreeNode(l,k,v,r,(hl > hr?hl:hr) + 1);
	}
	,compare: function(k1,k2) {
		return Reflect.compare(k1,k2);
	}
	,__class__: haxe_ds_BalancedTree
};
var haxe_ds_TreeNode = function(l,k,v,r,h) {
	if(h == null) h = -1;
	this.left = l;
	this.key = k;
	this.value = v;
	this.right = r;
	if(h == -1) this._height = ((function($this) {
		var $r;
		var _this = $this.left;
		$r = _this == null?0:_this._height;
		return $r;
	}(this)) > (function($this) {
		var $r;
		var _this1 = $this.right;
		$r = _this1 == null?0:_this1._height;
		return $r;
	}(this))?(function($this) {
		var $r;
		var _this2 = $this.left;
		$r = _this2 == null?0:_this2._height;
		return $r;
	}(this)):(function($this) {
		var $r;
		var _this3 = $this.right;
		$r = _this3 == null?0:_this3._height;
		return $r;
	}(this))) + 1; else this._height = h;
};
$hxClasses["haxe.ds.TreeNode"] = haxe_ds_TreeNode;
haxe_ds_TreeNode.__name__ = true;
haxe_ds_TreeNode.prototype = {
	__class__: haxe_ds_TreeNode
};
var haxe_ds_EnumValueMap = function() {
	haxe_ds_BalancedTree.call(this);
};
$hxClasses["haxe.ds.EnumValueMap"] = haxe_ds_EnumValueMap;
haxe_ds_EnumValueMap.__name__ = true;
haxe_ds_EnumValueMap.__interfaces__ = [haxe_IMap];
haxe_ds_EnumValueMap.__super__ = haxe_ds_BalancedTree;
haxe_ds_EnumValueMap.prototype = $extend(haxe_ds_BalancedTree.prototype,{
	compare: function(k1,k2) {
		var d = k1[1] - k2[1];
		if(d != 0) return d;
		var p1 = k1.slice(2);
		var p2 = k2.slice(2);
		if(p1.length == 0 && p2.length == 0) return 0;
		return this.compareArgs(p1,p2);
	}
	,compareArgs: function(a1,a2) {
		var ld = a1.length - a2.length;
		if(ld != 0) return ld;
		var _g1 = 0;
		var _g = a1.length;
		while(_g1 < _g) {
			var i = _g1++;
			var d = this.compareArg(a1[i],a2[i]);
			if(d != 0) return d;
		}
		return 0;
	}
	,compareArg: function(v1,v2) {
		if(Reflect.isEnumValue(v1) && Reflect.isEnumValue(v2)) return this.compare(v1,v2); else if((v1 instanceof Array) && v1.__enum__ == null && ((v2 instanceof Array) && v2.__enum__ == null)) return this.compareArgs(v1,v2); else return Reflect.compare(v1,v2);
	}
	,__class__: haxe_ds_EnumValueMap
});
var haxe_ds_StringMap = function() {
	this.h = { };
};
$hxClasses["haxe.ds.StringMap"] = haxe_ds_StringMap;
haxe_ds_StringMap.__name__ = true;
haxe_ds_StringMap.__interfaces__ = [haxe_IMap];
haxe_ds_StringMap.prototype = {
	set: function(key,value) {
		if(__map_reserved[key] != null) this.setReserved(key,value); else this.h[key] = value;
	}
	,get: function(key) {
		if(__map_reserved[key] != null) return this.getReserved(key);
		return this.h[key];
	}
	,setReserved: function(key,value) {
		if(this.rh == null) this.rh = { };
		this.rh["$" + key] = value;
	}
	,getReserved: function(key) {
		if(this.rh == null) return null; else return this.rh["$" + key];
	}
	,__class__: haxe_ds_StringMap
};
var js__$Boot_HaxeError = function(val) {
	Error.call(this);
	this.val = val;
	this.message = String(val);
	if(Error.captureStackTrace) Error.captureStackTrace(this,js__$Boot_HaxeError);
};
$hxClasses["js._Boot.HaxeError"] = js__$Boot_HaxeError;
js__$Boot_HaxeError.__name__ = true;
js__$Boot_HaxeError.__super__ = Error;
js__$Boot_HaxeError.prototype = $extend(Error.prototype,{
	__class__: js__$Boot_HaxeError
});
var js_Boot = function() { };
$hxClasses["js.Boot"] = js_Boot;
js_Boot.__name__ = true;
js_Boot.getClass = function(o) {
	if((o instanceof Array) && o.__enum__ == null) return Array; else {
		var cl = o.__class__;
		if(cl != null) return cl;
		var name = js_Boot.__nativeClassName(o);
		if(name != null) return js_Boot.__resolveNativeClass(name);
		return null;
	}
};
js_Boot.__string_rec = function(o,s) {
	if(o == null) return "null";
	if(s.length >= 5) return "<...>";
	var t = typeof(o);
	if(t == "function" && (o.__name__ || o.__ename__)) t = "object";
	switch(t) {
	case "object":
		if(o instanceof Array) {
			if(o.__enum__) {
				if(o.length == 2) return o[0];
				var str2 = o[0] + "(";
				s += "\t";
				var _g1 = 2;
				var _g = o.length;
				while(_g1 < _g) {
					var i1 = _g1++;
					if(i1 != 2) str2 += "," + js_Boot.__string_rec(o[i1],s); else str2 += js_Boot.__string_rec(o[i1],s);
				}
				return str2 + ")";
			}
			var l = o.length;
			var i;
			var str1 = "[";
			s += "\t";
			var _g2 = 0;
			while(_g2 < l) {
				var i2 = _g2++;
				str1 += (i2 > 0?",":"") + js_Boot.__string_rec(o[i2],s);
			}
			str1 += "]";
			return str1;
		}
		var tostr;
		try {
			tostr = o.toString;
		} catch( e ) {
			if (e instanceof js__$Boot_HaxeError) e = e.val;
			return "???";
		}
		if(tostr != null && tostr != Object.toString && typeof(tostr) == "function") {
			var s2 = o.toString();
			if(s2 != "[object Object]") return s2;
		}
		var k = null;
		var str = "{\n";
		s += "\t";
		var hasp = o.hasOwnProperty != null;
		for( var k in o ) {
		if(hasp && !o.hasOwnProperty(k)) {
			continue;
		}
		if(k == "prototype" || k == "__class__" || k == "__super__" || k == "__interfaces__" || k == "__properties__") {
			continue;
		}
		if(str.length != 2) str += ", \n";
		str += s + k + " : " + js_Boot.__string_rec(o[k],s);
		}
		s = s.substring(1);
		str += "\n" + s + "}";
		return str;
	case "function":
		return "<function>";
	case "string":
		return o;
	default:
		return String(o);
	}
};
js_Boot.__interfLoop = function(cc,cl) {
	if(cc == null) return false;
	if(cc == cl) return true;
	var intf = cc.__interfaces__;
	if(intf != null) {
		var _g1 = 0;
		var _g = intf.length;
		while(_g1 < _g) {
			var i = _g1++;
			var i1 = intf[i];
			if(i1 == cl || js_Boot.__interfLoop(i1,cl)) return true;
		}
	}
	return js_Boot.__interfLoop(cc.__super__,cl);
};
js_Boot.__instanceof = function(o,cl) {
	if(cl == null) return false;
	switch(cl) {
	case Int:
		return (o|0) === o;
	case Float:
		return typeof(o) == "number";
	case Bool:
		return typeof(o) == "boolean";
	case String:
		return typeof(o) == "string";
	case Array:
		return (o instanceof Array) && o.__enum__ == null;
	case Dynamic:
		return true;
	default:
		if(o != null) {
			if(typeof(cl) == "function") {
				if(o instanceof cl) return true;
				if(js_Boot.__interfLoop(js_Boot.getClass(o),cl)) return true;
			} else if(typeof(cl) == "object" && js_Boot.__isNativeObj(cl)) {
				if(o instanceof cl) return true;
			}
		} else return false;
		if(cl == Class && o.__name__ != null) return true;
		if(cl == Enum && o.__ename__ != null) return true;
		return o.__enum__ == cl;
	}
};
js_Boot.__cast = function(o,t) {
	if(js_Boot.__instanceof(o,t)) return o; else throw new js__$Boot_HaxeError("Cannot cast " + Std.string(o) + " to " + Std.string(t));
};
js_Boot.__nativeClassName = function(o) {
	var name = js_Boot.__toStr.call(o).slice(8,-1);
	if(name == "Object" || name == "Function" || name == "Math" || name == "JSON") return null;
	return name;
};
js_Boot.__isNativeObj = function(o) {
	return js_Boot.__nativeClassName(o) != null;
};
js_Boot.__resolveNativeClass = function(name) {
	return (Function("return typeof " + name + " != \"undefined\" ? " + name + " : null"))();
};
var js_Cookie = function() { };
$hxClasses["js.Cookie"] = js_Cookie;
js_Cookie.__name__ = true;
js_Cookie.set = function(name,value,expireDelay,path,domain) {
	var s = name + "=" + encodeURIComponent(value);
	if(expireDelay != null) {
		var d = DateTools.delta(new Date(),expireDelay * 1000);
		s += ";expires=" + d.toGMTString();
	}
	if(path != null) s += ";path=" + path;
	if(domain != null) s += ";domain=" + domain;
	window.document.cookie = s;
};
js_Cookie.all = function() {
	var h = new haxe_ds_StringMap();
	var a = window.document.cookie.split(";");
	var _g = 0;
	while(_g < a.length) {
		var e = a[_g];
		++_g;
		e = StringTools.ltrim(e);
		var t = e.split("=");
		if(t.length < 2) continue;
		h.set(t[0],decodeURIComponent(t[1].split("+").join(" ")));
	}
	return h;
};
js_Cookie.get = function(name) {
	return js_Cookie.all().get(name);
};
var scene_AbstractControllerScene = function() {
	this.cookieTime = Std.parseInt(Reflect.field(window,"COOKIE_TIME"));
	if(this.cookieTime == null) this.cookieTime = 18000;
};
$hxClasses["scene.AbstractControllerScene"] = scene_AbstractControllerScene;
scene_AbstractControllerScene.__name__ = true;
scene_AbstractControllerScene.prototype = {
	initialize: function(prevScene,args) {
	}
	,finalize: function() {
	}
	,update: function(context) {
	}
	,onTouchStart: function(event) {
	}
	,onTouchMove: function(event) {
	}
	,onTouchEnd: function(event) {
	}
	,getCookie: function(key) {
		return js_Cookie.get(key);
	}
	,setCookie: function(key,value) {
		js_Cookie.set(key,value,this.cookieTime);
	}
	,__class__: scene_AbstractControllerScene
};
var scene_ConfigScene = function() {
	scene_AbstractControllerScene.call(this);
};
$hxClasses["scene.ConfigScene"] = scene_ConfigScene;
scene_ConfigScene.__name__ = true;
scene_ConfigScene.__super__ = scene_AbstractControllerScene;
scene_ConfigScene.prototype = $extend(scene_AbstractControllerScene.prototype,{
	initialize: function(prevScene,args) {
		if(prevScene == EControllerScene.ControllerScene) {
			this.sensitivity = args[0];
			this.reverce = args[1];
		}
	}
	,update: function(context) {
		context.beginPath();
		context.font = "48px \"Times New Roman\"";
		context.fillStyle = "rgb(0, 0, 0)";
		context.textAlign = "center";
		context.textBaseline = "top";
		context.fillText("設定",0.25 * GameMgr.WIDTH,0.1 * GameMgr.HEIGHT - 6);
		context.font = "36px \"Times New Roman\"";
		context.fillText("戻る",0.75 * GameMgr.WIDTH,0.1 * GameMgr.HEIGHT);
		context.textAlign = "left";
		context.fillText("移動速度",0.1 * GameMgr.WIDTH,0.35 * GameMgr.HEIGHT);
		context.fillText("左右反転",0.1 * GameMgr.WIDTH,0.5 * GameMgr.HEIGHT);
		context.closePath();
		context.beginPath();
		context.font = "36px \"Times New Roman\"";
		context.fillStyle = "rgb(0, 0, 0)";
		context.textAlign = "center";
		context.textBaseline = "top";
		context.fillText(this.sensitivity + "%",0.75 * GameMgr.WIDTH,0.35 * GameMgr.HEIGHT);
		context.closePath();
		context.beginPath();
		context.fillStyle = "rgb(0, 0, 0)";
		context.moveTo(0.65 * GameMgr.WIDTH - 36,0.35 * GameMgr.HEIGHT + 18);
		context.lineTo(0.65 * GameMgr.WIDTH,0.35 * GameMgr.HEIGHT);
		context.lineTo(0.65 * GameMgr.WIDTH,0.35 * GameMgr.HEIGHT + 36);
		context.closePath();
		context.fill();
		context.beginPath();
		context.fillStyle = "rgb(0, 0, 0)";
		context.moveTo(0.85 * GameMgr.WIDTH + 36,0.35 * GameMgr.HEIGHT + 18);
		context.lineTo(0.85 * GameMgr.WIDTH,0.35 * GameMgr.HEIGHT);
		context.lineTo(0.85 * GameMgr.WIDTH,0.35 * GameMgr.HEIGHT + 36);
		context.closePath();
		context.fill();
		if(!this.reverce) {
			context.fillStyle = "rgb(0, 0, 0)";
			context.fillRect(0.75 * GameMgr.WIDTH - 134,0.5 * GameMgr.HEIGHT,130,36);
		} else {
			context.fillStyle = "rgb(0, 0, 0)";
			context.fillRect(0.75 * GameMgr.WIDTH + 4,0.5 * GameMgr.HEIGHT,130,36);
		}
		context.beginPath();
		context.font = "36px \"Times New Roman\"";
		context.textAlign = "center";
		context.textBaseline = "top";
		if(this.reverce) {
			context.fillStyle = "#000";
			context.fillText("しない",0.75 * GameMgr.WIDTH - 67,0.5 * GameMgr.HEIGHT);
			context.fillStyle = "#FFF";
			context.fillText("する",0.75 * GameMgr.WIDTH + 67,0.5 * GameMgr.HEIGHT);
		} else {
			context.fillStyle = "#FFF";
			context.fillText("しない",0.75 * GameMgr.WIDTH - 67,0.5 * GameMgr.HEIGHT);
			context.fillStyle = "#000";
			context.fillText("する",0.75 * GameMgr.WIDTH + 67,0.5 * GameMgr.HEIGHT);
		}
		context.closePath();
		context.beginPath();
		context.fillStyle = "rgb(0, 0, 0)";
		context.rect(0.75 * GameMgr.WIDTH - 134,0.5 * GameMgr.HEIGHT,130,36);
		context.closePath();
		context.stroke();
		context.beginPath();
		context.fillStyle = "rgb(0, 0, 0)";
		context.rect(0.75 * GameMgr.WIDTH + 4,0.5 * GameMgr.HEIGHT,130,36);
		context.closePath();
		context.stroke();
		context.beginPath();
		context.fillStyle = "rgb(0, 0, 0)";
		context.rect(0.75 * GameMgr.WIDTH - 70,0.1 * GameMgr.HEIGHT,140,36);
		context.closePath();
		context.stroke();
	}
	,onTouchStart: function(event) {
	}
	,onTouchMove: function(event) {
	}
	,onTouchEnd: function(event) {
		var x = event.changedTouches[0].pageX;
		var y = event.changedTouches[0].pageY;
		if(scene_ConfigScene.btns.speed.left.l() <= x && x <= scene_ConfigScene.btns.speed.left.r() && scene_ConfigScene.btns.speed.left.t() <= y && y < scene_ConfigScene.btns.speed.left.b()) {
			this.sensitivity -= 5;
			if(this.sensitivity < 0) this.sensitivity = 0;
		} else if(scene_ConfigScene.btns.speed.right.l() <= x && x <= scene_ConfigScene.btns.speed.right.r() && scene_ConfigScene.btns.speed.right.t() <= y && y < scene_ConfigScene.btns.speed.right.b()) {
			this.sensitivity += 5;
			if(this.sensitivity > 100) this.sensitivity = 100;
		} else if(scene_ConfigScene.btns.reverse.off.l() <= x && x <= scene_ConfigScene.btns.reverse.off.r() && scene_ConfigScene.btns.reverse.off.t() <= y && y < scene_ConfigScene.btns.reverse.off.b()) this.reverce = false; else if(scene_ConfigScene.btns.reverse.on.l() <= x && x <= scene_ConfigScene.btns.reverse.on.r() && scene_ConfigScene.btns.reverse.on.t() <= y && y < scene_ConfigScene.btns.reverse.on.b()) this.reverce = true; else if(scene_ConfigScene.btns.back.l() <= x && x <= scene_ConfigScene.btns.back.r() && scene_ConfigScene.btns.back.t() <= y && y < scene_ConfigScene.btns.back.b()) GameMgr.changeScene(EControllerScene.ControllerScene,[this.sensitivity,this.reverce]);
	}
	,__class__: scene_ConfigScene
});
var scene_ControllerScene = function() {
	this.playerShotColors = ["#E50000","#0000E5","#008700","#E500E5"];
	this.playerColors = ["#ff0000","#0000ff","#009600","#ff00ff"];
	this.shot = 0;
	this.state = 0;
	this.btnR = { l : function() {
		return 0.6 * GameMgr.WIDTH;
	}, t : function() {
		return 0.1 * GameMgr.HEIGHT;
	}, r : function() {
		return 0.9 * GameMgr.WIDTH;
	}, b : function() {
		return 0.17 * GameMgr.HEIGHT;
	}};
	this.btnL = { l : function() {
		return 0.1 * GameMgr.WIDTH;
	}, t : function() {
		return 0.1 * GameMgr.HEIGHT;
	}, r : function() {
		return 0.4 * GameMgr.WIDTH;
	}, b : function() {
		return 0.17 * GameMgr.HEIGHT;
	}};
	scene_AbstractControllerScene.call(this);
	var s = this.getCookie("configSensitivity");
	if(s != null) this.configSensitivity = Std.parseInt(s); else this.configSensitivity = 50;
	var r = this.getCookie("configReverse");
	if(s != null) if(r == "true") this.configReverse = true; else this.configReverse = false; else this.configReverse = false;
};
$hxClasses["scene.ControllerScene"] = scene_ControllerScene;
scene_ControllerScene.__name__ = true;
scene_ControllerScene.__super__ = scene_AbstractControllerScene;
scene_ControllerScene.prototype = $extend(scene_AbstractControllerScene.prototype,{
	initialize: function(prevScene,args) {
		var _g = this;
		this.outerCircleRadius = Math.min(GameMgr.WIDTH / 2,GameMgr.HEIGHT) / 4;
		this.innerCircleRadius = Math.min(GameMgr.WIDTH / 2,GameMgr.HEIGHT) / 10;
		this.touchInfo = null;
		if(prevScene == EControllerScene.TitleScene) {
			this.playerNumber = Std.parseInt(args[0]) % this.playerColors.length;
			GameMgr.com.allReady(function(on) {
				if(on) _g.state = 2; else _g.state = 0;
			});
		} else if(prevScene == EControllerScene.ConfigScene) {
			this.configSensitivity = args[0];
			this.configReverse = args[1];
			this.setCookie("configSensitivity",this.configSensitivity + "");
			this.setCookie("configReverse",this.configReverse?"true":"false");
		}
	}
	,finalize: function() {
		this.touchInfo = null;
	}
	,update: function(context) {
		this.outerCircleRadius = Math.min(GameMgr.WIDTH / 2,GameMgr.HEIGHT) / 3;
		this.innerCircleRadius = Math.min(GameMgr.WIDTH / 2,GameMgr.HEIGHT) / 9;
		context.lineWidth = 1.0;
		context.textAlign = "center";
		context.textBaseline = "middle";
		context.font = (0.1 * GameMgr.HEIGHT | 0) + "px \"Times New Roman\"";
		context.fillStyle = "#BBB";
		context.fillText("アナログパッド",(this.configReverse?0.75:0.25) * GameMgr.WIDTH,0.8 * GameMgr.HEIGHT);
		if(this.touchInfo != null) {
			context.lineWidth = 2.0;
			context.beginPath();
			context.arc(this.touchInfo.xs,this.touchInfo.ys,this.outerCircleRadius,0,Math.PI * 2);
			context.stroke();
			context.fillStyle = "#777";
			context.beginPath();
			context.arc(this.touchInfo.x,this.touchInfo.y,this.innerCircleRadius,0,Math.PI * 2);
			context.fill();
			context.beginPath();
			context.moveTo(this.touchInfo.xs - 5,this.touchInfo.ys);
			context.lineTo(this.touchInfo.xs + 5,this.touchInfo.ys);
			context.stroke();
			context.beginPath();
			context.moveTo(this.touchInfo.xs,this.touchInfo.ys - 5);
			context.lineTo(this.touchInfo.xs,this.touchInfo.ys + 5);
			context.stroke();
			context.beginPath();
			context.moveTo(this.touchInfo.xs,this.touchInfo.ys);
			context.lineTo(this.touchInfo.x,this.touchInfo.y);
			context.stroke();
		}
		context.lineWidth = 1.0;
		if(this.shot > 0) {
			context.fillStyle = this.playerShotColors[this.playerNumber];
			this.shot--;
		} else context.fillStyle = this.playerColors[this.playerNumber];
		context.fillRect(this.configReverse?0.0:GameMgr.WIDTH / 2,0.0,GameMgr.WIDTH / 2,GameMgr.HEIGHT);
		context.beginPath();
		context.moveTo(GameMgr.WIDTH / 2,0.0);
		context.lineTo(GameMgr.WIDTH / 2,GameMgr.HEIGHT);
		context.stroke();
		context.fillStyle = "#BBB";
		context.fillText("ショットボタン",(this.configReverse?0.25:0.75) * GameMgr.WIDTH,0.8 * GameMgr.HEIGHT);
		if(this.state == 0) {
			context.fillStyle = "#333";
			context.fillRect(this.btnL.l(),this.btnL.t(),this.btnL.r() - this.btnL.l(),this.btnL.b() - this.btnL.t());
			context.font = Std["int"](this.btnL.b() - this.btnL.t()) + "px \"Times New Roman\"";
			context.fillStyle = "#FFF";
			context.fillText("設定",(this.btnL.l() + this.btnL.r()) / 2,(this.btnL.t() + this.btnL.b()) / 2);
		}
		if(this.state != 2) {
			context.fillStyle = "#333";
			context.fillRect(this.btnR.l(),this.btnR.t(),this.btnR.r() - this.btnR.l(),this.btnR.b() - this.btnR.t());
			context.font = Std["int"](this.btnL.b() - this.btnL.t()) + "px \"Times New Roman\"";
			context.fillStyle = "#FFF";
			if(this.state == 0) context.fillText("準備完了",(this.btnR.l() + this.btnR.r()) / 2,(this.btnR.t() + this.btnR.b()) / 2); else context.fillText("準備解除",(this.btnR.l() + this.btnR.r()) / 2,(this.btnR.t() + this.btnR.b()) / 2);
		}
	}
	,onTouchStart: function(event) {
		var list = event.changedTouches;
		var _g1 = 0;
		var _g = list.length;
		while(_g1 < _g) {
			var i = _g1++;
			if(this.btnL.l() < list[i].pageX && list[i].pageX < this.btnL.r() && this.btnL.t() < list[i].pageY && list[i].pageY < this.btnL.b() && this.state == 0) GameMgr.changeScene(EControllerScene.ConfigScene,[this.configSensitivity,this.configReverse]); else if(this.btnR.l() < list[i].pageX && list[i].pageX < this.btnR.r() && this.btnR.t() < list[i].pageY && list[i].pageY < this.btnR.b() && this.state != 2) {
				if(this.state == 0) {
					this.state = 1;
					GameMgr.com.playerReady(true);
				} else if(this.state == 1) {
					this.state = 0;
					GameMgr.com.playerReady(false);
				}
			} else if(!this.configReverse && list[i].pageX < GameMgr.WIDTH / 2 || this.configReverse && list[i].pageX > GameMgr.WIDTH / 2) {
				if(this.touchInfo == null) this.touchInfo = { id : list[i].identifier, xs : list[i].pageX, ys : list[i].pageY, xn : list[i].pageX, yn : list[i].pageY, d : 0.0, r : 0.0, x : list[i].pageX, y : list[i].pageY};
			} else if(!this.configReverse && list[i].pageX > GameMgr.WIDTH / 2 || this.configReverse && list[i].pageX < GameMgr.WIDTH / 2) {
				if(GameMgr.com.shot()) this.shot = 5;
			}
		}
	}
	,onTouchMove: function(event) {
		var list = event.changedTouches;
		var _g1 = 0;
		var _g = list.length;
		while(_g1 < _g) {
			var i = _g1++;
			if(this.touchInfo != null && this.touchInfo.id == list[i].identifier) {
				var t = this.touchInfo;
				t.xn = list[i].pageX;
				t.yn = list[i].pageY;
				t.d = Math.min(Math.sqrt((t.yn - t.ys) * (t.yn - t.ys) + (t.xn - t.xs) * (t.xn - t.xs)),this.outerCircleRadius - this.innerCircleRadius);
				t.r = Math.atan2(t.yn - t.ys,t.xn - t.xs);
				t.x = t.xs + t.d * Math.cos(t.r);
				t.y = t.ys + t.d * Math.sin(t.r);
				GameMgr.com.move(t.r,t.d / (this.outerCircleRadius - this.innerCircleRadius) * (this.configSensitivity * 0.01 + 1.0));
			}
		}
	}
	,onTouchEnd: function(event) {
		var list = event.changedTouches;
		var _g1 = 0;
		var _g = list.length;
		while(_g1 < _g) {
			var i = _g1++;
			if(this.touchInfo != null && this.touchInfo.id == list[i].identifier) {
				this.touchInfo = null;
				GameMgr.com.move(0.0,0.0);
			}
		}
	}
	,__class__: scene_ControllerScene
});
var scene_InfoScene = function() {
	this.infoList = ["エラーが発生しました","部屋が見つかりませんでした","サーバーとの接続が切れました","サーバーとの接続に失敗しました","接続中"];
	scene_AbstractControllerScene.call(this);
};
$hxClasses["scene.InfoScene"] = scene_InfoScene;
scene_InfoScene.__name__ = true;
scene_InfoScene.__super__ = scene_AbstractControllerScene;
scene_InfoScene.prototype = $extend(scene_AbstractControllerScene.prototype,{
	initialize: function(prevScene,args) {
		if(args[0] >= 0 && args[0] < this.infoList.length) this.id = args[0]; else this.id = 0;
	}
	,update: function(context) {
		context.font = Std["int"](0.1 * Math.min(GameMgr.WIDTH,GameMgr.HEIGHT)) + "px \"Times New Roman\"";
		context.fillStyle = "rgb(0, 0, 0)";
		context.textAlign = "center";
		context.textBaseline = "middle";
		context.fillText(this.infoList[this.id],0.5 * GameMgr.WIDTH,0.5 * GameMgr.HEIGHT);
	}
	,__class__: scene_InfoScene
});
var scene_TitleScene = function() {
	this.btn = { l : function() {
		return 0.65 * scene_TitleScene.width;
	}, t : function() {
		return 0.3 * scene_TitleScene.height;
	}, r : function() {
		return 0.95 * scene_TitleScene.width;
	}, b : function() {
		return 0.3 * scene_TitleScene.height + Std["int"](0.1 * Math.min(scene_TitleScene.width,scene_TitleScene.height));
	}};
	this.textfield = null;
	scene_AbstractControllerScene.call(this);
};
$hxClasses["scene.TitleScene"] = scene_TitleScene;
scene_TitleScene.__name__ = true;
scene_TitleScene.__super__ = scene_AbstractControllerScene;
scene_TitleScene.prototype = $extend(scene_AbstractControllerScene.prototype,{
	initialize: function(prevScene,args) {
		scene_TitleScene.width = GameMgr.WIDTH;
		scene_TitleScene.height = GameMgr.HEIGHT;
		this.textfield = js_Boot.__cast(window.document.createElement("input") , HTMLInputElement);
		this.textfield.type = "text";
		this.textfield.setAttribute("maxlength","10");
		this.textfield.setAttribute("placeholder","名前を入力");
		var name = this.getCookie("playerName");
		if(name != null) {
			console.log("cookie");
			this.textfield.value = name;
		}
		this.textfield.style.position = "absolute";
		this.textfield.style.boxSizing = "border-box";
		this.textfield.style.zIndex = "100";
		this.textfield.style.left = 0.05 * scene_TitleScene.width + "px";
		this.textfield.style.width = 0.55 * scene_TitleScene.width + "px";
		this.textfield.style.top = 0.3 * scene_TitleScene.height + "px";
		this.textfield.style.fontSize = Std["int"](0.1 * Math.min(scene_TitleScene.width,scene_TitleScene.height)) + "px";
		window.document.body.appendChild(this.textfield);
		this.state = 0;
	}
	,finalize: function() {
		window.document.body.removeChild(this.textfield);
	}
	,update: function(context) {
		context.font = Std["int"](this.btn.b() - this.btn.t()) + "px \"Times New Roman\"";
		context.fillStyle = "#000";
		context.textAlign = "center";
		context.textBaseline = "middle";
		context.fillText("すまっとシューター",0.5 * scene_TitleScene.width,0.15 * scene_TitleScene.height);
		if(this.state != 1) {
			context.fillStyle = "#333";
			context.fillRect(this.btn.l(),this.btn.t(),this.btn.r() - this.btn.l(),this.btn.b() - this.btn.t());
			context.font = Std["int"](this.btn.b() - this.btn.t()) + "px \"Times New Roman\"";
			context.fillStyle = "#FFF";
			context.textAlign = "center";
			context.textBaseline = "middle";
			if(this.state == 0) context.fillText("入室",(this.btn.l() + this.btn.r()) / 2,(this.btn.t() + this.btn.b()) / 2); else {
				context.fillText("再入室",(this.btn.l() + this.btn.r()) / 2,(this.btn.t() + this.btn.b()) / 2);
				context.font = Std["int"]((this.btn.b() - this.btn.t()) * 0.5) + "px \"Times New Roman\"";
				context.fillStyle = "#F00";
				if(this.state == 3) context.fillText("ゲーム中です",(this.btn.l() + this.btn.r()) / 2,(this.btn.t() + this.btn.b()) / 2 - (this.btn.b() - this.btn.t())); else context.fillText("満員です",(this.btn.l() + this.btn.r()) / 2,(this.btn.t() + this.btn.b()) / 2 - (this.btn.b() - this.btn.t()));
			}
		} else {
			context.font = Std["int"](this.btn.b() - this.btn.t()) + "px \"Times New Roman\"";
			context.fillStyle = "#000";
			context.textAlign = "center";
			context.textBaseline = "middle";
			context.fillText("入室中...",(this.btn.l() + this.btn.r()) / 2,(this.btn.t() + this.btn.b()) / 2);
		}
		context.font = Std["int"]((this.btn.b() - this.btn.t()) / 2) + "px \"Times New Roman\"";
		context.fillStyle = "#F00";
		context.textAlign = "left";
		context.fillText("※ 横向きで使用してください",0.05 * scene_TitleScene.width,0.6 * scene_TitleScene.height);
	}
	,onTouchStart: function(event) {
		var _g2 = this;
		var list = event.changedTouches;
		var _g1 = 0;
		var _g = list.length;
		while(_g1 < _g) {
			var i = _g1++;
			if(this.btn.l() < list[i].pageX && list[i].pageX < this.btn.r() && this.btn.t() < list[i].pageY && list[i].pageY < this.btn.b() && this.state != 1) {
				console.log("ok");
				this.textfield.blur();
				this.textfield.disabled = true;
				var name = this.textfield.value;
				var reg = new EReg("[!\"#$%&'()\\*\\+\\-\\.,/:;<=>?@\\[\\\\\\]^_`{|}~]","g");
				name = reg.replace(name,"");
				if(name == "") name = "Player";
				this.textfield.value = name;
				this.setCookie("playerName",name);
				this.state = 1;
				GameMgr.com.enter(name,function(result) {
					if(result == "max") _g2.state = 2; else if(result == "play") _g2.state = 3; else if(result == "etc") GameMgr.changeScene(EControllerScene.InfoScene,[0]); else if(Std.parseInt(result) == null) GameMgr.changeScene(EControllerScene.InfoScene,[0]); else GameMgr.changeScene(EControllerScene.ControllerScene,[result]);
				});
				break;
			}
		}
	}
	,__class__: scene_TitleScene
});
var $_, $fid = 0;
function $bind(o,m) { if( m == null ) return null; if( m.__id__ == null ) m.__id__ = $fid++; var f; if( o.hx__closures__ == null ) o.hx__closures__ = {}; else f = o.hx__closures__[m.__id__]; if( f == null ) { f = function(){ return f.method.apply(f.scope, arguments); }; f.scope = o; f.method = m; o.hx__closures__[m.__id__] = f; } return f; }
$hxClasses.Math = Math;
String.prototype.__class__ = $hxClasses.String = String;
String.__name__ = true;
$hxClasses.Array = Array;
Array.__name__ = true;
Date.prototype.__class__ = $hxClasses.Date = Date;
Date.__name__ = ["Date"];
var Int = $hxClasses.Int = { __name__ : ["Int"]};
var Dynamic = $hxClasses.Dynamic = { __name__ : ["Dynamic"]};
var Float = $hxClasses.Float = Number;
Float.__name__ = ["Float"];
var Bool = Boolean;
Bool.__ename__ = ["Bool"];
var Class = $hxClasses.Class = { __name__ : ["Class"]};
var Enum = { };
var __map_reserved = {}
Communicator.MOVE_CORRECTION_VALUE = 10;
Communicator.SHOT_COOL_TIME = 250;
GameMgr.sceneMap = new haxe_ds_EnumValueMap();
js_Boot.__toStr = {}.toString;
scene_ConfigScene.btns = { speed : { left : { l : function() {
	return 0.65 * GameMgr.WIDTH - 36;
}, t : function() {
	return 0.35 * GameMgr.HEIGHT;
}, r : function() {
	return 0.65 * GameMgr.WIDTH;
}, b : function() {
	return 0.35 * GameMgr.HEIGHT + 36;
}}, right : { l : function() {
	return 0.85 * GameMgr.WIDTH;
}, t : function() {
	return 0.35 * GameMgr.HEIGHT;
}, r : function() {
	return 0.85 * GameMgr.WIDTH + 36;
}, b : function() {
	return 0.35 * GameMgr.HEIGHT + 36;
}}}, reverse : { off : { l : function() {
	return 0.75 * GameMgr.WIDTH - 134;
}, t : function() {
	return 0.5 * GameMgr.HEIGHT;
}, r : function() {
	return 0.75 * GameMgr.WIDTH - 4;
}, b : function() {
	return 0.5 * GameMgr.HEIGHT + 36;
}}, on : { l : function() {
	return 0.75 * GameMgr.WIDTH + 4;
}, t : function() {
	return 0.5 * GameMgr.HEIGHT;
}, r : function() {
	return 0.75 * GameMgr.WIDTH + 134;
}, b : function() {
	return 0.5 * GameMgr.HEIGHT + 36;
}}}, back : { l : function() {
	return 0.75 * GameMgr.WIDTH - 70;
}, t : function() {
	return 0.1 * GameMgr.HEIGHT;
}, r : function() {
	return 0.75 * GameMgr.WIDTH + 70;
}, b : function() {
	return 0.1 * GameMgr.HEIGHT + 36;
}}};
scene_ControllerScene.STATE_PREPARATION = 0;
scene_ControllerScene.STATE_READY = 1;
scene_ControllerScene.STATE_PLAY = 2;
scene_InfoScene.ERROR_UNKNOWN = 0;
scene_InfoScene.ERROR_ROOM_NOT_FOUND = 1;
scene_InfoScene.ERROR_CONNECTION_CLOSED = 2;
scene_InfoScene.ERROR_CONNECTION_FAILED = 3;
scene_InfoScene.INFO_CONNECTING = 4;
scene_TitleScene.STATE_NORMAL = 0;
scene_TitleScene.STATE_CONNECTING = 1;
scene_TitleScene.STATE_MAX = 2;
scene_TitleScene.STATE_PLAY = 3;
Main.main();
})(typeof console != "undefined" ? console : {log:function(){}});
