package;

@:native("QRCode")
extern class Qrcode {
	public function new(type:Int, level:Int);
	public function addData(text:String):Void;
	public function make():Void;
	public function getModuleCount():Int;
	public function isDark(row:Int, col:Int):Bool;
}