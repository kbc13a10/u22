package three;

@:native("THREE.PolyhedronGeometry")
extern class PolyhedronGeometry extends Geometry {
    function new( vertices : Array<Float>, faces : Array<Int>,
    			  ?radius : Float, ?detail : Int ) : Void;
}
