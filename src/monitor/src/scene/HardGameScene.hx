package scene;
import action.RotateAction;
import action.RotateRandomAction;
import action.JumpAction;
import obj3d.BreakObject;
import obj3d.InvincibleObject;
import obj3d.MeshEx;
import shape.CrystalShape;
import shape.CubeShape;
import shape.CylinderShape;
import shape.DrumShape;
import shape.FloorShape;
import event.InputEvent;
import three.Mesh;
import three.Raycaster;
import three.Vector3;
import scene.abs.AbstractMoveRouteScene;
import position.RoutePosition.RouteParameter;
import field.FieldObject;
import obj3d.TimerObject;
import shape.TextShape;
import action.MoveAction;

class HardGameScene extends AbstractMoveRouteScene {	
	public function new(context) {
		super(context, 1000);
		audioName = "hard";
	}
	
	override public function once() {
		if (!super.once()) return false;
		//setFloorTexture("images/grasslight-big.jpg", 16);
		
		var routes:Array<RouteParameter> = [
			{ time: 5000, point: new Vector3(0, 0, 3000) },		//5000
			{ time: 8000, point: new Vector3(-5000, 0, 3000) },	//13000
			{ time: 10000, point: new Vector3(-5000, 0, 13000) },	//23000
			{ time: 5000, point: new Vector3(2000, 0, 13000), degree:90 },	//28000
			{ time: 5000, point: new Vector3(2000, 0, 20000) },		//33000
			{ time: 8000, point: new Vector3(10000, 0, 20000) },	//41000
			{ time: 5000, point: new Vector3(10000, 0, 30000), degree:0 },	//46000
			{ time: 5000, point: new Vector3(7000, 0, 30000), degree:0 },	//51000
			{ time: 3000, point: new Vector3(0, 0, 30000), degree:0 },	//54000
			{ time: 6000, point: new Vector3(-3000, 0, 30000), degree:0 }	//59000
		];
		setRouteData(routes);
		
		var roteRightAction = new RotateAction({right:true});
		var jumpAction = new JumpAction( { waitTime: 0 } );
		var delayJump = new JumpAction( { waitTime: 100 } );
		var lowSpeedJump = new JumpAction( { waitTime: 0, gravity: 0.5, initialVelocity: 15 } );
		var middleSpeedJump = new JumpAction( { waitTime: 0, gravity: 1, initialVelocity: 20 } );
		var hiSpeedJump = new JumpAction( { waitTime: 0, gravity: 2, initialVelocity:25 } );
		var lowDropAction = new JumpAction( { waitTime: 0, gravity: 0.5, initialVelocity: 15, up: false } );
		var middleDropAction = new JumpAction( { waitTime: 0, gravity: 1, initialVelocity: 20, up: false } );
		var hiDropAction = new JumpAction( { waitTime: 0, gravity: 2, initialVelocity: 25, up: false } );
		var dropAction = new JumpAction( { waitTime: 0, gravity: 1, initialVelocity: 50, up: false } );
		var delayDrop1 = new JumpAction( { waitTime: 200, gravity: 1, initialVelocity: 50, up: false } );
		var delayDrop2 = new JumpAction( { waitTime: 400, gravity: 1, initialVelocity: 50, up: false } );
		var delayDrop3 = new JumpAction( { waitTime: 600, gravity: 1, initialVelocity: 50, up: false } );
		var randamRoteAction = new RotateRandomAction();
		var moveXAction = new MoveAction({moveType:x});
		var moveRXAction = new MoveAction( { distance: 1000, speed: 30, reverse: true, moveType:x } );
		var escapeXAction = new MoveAction( { distance: 800, speed: 45, reverse: true, moveType:x } );
		var moveX1 = new MoveAction( { distance: 1000, speed: 500, moveType:x } );
		var moveX2 = new MoveAction( { distance: 1000, speed: 500, reverse: true, moveType:x } );
		var slowMoveX = new MoveAction( { distance: 1000, speed: 10, reverse: true, moveType:x } );
		var moveYAction = new MoveAction();
		var escapeYAction = new MoveAction( { distance: 800, speed: 45, reverse: true } );
		var moveY1 = new MoveAction( { distance: 1000, speed: 250 } );
		var moveY2 = new MoveAction( { distance: 1000, speed: 250, reverse: true } );
		var slowMoveY = new MoveAction( { distance: 1000, speed: 100 } );
		var moveZAction = new MoveAction({moveType:z});
		var escapeZAction = new MoveAction( { distance: 800, speed: 45, reverse: true, moveType:z } );
		var moveZ1 = new MoveAction( { distance: 1000, speed: 500, moveType:z } );
		var moveZ2 = new MoveAction( { distance: 1000, speed: 500, reverse: true, moveType:z } );
		var slowMoveZ = new MoveAction( { distance: 1000, speed: 5, moveType:z } );
		var slowMoveRZ = new MoveAction( { distance: 1000, speed: 5, reverse: true, moveType:z } );
		var lastMoveZ = new MoveAction( { distance: 1000, speed: 200, moveType:z } );
		var lastMoveRZ = new MoveAction( { distance: 1000, speed: 200, reverse: true, moveType:z } );
		var lastSlowMoveZ = new MoveAction( { distance: 1000, speed: 10, moveType:z } );
		var lastSlowMoveRZ = new MoveAction( { distance: 1000, speed: 10, reverse: true, moveType:z } );
		
		var crystalColorShape = new CrystalShape( { color: 0xff5555 } );
		var crystalColorYellow = new CrystalShape( 850, { color: 0xfbec35 } );
		var crystalColorBlue = new CrystalShape( 850,{ color: 0x0b2bd7 } );
		var crystalColorGreen = new CrystalShape( 850, { color: 0x0bda51 } ); 
		var crystalColorRed = new CrystalShape( { color: 0xff0037 } ); 
		var crystalColorOrange = new CrystalShape( { color: 0xff5e19 } );
		var crystalColorRandom = new CrystalShape( { randomColor: true } );
		
		var crystalBlackShape = new CrystalShape( { color:0x000000 } );
		var cylinderShape = new CylinderShape();
		var cylinderShapeColorBule = new CylinderShape(900, { color:0x0b2bd7 } );
		
		var cubeShape = new CubeShape();
		var cubeShapeColorRed = new CubeShape( { color:0xcc0000 } );
		var cubeShapeColorBlue = new CubeShape(600, {color:0x4387e9 });
		var drumShape = new DrumShape();
		
		var masterObjs = [
			new BreakObject(crystalColorShape, [moveXAction]),	// 0:Xmoveクリスタル
			new BreakObject(crystalBlackShape, [roteRightAction]),	//1:黒クリスタル
			new BreakObject(cubeShape, [randamRoteAction]),			//2:キューブ
			new BreakObject(crystalColorShape, [jumpAction]),		//3:ジャンプクリスタル
			new BreakObject(cylinderShape, [roteRightAction]),		//4:コイン
			new InvincibleObject(drumShape),								//5:固定ドラム
			new BreakObject(crystalColorShape, [jumpAction, moveXAction]),	//6:Xmoveジャンプクリスタル
			new BreakObject(cubeShape, [randamRoteAction, lowSpeedJump]),	//7:lowスピードジャンプキューブ
			new BreakObject(cubeShape, [randamRoteAction, middleSpeedJump]),//8:middleスピードジャンプキューブ
			new BreakObject(cubeShape, [randamRoteAction, hiSpeedJump]),	//9:hiスピードジャンプキューブ
			new BreakObject(cubeShape, [randamRoteAction, lowDropAction]),	//10:lowスピード落下キューブ
			new BreakObject(cubeShape, [randamRoteAction, middleDropAction]),	//11:middleスピード落下キューブ
			new BreakObject(cubeShape, [randamRoteAction, hiDropAction]),	//12:hiスピード落下キューブ
			new BreakObject(cylinderShape, [roteRightAction, moveXAction]),	//13:Xmoveコイン
			new BreakObject(cylinderShape, [roteRightAction, moveZAction]),	//14:Zmoveコイン
			new BreakObject(crystalColorShape, [escapeXAction]),	//15:逃げクリスタル  遅い
			new BreakObject(crystalColorShape, [delayJump]),		//16:遅延ジャンプクリスタル
			new InvincibleObject(drumShape, [moveX1]),					//17:Xmoveドラム1　遅い
			new InvincibleObject(drumShape, [moveX2]),					//18:Xmoveドラム2　遅い
			new InvincibleObject(drumShape, [moveY1]),					//19:Ymoveドラム1
			new InvincibleObject(drumShape, [moveY2]),					//20:Ymoveドラム2
			new InvincibleObject(drumShape, [moveZ1]),					//21:Zmoveドラム1　遅い
			new InvincibleObject(drumShape, [moveZ2]),					//22:Zmoveドラム2　遅い
			new InvincibleObject(drumShape, [slowMoveX]),				//23:slowMoveXドラム　早い
			new InvincibleObject(drumShape, [slowMoveY]),				//24:slowMoveYドラム　
			new InvincibleObject(drumShape, [slowMoveZ]),				//25:slowMoveZドラム
			new BreakObject(crystalColorShape, [roteRightAction]),		//26:カラークリスタル
			new BreakObject(crystalColorYellow, [roteRightAction, lowSpeedJump, moveRXAction]),	//27:moveRXLowジャンプクリスタル
			new BreakObject(crystalColorBlue, [roteRightAction, middleSpeedJump, moveRXAction]),	//28:moveRXMiddleジャンプクリスタル
			new BreakObject(crystalColorGreen, [roteRightAction, hiSpeedJump, moveRXAction]),	//29:moveRXHiジャンプクリスタル
			new BreakObject(crystalColorOrange, [roteRightAction, slowMoveZ]),	//30:slowMoveZクリスタル　方向違い
			new BreakObject(crystalColorOrange, [roteRightAction, slowMoveRZ]),	//31:slowMoveRZクリスタル　方向違い
			new BreakObject(cylinderShapeColorBule, [roteRightAction, dropAction]),		//32:dropコイン
			new BreakObject(cylinderShapeColorBule, [roteRightAction, delayDrop1]),		//33:delayDropコイン1
			new BreakObject(cylinderShapeColorBule, [roteRightAction, delayDrop2]),		//34:delayDropコイン2
			new BreakObject(cylinderShapeColorBule, [roteRightAction, delayDrop3]),		//35:delayDropコイン3
			new InvincibleObject(drumShape, [lastSlowMoveRZ]),	//36:lastSlowMoveRZドラム
			new InvincibleObject(drumShape, [lastSlowMoveZ]),	//37:lastSlowMoveZドラム
			new BreakObject(crystalColorRed, [roteRightAction]),	//38:ランダムカラークリスタル
			new BreakObject(cubeShapeColorBlue, [randamRoteAction, lowSpeedJump]),	//39:lowスピードジャンプキューブ
		];
		
		var objs:Array<FieldObjectParameter> = [
			{
				obj:masterObjs[4],
				appTime: 1500,
				rpps: 500
			},
			{
				obj:masterObjs[4],
				appTime: 1500,
				distance: 200,
				degree: 270,
				positionY: 200,
				rpps: 500
			},
			{
				obj:masterObjs[4],
				appTime: 1500,
				distance: 200,
				degree: 90,
				positionY: 200,
				rpps: 500
			},
			{
				obj:masterObjs[4],
				appTime: 1500,
				positionY: 400,
				rpps: 500
			},
			{
				obj:masterObjs[4],
				appTime: 2000,
				positionY: 200,
				rpps: 500
			},
			{
				obj:masterObjs[4],
				appTime: 2000,
				distance: 200,
				degree: 270,
				rpps: 500
			},
			{
				obj:masterObjs[4],
				appTime: 2000,
				distance: 200,
				degree: 90,
				rpps: 500
			},
			{
				obj:masterObjs[4],
				appTime: 2000,
				positionY: 400,
				distance: 200,
				degree: 90,
				rpps: 500
			},
			{
				obj:masterObjs[4],
				appTime: 2000,
				positionY: 400,
				distance: 200,
				degree: 270,
				rpps: 500
			},
			{
				obj:masterObjs[4],
				appTime: 2500,
				positionY: 200,
				distance: 400,
				degree: 270,
				rpps: 500
			},
			{ // ここまでコイン
				obj:masterObjs[4],
				appTime: 2500,
				positionY: 200,
				distance: 400,
				degree: 90,
				rpps: 500
			},
			{
				obj:masterObjs[1],
				appTime: 5500,
				distance: 400,
				degree: 90
			},
			{
				obj:masterObjs[1],
				appTime: 6000,
				distance: 400,
				degree: 90
			},
			{
				obj:masterObjs[1],
				appTime: 6500,
				distance: 400,
				degree: 90
			},
			{
				obj:masterObjs[1],
				appTime: 5500,
				distance: 400,
				degree: 270
			},
			{
				obj:masterObjs[1],
				appTime: 6000,
				distance: 400,
				degree: 270
			},
			{
				obj:masterObjs[1],
				appTime: 6500,
				distance: 400,
				degree: 270
			},
			{
				obj:masterObjs[1],
				appTime: 5500,
			},
			{
				obj:masterObjs[1],
				appTime: 6000,
			},
			{	// ここまで黒クリスタル
				obj:masterObjs[1],
				appTime: 6500
			},
			{
				appTime:6000,
				obj:masterObjs[5],
				rpps:1000
			},
			{
				appTime:6000,
				obj:masterObjs[5],
				rpps:1000,
				distance: 300,
				degree: 270
			},
			{
				appTime:6000,
				obj:masterObjs[5],
				rpps:1000,
				distance: 300,
				degree: 90
			},
			{
				appTime:6300,
				obj:masterObjs[7],
				rpps:1000
			},
			{
				appTime:6300,
				obj:masterObjs[7],
				distance: 300,
				degree: 270,
				rpps:1000
			},
			{
				appTime:6300,
				obj:masterObjs[7],
				distance: 300,
				degree: 90,
				rpps:1000
			},
			{
				appTime:6600,
				obj:masterObjs[10],
				distance: 150,
				degree: 90,
				positionY: 400,
				rpps: 1000
			},
			{
				appTime:6600,
				obj:masterObjs[10],
				distance: 150,
				degree: 270,
				positionY: 400,
				rpps: 1000
			},
			{
				appTime:6600,
				obj:masterObjs[10],
				distance: 450,
				degree: 90,
				positionY: 400,
				rpps: 1000
			},
			{	
				appTime:6600,
				obj:masterObjs[10],
				distance: 450,
				degree: 270,
				positionY: 400,
				rpps: 1000
			},
			{
				appTime:7000,
				obj:masterObjs[5],
				rpps:1000
			},
			{
				appTime:7000,
				obj:masterObjs[5],
				rpps:1000,
				distance: 300,
				degree: 270
			},
			{
				appTime:7000,
				obj:masterObjs[5],
				rpps:1000,
				distance: 300,
				degree: 90
			},
			{
				appTime:7300,
				obj:masterObjs[8],
				rpps:1000
			},
			{
				appTime:7300,
				obj:masterObjs[8],
				distance: 300,
				degree: 270,
				rpps:1000
			},
			{
				appTime:7300,
				obj:masterObjs[8],
				distance: 300,
				degree: 90,
				rpps:1000
			},
			{
				appTime:7600,
				obj:masterObjs[11],
				distance: 150,
				degree: 90,
				positionY: 400,
				rpps: 1000
			},
			{
				appTime:7600,
				obj:masterObjs[11],
				distance: 150,
				degree: 270,
				positionY: 400,
				rpps: 1000
			},
			{
				appTime:7600,
				obj:masterObjs[11],
				distance: 450,
				degree: 90,
				positionY: 400,
				rpps: 1000
			},
			{
				appTime:7600,
				obj:masterObjs[11],
				distance: 450,
				degree: 270,
				positionY: 400,
				rpps: 1000
			},
			{
				appTime:8000,
				obj:masterObjs[5],
				rpps:1000
			},
			{
				appTime:8000,
				obj:masterObjs[5],
				distance: 300,
				degree: 270,
				rpps:1000
			},
			{
				appTime:8000,
				obj:masterObjs[5],
				distance: 300,
				degree: 90,
				rpps:1000
			},
			{
				appTime:8300,
				obj:masterObjs[9],
				rpps:1000
			},
			{
				appTime:8300,
				obj:masterObjs[9],
				distance: 300,
				degree: 270,
				rpps:1000
			},
			{
				appTime:8300,
				obj:masterObjs[9],
				distance: 300,
				degree: 90,
				rpps:1000
			},
			{
				appTime:8600,
				obj:masterObjs[12],
				distance: 150,
				degree: 90,
				positionY: 400,
				rpps: 1000
			},
			{
				appTime:8600,
				obj:masterObjs[12],
				distance: 150,
				degree: 270,
				positionY: 400,
				rpps: 1000
			},
			{
				appTime:8600,
				obj:masterObjs[12],
				distance: 450,
				degree: 90,
				positionY: 400,
				rpps: 1000
			},
			{	//ここまでジャンプ＋落下キューブ
				appTime:8600,
				obj:masterObjs[12],
				distance: 450,
				degree: 270,
				positionY: 400,
				rpps: 1000
			},
			{
				appTime:10000,
				obj:masterObjs[5],
				rpps: 2000
			},
			{
				appTime:10000,
				obj:masterObjs[5],
				distance: 300,
				degree: 270,
				rpps: 2000
			},
			{
				appTime:10000,
				obj:masterObjs[5],
				distance: 300,
				degree: 90,
				rpps: 2000
			},
			{
				appTime:11000,
				obj:masterObjs[15]
			},
			{
				appTime:11000,
				obj:masterObjs[15],
				distance: 300,
				degree: 270,
			},
			{
				appTime:11000,
				obj:masterObjs[15],
				distance: 300,
				degree: 90
			},
			{
				appTime:11500,
				obj:masterObjs[16],
				rpps: 1000
			},
			{
				appTime:11500,
				obj:masterObjs[16],
				rpps: 1000,
				distance: 300,
				degree: 90
			},
			{	//ここまで逃げ＋遅延ジャンプクリスタル
				appTime:11500,
				obj:masterObjs[16],
				rpps: 1000,
				distance: 300,
				degree: 270
			},
			{
				appTime:13000,
				obj:masterObjs[17],
				rpps: 1000
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 1000
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 1000,
				distance: 200,
				degree: 90
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 1000,
				distance: 400,
				degree: 90
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 1000,
				distance: 600,
				degree: 90
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 1000,
				distance: 800,
				degree: 90
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 1000,
				distance: 200,
				degree: 270
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 1000,
				distance: 400,
				degree: 270
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 1000,
				distance: 600,
				degree: 270
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 1000,
				distance: 800,
				degree: 270
			},
			{
				appTime: 13000,
				obj:masterObjs[18],
				rpps: 2000
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 2000
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 2000,
				distance: 200,
				degree: 90
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 2000,
				distance: 400,
				degree: 90
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 2000,
				distance: 600,
				degree: 90
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 2000,
				distance: 800,
				degree: 90
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 2000,
				distance: 200,
				degree: 270
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 2000,
				distance: 400,
				degree: 270
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 2000,
				distance: 600,
				degree: 270
			},
			{	//ここまで左右移動ドラム
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 2000,
				distance: 800,
				degree: 270
			},
			{
				appTime: 13000,
				obj:masterObjs[21],
				rpps: 4000,
				distance: 600,
				degree: 90
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 3000,
				distance: 600,
				degree: 90
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 3200,
				distance: 600,
				degree: 90
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 3400,
				distance: 600,
				degree: 90
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 3600,
				distance: 600,
				degree: 90
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 3800,
				distance: 600,
				degree: 90
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 4000,
				distance: 600,
				degree: 90
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 4200,
				distance: 600,
				degree: 90
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 4400,
				distance: 600,
				degree: 90
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 4600,
				distance: 600,
				degree: 90
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 4800,
				distance: 600,
				degree: 90
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 5000,
				distance: 600,
				degree: 90
			},
			{
				appTime: 13000,
				obj:masterObjs[21],
				rpps: 4000,
				distance: 600,
				degree: 270
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 3000,
				distance: 600,
				degree: 270
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 3200,
				distance: 600,
				degree: 270
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 3400,
				distance: 600,
				degree: 270
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 3600,
				distance: 600,
				degree: 270
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 3800,
				distance: 600,
				degree: 270
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 4000,
				distance: 600,
				degree: 270
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 4200,
				distance: 600,
				degree: 270
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 4400,
				distance: 600,
				degree: 270
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 4600,
				distance: 600,
				degree: 270
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 4800,
				distance: 600,
				degree: 270
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 5000,
				distance: 600,
				degree: 270
			},
			{
				appTime: 13000,
				obj:masterObjs[22],
				rpps: 4000
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 3000
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 3200
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 3400
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 3600
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 3800
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 4000
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 4200
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 4400
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 4600
			},
			{
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 4800
			},
			{	//ここまで前後ドラム
				appTime: 13000,
				obj:masterObjs[1],
				rpps: 5000
			},
			{
				appTime: 16000,
				obj:masterObjs[19],
				rpps: 4000,
				distance: 600,
				degree: 90
			},
			{
				appTime: 16000,
				obj:masterObjs[1],
				rpps: 4000,
				distance: 600,
				degree: 90
			},
			{
				appTime: 16000,
				obj:masterObjs[1],
				rpps: 4000,
				positionY: 200,
				distance: 600,
				degree: 90
			},
			{
				appTime: 16000,
				obj:masterObjs[1],
				rpps: 4000,
				positionY: 400,
				distance: 600,
				degree: 90
			},
			{
				appTime: 16000,
				obj:masterObjs[1],
				rpps: 4000,
				positionY: 600,
				distance: 600,
				degree: 90
			},
			{
				appTime: 16000,
				obj:masterObjs[1],
				rpps: 4000,
				positionY: 800,
				distance: 600,
				degree: 90
			},
			{
				appTime: 16000,
				obj:masterObjs[1],
				rpps: 4000,
				positionY: 1000,
				distance: 600,
				degree: 90
			},
			{
				appTime: 16000,
				obj:masterObjs[19],
				rpps: 4000,
				distance: 600,
				degree: 270
			},
			{
				appTime: 16000,
				obj:masterObjs[1],
				rpps: 4000,
				distance: 600,
				degree: 270
			},
			{
				appTime: 16000,
				obj:masterObjs[1],
				rpps: 4000,
				positionY: 200,
				distance: 600,
				degree: 270
			},
			{
				appTime: 16000,
				obj:masterObjs[1],
				rpps: 4000,
				positionY: 400,
				distance: 600,
				degree: 270
			},
			{
				appTime: 16000,
				obj:masterObjs[1],
				rpps: 4000,
				positionY: 600,
				distance: 600,
				degree: 270
			},
			{
				appTime: 16000,
				obj:masterObjs[1],
				rpps: 4000,
				positionY: 800,
				distance: 600,
				degree: 270
			},
			{
				appTime: 16000,
				obj:masterObjs[1],
				rpps: 4000,
				positionY: 1000,
				distance: 600,
				degree: 270
			},
			{
				appTime: 16000,
				obj:masterObjs[20],
				rpps: 4000
			},
			{
				appTime: 16000,
				obj:masterObjs[1],
				rpps: 4000
			},
			{
				appTime: 16000,
				obj:masterObjs[1],
				rpps: 4000,
				positionY: 200
			},
			{
				appTime: 16000,
				obj:masterObjs[1],
				rpps: 4000,
				positionY: 400
			},
			{
				appTime: 16000,
				obj:masterObjs[1],
				rpps: 4000,
				positionY: 600
			},
			{
				appTime: 16000,
				obj:masterObjs[1],
				rpps: 4000,
				positionY: 800
			},
			{	//ここまで上下ドラム
				appTime: 16000,
				obj:masterObjs[1],
				rpps: 4000,
				positionY: 1000
			},
			{
				appTime: 20000,
				obj:masterObjs[23],
				rpps: 2000,
				distance: 300,
				degree: 270
			},
			{
				appTime: 20000,
				obj:masterObjs[23],
				rpps: 2000,
				distance: 400,
				degree: 270
			},
			{
				appTime: 20000,
				obj:masterObjs[23],
				rpps: 2000,
				distance: 500,
				degree: 270
			},
			{
				appTime: 20000,
				obj:masterObjs[23],
				rpps: 2000,
				distance: 600,
				degree: 270
			},
			{
				appTime: 20000,
				obj:masterObjs[23],
				rpps: 2000,
				positionY: 150,
				distance: 300,
				degree: 270
			},
			{
				appTime: 20000,
				obj:masterObjs[23],
				rpps: 2000,
				positionY: 150,
				distance: 400,
				degree: 270
			},
			{
				appTime: 20000,
				obj:masterObjs[23],
				rpps: 2000,
				positionY: 150,
				distance: 500,
				degree: 270
			},
			{
				appTime: 20000,
				obj:masterObjs[23],
				rpps: 2000,
				positionY: 150,
				distance: 600,
				degree: 270
			},
			{
				appTime: 20000,
				obj:masterObjs[23],
				rpps: 2000,
				positionY: 300,
				distance: 300,
				degree: 270
			},
			{
				appTime: 20000,
				obj:masterObjs[23],
				rpps: 2000,
				positionY: 300,
				distance: 400,
				degree: 270
			},
			{
				appTime: 20000,
				obj:masterObjs[23],
				rpps: 2000,
				positionY: 300,
				distance: 500,
				degree: 270
			},
			{
				appTime: 20000,
				obj:masterObjs[23],
				rpps: 2000,
				positionY: 300,
				distance: 600,
				degree: 270
			},
			{
				appTime: 20000,
				obj:masterObjs[23],
				rpps: 2000,
				distance: 300,
				degree: 270
			},
			{
				appTime: 20000,
				obj:masterObjs[23],
				rpps: 2000,
				distance: 400,
				degree: 270
			},
			{
				appTime: 20000,
				obj:masterObjs[23],
				rpps: 2000,
				distance: 500,
				degree: 270
			},
			{
				appTime: 20000,
				obj:masterObjs[23],
				rpps: 2000,
				distance: 600,
				degree: 270
			},
			{
				appTime: 19000,
				obj:masterObjs[26],
				rpps: 3200
			},
			{
				appTime: 19000,
				obj:masterObjs[26],
				rpps: 3200,
				distance: 100,
				degree: 270
			},
			{
				appTime: 19000,
				obj:masterObjs[26],
				rpps: 3200,
				distance: 100,
				degree: 90
			},
			{
				appTime: 19000,
				obj:masterObjs[26],
				rpps: 3200,
				positionY: 150,
			},
			{
				appTime: 19000,
				obj:masterObjs[26],
				rpps: 3200,
				positionY: 150,
				distance: 100,
				degree: 270
			},
			{
				appTime: 19000,
				obj:masterObjs[26],
				rpps: 3200,
				positionY: 150,
				distance: 100,
				degree: 90
			},
			{
				appTime: 19000,
				obj:masterObjs[26],
				rpps: 3200,
				positionY: 300
			},
			{
				appTime: 19000,
				obj:masterObjs[26],
				rpps: 3200,
				positionY: 300,
				distance: 100,
				degree: 270
			},
			{	//ここまでドラム隠しクリスタル
				appTime: 19000,
				obj:masterObjs[26],
				rpps: 3200,
				positionY: 300,
				distance: 100,
				degree: 90
			},
			{
				appTime: 24000,
				obj:masterObjs[27],
				distance: 800,
				degree: 270
			},
			{
				appTime: 24000,
				obj:masterObjs[27],
				distance: 800,
				degree: 270,
				rpps: 250
			},
			{
				appTime: 24000,
				obj:masterObjs[27],
				distance: 800,
				degree: 270,
				rpps: 500
			},
			{
				appTime: 24000,
				obj:masterObjs[27],
				distance: 800,
				degree: 270,
				rpps: 750
			},
			{
				appTime: 24000,
				obj:masterObjs[27],
				distance: 800,
				degree: 270,
				rpps: 1000
			},
			{
				appTime: 24000,
				obj:masterObjs[27],
				distance: 800,
				degree: 270,
				positionY: 200
			},
			{
				appTime: 24000,
				obj:masterObjs[27],
				distance: 800,
				degree: 270,
				rpps: 250,
				positionY: 200
			},
			{
				appTime: 24000,
				obj:masterObjs[27],
				distance: 800,
				degree: 270,
				rpps: 500,
				positionY: 200
			},
			{
				appTime: 24000,
				obj:masterObjs[27],
				distance: 800,
				degree: 270,
				rpps: 750,
				positionY: 200
			},
			{
				appTime: 24000,
				obj:masterObjs[27],
				distance: 800,
				degree: 270,
				rpps: 1000,
				positionY: 200
			},
			{
				appTime: 24000,
				obj:masterObjs[27],
				distance: 800,
				degree: 270,
				positionY: 400
			},
			{
				appTime: 24000,
				obj:masterObjs[27],
				distance: 800,
				degree: 270,
				rpps: 250,
				positionY: 400
			},
			{
				appTime: 24000,
				obj:masterObjs[27],
				distance: 800,
				degree: 270,
				rpps: 500,
				positionY: 400
			},
			{
				appTime: 24000,
				obj:masterObjs[27],
				distance: 800,
				degree: 270,
				rpps: 750,
				positionY: 400
			},
			{
				appTime: 24000,
				obj:masterObjs[27],
				distance: 800,
				degree: 270,
				rpps: 1000,
				positionY: 400
			},
			{
				appTime: 25000,
				obj:masterObjs[28],
				distance: 1600,
				degree: 270
			},
			{
				appTime: 25000,
				obj:masterObjs[28],
				distance: 1600,
				degree: 270,
				rpps: 250
			},
			{
				appTime: 25000,
				obj:masterObjs[28],
				distance: 1600,
				degree: 270,
				rpps: 500
			},
			{
				appTime: 25000,
				obj:masterObjs[28],
				distance: 1600,
				degree: 270,
				rpps: 750
			},
			{
				appTime: 25000,
				obj:masterObjs[28],
				distance: 1600,
				degree: 270,
				rpps: 1000
			},
			{
				appTime: 25000,
				obj:masterObjs[28],
				distance: 1600,
				degree: 270,
				positionY: 200
			},
			{
				appTime: 25000,
				obj:masterObjs[28],
				distance: 1600,
				degree: 270,
				rpps: 250,
				positionY: 200
			},
			{
				appTime: 25000,
				obj:masterObjs[28],
				distance: 1600,
				degree: 270,
				rpps: 500,
				positionY: 200
			},
			{
				appTime: 25000,
				obj:masterObjs[28],
				distance: 1600,
				degree: 270,
				rpps: 750,
				positionY: 200
			},
			{
				appTime: 25000,
				obj:masterObjs[28],
				distance: 1600,
				degree: 270,
				rpps: 1000,
				positionY: 200
			},
			{
				appTime: 25000,
				obj:masterObjs[28],
				distance: 1600,
				degree: 270,
				positionY: 400
			},
			{
				appTime: 25000,
				obj:masterObjs[28],
				distance: 1600,
				degree: 270,
				rpps: 250,
				positionY: 400
			},
			{
				appTime: 25000,
				obj:masterObjs[28],
				distance: 1600,
				degree: 270,
				rpps: 500,
				positionY: 400
			},
			{
				appTime: 25000,
				obj:masterObjs[28],
				distance: 1600,
				degree: 270,
				rpps: 750,
				positionY: 400
			},
			{
				appTime: 25000,
				obj:masterObjs[28],
				distance: 1600,
				degree: 270,
				rpps: 1000,
				positionY: 400
			},
			{
				appTime: 26000,
				obj:masterObjs[29],
				distance: 2400,
				degree: 270
			},
			{
				appTime: 26000,
				obj:masterObjs[29],
				distance: 2400,
				degree: 270,
				rpps: 250
			},
			{
				appTime: 26000,
				obj:masterObjs[29],
				distance: 2400,
				degree: 270,
				rpps: 500
			},
			{
				appTime: 26000,
				obj:masterObjs[29],
				distance: 2400,
				degree: 270,
				rpps: 750
			},
			{
				appTime: 26000,
				obj:masterObjs[29],
				distance: 2400,
				degree: 270,
				rpps: 1000
			},
			{
				appTime: 26000,
				obj:masterObjs[29],
				distance: 2400,
				degree: 270,
				positionY: 200
			},
			{
				appTime: 26000,
				obj:masterObjs[29],
				distance: 2400,
				degree: 270,
				rpps: 250,
				positionY: 200
			},
			{
				appTime: 26000,
				obj:masterObjs[29],
				distance: 2400,
				degree: 270,
				rpps: 500,
				positionY: 200
			},
			{
				appTime: 26000,
				obj:masterObjs[29],
				distance: 2400,
				degree: 270,
				rpps: 750,
				positionY: 200
			},
			{
				appTime: 26000,
				obj:masterObjs[29],
				distance: 2400,
				degree: 270,
				rpps: 1000,
				positionY: 200
			},
			{
				appTime: 26000,
				obj:masterObjs[29],
				distance: 2400,
				degree: 270,
				positionY: 400
			},
			{
				appTime: 26000,
				obj:masterObjs[29],
				distance: 2400,
				degree: 270,
				rpps: 250,
				positionY: 400
			},
			{
				appTime: 26000,
				obj:masterObjs[29],
				distance: 2400,
				degree: 270,
				rpps: 500,
				positionY: 400
			},
			{
				appTime: 26000,
				obj:masterObjs[29],
				distance: 2400,
				degree: 270,
				rpps: 750,
				positionY: 400
			},
			{	//ここまで飛び跳ねるクリスタル
				appTime: 26000,
				obj:masterObjs[29],
				distance: 2400,
				degree: 270,
				rpps: 1000,
				positionY: 400
			},
			{
				appTime: 28000,
				obj:masterObjs[39],
				rpps: 1000,
				positionY: -150
			},
			{
				appTime: 28000,
				obj:masterObjs[39],
				rpps: 1000,
				positionY: -150,
				distance: 400,
				degree: 270
			},
			{
				appTime: 28000,
				obj:masterObjs[39],
				rpps: 1000,
				positionY: -150,
				distance: 400,
				degree: 90
			},
			{
				appTime: 28000,
				obj:masterObjs[39],
				rpps: 1250,
				distance: 200,
				degree: 270,
				positionY: -150
			},
			{
				appTime: 28000,
				obj:masterObjs[39],
				rpps: 1250,
				distance: 200,
				degree: 90,
				positionY: -150
			},
			{
				appTime: 28500,
				obj:masterObjs[39],
				rpps: 1000,
				positionY: -150
			},
			{
				appTime: 28500,
				obj:masterObjs[39],
				rpps: 1000,
				positionY: -150,
				distance: 400,
				degree: 270
			},
			{
				appTime: 28500,
				obj:masterObjs[39],
				rpps: 1000,
				positionY: -150,
				distance: 400,
				degree: 90
			},
			{
				appTime: 28500,
				obj:masterObjs[39],
				rpps: 1250,
				distance: 200,
				degree: 270,
				positionY: -150
			},
			{
				appTime: 28500,
				obj:masterObjs[39],
				rpps: 1250,
				distance: 200,
				degree: 90,
				positionY: -150
			},
			{
				appTime: 29000,
				obj:masterObjs[39],
				rpps: 1000,
				positionY: -150
			},
			{
				appTime: 29000,
				obj:masterObjs[39],
				rpps: 1000,
				positionY: -150,
				distance: 400,
				degree: 270
			},
			{
				appTime: 29000,
				obj:masterObjs[39],
				rpps: 1000,
				positionY: -150,
				distance: 400,
				degree: 90
			},
			{
				appTime: 29000,
				obj:masterObjs[39],
				rpps: 1250,
				distance: 200,
				degree: 270,
				positionY: -150
			},
			{
				appTime: 29000,
				obj:masterObjs[39],
				rpps: 1250,
				distance: 200,
				degree: 90,
				positionY: -150
			},
			{
				appTime: 29500,
				obj:masterObjs[39],
				rpps: 1000,
				positionY: -150
			},
			{
				appTime: 29500,
				obj:masterObjs[39],
				rpps: 1000,
				positionY: -150,
				distance: 400,
				degree: 270
			},
			{
				appTime: 29500,
				obj:masterObjs[39],
				rpps: 1000,
				positionY: -150,
				distance: 400,
				degree: 90
			},
			{
				appTime: 29500,
				obj:masterObjs[39],
				rpps: 1250,
				distance: 200,
				degree: 270,
				positionY: -150
			},
			{
				appTime: 29500,
				obj:masterObjs[39],
				rpps: 1250,
				distance: 200,
				degree: 90,
				positionY: -150
			},
			{
				appTime: 30000,
				obj:masterObjs[39],
				rpps: 1000,
				positionY: -150
			},
			{
				appTime: 30000,
				obj:masterObjs[39],
				rpps: 1000,
				positionY: -150,
				distance: 400,
				degree: 270
			},
			{
				appTime: 30000,
				obj:masterObjs[39],
				rpps: 1000,
				positionY: -150,
				distance: 400,
				degree: 90
			},
			{
				appTime: 30000,
				obj:masterObjs[39],
				rpps: 1250,
				distance: 200,
				degree: 270,
				positionY: -150
			},
			{	//ここまで生えるキューブ
				appTime: 30000,
				obj:masterObjs[39],
				rpps: 1250,
				distance: 200,
				degree: 90,
				positionY: -150
			},
			{
				appTime: 33000,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 100,
				degree: 90
			},
			{
				appTime: 33000,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 100,
				degree: 270
			},
			{
				appTime: 33000,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 200,
				degree: 90
			},
			{
				appTime: 33000,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 200,
				degree: 270
			},
			{
				appTime: 33000,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 300,
				degree: 90
			},
			{
				appTime: 33000,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 300,
				degree: 270
			},
			{
				appTime: 33500,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 100,
				degree: 90
			},
			{
				appTime: 33500,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 100,
				degree: 270
			},
			{
				appTime: 33500,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 200,
				degree: 90
			},
			{
				appTime: 33500,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 200,
				degree: 270
			},
			{
				appTime: 33500,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 300,
				degree: 90
			},
			{
				appTime: 33500,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 300,
				degree: 270
			},
			{
				appTime: 34000,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 100,
				degree: 90
			},
			{
				appTime: 34000,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 100,
				degree: 270
			},
			{
				appTime: 34000,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 200,
				degree: 90
			},
			{
				appTime: 34000,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 200,
				degree: 270
			},
			{
				appTime: 34000,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 300,
				degree: 90,
			},
			{
				appTime: 34000,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 300,
				degree: 270
			},
			{
				appTime: 34500,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 100,
				degree: 90
			},
			{
				appTime: 34500,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 100,
				degree: 270
			},
			{
				appTime: 34500,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 200,
				degree: 90
			},
			{
				appTime: 34500,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 200,
				degree: 270
			},
			{
				appTime: 34500,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 300,
				degree: 90
			},
			{
				appTime: 34500,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 300,
				degree: 270
			},
			{
				appTime: 35000,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 100,
				degree: 90
			},
			{
				appTime: 35000,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 100,
				degree: 270
			},
			{
				appTime: 35000,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 200,
				degree: 90
			},
			{
				appTime: 35000,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 200,
				degree: 270
			},
			{
				appTime: 35000,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 300,
				degree: 90
			},
			{
				appTime: 35000,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 300,
				degree: 270
			},
			{
				appTime: 35500,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 100,
				degree: 90
			},
			{
				appTime: 35500,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 100,
				degree: 270
			},
			{
				appTime: 35500,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 200,
				degree: 90
			},
			{
				appTime: 35500,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 200,
				degree: 270
			},
			{
				appTime: 35500,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 300,
				degree: 90
			},
			{
				appTime: 35500,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 300,
				degree: 270
			},
			{
				appTime: 36000,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 100,
				degree: 90
			},
			{
				appTime: 36000,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 100,
				degree: 270
			},
			{
				appTime: 36000,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 200,
				degree: 90
			},
			{
				appTime: 36000,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 200,
				degree: 270
			},
			{
				appTime: 36000,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 300,
				degree: 90
			},
			{
				appTime: 36000,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 300,
				degree: 270
			},
			{
				appTime: 36500,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 100,
				degree: 90
			},
			{
				appTime: 36500,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 100,
				degree: 270
			},
			{
				appTime: 36500,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 200,
				degree: 90
			},
			{
				appTime: 36500,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 200,
				degree: 270
			},
			{
				appTime: 36500,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 300,
				degree: 90
			},
			{
				appTime: 36500,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 300,
				degree: 270
			},
			{
				appTime: 33000,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 100,
				degree: 90,
				positionY: 200
			},
			{
				appTime: 33000,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 100,
				degree: 270,
				positionY: 200
			},
			{
				appTime: 33000,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 200,
				degree: 90,
				positionY: 200
			},
			{
				appTime: 33000,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 200,
				degree: 270,
				positionY: 200
			},
			{
				appTime: 33000,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 300,
				degree: 90,
				positionY: 200
			},
			{
				appTime: 33000,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 300,
				degree: 270,
				positionY: 200
			},
			{
				appTime: 33500,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 100,
				degree: 90,
				positionY: 200
			},
			{
				appTime: 33500,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 100,
				degree: 270,
				positionY: 200
			},
			{
				appTime: 33500,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 200,
				degree: 90,
				positionY: 200
			},
			{
				appTime: 33500,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 200,
				degree: 270,
				positionY: 200
			},
			{
				appTime: 33500,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 300,
				degree: 90,
				positionY: 200
			},
			{
				appTime: 33500,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 300,
				degree: 270,
				positionY: 200
			},
			{
				appTime: 34000,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 100,
				degree: 90,
				positionY: 200
			},
			{
				appTime: 34000,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 100,
				degree: 270,
				positionY: 200
			},
			{
				appTime: 34000,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 200,
				degree: 90,
				positionY: 200
			},
			{
				appTime: 34000,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 200,
				degree: 270,
				positionY: 200
			},
			{
				appTime: 34000,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 300,
				degree: 90,
				positionY: 200
			},
			{
				appTime: 34000,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 300,
				degree: 270,
				positionY: 200
			},
			{
				appTime: 34500,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 100,
				degree: 90,
				positionY: 200
			},
			{
				appTime: 34500,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 100,
				degree: 270,
				positionY: 200
			},
			{
				appTime: 34500,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 200,
				degree: 90,
				positionY: 200
			},
			{
				appTime: 34500,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 200,
				degree: 270,
				positionY: 200
			},
			{
				appTime: 34500,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 300,
				degree: 90,
				positionY: 200
			},
			{
				appTime: 34500,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 300,
				degree: 270,
				positionY: 200
			},
			{
				appTime: 35000,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 100,
				degree: 90,
				positionY: 200
			},
			{
				appTime: 35000,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 100,
				degree: 270,
				positionY: 200
			},
			{
				appTime: 35000,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 200,
				degree: 90,
				positionY: 200
			},
			{
				appTime: 35000,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 200,
				degree: 270,
				positionY: 200
			},
			{
				appTime: 35000,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 300,
				degree: 90,
				positionY: 200
			},
			{
				appTime: 35000,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 300,
				degree: 270,
				positionY: 200
			},
			{
				appTime: 35500,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 100,
				degree: 90,
				positionY: 200
			},
			{
				appTime: 35500,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 100,
				degree: 270,
				positionY: 200
			},
			{
				appTime: 35500,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 200,
				degree: 90,
				positionY: 200
			},
			{
				appTime: 35500,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 200,
				degree: 270,
				positionY: 200
			},
			{
				appTime: 35500,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 300,
				degree: 90,
				positionY: 200
			},
			{
				appTime: 35500,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 300,
				degree: 270,
				positionY: 200
			},
			{
				appTime: 36000,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 100,
				degree: 90,
				positionY: 200
			},
			{
				appTime: 36000,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 100,
				degree: 270,
				positionY: 200
			},
			{
				appTime: 36000,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 200,
				degree: 90,
				positionY: 200
			},
			{
				appTime: 36000,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 200,
				degree: 270,
				positionY: 200
			},
			{
				appTime: 36000,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 300,
				degree: 90,
				positionY: 200
			},
			{
				appTime: 36000,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 300,
				degree: 270,
				positionY: 200
			},
			{
				appTime: 36500,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 100,
				degree: 90,
				positionY: 200
			},
			{
				appTime: 36500,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 100,
				degree: 270,
				positionY: 200
			},
			{
				appTime: 36500,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 200,
				degree: 90,
				positionY: 200
			},
			{
				appTime: 36500,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 200,
				degree: 270,
				positionY: 200
			},
			{
				appTime: 36500,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 300,
				degree: 90,
				positionY: 200
			},
			{
				appTime: 36500,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 300,
				degree: 270,
				positionY: 200
			},
			{
				appTime: 33000,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 100,
				degree: 90,
				positionY: 400
			},
			{
				appTime: 33000,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 100,
				degree: 270,
				positionY: 400
			},
			{
				appTime: 33000,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 200,
				degree: 90,
				positionY: 400
			},
			{
				appTime: 33000,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 200,
				degree: 270,
				positionY: 400
			},
			{
				appTime: 33000,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 300,
				degree: 90,
				positionY: 400
			},
			{
				appTime: 33000,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 300,
				degree: 270,
				positionY: 400
			},
			{
				appTime: 33500,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 100,
				degree: 90,
				positionY: 400
			},
			{
				appTime: 33500,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 100,
				degree: 270,
				positionY: 400
			},
			{
				appTime: 33500,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 200,
				degree: 90,
				positionY: 400
			},
			{
				appTime: 33500,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 200,
				degree: 270,
				positionY: 400
			},
			{
				appTime: 33500,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 300,
				degree: 90,
				positionY: 400
			},
			{
				appTime: 33500,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 300,
				degree: 270,
				positionY: 400
			},
			{
				appTime: 34000,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 100,
				degree: 90,
				positionY: 400
			},
			{
				appTime: 34000,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 100,
				degree: 270,
				positionY: 400
			},
			{
				appTime: 34000,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 200,
				degree: 90,
				positionY: 400
			},
			{
				appTime: 34000,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 200,
				degree: 270,
				positionY: 400
			},
			{
				appTime: 34000,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 300,
				degree: 90,
				positionY: 400
			},
			{
				appTime: 34000,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 300,
				degree: 270,
				positionY: 400
			},
			{
				appTime: 34500,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 100,
				degree: 90,
				positionY: 400
			},
			{
				appTime: 34500,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 100,
				degree: 270,
				positionY: 400
			},
			{
				appTime: 34500,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 200,
				degree: 90,
				positionY: 400
			},
			{
				appTime: 34500,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 200,
				degree: 270,
				positionY: 400
			},
			{
				appTime: 34500,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 300,
				degree: 90,
				positionY: 400
			},
			{
				appTime: 34500,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 300,
				degree: 270,
				positionY: 400
			},
			{
				appTime: 35000,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 100,
				degree: 90,
				positionY: 400
			},
			{
				appTime: 35000,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 100,
				degree: 270,
				positionY: 400
			},
			{
				appTime: 35000,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 200,
				degree: 90,
				positionY: 400
			},
			{
				appTime: 35000,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 200,
				degree: 270,
				positionY: 400
			},
			{
				appTime: 35000,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 300,
				degree: 90,
				positionY: 400
			},
			{
				appTime: 35000,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 300,
				degree: 270,
				positionY: 400
			},
			{
				appTime: 35500,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 100,
				degree: 90,
				positionY: 400
			},
			{
				appTime: 35500,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 100,
				degree: 270,
				positionY: 400
			},
			{
				appTime: 35500,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 200,
				degree: 90,
				positionY: 400
			},
			{
				appTime: 35500,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 200,
				degree: 270,
				positionY: 400
			},
			{
				appTime: 35500,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 300,
				degree: 90,
				positionY: 400
			},
			{
				appTime: 35500,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 300,
				degree: 270,
				positionY: 400
			},
			{
				appTime: 36000,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 100,
				degree: 90,
				positionY: 400
			},
			{
				appTime: 36000,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 100,
				degree: 270,
				positionY: 400
			},
			{
				appTime: 36000,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 200,
				degree: 90,
				positionY: 400
			},
			{
				appTime: 36000,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 200,
				degree: 270,
				positionY: 400
			},
			{
				appTime: 36000,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 300,
				degree: 90,
				positionY: 400
			},
			{
				appTime: 36000,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 300,
				degree: 270,
				positionY: 400
			},
			{
				appTime: 36500,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 100,
				degree: 90,
				positionY: 400
			},
			{
				appTime: 36500,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 100,
				degree: 270,
				positionY: 400
			},
			{
				appTime: 36500,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 200,
				degree: 90,
				positionY: 400
			},
			{
				appTime: 36500,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 200,
				degree: 270,
				positionY: 400
			},
			{
				appTime: 36500,
				obj:masterObjs[30],
				rpps: 1500,
				distance: 300,
				degree: 90,
				positionY: 400
			},
			{	//ここまでモーゼのクリスタル
				appTime: 36500,
				obj:masterObjs[31],
				rpps: 1500,
				distance: 300,
				degree: 270,
				positionY: 400
			},
			{
				appTime: 41500,
				obj:masterObjs[32],
				rpps: 500,
				positionY: 800
			},
			{
				appTime: 41500,
				obj:masterObjs[33],
				rpps: 1000,
				positionY: 800
			},
			{
				appTime: 41500,
				obj:masterObjs[34],
				rpps: 1500,
				positionY: 800
			},
			{
				appTime: 41500,
				obj:masterObjs[35],
				rpps: 2000,
				positionY: 800
			},
			{
				appTime: 42000,
				obj:masterObjs[32],
				rpps: 500,
				positionY: 800
			},
			{
				appTime: 42000,
				obj:masterObjs[33],
				rpps: 1000,
				positionY: 800
			},
			{
				appTime: 42000,
				obj:masterObjs[34],
				rpps: 1500,
				positionY: 800
			},
			{
				appTime: 42000,
				obj:masterObjs[35],
				rpps: 2000,
				positionY: 800
			},
			{
				appTime: 42500,
				obj:masterObjs[35],
				rpps: 500,
				positionY: 800
			},
			{
				appTime: 42500,
				obj:masterObjs[34],
				rpps: 1000,
				positionY: 800
			},
			{
				appTime: 42500,
				obj:masterObjs[33],
				rpps: 1500,
				positionY: 800
			},
			{
				appTime: 42500,
				obj:masterObjs[32],
				rpps: 2000,
				positionY: 800
			},
			{
				appTime: 43000,
				obj:masterObjs[35],
				rpps: 500,
				positionY: 800
			},
			{
				appTime: 43000,
				obj:masterObjs[34],
				rpps: 1000,
				positionY: 800
			},
			{
				appTime: 43000,
				obj:masterObjs[32],
				rpps: 1500,
				positionY: 800
			},
			{
				appTime: 43000,
				obj:masterObjs[33],
				rpps: 2000,
				positionY: 800
			},
			{
				appTime: 43500,
				obj:masterObjs[34],
				rpps: 500,
				positionY: 800
			},
			{
				appTime: 43500,
				obj:masterObjs[35],
				rpps: 1000,
				positionY: 800
			},
			{
				appTime: 43500,
				obj:masterObjs[33],
				rpps: 1500,
				positionY: 800
			},
			{
				appTime: 43500,
				obj:masterObjs[32],
				rpps: 2000,
				positionY: 800
			},
			{
				appTime: 44000,
				obj:masterObjs[33],
				rpps: 500,
				positionY: 800
			},
			{
				appTime: 44000,
				obj:masterObjs[35],
				rpps: 1000,
				positionY: 800
			},
			{
				appTime: 44000,
				obj:masterObjs[32],
				rpps: 1500,
				positionY: 800
			},
			{	//ここまでコインの雨
				appTime: 44000,
				obj:masterObjs[34],
				rpps: 2000,
				positionY: 800
			},
			{
				appTime: 47000,
				obj:masterObjs[2],
				positionY: 200
			},
			{
				appTime: 47000,
				obj:masterObjs[2],
				positionY: 400
			},
			{
				appTime: 47000,
				obj:masterObjs[2],
				positionY: 600
			},
			{
				appTime: 47000,
				obj:masterObjs[2],
				positionY: 200,
				distance: 200,
				degree: 90
			},
			{
				appTime: 47000,
				obj:masterObjs[2],
				positionY: 400,
				distance: 200,
				degree: 90
			},
			{
				appTime: 47000,
				obj:masterObjs[2],
				positionY: 600,
				distance: 200,
				degree: 90
			},
			{
				appTime: 47000,
				obj:masterObjs[2],
				positionY: 200,
				distance: 200,
				degree: 270
			},
			{
				appTime: 47000,
				obj:masterObjs[2],
				positionY: 400,
				distance: 200,
				degree: 270
			},
			{	//ここまで浮遊キューブ
				appTime: 47000,
				obj:masterObjs[2],
				positionY: 600,
				distance: 200,
				degree: 270
			},
			{
				appTime: 50000,
				obj:masterObjs[1],
				distance: 100,
				degree: 270,
				rpps: 1300
			},
			{
				appTime: 50000,
				obj:masterObjs[1],
				positionY: 150,
				distance: 100,
				degree: 270,
				rpps: 1300
			},
			{
				appTime: 50000,
				obj:masterObjs[1],
				positionY: 300,
				distance: 100,
				degree: 270,
				rpps: 1300
			},
			{
				appTime: 50000,
				obj:masterObjs[1],
				distance: 200,
				degree: 270,
				rpps: 1300
			},
			{
				appTime: 50000,
				obj:masterObjs[1],
				positionY: 150,
				distance: 200,
				degree: 270,
				rpps: 1300
			},
			{
				appTime: 50000,
				obj:masterObjs[1],
				positionY: 300,
				distance: 200,
				degree: 270,
				rpps: 1300
			},
			{
				appTime: 50000,
				obj:masterObjs[1],
				distance: 100,
				degree: 90,
				rpps: 1300
			},
			{
				appTime: 50000,
				obj:masterObjs[1],
				positionY: 150,
				distance: 100,
				degree: 90,
				rpps: 1300
			},
			{
				appTime: 50000,
				obj:masterObjs[1],
				positionY: 300,
				distance: 100,
				degree: 90,
				rpps: 1300
			},
			{
				appTime: 50000,
				obj:masterObjs[1],
				distance: 200,
				degree: 90,
				rpps: 1300
			},
			{
				appTime: 50000,
				obj:masterObjs[1],
				positionY: 150,
				distance: 200,
				degree: 90,
				rpps: 1300
			},
			{
				appTime: 50000,
				obj:masterObjs[1],
				positionY: 300,
				distance: 200,
				degree: 90,
				rpps: 1300
			},
			{
				appTime: 50000,
				obj:masterObjs[1],
				rpps: 1300
			},
			{
				appTime: 50000,
				obj:masterObjs[1],
				positionY: 150,
				rpps: 1300
			},
			{
				appTime: 50000,
				obj:masterObjs[1],
				positionY: 300,
				rpps: 1300
			},
			{	//ここまで直前に出現する黒クリスタル
				appTime: 50000,
				obj:masterObjs[1],
				positionY: 300,
				distance: 100,
				degree: 90,
				rpps: 1300
			},
			{
				appTime: 50500,
				obj:masterObjs[5],
				rpps: 1000
			},
			{
				appTime: 50500,
				obj:masterObjs[5],
				distance: 100,
				degree: 90,
				rpps: 1000
			},
			{
				appTime: 50500,
				obj:masterObjs[5],
				distance: 100,
				degree: 270,
				rpps: 1000
			},
			{
				appTime: 50500,
				obj:masterObjs[5],
				distance: 200,
				degree: 90,
				rpps: 1000
			},
			{
				appTime: 50500,
				obj:masterObjs[5],
				distance: 200,
				degree: 270,
				rpps: 1000
			},
			{
				appTime: 51000,
				obj:masterObjs[5],
				positionY: 150,
				rpps: 500
			},
			{
				appTime: 51000,
				obj:masterObjs[5],
				distance: 100,
				degree: 90,
				positionY: 150,
				rpps: 500
			},
			{
				appTime: 51000,
				obj:masterObjs[5],
				distance: 100,
				degree: 270,
				positionY: 150,
				rpps: 500
			},
			{
				appTime: 51000,
				obj:masterObjs[5],
				distance: 200,
				degree: 90,
				positionY: 150,
				rpps: 500
			},
			{
				appTime: 51000,
				obj:masterObjs[5],
				distance: 200,
				degree: 270,
				positionY: 150,
				rpps: 500
			},
			{
				appTime: 51500,
				obj:masterObjs[5],
				positionY: 300
			},
			{
				appTime: 51500,
				obj:masterObjs[5],
				distance: 100,
				degree: 90,
				positionY: 300
			},
			{
				appTime: 51500,
				obj:masterObjs[5],
				distance: 100,
				degree: 270,
				positionY: 300
			},
			{
				appTime: 51500,
				obj:masterObjs[5],
				distance: 200,
				degree: 90,
				positionY: 300
			},
			{	//ここまで妨害するドラム缶
				appTime: 51500,
				obj:masterObjs[5],
				distance: 200,
				degree: 270,
				positionY: 300
			},
			{
				appTime: 53000,
				obj:masterObjs[7],
				distance: 500,
				degree: 270
			},
			{
				appTime: 53000,
				obj:masterObjs[7],
				distance: 500,
				degree: 90
			},
			{
				appTime: 53000,
				obj:masterObjs[7],
				distance: 1000,
				degree: 270
			},
			{
				appTime: 53000,
				obj:masterObjs[7],
				distance: 1000,
				degree: 90
			},
			{
				appTime: 53000,
				obj:masterObjs[7]
			},
			{
				appTime: 53500,
				obj:masterObjs[8],
				distance: 250,
				degree: 270
			},
			{
				appTime: 53500,
				obj:masterObjs[8],
				distance: 250,
				degree: 90
			},
			{
				appTime: 53500,
				obj:masterObjs[8],
				distance: 750,
				degree: 270
			},
			{
				appTime: 53500,
				obj:masterObjs[8],
				distance: 750,
				degree: 90
			},
			{
				appTime: 53500,
				obj:masterObjs[8],
				distance: 1250,
				degree: 270
			},
			{
				appTime: 53500,
				obj:masterObjs[8],
				distance: 1250,
				degree: 90
			},
			{
				appTime: 54000,
				obj:masterObjs[9],
				distance: 500,
				degree: 270
			},
			{
				appTime: 54000,
				obj:masterObjs[9],
				distance: 500,
				degree: 90
			},
			{
				appTime: 54000,
				obj:masterObjs[9],
				distance: 1000,
				degree: 270
			},
			{
				appTime: 54000,
				obj:masterObjs[9],
				distance: 1000,
				degree: 90
			},
			{
				appTime: 54000,
				obj:masterObjs[9],
				distance: 1500,
				degree: 270
			},
			{
				appTime: 54000,
				obj:masterObjs[9],
				distance: 1500,
				degree: 90
			},
			{	//ここまで跳ねるキューブPart2
				appTime: 54000,
				obj:masterObjs[9]
			},
			{
				appTime: 56000,
				obj:masterObjs[38]
			},
			{
				appTime: 56000,
				obj:masterObjs[38],
				distance: 100,
				degree: 270
			},
			{
				appTime: 56000,
				obj:masterObjs[38],
				distance: 100,
				degree: 90
			},
			{
				appTime: 56000,
				obj:masterObjs[38],
				distance: 200,
				degree: 270
			},
			{
				appTime: 56000,
				obj:masterObjs[38],
				distance: 200,
				degree: 90
			},
			{
				appTime: 56000,
				obj:masterObjs[38],
				positionY: 150
			},
			{
				appTime: 56000,
				obj:masterObjs[38],
				distance: 100,
				degree: 270,
				positionY: 150
			},
			{
				appTime: 56000,
				obj:masterObjs[38],
				distance: 100,
				degree: 90,
				positionY: 150
			},
			{
				appTime: 56000,
				obj:masterObjs[38],
				distance: 200,
				degree: 270,
				positionY: 150
			},
			{
				appTime: 56000,
				obj:masterObjs[38],
				distance: 200,
				degree: 90,
				positionY: 150
			},
			{
				appTime: 56000,
				obj:masterObjs[38],
				positionY: 300
			},
			{
				appTime: 56000,
				obj:masterObjs[38],
				distance: 100,
				degree: 270,
				positionY: 300
			},
			{
				appTime: 56000,
				obj:masterObjs[38],
				distance: 100,
				degree: 90,
				positionY: 300
			},
			{
				appTime: 56000,
				obj:masterObjs[38],
				distance: 200,
				degree: 270,
				positionY: 300
			},
			{
				appTime: 56000,
				obj:masterObjs[38],
				distance: 200,
				degree: 90,
				positionY: 300
			},
			{
				appTime: 56500,
				obj:masterObjs[38]
			},
			{
				appTime: 56500,
				obj:masterObjs[38],
				distance: 100,
				degree: 270
			},
			{
				appTime: 56500,
				obj:masterObjs[38],
				distance: 100,
				degree: 90
			},
			{
				appTime: 56500,
				obj:masterObjs[38],
				distance: 200,
				degree: 270
			},
			{
				appTime: 56500,
				obj:masterObjs[38],
				distance: 200,
				degree: 90
			},
			{
				appTime: 56500,
				obj:masterObjs[38],
				positionY: 150
			},
			{
				appTime: 56500,
				obj:masterObjs[38],
				distance: 100,
				degree: 270,
				positionY: 150
			},
			{
				appTime: 56500,
				obj:masterObjs[38],
				distance: 100,
				degree: 90,
				positionY: 150
			},
			{
				appTime: 56500,
				obj:masterObjs[38],
				distance: 200,
				degree: 270,
				positionY: 150
			},
			{
				appTime: 56500,
				obj:masterObjs[38],
				distance: 200,
				degree: 90,
				positionY: 150
			},
			{
				appTime: 56500,
				obj:masterObjs[38],
				positionY: 300
			},
			{
				appTime: 56500,
				obj:masterObjs[38],
				distance: 100,
				degree: 270,
				positionY: 300
			},
			{
				appTime: 56500,
				obj:masterObjs[38],
				distance: 100,
				degree: 90,
				positionY: 300
			},
			{
				appTime: 56500,
				obj:masterObjs[38],
				distance: 200,
				degree: 270,
				positionY: 300
			},
			{
				appTime: 56500,
				obj:masterObjs[38],
				distance: 200,
				degree: 90,
				positionY: 300
			},
			{
				appTime: 57000,
				obj:masterObjs[38]
			},
			{
				appTime: 57000,
				obj:masterObjs[38],
				distance: 100,
				degree: 270
			},
			{
				appTime: 57000,
				obj:masterObjs[38],
				distance: 100,
				degree: 90
			},
			{
				appTime: 57000,
				obj:masterObjs[38],
				distance: 200,
				degree: 270
			},
			{
				appTime: 57000,
				obj:masterObjs[38],
				distance: 200,
				degree: 90
			},
			{
				appTime: 57000,
				obj:masterObjs[38],
				positionY: 150
			},
			{
				appTime: 57000,
				obj:masterObjs[38],
				distance: 100,
				degree: 270,
				positionY: 150
			},
			{
				appTime: 57000,
				obj:masterObjs[38],
				distance: 100,
				degree: 90,
				positionY: 150
			},
			{
				appTime: 57000,
				obj:masterObjs[38],
				distance: 200,
				degree: 270,
				positionY: 150
			},
			{
				appTime: 57000,
				obj:masterObjs[38],
				distance: 200,
				degree: 90,
				positionY: 150
			},
			{
				appTime: 57000,
				obj:masterObjs[38],
				positionY: 300
			},
			{
				appTime: 57000,
				obj:masterObjs[38],
				distance: 100,
				degree: 270,
				positionY: 300
			},
			{
				appTime: 57000,
				obj:masterObjs[38],
				distance: 100,
				degree: 90,
				positionY: 300
			},
			{
				appTime: 57000,
				obj:masterObjs[38],
				distance: 200,
				degree: 270,
				positionY: 300
			},
			{
				appTime: 57000,
				obj:masterObjs[38],
				distance: 200,
				degree: 90,
				positionY: 300
			},
			{
				appTime: 57500,
				obj:masterObjs[38]
			},
			{
				appTime: 57500,
				obj:masterObjs[38],
				distance: 100,
				degree: 270
			},
			{
				appTime: 57500,
				obj:masterObjs[38],
				distance: 100,
				degree: 90
			},
			{
				appTime: 57500,
				obj:masterObjs[38],
				distance: 200,
				degree: 270
			},
			{
				appTime: 57500,
				obj:masterObjs[38],
				distance: 200,
				degree: 90
			},
			{
				appTime: 57500,
				obj:masterObjs[38],
				positionY: 150
			},
			{
				appTime: 57500,
				obj:masterObjs[38],
				distance: 100,
				degree: 270,
				positionY: 150
			},
			{
				appTime: 57500,
				obj:masterObjs[38],
				distance: 100,
				degree: 90,
				positionY: 150
			},
			{
				appTime: 57500,
				obj:masterObjs[38],
				distance: 200,
				degree: 270,
				positionY: 150
			},
			{
				appTime: 57500,
				obj:masterObjs[38],
				distance: 200,
				degree: 90,
				positionY: 150
			},
			{
				appTime: 57500,
				obj:masterObjs[38],
				positionY: 300
			},
			{
				appTime: 57500,
				obj:masterObjs[38],
				distance: 100,
				degree: 270,
				positionY: 300
			},
			{
				appTime: 57500,
				obj:masterObjs[38],
				distance: 100,
				degree: 90,
				positionY: 300
			},
			{
				appTime: 57500,
				obj:masterObjs[38],
				distance: 200,
				degree: 270,
				positionY: 300
			},
			{
				appTime: 57500,
				obj:masterObjs[38],
				distance: 200,
				degree: 90,
				positionY: 300
			},
			{
				appTime: 58000,
				obj:masterObjs[38]
			},
			{
				appTime: 58000,
				obj:masterObjs[38],
				distance: 100,
				degree: 270
			},
			{
				appTime: 58000,
				obj:masterObjs[38],
				distance: 100,
				degree: 90
			},
			{
				appTime: 58000,
				obj:masterObjs[38],
				distance: 200,
				degree: 270
			},
			{
				appTime: 58000,
				obj:masterObjs[38],
				distance: 200,
				degree: 90
			},
			{
				appTime: 58000,
				obj:masterObjs[38],
				positionY: 150
			},
			{
				appTime: 58000,
				obj:masterObjs[38],
				distance: 100,
				degree: 270,
				positionY: 150
			},
			{
				appTime: 58000,
				obj:masterObjs[38],
				distance: 100,
				degree: 90,
				positionY: 150
			},
			{
				appTime: 58000,
				obj:masterObjs[38],
				distance: 200,
				degree: 270,
				positionY: 150
			},
			{
				appTime: 58000,
				obj:masterObjs[38],
				distance: 200,
				degree: 90,
				positionY: 150
			},
			{
				appTime: 58000,
				obj:masterObjs[38],
				positionY: 300
			},
			{
				appTime: 58000,
				obj:masterObjs[38],
				distance: 100,
				degree: 270,
				positionY: 300
			},
			{
				appTime: 58000,
				obj:masterObjs[38],
				distance: 100,
				degree: 90,
				positionY: 300
			},
			{
				appTime: 58000,
				obj:masterObjs[38],
				distance: 200,
				degree: 270,
				positionY: 300
			},
			{
				appTime: 58000,
				obj:masterObjs[38],
				distance: 200,
				degree: 90,
				positionY: 300
			},
			{
				appTime: 59000,
				obj:masterObjs[5]
			},
			{
				appTime: 59000,
				obj:masterObjs[5],
				distance: 100,
				degree: 270
			},
			{
				appTime: 59000,
				obj:masterObjs[5],
				distance: 100,
				degree: 90
			},
			{
				appTime: 59000,
				obj:masterObjs[5],
				distance: 200,
				degree: 270
			},
			{
				appTime: 59000,
				obj:masterObjs[5],
				distance: 200,
				degree: 90
			},
			{
				appTime: 59000,
				obj:masterObjs[5],
				positionY: 150
			},
			{
				appTime: 59000,
				obj:masterObjs[5],
				distance: 100,
				degree: 270,
				positionY: 150
			},
			{
				appTime: 59000,
				obj:masterObjs[5],
				distance: 100,
				degree: 90,
				positionY: 150
			},
			{
				appTime: 59000,
				obj:masterObjs[5],
				distance: 200,
				degree: 270,
				positionY: 150
			},
			{
				appTime: 59000,
				obj:masterObjs[5],
				distance: 200,
				degree: 90,
				positionY: 150
			},
			{
				appTime: 59000,
				obj:masterObjs[5],
				positionY: 300
			},
			{
				appTime: 59000,
				obj:masterObjs[5],
				distance: 100,
				degree: 270,
				positionY: 300
			},
			{
				appTime: 59000,
				obj:masterObjs[5],
				distance: 100,
				degree: 90,
				positionY: 300
			},
			{
				appTime: 59000,
				obj:masterObjs[5],
				distance: 200,
				degree: 270,
				positionY: 300
			},
			{
				appTime: 59000,
				obj:masterObjs[5],
				distance: 200,
				degree: 90,
				positionY: 300
			}
			
		];
		
		setTimeObject(objs);
		
		stageId = 3;
		return true;
	}
	
	override public function init(args:Array<Dynamic>) {
		super.init(args);
	}
	
	override public function update(time:Float) {
		if (first) time = Math.min(time, 1000);
		super.update(time);
	}
}