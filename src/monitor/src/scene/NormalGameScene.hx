package scene;

import action.MoveAction;
import action.RotateAction;
import action.RotateRandomAction;
import action.JumpAction;
import obj2d.Reticle;
import obj3d.BreakObject;
import obj3d.InvincibleObject;
import obj3d.MeshEx;
import obj3d.TimerObject;
import shape.ChestShape;
import shape.CrystalShape;
import shape.CubeShape;
import shape.CylinderShape;
import shape.DrumShape;
import shape.FloorShape;
import event.InputEvent;
import shape.TextShape;
import three.Mesh;
import three.Raycaster;
import three.Vector3;
import scene.abs.AbstractMoveRouteScene;
import position.RoutePosition.RouteParameter;
import field.FieldObject;
import shape.ArrowShape;

class NormalGameScene extends AbstractMoveRouteScene {	
	public function new(context) {
		super(context, 1000);
		audioName = "normal";
	}
	override public function once() {
		if (!super.once()) return false;
		//setFloorTexture("images/schnee-texture.jpg", 16);
		setGroudTexture(GameMgr.DIRECTORY + "images/schnee-texture.jpg", 128);
		
		var easy_routes:Array<RouteParameter> = [ 
			{ time: 10000, point: new Vector3(0, 0, 4000) },
			{ time: 10000, point: new Vector3(-4000, 0, 4000), degree:90 },
			{ time: 10000, point: new Vector3( -4000, 0, 8000) },
			{ time: 10000, point: new Vector3( -8000, 0, 8000), degree:90 },
			{ time: 10000, point: new Vector3( -8000, 0, 12000) },
			{ time: 10000, point: new Vector3( -12000, 0, 12000), degree:90 },
		];
		setRouteData(easy_routes);
		
		var roteRightAction = new RotateAction({right:true});
		var jumpAction = new JumpAction({waitTime: 0});
		var randamRoteAction = new RotateRandomAction();
		var moveXAction = new MoveAction( { moveType:x } );
		var moveXRightAction = new MoveAction({reverse: true, moveType:x});
		var moveZAction = new MoveAction({moveType:z});
		
		var crystalColorShape = new CrystalShape( { color: 0xff5555 } );
		var crystalBlackShape = new CrystalShape( { color:0x000000 } );
		var cylinderShape = new CylinderShape();
		var cubeShape = new CubeShape();
		var drumShape = new DrumShape();
		var chestShape = new ChestShape();
		var arrowShape = new ArrowShape();
		
		var masterObjs = [
			new BreakObject(crystalColorShape, [moveXAction]),//0
			new BreakObject(crystalBlackShape, [roteRightAction]),//1
			new BreakObject(cubeShape, [randamRoteAction]),//2
			new BreakObject(cylinderShape, [jumpAction, roteRightAction]),//3
			new BreakObject(cylinderShape, [roteRightAction]),//4
			new BreakObject(cylinderShape, [new MoveAction({speed: 20, moveType:x}), roteRightAction]),//オリジナル5
			new InvincibleObject(drumShape),//6
			new BreakObject(cylinderShape, [new MoveAction({moveType:x}), jumpAction, roteRightAction]),
			new BreakObject(cylinderShape, [new JumpAction({ gravity: 0.2, initialVelocity:10, waitTime:0}), roteRightAction]),
			new InvincibleObject(chestShape),//9
			new InvincibleObject(new ChestShape( { Xsize:200, Ysize:200, Zsize:50 } )),//10
			new BreakObject(cylinderShape, [new JumpAction( { gravity: 0.1, initialVelocity:10, waitTime:0, up:true } ), roteRightAction])
		];
		
		var easy_objs:Array<FieldObjectParameter> = [
			//ルート1のオブジェクト
			{ obj:masterObjs[4], appTime: 0, distance: 1000, degree:355, positionY:200 },
			{ obj:masterObjs[4], appTime: 0, distance: 1000, degree:5, positionY:200 },
			{ obj:masterObjs[4], appTime: 500, distance: 1100, degree:345, positionY:400},
			{ obj:masterObjs[4], appTime: 500, distance: 1100, degree:15, positionY:400 },
			{ obj:masterObjs[4], appTime: 1000, distance: 1200, degree:330, positionY:600 },
			{ obj:masterObjs[4], appTime: 1000, distance: 1200, degree:30, positionY:600  },
			{ obj:masterObjs[4], appTime: 1500, distance: 1300, degree:330, positionY:100 },
			{ obj:masterObjs[4], appTime: 1500, distance: 1300, degree:30, positionY:100 },
			{ obj:masterObjs[4], appTime: 2000, distance: 1400, degree:345, positionY:300  },
			{ obj:masterObjs[4], appTime: 2000, distance: 1400, degree:15, positionY:300  },
			{ obj:masterObjs[4], appTime: 2500, distance: 1500, degree:345, positionY:200 },
			{ obj:masterObjs[4], appTime: 2500, distance: 1500, degree:15, positionY:200 },
			{ obj:masterObjs[4], appTime: 3000, distance: 1600, degree:330, positionY:200  },
			{ obj:masterObjs[4], appTime: 3000, distance: 1600, degree:30, positionY:200  },
			{ obj:masterObjs[4], appTime: 3000, distance: 1600, degree:355, positionY:300 },
			{ obj:masterObjs[4], appTime: 3000, distance: 1600, degree:5, positionY:300 },
			{ obj:masterObjs[4], appTime: 3500, distance: 1700, degree:330, positionY:400 },
			{ obj:masterObjs[4], appTime: 3500, distance: 1700, degree:30, positionY:400 },
			{ obj:masterObjs[4], appTime: 4000, distance: 1800, degree:345, positionY:100  },
			{ obj:masterObjs[4], appTime: 4000, distance: 1800, degree:15, positionY:100  },
			{ obj:masterObjs[4], appTime: 4500, distance: 1900, degree:345, positionY:600 },
			{ obj:masterObjs[4], appTime: 4500, distance: 1900, degree:15, positionY:600 },
			{ obj:masterObjs[4], appTime: 5000, distance: 2000, degree:330, positionY:400  },
			{ obj:masterObjs[4], appTime: 5000, distance: 2000, degree:30, positionY:400  },
			{ obj:masterObjs[4], appTime: 5500, distance: 2100, degree:355, positionY:500 },
			{ obj:masterObjs[4], appTime: 5500, distance: 2100, degree:5, positionY:500 },
			//ルート2のオブジェクト
			{ obj:masterObjs[4], appTime: 10000, rpps: 3000, distance: -200, degree:0 , positionY:500 },
			{ obj:masterObjs[4], appTime: 10000, rpps: 3500, distance: -200, degree:0 , positionY:500 },
			{ obj:masterObjs[4], appTime: 10000, rpps: 4000, distance: -200, degree:0 , positionY:500 },
			{ obj:masterObjs[4], appTime: 10000, rpps: 4500, distance: -200, degree:0 , positionY:500 },
			{ obj:masterObjs[4], appTime: 10000, rpps: 5000, distance: -200, degree:0 , positionY:500 },
			{ obj:masterObjs[4], appTime: 10000, rpps: 5500, distance: -200, degree:0 , positionY:500 },
			{ obj:masterObjs[4], appTime: 10000, rpps: 6000, distance: -200, degree:0 , positionY:500 },
			{ obj:masterObjs[4], appTime: 10000, rpps: 6500, distance: -200, degree:0 , positionY:500 },
			{ obj:masterObjs[4], appTime: 10000, rpps: 7000, distance: -200, degree:0 , positionY:500 },
			{ obj:masterObjs[4], appTime: 10000, rpps: 7500, distance: -200, degree:0 , positionY:500 },
			
			{ obj:masterObjs[4], appTime: 10000, rpps: 3000, distance: -400, degree:0 , positionY:250 },
			{ obj:masterObjs[4], appTime: 10000, rpps: 3500, distance: -400, degree:0 , positionY:250 },
			{ obj:masterObjs[4], appTime: 10000, rpps: 4000, distance: -400, degree:0 , positionY:250 },
			{ obj:masterObjs[4], appTime: 10000, rpps: 4500, distance: -400, degree:0 , positionY:250 },
			{ obj:masterObjs[4], appTime: 10000, rpps: 5000, distance: -400, degree:0 , positionY:250 },
			{ obj:masterObjs[4], appTime: 10000, rpps: 5500, distance: -400, degree:0 , positionY:250 },
			{ obj:masterObjs[4], appTime: 10000, rpps: 6000, distance: -400, degree:0 , positionY:250 },
			{ obj:masterObjs[4], appTime: 10000, rpps: 6500, distance: -400, degree:0 , positionY:250 },
			{ obj:masterObjs[4], appTime: 10000, rpps: 7000, distance: -400, degree:0 , positionY:250 },
			{ obj:masterObjs[4], appTime: 10000, rpps: 7500, distance: -400, degree:0 , positionY:250 },
			
			{ obj:masterObjs[4], appTime: 10000, rpps: 3000, distance: -600, degree:0 },
			{ obj:masterObjs[4], appTime: 10000, rpps: 3500, distance: -600, degree:0 },
			{ obj:masterObjs[4], appTime: 10000, rpps: 4000, distance: -600, degree:0 },
			{ obj:masterObjs[4], appTime: 10000, rpps: 4500, distance: -600, degree:0 },
			{ obj:masterObjs[4], appTime: 10000, rpps: 5000, distance: -600, degree:0 },
			{ obj:masterObjs[4], appTime: 10000, rpps: 5500, distance: -600, degree:0 },
			{ obj:masterObjs[4], appTime: 10000, rpps: 6000, distance: -600, degree:0 },
			{ obj:masterObjs[4], appTime: 10000, rpps: 6500, distance: -600, degree:0 },
			{ obj:masterObjs[4], appTime: 10000, rpps: 7000, distance: -600, degree:0 },
			{ obj:masterObjs[4], appTime: 10000, rpps: 7500, distance: -600, degree:0 },
			//{ obj:masterObjs[4], appTime: 10000, rpps: 8000, distance: -400, degree:0 },
			//{ obj:masterObjs[4], appTime: 10000, rpps: 8500, distance: -400, degree:0 },
			
			//ルート3のオブジェクト
			{ obj:masterObjs[5], appTime: 20000, distance: 1000, degree:330},
			{ obj:masterObjs[5], appTime: 20000, distance: 1000, degree:30 },
			{ obj:masterObjs[5], appTime: 20000, distance: 1100, degree:355 },
			{ obj:masterObjs[5], appTime: 20000, distance: 1100, degree:5 },
			
			{ obj:masterObjs[5], appTime: 21000, distance: 1000, degree:330},
			{ obj:masterObjs[5], appTime: 21000, distance: 1000, degree:30 },
			{ obj:masterObjs[5], appTime: 21000, distance: 1100, degree:355 },
			{ obj:masterObjs[5], appTime: 21000, distance: 1100, degree:5 },
			
			{ obj:masterObjs[5], appTime: 22000, distance: 1100, degree:345},
			{ obj:masterObjs[5], appTime: 22000, distance: 1100, degree:15 },
			{ obj:masterObjs[5], appTime: 22000, distance: 1200, degree:355 },
			{ obj:masterObjs[5], appTime: 22000, distance: 1200, degree:5 },
			
			{ obj:masterObjs[5], appTime: 23000, distance: 1100, degree:330 },
			{ obj:masterObjs[5], appTime: 23000, distance: 1100, degree:30 },
			{ obj:masterObjs[5], appTime: 23000, distance: 1300, degree:355 },
			{ obj:masterObjs[5], appTime: 23000, distance: 1300, degree:5 },
			
			{ obj:masterObjs[5], appTime: 24000, distance: 1200, degree:345 },
			{ obj:masterObjs[5], appTime: 24000, distance: 1200, degree:15 },
			{ obj:masterObjs[5], appTime: 24000, distance: 1400, degree:355 },
			{ obj:masterObjs[5], appTime: 24000, distance: 1400, degree:5 },
			
			{ obj:masterObjs[5], appTime: 25000, distance: 1300, degree:330 },
			{ obj:masterObjs[5], appTime: 25000, distance: 1300, degree:30 },
			{ obj:masterObjs[5], appTime: 25000, distance: 1500, degree:355 },
			{ obj:masterObjs[5], appTime: 25000, distance: 1500, degree:5 },
			
			{ obj:masterObjs[5], appTime: 26000, distance: 1400, degree:330 },
			{ obj:masterObjs[5], appTime: 26000, distance: 1400, degree:30 },
			{ obj:masterObjs[5], appTime: 26000, distance: 1500, degree:355 },
			{ obj:masterObjs[5], appTime: 26000, distance: 1500, degree:5 },
			
			//ルート4のオブジェクト
			{ obj:masterObjs[4], appTime: 30000, rpps: 3000, distance: -200, degree:0 , positionY:500 },
			{ obj:masterObjs[4], appTime: 30000, rpps: 3500, distance: -200, degree:0 , positionY:500 },
			{ obj:masterObjs[4], appTime: 30000, rpps: 4000, distance: -200, degree:0 , positionY:500 },
			{ obj:masterObjs[4], appTime: 30000, rpps: 4500, distance: -200, degree:0 , positionY:500 },
			{ obj:masterObjs[4], appTime: 30000, rpps: 5000, distance: -200, degree:0 , positionY:500 },
			{ obj:masterObjs[4], appTime: 30000, rpps: 5500, distance: -200, degree:0 , positionY:500 },
			{ obj:masterObjs[4], appTime: 30000, rpps: 6000, distance: -200, degree:0 , positionY:500 },
			{ obj:masterObjs[4], appTime: 30000, rpps: 6500, distance: -200, degree:0 , positionY:500 },
			{ obj:masterObjs[4], appTime: 30000, rpps: 7000, distance: -200, degree:0 , positionY:500 },
			{ obj:masterObjs[4], appTime: 30000, rpps: 7500, distance: -200, degree:0 , positionY:500 },
			
			{ obj:masterObjs[4], appTime: 30000, rpps: 3000, distance: -400, degree:0 , positionY:250 },
			{ obj:masterObjs[4], appTime: 30000, rpps: 3500, distance: -400, degree:0 , positionY:250 },
			{ obj:masterObjs[4], appTime: 30000, rpps: 4000, distance: -400, degree:0 , positionY:250 },
			{ obj:masterObjs[4], appTime: 30000, rpps: 4500, distance: -400, degree:0 , positionY:250 },
			{ obj:masterObjs[4], appTime: 30000, rpps: 5000, distance: -400, degree:0 , positionY:250 },
			{ obj:masterObjs[4], appTime: 30000, rpps: 5500, distance: -400, degree:0 , positionY:250 },
			{ obj:masterObjs[4], appTime: 30000, rpps: 6000, distance: -400, degree:0 , positionY:250 },
			{ obj:masterObjs[4], appTime: 30000, rpps: 6500, distance: -400, degree:0 , positionY:250 },
			{ obj:masterObjs[4], appTime: 30000, rpps: 7000, distance: -400, degree:0 , positionY:250 },
			{ obj:masterObjs[4], appTime: 30000, rpps: 7500, distance: -400, degree:0 , positionY:250 },
			
			{ obj:masterObjs[4], appTime: 30000, rpps: 3000, distance: -600, degree:0 },
			{ obj:masterObjs[4], appTime: 30000, rpps: 3500, distance: -600, degree:0 },
			{ obj:masterObjs[4], appTime: 30000, rpps: 4000, distance: -600, degree:0 },
			{ obj:masterObjs[4], appTime: 30000, rpps: 4500, distance: -600, degree:0 },
			{ obj:masterObjs[4], appTime: 30000, rpps: 5000, distance: -600, degree:0 },
			{ obj:masterObjs[4], appTime: 30000, rpps: 5500, distance: -600, degree:0 },
			{ obj:masterObjs[4], appTime: 30000, rpps: 6000, distance: -600, degree:0 },
			{ obj:masterObjs[4], appTime: 30000, rpps: 6500, distance: -600, degree:0 },
			{ obj:masterObjs[4], appTime: 30000, rpps: 7000, distance: -600, degree:0 },
			{ obj:masterObjs[4], appTime: 30000, rpps: 7500, distance: -600, degree:0 },
			
			//ルート5のオブジェクト
			{ obj:masterObjs[8], appTime: 40000, rpps: 2000, distance: 200, degree:80 },
			{ obj:masterObjs[10], appTime: 0, rpps: 42000, distance: -200, degree:80 },
			{ obj:masterObjs[8], appTime: 40000, rpps: 2000, distance: 200, degree:280 },
			{ obj:masterObjs[10], appTime: 0, rpps: 42000, distance: -200, degree:280 },
			
			{ obj:masterObjs[8], appTime: 41000, rpps: 2000, distance: -500, degree:90 },
			{ obj:masterObjs[10], appTime: 0, rpps: 43000, distance: -500, degree:80 },
			{ obj:masterObjs[8], appTime: 41000, rpps: 2000, distance: -500, degree:270 },
			{ obj:masterObjs[10], appTime: 0, rpps: 43000, distance: -500, degree:280 },
			
			{ obj:masterObjs[8], appTime: 42000, rpps: 2000, distance: 200, degree:80 },
			{ obj:masterObjs[10], appTime: 0, rpps: 44000, distance: -200, degree:80 },
			{ obj:masterObjs[8], appTime: 42000, rpps: 2000, distance: 200, degree:280 },
			{ obj:masterObjs[10], appTime: 0, rpps: 44000, distance: -200, degree:280 },
			
			{ obj:masterObjs[8], appTime: 43000, rpps: 2000, distance: -500, degree:90 },
			{ obj:masterObjs[10], appTime: 0, rpps: 45000, distance: -500, degree:80 },
			{ obj:masterObjs[8], appTime: 43000, rpps: 2000, distance: -500, degree:270 },
			{ obj:masterObjs[10], appTime: 0, rpps: 45000, distance: -500, degree:280 },
			
			{ obj:masterObjs[8], appTime: 44000, rpps: 2000, distance: 200, degree:80 },
			{ obj:masterObjs[10], appTime: 0, rpps: 46000, distance: -200, degree:80 },
			{ obj:masterObjs[8], appTime: 44000, rpps: 2000, distance: 200, degree:280 },
			{ obj:masterObjs[10], appTime: 0, rpps: 46000, distance: -200, degree:280 },
			
			{ obj:masterObjs[8], appTime: 45000, rpps: 2000, distance: -500, degree:90 },
			{ obj:masterObjs[10], appTime: 0, rpps: 47000, distance: -500, degree:80 },
			{ obj:masterObjs[8], appTime: 45000, rpps: 2000, distance: -500, degree:270 },
			{ obj:masterObjs[10], appTime: 0, rpps: 47000, distance: -500, degree:280 },
			
			{ obj:masterObjs[8], appTime: 46000, rpps: 2000, distance: 200, degree:80 },
			{ obj:masterObjs[10], appTime: 0, rpps: 48000, distance: -200, degree:80 },
			{ obj:masterObjs[8], appTime: 46000, rpps: 2000, distance: 200, degree:280 },
			{ obj:masterObjs[10], appTime: 0, rpps: 48000, distance: -200, degree:280 },
			
			//ルート6のオブジェクト
			{ obj:masterObjs[8], appTime: 50000, rpps: 2000, distance: -450, degree:0 },
			{ obj:masterObjs[10], appTime: 0, rpps: 52000, distance: -500, degree:0 },
			
			{ obj:masterObjs[11], appTime: 50500, rpps: 2000, distance: -450, degree:0 },
			{ obj:masterObjs[10], appTime: 0, rpps: 52500, distance: -500, degree:0 },
			
			{ obj:masterObjs[8], appTime: 51000, rpps: 2000, distance: -450, degree:0 },
			{ obj:masterObjs[10], appTime: 0, rpps: 53000, distance: -500, degree:0 },
			
			{ obj:masterObjs[11], appTime: 51500, rpps: 2000, distance: -450, degree:0 },
			{ obj:masterObjs[10], appTime: 0, rpps: 53500, distance: -500, degree:0 },
			
			{ obj:masterObjs[8], appTime: 52000, rpps: 2000, distance: -450, degree:0 },
			{ obj:masterObjs[10], appTime: 0, rpps: 54000, distance: -500, degree:0 },
			
			{ obj:masterObjs[11], appTime: 52500, rpps: 2000, distance: -450, degree:0 },
			{ obj:masterObjs[10], appTime: 0, rpps: 54500, distance: -500, degree:0 },
			
			{ obj:masterObjs[8], appTime: 53000, rpps: 2000, distance: -450, degree:0 },
			{ obj:masterObjs[10], appTime: 0, rpps: 55000, distance: -500, degree:0 },
			
			{ obj:masterObjs[11], appTime: 53500, rpps: 2000, distance: -450, degree:0 },
			{ obj:masterObjs[10], appTime: 0, rpps: 55500, distance: -500, degree:0 },
			
			{ obj:masterObjs[8], appTime: 54000, rpps: 2000, distance: -450, degree:0 },
			{ obj:masterObjs[10], appTime: 0, rpps: 56000, distance: -500, degree:0 }	
		];
		
		
		var objs:Array<FieldObjectParameter> = [
			{
				rpps:1000,
				obj:masterObjs[0],
				appTime: 2000
			},
			{
				rpps:1000,
				obj:masterObjs[2],
				appTime: 8000,
				positionY: 200
			},
			{
				appTime:6000,
				obj:masterObjs[5],
				rpps:3000
			},
			{
				appTime:4000,
				obj:masterObjs[1]
			},
			{
				appTime:10000,
				obj:masterObjs[3],
				distance: 500,
				degree: 270
			},
			{
				appTime:10000,
				obj:masterObjs[3],
				distance: 500,
				degree: 90
			},
			{
				appTime:12000,
				obj:masterObjs[1],
				distance: 300,
				degree: 225
			},
			{
				appTime:12000,
				obj:masterObjs[1]
			},
			{
				appTime:12000,
				obj:masterObjs[1],
				distance: 300,
				degree: 135
			},
			{
				appTime:6000,
				obj:masterObjs[4],
			}
		];
		setTimeObject(easy_objs);
		setSkyTexture(GameMgr.DIRECTORY + "images/1874-", ["left", "right", "up", "down", "back", "front"], null, 500);
		
		stageId = 2;
		return true;
	}
	override public function init(args:Array<Dynamic>) {
		super.init(args);
	}
	override public function update(time:Float) {
		if (first) time = Math.min(time, 1000);
		super.update(time);
	}
}