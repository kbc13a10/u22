package scene;
import action.RotateAction;
import action.SetMoveAction;
import event.InputEvent;
import js.html.CanvasRenderingContext2D;
import obj2d.AbstractObj2d;
import obj2d.AudioButton;
import objmgr.BattleModeMgr;
import obj2d.MoveText;
import js.Browser;
import obj2d.PlayerBanner;
import obj2d.QrcodeObj;
import obj3d.BreakObject;
import obj3d.Lignt;
import scene.abs.AbstractFixedScene;
import shape.CrystalShape;
import shape.CubeShape;
import obj3d.DefaultGround;
import three.Vector3;
import haxe.Timer;
import obj3d.MeshEx;
import three.Raycaster;
import obj3d.InvincibleObject;
import objmgr.MoveTextMgr;
import objmgr.TeamTypeMgr;

class TitleScene extends AbstractFixedScene {
	private var url = 'http://' + Browser.location.hostname + ':' + Browser.location.port + '/c/';
	private var playerBanner:PlayerBanner;
	private var title:String = 'すまっとシューター';
	private var teamTypeMgr:TeamTypeMgr;
	private var battleModeMgr:BattleModeMgr;
	private var audioButton:AudioButton;
	private var moveTextMgr:MoveTextMgr;
	private var qrCode:QrcodeObj;
	private var hidden:HiddenButton;
	public function new(context:CanvasRenderingContext2D) {
		super(context);
		// 音声のキー
		audioName = "title";
		
		// ライト
		var light;
		scene.add( new three.AmbientLight( 0x666666 ) );
		light = new Lignt( 0xdfebff, 1.75 );
		scene.add( light );
		
		camera.position.set(0,300,-400);
		camera.lookAt(scene.position);
		
		audioButton = new AudioButton(10, GameMgr.HEIGHT - GameMgr.WIDTH / 12 - 10);
		GameMgr.input.addEventListener(EScene.TitleScene, function(point:InputEvent) {
			audioButton.checkHit(point.x, point.y);
		});
	}
	override public function once():Bool {
		if (!super.once()) return false;
		// 床
		var ground = new DefaultGround();
		scene.add( ground.getMesh() );
		return true;
	}
	override public function playerShot(player:Player) {
		super.playerShot(player);
		var x = player.getX();
		var y = player.getY();
		
		// バトルモード選択
		teamTypeMgr.checkType(x, y);
		
		// 難易度選択
		battleModeMgr.checkHit(x, y);
		
		// 音声
		audioButton.checkHit(x, y);
		
		//ランダムマップ
		hidden.checkHit(x, y);
		
		var pos = new Vector3((x / GameMgr.WIDTH) * 2 - 1, -(y / GameMgr.HEIGHT) * 2 + 1, 1);
		pos.unproject(camera);
		// 始点、向きベクトルを渡してレイを作成
		var ray = new Raycaster(camera.position, pos.sub(camera.position).normalize());
		var target = [];
		for ( i in obj3d) {
			target.push(i);
		}
		var objs = ray.intersectObjects(target);
		if (objs.length > 0) {
			var obj = cast(objs[0].object, MeshEx);
			teamTypeMgr.checkTeam(player, obj);
			if (obj.hit() == true) {
				obj3d.remove(obj);
				scene.remove(obj);
				audio.playSE("break");
			}
		}
		
		// ゲーム開始判定
		var selectMode = battleModeMgr.getSelectMode(x, y);
		if (!Type.enumEq(selectMode, EBattleMode.none)) {
			GameMgr.allReady();
			switch(selectMode) {
				case Hard:
					GameMgr.changeScene(EScene.HardGameScene);
				case Normal:
					GameMgr.changeScene(EScene.NormalGameScene);
				case Easy:
					GameMgr.changeScene(EScene.EasyGameScene);
				case Ran:
					GameMgr.changeScene(EScene.RandomGameScene);
				case none:
			}
		}
	}
	override public function init(args:Array<Dynamic>) {
		super.init(args);
		
		obj2d.add(audioButton);
		
		playerBanner = new PlayerBanner(GameMgr.WIDTH / 11, GameMgr.HEIGHT / 40 * 7);
		obj2d.add(playerBanner);
		
		moveTextMgr = new MoveTextMgr(playerBanner.y + playerBanner.getHeight() + GameMgr.HEIGHT / 15, GameMgr.HEIGHT / 20);
		
		if (isFirst()) {
			teamTypeMgr = new TeamTypeMgr(moveTextMgr.y + 10);
			
			battleModeMgr = new BattleModeMgr(moveTextMgr.y + 10);
			
			url += args[0];
			
			// QRコード生成
			qrCode = new QrcodeObj(GameMgr.WIDTH * 0.85, GameMgr.HEIGHT / 8, -1, 2, url, GameMgr.WIDTH / 1280);
			hidden = new HiddenButton(GameMgr.WIDTH * 0.85, GameMgr.HEIGHT / 8, qrCode.getWidth(), qrCode.getHeight());
			
		} else {
			for (p in GameMgr.players) {
				obj2d.add(p.getObj());
			}
		}
		
	}
	
	// ブロックの自動生成
	private var cubeArray:Array<BreakObject> = [];
	private var cubeNum:Int = 12;
	private var arrayMax:Int = 24;
	private var delay:Float = 1500;
	private var cntTime:Float = 2000;
	private var col:Int = 8;
	private function appCube(time:Float) {
		if (!GameMgr.isReadyAll() && !GameMgr.isTeamBattle()) {
			cntTime += time;
			if (cntTime > delay && obj3d.length < cubeNum) {
				cntTime = 0;
				for (i in 0...arrayMax) {
					if (cubeArray[i] != null && !cubeArray[i].isLife()) cubeArray[i] = null;
				}
				var r = Std.int(Math.random() * arrayMax);
				while (cubeArray[r] != null) {
					r = Std.int(Math.random() * arrayMax);
				}
				var cube = new BreakObject(new CubeShape(0, { name:'' + r, color:Std.int(0xffffff * Math.random()), size:40 } ), [new SetMoveAction([ { time:500, point:new Vector3(0, 40, 0) } ]) ]);
				cube.position.y -= 40;
				cube.position.x = ((r % col) - (col / 2)) * (cube.getWidth() * 1.5);
				cube.position.z = (Std.int(r / col) + 1) * - (cube.getWidth() * 1.5);
				cubeArray[r] = cube;
				obj3d.add(cube);
				scene.add(cube);
			}
		} else if(cubeArray.length > 0) {
			for (c in cubeArray) {
				obj3d.remove(c);
				scene.remove(c);
			}
			cubeArray = [];
		}
	}
	override public function update(time:Float) {
		// 流れるテキスト
		moveTextMgr.update(context, time);
		
		// タイトル
		context.beginPath();
		context.font = GameMgr.HEIGHT / 16 + 'px "Times New Roman"';
		context.fillStyle = '#ffffff';
		context.textAlign = 'left';
		context.fillText(title, 20, GameMgr.HEIGHT / 8);
		context.closePath();
		
		// URL
		context.beginPath();
		context.font = GameMgr.HEIGHT / 20 + 'px "Times New Roman"';
		context.fillStyle = '#ffffff';
		context.textAlign = 'right';
		context.fillText(url, GameMgr.WIDTH - 30, GameMgr.HEIGHT / 12);
		context.closePath();
		
		// ランダムキューブ
		appCube(time);
		// チーム選択
		teamTypeMgr.update(obj3d, scene, context, time);
		// 難易度選択
		battleModeMgr.draw(context, time);
		
		super.update(time);
		
		qrCode.draw(context, time);
	}
}