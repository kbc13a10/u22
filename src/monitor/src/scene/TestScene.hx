package scene;

import action.RotateAction;
import action.SetRotaAction;
import js.Browser;
import obj3d.ArrowObj;
import obj3d.BreakObject;
import obj3d.InvincibleObject;
import scene.abs.AbstractFixedScene;
import shape.ArrowShape;
import shape.CrystalShape;
import shape.CubeShape;
import shape.HexagonalShape;
import shape.TreeShape;
import three.MeshPhongMaterial;
import three.ImageUtils;
import three.Mesh;
import three.Object3D;
import three.PlaneBufferGeometry;
import three.Vector3;
import three.Quaternion;


class TestScene extends AbstractFixedScene {
	var controls:three.OrbitControls;
	public function new(context) {
		super(context);
		var light;
		scene.add( new three.AmbientLight( 0x666666 ) );
		light = new three.DirectionalLight( 0xdfebff, 1.75 );
		light.position.set( -50, 1000, -200 );
		light.position.multiplyScalar( 1.3 );
		light.castShadow = true;
		light.shadowCameraVisible = true;
		light.shadowMapWidth = 1024;
		light.shadowMapHeight = 1024;
		scene.add( light );
		
		// ground
		var groundTexture = three.ImageUtils.loadTexture( GameMgr.DIRECTORY + "images/checkerboard.jpg" );
		groundTexture.wrapS = groundTexture.wrapT = Three.WrappingMode.RepeatWrapping;
		groundTexture.repeat.set( 32, 32 );
		groundTexture.anisotropy = 16;
		var groundMaterial = new MeshPhongMaterial( { color: 0xffffff, map: groundTexture, shininess:10 } );
		
		var size:Float = 20000;
		var mesh = new three.Mesh( new PlaneBufferGeometry( size,size ), groundMaterial );
		//mesh.position.y = -250;
		mesh.position.y = -0.5;
		mesh.rotation.x = - Math.PI / 2;
		mesh.receiveShadow = true;
		scene.add( mesh );
		
		camera.position.set(0,350,-1500);
		camera.lookAt(scene.position);
		
		controls = new three.OrbitControls( camera, GameMgr.input.div );
	}
	var arrow:ArrowObj;
	override public function init(args:Array<Dynamic>) {
		super.init(args);
		arrow = new ArrowObj(1700, camera.position);
		
		Browser.window.setTimeout(function() {
			audio.stopBGM();
		}, 5000);
		Browser.window.setTimeout(function() {
			audio.playBGM("test");
		}, 6000);
		
		//arrow.position.copy(camera.position);
		//arrow.position.z += 100;
		//arrow.position.y -= 115 - 3000/ 50;
		scene.add(arrow);
		obj3d.add(arrow);
		arrow.setPosi( camera.position, 180 );
		arrow.setDirection(new Vector3(0, 0, 0), new Vector3(0, 0, 1));
		var cry = new BreakObject(new CrystalShape(), [new RotateAction()]);
		var hexa = new InvincibleObject(new HexagonalShape(), [new SetRotaAction(null, 10)]);
		scene.add(hexa);
		obj3d.add(hexa);
		cry.position.z = hexa.getWidth() / 2 + cry.getWidth() / 2;
		scene.add(cry);
		obj3d.add(cry);
		var cry3 = new BreakObject(new CrystalShape(), [new RotateAction()]);
		cry3.position.z = 100;
		cry3.position.y += cry.getHeight();
		scene.add(cry3);
		obj3d.add(cry3);
		var hexa2 = new InvincibleObject(new HexagonalShape(), [new SetRotaAction(null, 10)]);
		hexa2.position.x -= hexa.getWidth();
		scene.add(hexa2);
		obj3d.add(hexa2);
		var cry2 = new BreakObject(new CrystalShape(), [new RotateAction()]);
		cry2.position.z = 100;
		cry2.position.x = hexa2.position.x;
		scene.add(cry2);
		obj3d.add(cry2);
		var tree = new InvincibleObject(new TreeShape());
		tree.position.x = 300;
		scene.add(tree);
		obj3d.add(tree);
		var tree2 = new InvincibleObject(new TreeShape());
		tree2.position.x = 300 + tree.getWidth();
		scene.add(tree2);
		obj3d.add(tree2);
		var cube = new BreakObject(new CubeShape());
		cube.position.z -= 200;
		scene.add(cube);
		obj3d.add(cube);
		var cube2 = new BreakObject(new CubeShape());
		cube2.position.z -= 200;
		cube2.position.x = cube.getWidth();
		scene.add(cube2);
		obj3d.add(cube2);
		
		var ran = new Random(1);
		for (i in 0...10) {
			trace(ran.nextUInt() % 2000);
		}
	}
	private var total = 0.0;
	override public function update(time:Float) {
		super.update(time);
		controls.update();
		total += time;
		
		//var q = new Quaternion();  
		////３軸
		//var Axis = {  
			//x : new Vector3(1, 0, 0).normalize(),
			//y : new Vector3(0, 1, 0).normalize(),
			//z : new Vector3(0, 0, 1).normalize()
		//};
		////x軸で半回転させたい
		//q.setFromAxisAngle(Axis.z, Math.PI / 180);
//
		////お手本みたいに回転を加えてみる
		//q.multiply(arrow.quaternion.clone());  
		//arrow.quaternion.copy(q);
	}
}