package scene;

import scene.abs.AbstractFixedScene;


class EndScene extends AbstractFixedScene {

	public function new(context) {
		super(context);
	}
	override public function init(args:Array<Dynamic>) {
		super.init(args);
		if (args.length > 0) {
			msg = args[0];
		}
		audio.stopBGM();
	}
	private var msg:String = "サーバーとの接続が切れました";
	override public function update(time:Float) {
		context.beginPath();
		context.font = GameMgr.WIDTH / 24 + 'px "Times New Roman"';
		context.fillStyle = 'rgb(0, 0, 0)';
		context.textAlign = 'center';
		context.fillText(msg, GameMgr.WIDTH / 2, GameMgr.HEIGHT / 2);
		context.closePath();
	}
}