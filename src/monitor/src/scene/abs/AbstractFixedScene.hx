package scene.abs;
import camera.FixedCamera;

/**
 * ...
 * @author taka
 */
class AbstractFixedScene extends AbstractScene {
	public function new(context) {
		super(context);
		camera = new FixedCamera();
	}
}