package scene.abs;

import field.FieldFloor;
import js.html.CanvasRenderingContext2D;
import position.RoutePosition;
import scene.abs.AbstractMoveScene.MoveParameter;
import three.BoxGeometry;
import three.CubeGeometry;
import three.MeshFaceMaterial;
import three.MeshPhongMaterial;
import three.Material;
import three.MeshBasicMaterial;
import three.Mesh;
import three.ImageUtils;
import three.Texture;
import three.Raycaster;
import three.Vector3;
import obj3d.TimerObject;
import obj3d.MeshEx;
import shape.TextShape;

class AbstractMoveRouteScene extends AbstractMoveScene {
	private var floorMgr:FieldFloor;
	private var groundMaterial:MeshPhongMaterial;
	private var skyMaterials:Array<Texture>;
	private var skyBox:Mesh;
	private var skyBoxAddY:Int;
	private var floorObjs = [];
	
	public function new(context:CanvasRenderingContext2D, ?param:MoveParameter, ?roteTime:Float) {
		super(context, param);
		if (param == null) param = { };
		positionMgr = new RoutePosition(param.startWait, roteTime);
		floorMgr = new FieldFloor(visualRange);
	}
	public function setRouteData(data:Array<RouteParameter>) {
		positionMgr.setRouteData(data);
		floorMgr.setRouteData(data);
		maxTime = positionMgr.getMaxTime();
	}
	public function setFloorTexture(floor:String = null, ?pattern:Int = 16) {
		if (floor == null) {
			floor = GameMgr.DIRECTORY + "images/floor01.jpg";
		}
		floorMgr.setTexture(floor, pattern);
	}
	public function setGroudTexture(ground:String = null, ?pattern:Int = 128) {
		if (ground == null) {
			ground = GameMgr.DIRECTORY + "images/grasslight-big.jpg";
		}
		var groundTexture = three.ImageUtils.loadTexture(ground);
		groundTexture.wrapS = groundTexture.wrapT = Three.WrappingMode.RepeatWrapping;
		groundTexture.repeat.set( pattern, pattern );
		groundTexture.anisotropy = 16;
		groundMaterial = new three.MeshPhongMaterial( { color: 0xffffff, specular: 0x111111, map: groundTexture } );
	}
	public function setSkyTexture(imagePrefix:String = null, ?directions:Array<String>, imageSuffix:String = ".png", addY:Int = 0) {
		if (imagePrefix == null) {
			imagePrefix = GameMgr.DIRECTORY + "images/dawnmountain-";
		}
		skyMaterials = [];
		var imageSuffix = ".png";
		if (directions == null) directions = ["xpos", "xneg", "ypos", "yneg", "zpos", "zneg"];
		for (d in directions) {
			skyMaterials.push(ImageUtils.loadTexture( imagePrefix + d + imageSuffix ));
		}
		skyBoxAddY = addY;
	}
	override public function once() {
		if (!super.once()) return false;
		var light;
		scene.add( new three.AmbientLight( 0x666666 ) );
		light = new three.DirectionalLight( 0xdfebff, 1.75 );
		light.position.set( -50, 250, -200 );
		light.position.multiplyScalar( 1.3 );
		light.castShadow = true;
		light.shadowCameraVisible = true;
		light.shadowMapWidth = 1024;
		light.shadowMapHeight = 1024;
		scene.add( light );
		setSkyTexture();
		setFloorTexture();
		setGroudTexture();
		return true;
	}
	override public function init(args) {
		super.init(args);
		if (isFirst()) {
			//	Skybox
			var skyGeometry = new BoxGeometry( 10000, 10000, 10000 );
			var materialArray:Array<Material> = [];
			for (i in 0...6)
				materialArray.push( new MeshBasicMaterial({
					map: skyMaterials[i],
					side: Three.Side.BackSide
				}));
			var skyMaterial = new MeshFaceMaterial( materialArray );
			skyBox = new Mesh( skyGeometry, skyMaterial );
			scene.add( skyBox );
			
			//	Ground
			var size:Float = 200000;
			var mesh = new three.Mesh( new three.PlaneBufferGeometry( size,size ), groundMaterial );
			mesh.position.y = -3.5;
			mesh.rotation.x = - Math.PI / 2;
			mesh.receiveShadow = true;
			scene.add( mesh );
		}
		for (f in floorObjs) {
			scene.remove(f);
		}
		floorObjs = [];
		var floors = floorMgr.getFloorObjects(stageId);
		for (i in floors) {
			scene.add(i);
			floorObjs.push(i);
		}
		for (i in GameMgr.players) {
			obj2d.add(i.getObj());
		}
	}
	
	override public function playerShot(player:Player) {
		super.playerShot(player);
		var x = player.getX();
		var y = player.getY();
		
		if (end) return;
		var pos = new Vector3((x / GameMgr.WIDTH) * 2 - 1, -(y / GameMgr.HEIGHT) * 2 + 1, 1);
		pos.unproject(camera);
		var ray = new Raycaster(camera.position, pos.sub(camera.position).normalize());
		var target = [];
		for ( i in obj3d) {
			target.push(i);
		}
		var objs = ray.intersectObjects(target);
		if (objs.length > 0) {
			var i = 0;
			var obj = cast(objs[i].object, MeshEx);
			while (obj.getShapeName() == 'text' && objs.length > i + 1) {
				i++;
				obj = cast(objs[i].object, MeshEx);
			}
			if (obj.hit() == true) {
				var score = obj.getScore();
				var text = new TimerObject(new TextShape( { text:"" + score, frontColor: player.getColor(), SideColor:player.getColor(), opacity: 0.6, size:60  } ));
				text.position.copy(obj.position);
				var rad = Math.atan2(camera.position.z - obj.position.z, obj.position.x - camera.position.x);
				text.rotation.y = rad - Math.PI / 2;
				player.addScore(score);
				obj3d.add(text);
				scene.add(text);
				obj3d.remove(obj);
				scene.remove(obj);
				audio.playSE("break");
			} else {
				if (obj.getShapeName() != 'text') {
					audio.playSE("block");
				}
			}
		}
	}
	
	override public function update(time:Float) {
		super.update(time);
		if (skyBox != null) {
			var p = positionMgr.getPosition().point;
			p.y += skyBoxAddY;
			skyBox.position.copy(p);
		}
	}
}