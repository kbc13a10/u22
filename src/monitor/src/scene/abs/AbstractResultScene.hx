package scene.abs;
import obj2d.TargetRect;
import obj3d.Lignt;
import obj3d.DefaultGround;

class AbstractResultScene extends AbstractFixedScene {
	// プレイヤー配列
	private var players:Array<Player> = new Array<Player>();
	private var waitTime:Float = 3000;
	private var cntTime:Float = 0;
	private var titleText:TargetRect;
	private var againText:TargetRect;
	private var beforeScene:EScene;
	private var againPlayer:String = '';
	private var againNumbers:Array<Int> = [];
	private var texts = ['タイトルへ', 'もう一度'];
	public function new(context) {
		super(context);
		audioName = "result";
	}
	override public function init(args:Array<Dynamic>) {
		super.init(args);
		cntTime = 0;
		beforeScene = cast(args[1], EScene);
		if (Type.enumEq(beforeScene, EScene.HardGameScene)) GameMgr.setHardClear(true);
		againPlayer = '';
		againNumbers = [];
	}
	override public function once():Bool {
		if (!super.once()) return false;
		
		var light;
		scene.add( new three.AmbientLight( 0x666666 ) );
		light = new Lignt( 0xdfebff, 1.75 );
		scene.add( light );
		
		var ground = new DefaultGround();
		scene.add( ground.getMesh() );
		
		camera.position.set(0,500,0);
		camera.lookAt(scene.position);
		
		return true;
	}
	override public function playerShot(player:Player) {
		super.playerShot(player);
		var x = player.getX();
		var y = player.getY();
		
		if (titleText != null && titleText.checkHit(x, y)) {
			GameMgr.resetReady();
			GameMgr.changeScene(EScene.TitleScene);
		}
		if (againText != null && againText.checkHit(x, y)) {
			if (againPlayer.indexOf('' + player.getNumber()) == -1) {
				againPlayer += player.getNumber();
				againNumbers.push(player.getNumber());
			}
		}
	}
	override public function update(time:Float) {
		if (GameMgr.entryNum == 0 || players.length == againNumbers.length) {
			cntTime += time;
			if(waitTime <= cntTime) {
				if(GameMgr.entryNum == 0) {
					GameMgr.resetReady();
					GameMgr.changeScene(EScene.TitleScene);
				} else if (players.length == againNumbers.length) {
					for (p in players) {
						p.setScore(0);
					}
					GameMgr.changeScene(beforeScene, ['again']);
				}
			}
			var cnt = '' + Std.int((waitTime - cntTime) / 1000 + 1);
			context.beginPath();
			context.textAlign = 'center';
			context.fillStyle = '#000000';
			context.textAlign = 'left';
			context.font = GameMgr.HEIGHT / 17 + 'px bold "Times New Roman"';
			context.fillText(cnt, GameMgr.WIDTH - (GameMgr.HEIGHT / 17) * 2, GameMgr.HEIGHT / 17 * 2);
			context.closePath();
		}
		if (againText != null) {
			if (againText.isShow()) {
				if (players.length != GameMgr.entryNum) againText.hideText();
				for (i in 0...againNumbers.length) {
					context.beginPath();
					context.globalAlpha = 0.7;
					context.fillStyle = '#' + GameMgr.playerColor[againNumbers[i]];
					context.fillRect(againText.x + (againText.getWidth() / players.length) * i, againText.y, againText.getWidth() / players.length, againText.getHeight());
					context.globalAlpha = 1;
					context.closePath();
				}
			}
		}
		super.update(time);
	}
}