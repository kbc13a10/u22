package scene.abs;

import camera.MoveCamera;
import field.ObjectPerTime;
import js.html.CanvasRenderingContext2D;
import obj3d.ArrowObj;
import obj3d.InvincibleObject;
import position.AbstractPosition;
import scene.abs.AbstractScene;
import field.FieldObject;
import obj3d.TimerObject;
import shape.ArrowShape;
import shape.TextShape;
import obj2d.AbstractObj2d;
import obj2d.MultiScoreBar;
import obj2d.TeamScoreBar;

/**
 * カメラの設定
 * startWait visualRange potisionY
 * @param float startWait スタートの待ち時間
 * @param Int visualRange 物体が出現するカメラからの距離
 * @param Int potisionY カメラの高さ
 */
typedef MoveParameter = {
	@:optional var startWait:Float;
	@:optional var visualRange:Int;
	@:optional var potisionY:Int;
}
/**
 * 継承前提クラス
 * カメラが動くシーンの抽象クラス
 */
class AbstractMoveScene extends AbstractScene {
	private var waitTime:Float;
	private var positionMgr:AbstractPosition;
	private var opt:ObjectPerTime;
	private var visualRange:Int;
	private var progress:Float = 0;
	private var nowTime:Int = 0;
	private var debugTime:Float = 0;
	private var first:Bool = true;
	private var maxTime:Int = 0;
	private var endTime:Float = 2000;
	private var end:Bool = false;
	private var arrow:ArrowObj;
	private var players:Array<Player> = [];
	private var stageId:Int = 1;
	private var errorTimeCnt:Float = 0;
	
	public function new(context:CanvasRenderingContext2D, ?param:MoveParameter) {
		super(context);
		if (param == null) param = { };
		waitTime = if (param.startWait != null) param.startWait else 3000;
		visualRange = if (param.visualRange != null) param.visualRange else 1500;
		var py = if (param.potisionY != null) param.potisionY else 350;
		camera = new MoveCamera(visualRange, py);
		opt = new ObjectPerTime(waitTime);
		arrow = new ArrowObj(visualRange, camera.position);
		scene.add(arrow);
	}
	public function setDebugTime(startTime:Float, ?first:Bool) {
		this.debugTime = startTime;
		if (first != null) this.first = first;
	}
	override public function init(args) {
		super.init(args);
		positionMgr.init();
		opt.init();
		nowTime = Std.int(waitTime / 1000) + 1;
		progress = 0;
		first = true;
		maxTime = positionMgr.getMaxTime();
		end = false;
		
		players = [];
		for (p in GameMgr.players) {
			players.push(p);
		}
		var scoreBar:AbstractObj2d;
		if (GameMgr.isTeamBattle()) {
			scoreBar = new TeamScoreBar(17, GameMgr.WIDTH, players, GameMgr.teamColor);
		} else {
			scoreBar = new MultiScoreBar(17, GameMgr.WIDTH, players);
		}
		obj2d.add(scoreBar);
	}
	override public function update(time:Float) {
		if (debugTime != 0) {
			time += debugTime;
			debugTime = 0;
		}
		super.update(time);
		progress += time;
		if (isError()) return;
		// 現在位置の更新
		positionMgr.update(time);
		var posiData = positionMgr.getPosition();
		
		// カメラの位置移動
		camera.setPosition(posiData);
		
		// オブジェクトの追加
		addObjs(time);
		
		// スタートのカウントダウン
		if (nowTime > 0) startCount();
		
		// 方向矢印をカメラの位置に合わせる
		var arrowTime = if (nowTime > 0) waitTime else 2000;
		arrow.setPosi(camera.position, posiData.deg);
		
		// ゲーム終了か
		if (progress >= maxTime + waitTime) {
			endCount();
		} else {
			// 方向矢印の向き調整
			arrow.setDirection(camera.position, positionMgr.getAfterPosition(arrowTime).point);
		}
		first = false;
	}
	private function setTimeObject(timeArray:Array<FieldObjectParameter>) {
		opt.setTimeObjects(timeArray);
	}
	private function addObjs(time:Float) {
		// オブジェクト時間の更新
		opt.update(time);
		// 出現オブジェクトの取得
		var objs:Array<FieldObject> = opt.getSetObject();
		if (objs.length > 0 && first == false) {
			for (o in objs) {
				// オブジェクトの出現位置取得
				var ap = positionMgr.getAfterPosition(o.getAfterTime());
				// カメラの見ている距離に調整
				ap = camera.getTarget(ap);
				// オブジェクトに当てはめる
				o.setObjPosition(ap.point, ap.deg);
				// シーンに追加
				var obj = o.getObj();
				obj3d.add(obj);
				scene.add(obj);
			}
		}
	}
	private function startCount() {
		var prInt = Std.int(progress / 1000);
		var num = Std.int(waitTime / 1000) - prInt;
		if (nowTime != num) {
			nowTime = num;
			if (nowTime < 0) return;
			var text:String = if (nowTime == 0) "Start!" else "" + nowTime;
			var positionData = positionMgr.getPosition();
			var textObj:TimerObject = new TimerObject(new TextShape( { text:text, size:200 } ), { limit: Math.min((prInt + 1) * 1000 - progress, 800) } );
			textObj.position.copy(positionData.point);
			var radian = Math.PI / 180 * positionData.deg;
			textObj.position.z += 800 * Math.sin(radian);
			textObj.position.x += (100 * text.length) / 2;
			textObj.position.y += 100;
			textObj.rotation.y = Math.PI;
			obj3d.add(textObj);
			scene.add(textObj);
		}
	}
	private function endCount() {
		if (!end) {
			end = true;
			var positionData = positionMgr.getPosition();
			positionData = camera.getTarget(positionData);
			var text = "FINISH!";
			var textObj:TimerObject = new TimerObject(new TextShape( { text:text, size:200 } ), { limit: endTime } );
			textObj.position.copy(positionData.point);
			var radian = Math.PI / 180 * positionData.deg;
			textObj.position.z += 500 * Math.sin(radian - Math.PI  + Math.PI / 4);
			textObj.position.x += 500 * Math.cos(radian - Math.PI  + Math.PI / 4);
			textObj.position.y += 100;
			textObj.rotation.y = Math.PI / 180 * (90 - positionData.deg) + Math.PI;
			obj3d.add(textObj);
			scene.add(textObj);
			if(players.length == 1 && !Type.enumEq(GameMgr.nowScene, EScene.RandomGameScene)) GameMgr.sendPlayerRank(players[0], stageId);
		}
		if (progress - maxTime - waitTime >= endTime) {
			if (players.length == 1) {
				GameMgr.changeScene(EScene.ResultRankScene, [players,GameMgr.nowScene]);
			} else if (GameMgr.isTeamBattle()) {
				GameMgr.changeScene(EScene.ResultTeamScene, [players,GameMgr.nowScene]);
			}else{
				GameMgr.changeScene(EScene.ResultSingleScene, [players,GameMgr.nowScene]);
			}
		}
	}
	private function isError() : Bool{
		if (GameMgr.entryNum > 0) return false;
		if (!end) progress = maxTime + waitTime;
		endCount();
		return true;
	}
}