package scene.abs;

import js.html.CanvasRenderingContext2D;
import js.html.MessageEvent;
import obj2d.AbstractObj2d;
import three.Camera;
import three.PerspectiveCamera;
import three.Scene;
import three.Object3D;
import camera.AbstractCamera;

class AbstractScene {
	public var scene(default, null):Scene;
	public var camera(default, null):AbstractCamera;
	
	private var context:CanvasRenderingContext2D;
	private var obj2d = new List<AbstractObj2d>();
	private var obj3d = new List<Object3D>();
	private var audio:Audio;
	private var audioName:String;
	
	public function new(context:CanvasRenderingContext2D) {
		this.context = context;
		scene = new Scene();
		audio = Audio.getInstance();
	}
	
	private var firstInit = true;
	public function init(args:Array<Dynamic>) {
		obj3d.clear();
		GameMgr.setBackColor(0xccffff);
		if (audioName != null) {
			audio.playBGM(audioName);
		}
	}
	public function isFirst() {
		if (!firstInit) return false;
		firstInit = false;
		return true;
	}
	
	public function update(time:Float) {
		for (o2 in obj2d) {
			o2.draw(context, time);
		}
		var removes = [];
		for (o3 in obj3d) {
			o3.update(time);
			if (!o3.isLife()) {
				scene.remove(o3);
				removes.push(o3);
			}
		}
		for (r in removes) {
			obj3d.remove(r);
		}
	}
	
	public function clear() {
		for (o3 in obj3d) {
			scene.remove(o3);
		}
		obj3d.clear();
		obj2d.clear();
	}
	
	public function addObj2D(obj:AbstractObj2d) {
		obj2d.add(obj);
	}
	
	public function removeObj2D(obj:AbstractObj2d) {
		obj2d.remove(obj);
	}
	
	public function playerShot(player:Player) {
		player.shot();
	}
	
	private var doOnce = true;
	public function once() : Bool {
		if (!doOnce) return false;
		doOnce = false;
		return true;
	}
	
}