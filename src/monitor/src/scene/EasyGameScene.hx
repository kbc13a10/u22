package scene;

//import action.MoveXSideAction;
//import action.MoveZSideAction;
import action.MoveAction;
import action.RotateAction;
import action.RotateRandomAction;
import action.JumpAction;
import obj3d.BreakObject;
import obj3d.InvincibleObject;
import shape.CrystalShape;
import shape.CubeShape;
import shape.CylinderShape;
import shape.DrumShape;
import shape.FloorShape;
import event.InputEvent;
import three.Mesh;
import three.Vector3;
import scene.abs.AbstractMoveRouteScene;
import position.RoutePosition.RouteParameter;
import field.FieldObject;

class EasyGameScene extends AbstractMoveRouteScene {	
	//var controls:three.OrbitControls;
	public function new(context) {
		super(context, 1000);
		audioName = "easy";
	}
	override public function once() {
		if (!super.once()) return false;
		
		var routes:Array<RouteParameter> = [
			{ time: 5000, point: new Vector3(0, 0, 2000) },
			{ time: 10000, point: new Vector3(6000, 0, 2000) },
			{ time: 10000, point: new Vector3(6000, 0, 8000) },
			{ time: 10000, point: new Vector3(0, 0, 8000) },
			{ time: 10000, point: new Vector3(-2000, 0, 14000) },
			{ time: 10000, point: new Vector3(3000, 0, 14000) },
			{ time: 5000, point: new Vector3( 6000, 0, 14000) },
		];
		setRouteData(routes);
		
		var roteRightAction = new RotateAction({right:true});
		var jumpAction = new JumpAction({waitTime: 0});
		var randamRoteAction = new RotateRandomAction();
		var moveXAction = new MoveAction({moveType:x});
		var moveXRightAction = new MoveAction({reverse: true, moveType:x});
		var moveZAction = new MoveAction({moveType:z});
		
		var crystalColorShape = new CrystalShape( { color: 0xff5555 } );
		var crystalBlackShape = new CrystalShape( { color:0x000000 } );
		var crystalSingleShape = new CrystalShape(900);
		var cylinderShape = new CylinderShape();
		var cubeShape = new CubeShape();
		var drumShape = new DrumShape();
		
		var masterObjs = [
			new BreakObject(crystalColorShape, [moveXAction]),
			new BreakObject(crystalBlackShape, [roteRightAction]),
			new BreakObject(cubeShape, [randamRoteAction]),
			new BreakObject(crystalColorShape, [jumpAction]),
			new BreakObject(cylinderShape, [roteRightAction]),
			new BreakObject(cylinderShape, [jumpAction]),
			new InvincibleObject(drumShape),
			new BreakObject(crystalColorShape, [moveXRightAction]),
			new BreakObject(crystalSingleShape, [jumpAction]),
		];
		
		var objs:Array<FieldObjectParameter> = [
			
			//ドラム缶
			{ obj:masterObjs[6], appTime:3000, rpps:4500, distance:-300 },
			{obj:masterObjs[6],appTime:3000,rpps:4500,distance:300},
			{obj:masterObjs[6],appTime:4000,rpps:4500,distance:-1000},
			{obj:masterObjs[6],appTime:4000,rpps:4500,distance:1000},
			{obj:masterObjs[6], appTime:4000, rpps:5500, distance:1000 },
			{obj:masterObjs[6], appTime:4000, rpps:5500, distance: -1000 },
			{obj:masterObjs[6], appTime:4000, rpps:6000, distance: 500 },
			{obj:masterObjs[6], appTime:4000, rpps:6000, distance: -500 },
			
			//ジャンプコイン
			{obj:masterObjs[5],appTime:3000,rpps:4600,distance:300,positionY:-100},
			{obj:masterObjs[5],appTime:3000,rpps:4600,distance: -300,positionY:-100},
			{obj:masterObjs[5],appTime:4000,rpps:4600,distance:1000,positionY:-100},
			{obj:masterObjs[5], appTime:4000, rpps:4600, distance: -1000, positionY: -100 },
			{obj:masterObjs[5],appTime:4000,rpps:5600,distance:1000,positionY:-100},
			{obj:masterObjs[5], appTime:4000, rpps:5600, distance: -1000, positionY: -100 },
			{obj:masterObjs[5], appTime:3000, rpps:7100, distance: 500, positionY: -100 },
			{obj:masterObjs[5], appTime:3000, rpps:7100, distance: -500, positionY: -100 },
			
			//ひし形
			{obj:masterObjs[1],appTime:0,rpps:3000,degree:1000},
			{obj:masterObjs[1],appTime:0,rpps:3000,distance:300,degree:1000},
			{obj:masterObjs[1],appTime:0,rpps:3000,distance:300,degree: -1000},
			
			//四角
			{obj:masterObjs[2],appTime:6000,rpps:3000,positionY:230},
			{obj:masterObjs[2],appTime:6000,rpps:3000,positionY:400,distance:200},
			{obj:masterObjs[2],appTime:6000,rpps:3000,distance: -200,positionY:50},
			{obj:masterObjs[2],appTime:6000,rpps:3000,positionY:400,distance:-200},
			{obj:masterObjs[2],appTime:6000,rpps:3000,positionY:50,distance:200},
			
			//ひし形中段
			{
				obj:masterObjs[1],
				appTime:4000,
				rpps:2000,
				positionY:200,
				distance:1000
			},
			{
				obj:masterObjs[1],
				appTime:4000,
				rpps:2000,
				positionY:200,
				distance:800
			},
			{
				obj:masterObjs[1],
				appTime:4000,
				rpps:2000,
				positionY:200,
				distance:600
			},
			{
				obj:masterObjs[1],
				appTime:4000,
				rpps:2000,
				positionY:200,
				distance:400
			},
			{
				obj:masterObjs[1],
				appTime:4000,
				rpps:2000,
				positionY:200,
				distance:200
			},
			{
				obj:masterObjs[1],
				appTime:4000,
				rpps:2000,
				positionY:200
			},
			{
				obj:masterObjs[1],
				appTime:4000,
				rpps:2000,
				positionY:200,
				distance:-200
			},
			{
				obj:masterObjs[1],
				appTime:4000,
				rpps:2000,
				positionY:200,
				distance:-400
			},
			{
				obj:masterObjs[1],
				appTime:4000,
				rpps:2000,
				positionY:200,
				distance:-600
			},
			{
				obj:masterObjs[1],
				appTime:4000,
				rpps:2000,
				positionY:200,
				distance:-800
			},
			{
				obj:masterObjs[1],
				appTime:4000,
				rpps:2000,
				positionY:200,
				distance:-1000
			},
			
			//ひし形上段
			{
				obj:masterObjs[1],
				appTime:4000,
				rpps:2000,
				positionY:400,
				distance:1000
			},
			{
				obj:masterObjs[1],
				appTime:4000,
				rpps:2000,
				positionY:400,
				distance:800
			},
			{
				obj:masterObjs[1],
				appTime:4000,
				rpps:2000,
				positionY:400,
				distance:600
			},
			{
				obj:masterObjs[1],
				appTime:4000,
				rpps:2000,
				positionY:400,
				distance:400
			},
			{
				obj:masterObjs[1],
				appTime:4000,
				rpps:2000,
				positionY:400,
				distance:200
			},
			{
				obj:masterObjs[1],
				appTime:4000,
				rpps:2000,
				positionY:400
			},
			{
				obj:masterObjs[1],
				appTime:4000,
				rpps:2000,
				positionY:400,
				distance:-200
			},
			{
				obj:masterObjs[1],
				appTime:4000,
				rpps:2000,
				positionY:400,
				distance:-400
			},
			{
				obj:masterObjs[1],
				appTime:4000,
				rpps:2000,
				positionY:400,
				distance:-600
			},
			{
				obj:masterObjs[1],
				appTime:4000,
				rpps:2000,
				positionY:400,
				distance:-800
			},
			{
				obj:masterObjs[1],
				appTime:4000,
				rpps:2000,
				positionY:400,
				distance:-1000
			},
			
			//ひし形下段
			{
				obj:masterObjs[1],
				appTime:4000,
				rpps:2000,
				distance:1000
			},
			{
				obj:masterObjs[1],
				appTime:4000,
				rpps:2000,
				distance:800
			},
			{
				obj:masterObjs[1],
				appTime:4000,
				rpps:2000,
				distance:600
			},
			{
				obj:masterObjs[1],
				appTime:4000,
				rpps:2000,
				distance:400
			},
			{
				obj:masterObjs[1],
				appTime:4000,
				rpps:2000,
				distance:200
			},
			{
				obj:masterObjs[1],
				appTime:4000,
				rpps:2000,
			},
			{
				obj:masterObjs[1],
				appTime:4000,
				rpps:2000,
				distance:-200
			},
			{
				obj:masterObjs[1],
				appTime:4000,
				rpps:2000,
				distance:-400
			},
			{
				obj:masterObjs[1],
				appTime:4000,
				rpps:2000,
				distance:-600
			},
			{
				obj:masterObjs[1],
				appTime:4000,
				rpps:2000,
				distance:-800
			},
			{
				obj:masterObjs[1],
				appTime:4000,
				rpps:2000,
				distance:-1000
			},
			
			//
			{
				obj:masterObjs[8],
				appTime:8000,
				rpps:4000
			},
			
		];
		
		//交差する立方体
		var maxHeight = 500;
		for (i in 0...20) {
		 objs.push( {obj:masterObjs[2],appTime:48000 + i * 500,rpps: 1000,distance:1000,degree: (90 - i * 10 + 360) % 360, positionY:maxHeight * i / 20});
		}
		for (i in 0...20) {
		 objs.push( {obj:masterObjs[2],appTime:48000 + i * 500,rpps: 1000,distance: 1000,degree: (270 + i * 10)%360, positionY:maxHeight * i / 20});
		}
		for (i in 0...20) {
		 objs.push( {obj:masterObjs[2],appTime:48000 + i * 500,rpps: 1000,distance:1000,degree: (90 - i * 10 + 360) % 360, positionY:maxHeight - maxHeight * i / 20});
		}
		for (i in 0...20) {
		 objs.push( {obj:masterObjs[2],appTime:48000 + i * 500,rpps: 1000,distance: 1000,degree: (270 + i * 10)%360, positionY:maxHeight - maxHeight * i / 20});
		}
		
		//大量のコイン
		for (i in 0...18) {
			objs.push( {obj:masterObjs[4],appTime:10000 + i * 500,rpps: 6000,distance:0});
		}
		for (i in 0...18) {
			objs.push( {obj:masterObjs[4],appTime:10000 + i * 500,rpps: 6000,distance:-200});
		}
		
		for (i in 0...18) {
			objs.push( {obj:masterObjs[4],appTime:10000 + i * 500,rpps: 6000,distance:-400});
		}
		for (i in 0...18) {
			objs.push( {obj:masterObjs[4],appTime:10000 + i * 500,rpps: 6000,distance:-600});
		}
		for (i in 0...18) {
			objs.push( {obj:masterObjs[4],appTime:10000 + i * 500,rpps: 6000,distance:-800});
		}
		for (i in 0...18) {
			objs.push( {obj:masterObjs[4],appTime:10000 + i * 500,rpps: 6000,distance:-1000});
		}
		for (i in 0...18) {
		 objs.push( {obj:masterObjs[4],appTime:10000 + i * 500,rpps:6000,distance:200});
		}
		for (i in 0...18) {
		 objs.push( {obj:masterObjs[4],appTime:10000 + i * 500,rpps:6000,distance:400});
		}
		for (i in 0...18) {
		 objs.push( {obj:masterObjs[4],appTime:10000 + i * 500,rpps:6000,distance:600});
		}
		for (i in 0...18) {
		 objs.push( {obj:masterObjs[4],appTime:10000 + i * 500,rpps:6000,distance:800});
		}
		for (i in 0...18) {
		 objs.push( {obj:masterObjs[4],appTime:10000 + i * 500,rpps:6000,distance:1000});
		}
		
		//跳ねる
		for (i in 0...15) {
			objs.push( {obj:masterObjs[3],appTime:23000 + i * 600,rpps:3000});
		}
		
		for (i in 0...15) {
			objs.push( {obj:masterObjs[3],appTime:23000 + i * 600,rpps:3000,distance: 600});
		}
		
		for (i in 0...15) {
			objs.push( {obj:masterObjs[3],appTime:23000 + i * 600,rpps:3000,distance: -600});
		}
		
		//横に動く
		for (i in 0...15) {
				objs.push( { obj:masterObjs[7], appTime:33000 + i * 600, rpps:3000, distance: 600, degree: 270 } );
				objs.push( { obj:masterObjs[7], appTime:33000 + i * 600, rpps:3000, distance: 600, degree: 370 } );
		}

		setTimeObject(objs);
		setSkyTexture( GameMgr.DIRECTORY + "images/fulllg_", ["px", "nx", "py", "ny", "pz", "nz"]);
		
		stageId = 1;
		return true;
	}
	override public function init(args:Array<Dynamic>) {
		super.init(args);
	}
	override public function update(time:Float) {
		if (first) time = Math.min(time, 1000);
		super.update(time);
	}
}