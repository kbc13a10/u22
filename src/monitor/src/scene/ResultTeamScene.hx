package scene;

import event.InputEvent;
import js.Browser;
import js.html.CanvasRenderingContext2D;
import obj2d.TeamResult;
import obj2d.TargetRect;
import obj3d.DefaultGround;
import obj3d.Lignt;
import Player;
import scene.abs.AbstractFixedScene;
import scene.abs.AbstractResultScene;
import three.MeshPhongMaterial;
import three.PlaneBufferGeometry;

class ResultTeamScene extends AbstractResultScene {
	private var bluescore:Float = 0;
	private var redscore:Float = 0;
	private var textSize = 17;
	
	public function new(context:CanvasRenderingContext2D) {
		super(context);
	}

	override public function init(args) {
		super.init(args);
		//// 配列を回す
		players = [];//配列の初期化
		var array = args[0];
		array = cast(array, Array<Dynamic>);
		for (i in array) {
			// プレイヤーリスト にキャストして挿入
			players.push(cast(i, Player));
		}
		 
		var redNames = new Array<String>();
		var blueNames = new Array<String>();
		
		for (player in players) {
				switch(player.getTeamColor()) {
					case red:
						redNames[redNames.length] = player.getName();
						redscore += player.getScore();
					case blue:
						blueNames[blueNames.length] = player.getName();
						bluescore += player.getScore();
				}
		}
		
		var redResult = "DRAW ";
		var blueResult = "DRAW ";
		var redType = "0";
		var blueType = "0";
		var redY:Float = GameMgr.HEIGHT;
		var blueY:Float = GameMgr.HEIGHT;
		
		if (redscore > bluescore) {
			redResult = "WIN ";
			redType = "1";
			redY =  -(GameMgr.HEIGHT / 6 + 20);
			
			blueResult = "LOSE ";
			blueType = "0";
			blueY = GameMgr.HEIGHT;
		} else if (bluescore > redscore) {
			redResult = "LOSE ";
			redType = "0";
			redY = GameMgr.HEIGHT;
			
			blueResult = "WIN ";
			blueType = "1";
			blueY = -(GameMgr.HEIGHT / 6 + 20);
		}
		
		obj2d.add(new TeamResult(redResult, GameMgr.WIDTH / 6, redY,'#F78181', redscore, redNames, redType));
		obj2d.add(new TeamResult(blueResult, GameMgr.WIDTH / 2, blueY, '#81DAF5', bluescore, blueNames, blueType));
		redscore = 0;
		bluescore = 0;
		
		titleText = new TargetRect(GameMgr.WIDTH * 0.7, GameMgr.HEIGHT / 5, texts[0], GameMgr.WIDTH / 30, GameMgr.WIDTH / 30 * 7);
		obj2d.add(titleText);
		
		againText = new TargetRect(GameMgr.WIDTH * 0.4, GameMgr.HEIGHT / 5, texts[1], GameMgr.WIDTH / 30, GameMgr.WIDTH / 30 * 7);
		obj2d.add(againText);
		
		for (p in GameMgr.players) {
			obj2d.add(p.getObj());
		}
	}
}