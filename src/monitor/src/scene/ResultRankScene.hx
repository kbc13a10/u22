package scene;

import obj2d.RankResult;
import scene.abs.AbstractFixedScene;
import obj2d.TargetRect;
import obj3d.Lignt;
import obj3d.DefaultGround;
import scene.abs.AbstractResultScene;

class ResultRankScene extends AbstractResultScene {
	private var rank:String;
	private var isGetRank:Bool = false;
	private var showRank:Bool = true;
	public function new(context) {
		super(context);
	}
	public function setRank(rank:String) {
		this.rank = rank;
	}
	override public function init(args) {
		super.init(args);
		if (Type.enumEq(beforeScene, EScene.RandomGameScene)) {
			showRank = false;
		} else {
			showRank = true;
		}
		var player = cast(args[0][0], Player);
		players = [player];
		
		titleText = new TargetRect(GameMgr.WIDTH * 0.75, GameMgr.HEIGHT / 5, texts[0], GameMgr.WIDTH / 30, GameMgr.WIDTH / 30 * 7);
		obj2d.add(titleText);
		
		againText = new TargetRect(GameMgr.WIDTH * 0.75, GameMgr.HEIGHT / 5 * 2, texts[1], GameMgr.WIDTH / 30, GameMgr.WIDTH / 30 * 7);
		obj2d.add(againText);
		
		isGetRank = false;
		if (this.rank != null && showRank) {
			var rankArray = this.rank.split(',');
			if (rankArray.length > 0) {
				var myRank = new RankResult(
					GameMgr.WIDTH / 6, 
					GameMgr.HEIGHT - GameMgr.HEIGHT / 6 - GameMgr.HEIGHT / 17, 
					//GameMgr.HEIGHT / 2,
					rankArray[0] + ' 位 ' + player.getScore(),
					player.getName(),
					GameMgr.WIDTH / 3 * 2,
					true
				);
				obj2d.add(myRank);
				var r = 1;
				var oldScore = 0;
				for (i in 1...rankArray.length) {
					var dataArray = rankArray[i].split(':');
					var name = dataArray[0];
					var score = Std.parseInt(dataArray[1]);
					if (i == 1) {
						oldScore = score;
					} else if(score < oldScore) {
						r = i;
						oldScore = score;
					}
					var dbRank = new RankResult(
						GameMgr.WIDTH / 9, 
						GameMgr.HEIGHT / 10 + (i - 1) * (GameMgr.HEIGHT / 10 + 10), 
						r + ' 位 ' + score,
						name
					);
					obj2d.add(dbRank);
				}
				isGetRank = true;
			}
		} else {
			var myRank = new RankResult(
				GameMgr.WIDTH / 6, 
				GameMgr.HEIGHT - GameMgr.HEIGHT / 6 - GameMgr.HEIGHT / 17,
				!showRank ? 'スコア ' + player.getScore() : '- 位 ' + player.getScore(),
				player.getName(),
				GameMgr.WIDTH / 3 * 2,
				true
			);
			obj2d.add(myRank);
		}
		this.rank = null;
		obj2d.add(player.getObj());
	}
	override public function update(time:Float) {
		if (!isGetRank && showRank) {
			context.beginPath();
			context.textAlign = 'left';
			context.fillStyle = '#000000';
			context.font = GameMgr.HEIGHT / 17 + 'px bold "Times New Roman"';
			context.fillText('ランキングデータの取得に失敗しました。', GameMgr.WIDTH / 6, GameMgr.HEIGHT / 6);
			context.closePath();
		}
		super.update(time);
	}
}