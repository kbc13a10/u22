package scene;

import event.InputEvent;
import js.Browser;
import js.html.CanvasRenderingContext2D;
import obj2d.PlayerResult;
import obj2d.TargetRect;
import obj3d.Lignt;
import Player;
import scene.abs.AbstractFixedScene;
import obj3d.DefaultGround;
import scene.abs.AbstractResultScene;


class ResultSingleScene extends AbstractResultScene {
	public function new(context:CanvasRenderingContext2D) {
		super(context);
	}

	override public function init(args:Array<Dynamic>) {
		super.init(args);
		// 配列を回す
		players = [];//配列の初期化
		var array = args[0];
		array = cast(array, Array<Dynamic>);
		for (i in array) {
			// プレイヤーリスト にキャストして挿入
			players.push(cast(i, Player));
		}
		
		var isDrow = true;
		//順位を決める
		for (h in 0...players.length - 1) {
			for (l in (h + 1)...players.length) {
				var high = players[h].getScore();
				var low = players[l].getScore();
				if (high != low) isDrow = false;
				if ((high < low && !players[l].isRetire()) || players[h].isRetire()) {
					var tm = players[l];
					players[l] = players[h];
					players[h] = tm;
				}	
			}
		}
		endTime = 500;
		elapsedTime = 0;
		
		var num = 1;
		var rank = 1;
		var wait = 300;
		var prevScore = players[0].getScore();
		var maxNum = players.length;
		for (p in players) {
			var text = 'リタイア';
			var score = p.getScore();
			if (!p.isRetire()) {
				if (score < prevScore) {
					rank = num;
					prevScore = score;
				}
				text = !isDrow ? rank + '位' : 'drow';
			}
			endTime += wait;
			obj2d.add(new PlayerResult(text, num, maxNum, p.getNumber(), score, p.getName(), wait * (maxNum - num)));
			num++;
		}
		
		titleText = new TargetRect(GameMgr.WIDTH * 0.7, GameMgr.HEIGHT / 5, texts[0], GameMgr.WIDTH / 27, GameMgr.WIDTH / 27 * 7);
		titleText.hideText();
		obj2d.add(titleText);
		
		againText = new TargetRect(GameMgr.WIDTH * 0.7, GameMgr.HEIGHT / 5 * 2, texts[1], GameMgr.WIDTH / 27, GameMgr.WIDTH / 27 * 7);
		againText.hideText();
		obj2d.add(againText);
		
		for (p in GameMgr.players) {
			obj2d.add(p.getObj());
		}
	}
	
	private var endTime:Float;
	private var elapsedTime:Float;
	override public function update(timestamp) {
		if (!titleText.isShow() && elapsedTime >= endTime) {
			titleText.showText();
			if(GameMgr.entryNum == players.length) {
				againText.showText();
			}
		}
		super.update(timestamp);
		elapsedTime += timestamp;
	}
	
}