package scene;
import objmgr.RandomObjMgr;
import scene.abs.AbstractMoveRouteScene;
import field.FieldObject.FieldObjectParameter;
import position.RoutePosition.RouteParameter;
import three.Vector3;

/**
 * ゲームシーンをランダムに作成するシーン
 * ルートを6分割し、ルート毎にオブジェクトを配置する
 */
class RandomGameScene extends AbstractMoveRouteScene {
	private var randomMgr:RandomObjMgr;
	private var splitTime = 10000; // ルートごとの時間
	private var splitDistance = 5000; // ルートごとの距離
	private var masterRoutes:Array<{x:Int, y:Int}> = [];
	public function new(context) {
		super(context, 1000);
	}
	override public function once() {
		if (!super.once()) return false;
		
		randomMgr = new RandomObjMgr(visualRange);
		
		stageId = 5;
		setSkyTexture( GameMgr.DIRECTORY + "images/fulllg_", ["px", "nx", "py", "ny", "pz", "nz"]);
		return true;
	}
	override public function init(args:Array<Dynamic>) {
		if (args != null &&  args.length > 0) {
			randomMgr.again();
		} else {
			randomMgr.init();
			masterRoutes = [ {x:0, y:0}, {x:0, y:1}];
		}
		
		
		var ram = Std.int(Math.random() * 3);
		switch(ram) {
			case 0:
				audioName = 'hard';
			case 1:
				audioName = 'normal';
			case 2:
				audioName = 'easy';
		}
		
		var routes:Array<RouteParameter> = [
			{ time: splitTime, point: new Vector3(0, 0, splitDistance) }
		];

		var x = 0;
		var y = 1;
		var old = 2;
		var branch = [0, 1, 2, 3]; // 上:0, 左:1, 下:2, 右:3
		while (masterRoutes.length < 7) {
			var canBranch = [];
			for (i in branch) {
				if (i != old) {
					canBranch.push(i);
				}
			}
			var flag = false;
			while (!flag) {
				var tmpX = x;
				var tmpY = y;
				var ram = Std.int(Math.random() * canBranch.length);
				var b = canBranch[ram];
				switch(b) {
					case 0:
						tmpY++;
					case 1:
						tmpX++;
					case 2:
						tmpY--;
					case 3:
						tmpX--;
				}
				flag = true;
				var next:{x:Int, y:Int} = {x:tmpX, y:tmpY}
				for (m in masterRoutes) {
					if (m.x == next.x && m.y == next.y) {
						flag = false;
						break;
					}
				}
				if (flag) {
					x = tmpX;
					y = tmpY;
					masterRoutes.push({x:x, y:y});
					old = (b + 2) % 4;
				} else {
					var tmpArray = [];
					for (i in canBranch) {
						if (i != b) tmpArray.push(i);
					}
					canBranch = tmpArray;
				}
			}
		}
		for (i in 2...masterRoutes.length) {
			routes.push({ time: splitTime, point: new Vector3(masterRoutes[i].x * splitDistance, 0, masterRoutes[i].y * splitDistance) });
		}
		setRouteData(routes);
		
		var objs:Array<FieldObjectParameter> = [];
		
		for (i in 0...masterRoutes.length - 1) {
			var radian = Math.atan2(masterRoutes[i + 1].y - masterRoutes[i].y,
						masterRoutes[i + 1].x - masterRoutes[i].x);
			for (j in 0...3) {
				var random = randomMgr.getRandomObjs(i * splitTime + 500 + j * 3000, radian);
				for (r in random) {
					objs.push(r);
				}
			}
		}
		
		setTimeObject(objs);
		
		super.init(args);
		setDebugTime(0);
	}
	override public function update(time:Float) {
		if (first) time = Math.min(time, 1000);
		super.update(time);
	}
}