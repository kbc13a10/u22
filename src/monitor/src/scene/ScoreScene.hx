package scene;

import action.MoveXSideAction;
import action.MoveZSideAction;
import action.RotateAction;
import action.RotateRandomAction;
import action.JumpAction;
import obj3d.BreakObject;
import obj3d.InvincibleObject;
import obj3d.MeshEx;
import scene.abs.AbstractTurnScene;
import shape.CrystalShape;
import shape.CubeShape;
import shape.CylinderShape;
import shape.DrumShape;
import shape.FloorShape;
import event.InputEvent;
import three.Mesh;
import three.Raycaster;
import three.Vector3;
import scene.abs.AbstractMoveRouteScene;
import position.RoutePosition.RouteParameter;
import field.FieldObject;


class TestScene extends AbstractMoveRouteScene {	
	
	private var player1 = GameMgr.players.get('0');
	private	var player2 = GameMgr.players.get('1');
	private	var player3 = GameMgr.players.get('2');
	private	var player4 = GameMgr.players.get('3');
	
	//var controls:three.OrbitControls;
	public function new(context) {
		super(context, 1000);
		
		for (player in GameMgr.players) {
			switch(player.getNumber()){
				case 0:
					player1 = player;
				case 1:
					player2 = player;
				case 2:
					player3 = player;
				case 3:
					player4 = player;
			}
		}
		setFloorTexture(GameMgr.DIRECTORY + "images/grasslight-big.jpg", 16);
		
		var routes:Array<RouteParameter> = [
			{ time: 5000, point: new Vector3(0, 0, 2000) },
			{ time: 10000, point: new Vector3(6000, 0, 2000) },
			{ time: 10000, point: new Vector3(6000, 0, 8000) },
			{ time: 10000, point: new Vector3(0, 0, 8000) },
			{ time: 10000, point: new Vector3(-2000, 0, 14000) },
			{ time: 10000, point: new Vector3(3000, 0, 14000) },
			{ time: 5000, point: new Vector3( 6000, 0, 14000) },
		];
		setRouteData(routes);
		
		var roteRightAction = new RotateAction({right:true});
		var jumpAction = new JumpAction({waitTime: 0});
		var randamRoteAction = new RotateRandomAction();
		var moveXAction = new MoveXSideAction();
		var moveXRightAction = new MoveXSideAction({right: true});
		var moveZAction = new MoveZSideAction();
		
		var crystalColorShape = new CrystalShape( { color: 0xff5555 } );
		var crystalBlackShape = new CrystalShape( { color:0x000000 } );
		var cylinderShape = new CylinderShape();
		var cubeShape = new CubeShape();
		var drumShape = new DrumShape();
		
		var masterObjs = [
			new BreakObject(crystalColorShape.clone(), [moveXAction.clone()]),
			new BreakObject(crystalBlackShape.clone(), [roteRightAction.clone()]),
			new BreakObject(cubeShape.clone(), [randamRoteAction.clone()]),
			new BreakObject(crystalColorShape.clone(), [jumpAction.clone()]),
			new BreakObject(cylinderShape.clone(), [roteRightAction.clone()]),
			new BreakObject(cylinderShape.clone(), [jumpAction.clone()]),
			new InvincibleObject(drumShape.clone()),
			new BreakObject(crystalColorShape.clone(), [moveXRightAction.clone()]),
		];
		
		var objs:Array<FieldObjectParameter> = [
			
			//ドラム缶
			{ obj:masterObjs[6].clone(), appTime:3000, rpps:4500, distance:-300 },
			{obj:masterObjs[6].clone(),appTime:3000,rpps:4500,distance:300},
			{obj:masterObjs[6].clone(),appTime:4000,rpps:4500,distance:-1000},
			{obj:masterObjs[6].clone(),appTime:4000,rpps:4500,distance:1000},
			{obj:masterObjs[6].clone(), appTime:4000, rpps:5500, distance:1000 },
			{obj:masterObjs[6].clone(), appTime:4000, rpps:5500, distance: -1000 },
			{obj:masterObjs[6].clone(), appTime:4000, rpps:6000, distance: 500 },
			{obj:masterObjs[6].clone(), appTime:4000, rpps:6000, distance: -500 },
			
			//ジャンプコイン
			{obj:masterObjs[5].clone(),appTime:3000,rpps:4600,distance:300,positionY:-100},
			{obj:masterObjs[5].clone(),appTime:3000,rpps:4600,distance: -300,positionY:-100},
			{obj:masterObjs[5].clone(),appTime:4000,rpps:4600,distance:1000,positionY:-100},
			{obj:masterObjs[5].clone(), appTime:4000, rpps:4600, distance: -1000, positionY: -100 },
			{obj:masterObjs[5].clone(),appTime:4000,rpps:5600,distance:1000,positionY:-100},
			{obj:masterObjs[5].clone(), appTime:4000, rpps:5600, distance: -1000, positionY: -100 },
			{obj:masterObjs[5].clone(), appTime:3000, rpps:7100, distance: 500, positionY: -100 },
			{obj:masterObjs[5].clone(), appTime:3000, rpps:7100, distance: -500, positionY: -100 },
			
			//ひし形
			{obj:masterObjs[1].clone(),appTime:0,rpps:3000,degree:1000},
			{obj:masterObjs[1].clone(),appTime:0,rpps:3000,distance:300,degree:1000},
			{obj:masterObjs[1].clone(),appTime:0,rpps:3000,distance:300,degree: -1000},
			
			//四角
			{obj:masterObjs[2].clone(),appTime:6000,rpps:3000,positionY:230},
			{obj:masterObjs[2].clone(),appTime:6000,rpps:3000,positionY:400,distance:200},
			{obj:masterObjs[2].clone(),appTime:6000,rpps:3000,distance: -200,positionY:50},
			{obj:masterObjs[2].clone(),appTime:6000,rpps:3000,positionY:400,distance:-200},
			{obj:masterObjs[2].clone(),appTime:6000,rpps:3000,positionY:50,distance:200},
			
			//ひし形中段
			{
				obj:masterObjs[1].clone(),
				appTime:4000,
				rpps:2000,
				positionY:200,
				distance:1000
			},
			{
				obj:masterObjs[1].clone(),
				appTime:4000,
				rpps:2000,
				positionY:200,
				distance:800
			},
			{
				obj:masterObjs[1].clone(),
				appTime:4000,
				rpps:2000,
				positionY:200,
				distance:600
			},
			{
				obj:masterObjs[1].clone(),
				appTime:4000,
				rpps:2000,
				positionY:200,
				distance:400
			},
			{
				obj:masterObjs[1].clone(),
				appTime:4000,
				rpps:2000,
				positionY:200,
				distance:200
			},
			{
				obj:masterObjs[1].clone(),
				appTime:4000,
				rpps:2000,
				positionY:200
			},
			{
				obj:masterObjs[1].clone(),
				appTime:4000,
				rpps:2000,
				positionY:200,
				distance:-200
			},
			{
				obj:masterObjs[1].clone(),
				appTime:4000,
				rpps:2000,
				positionY:200,
				distance:-400
			},
			{
				obj:masterObjs[1].clone(),
				appTime:4000,
				rpps:2000,
				positionY:200,
				distance:-600
			},
			{
				obj:masterObjs[1].clone(),
				appTime:4000,
				rpps:2000,
				positionY:200,
				distance:-800
			},
			{
				obj:masterObjs[1].clone(),
				appTime:4000,
				rpps:2000,
				positionY:200,
				distance:-1000
			},
			
			//ひし形上段
			{
				obj:masterObjs[1].clone(),
				appTime:4000,
				rpps:2000,
				positionY:400,
				distance:1000
			},
			{
				obj:masterObjs[1].clone(),
				appTime:4000,
				rpps:2000,
				positionY:400,
				distance:800
			},
			{
				obj:masterObjs[1].clone(),
				appTime:4000,
				rpps:2000,
				positionY:400,
				distance:600
			},
			{
				obj:masterObjs[1].clone(),
				appTime:4000,
				rpps:2000,
				positionY:400,
				distance:400
			},
			{
				obj:masterObjs[1].clone(),
				appTime:4000,
				rpps:2000,
				positionY:400,
				distance:200
			},
			{
				obj:masterObjs[1].clone(),
				appTime:4000,
				rpps:2000,
				positionY:400
			},
			{
				obj:masterObjs[1].clone(),
				appTime:4000,
				rpps:2000,
				positionY:400,
				distance:-200
			},
			{
				obj:masterObjs[1].clone(),
				appTime:4000,
				rpps:2000,
				positionY:400,
				distance:-400
			},
			{
				obj:masterObjs[1].clone(),
				appTime:4000,
				rpps:2000,
				positionY:400,
				distance:-600
			},
			{
				obj:masterObjs[1].clone(),
				appTime:4000,
				rpps:2000,
				positionY:400,
				distance:-800
			},
			{
				obj:masterObjs[1].clone(),
				appTime:4000,
				rpps:2000,
				positionY:400,
				distance:-1000
			},
			
			//ひし形下段
			{
				obj:masterObjs[1].clone(),
				appTime:4000,
				rpps:2000,
				distance:1000
			},
			{
				obj:masterObjs[1].clone(),
				appTime:4000,
				rpps:2000,
				distance:800
			},
			{
				obj:masterObjs[1].clone(),
				appTime:4000,
				rpps:2000,
				distance:600
			},
			{
				obj:masterObjs[1].clone(),
				appTime:4000,
				rpps:2000,
				distance:400
			},
			{
				obj:masterObjs[1].clone(),
				appTime:4000,
				rpps:2000,
				distance:200
			},
			{
				obj:masterObjs[1].clone(),
				appTime:4000,
				rpps:2000,
			},
			{
				obj:masterObjs[1].clone(),
				appTime:4000,
				rpps:2000,
				distance:-200
			},
			{
				obj:masterObjs[1].clone(),
				appTime:4000,
				rpps:2000,
				distance:-400
			},
			{
				obj:masterObjs[1].clone(),
				appTime:4000,
				rpps:2000,
				distance:-600
			},
			{
				obj:masterObjs[1].clone(),
				appTime:4000,
				rpps:2000,
				distance:-800
			},
			{
				obj:masterObjs[1].clone(),
				appTime:4000,
				rpps:2000,
				distance:-1000
			},
			
			//
			{
				obj:masterObjs[3].clone(),
				appTime:8000,
				rpps:4000
			},
			
			
			
		];
		
		//交差する立方体
		var maxHeight = 500;
		for (i in 0...20) {
		 objs.push( {obj:masterObjs[2].clone(),appTime:48000 + i * 500,rpps: 1000,distance:1000,degree: (90 - i * 10 + 360) % 360, positionY:maxHeight * i / 20});
		}
		for (i in 0...20) {
		 objs.push( {obj:masterObjs[2].clone(),appTime:48000 + i * 500,rpps: 1000,distance: 1000,degree: (270 + i * 10)%360, positionY:maxHeight * i / 20});
		}
		for (i in 0...20) {
		 objs.push( {obj:masterObjs[2].clone(),appTime:48000 + i * 500,rpps: 1000,distance:1000,degree: (90 - i * 10 + 360) % 360, positionY:maxHeight - maxHeight * i / 20});
		}
		for (i in 0...20) {
		 objs.push( {obj:masterObjs[2].clone(),appTime:48000 + i * 500,rpps: 1000,distance: 1000,degree: (270 + i * 10)%360, positionY:maxHeight - maxHeight * i / 20});
		}
		
		//大量のコイン
		for (i in 0...18) {
			objs.push( {obj:masterObjs[4].clone(),appTime:10000 + i * 500,rpps: 6000,distance:0});
		}
		for (i in 0...18) {
			objs.push( {obj:masterObjs[4].clone(),appTime:10000 + i * 500,rpps: 6000,distance:-200});
		}
		
		for (i in 0...18) {
			objs.push( {obj:masterObjs[4].clone(),appTime:10000 + i * 500,rpps: 6000,distance:-400});
		}
		for (i in 0...18) {
			objs.push( {obj:masterObjs[4].clone(),appTime:10000 + i * 500,rpps: 6000,distance:-600});
		}
		for (i in 0...18) {
			objs.push( {obj:masterObjs[4].clone(),appTime:10000 + i * 500,rpps: 6000,distance:-800});
		}
		for (i in 0...18) {
			objs.push( {obj:masterObjs[4].clone(),appTime:10000 + i * 500,rpps: 6000,distance:-1000});
		}
		for (i in 0...18) {
		 objs.push( {obj:masterObjs[4].clone(),appTime:10000 + i * 500,rpps:6000,distance:200});
		}
		for (i in 0...18) {
		 objs.push( {obj:masterObjs[4].clone(),appTime:10000 + i * 500,rpps:6000,distance:400});
		}
		for (i in 0...18) {
		 objs.push( {obj:masterObjs[4].clone(),appTime:10000 + i * 500,rpps:6000,distance:600});
		}
		for (i in 0...18) {
		 objs.push( {obj:masterObjs[4].clone(),appTime:10000 + i * 500,rpps:6000,distance:800});
		}
		for (i in 0...18) {
		 objs.push( {obj:masterObjs[4].clone(),appTime:10000 + i * 500,rpps:6000,distance:1000});
		}
		
		//跳ねる
		for (i in 0...15) {
			objs.push( {obj:masterObjs[3].clone(),appTime:23000 + i * 600,rpps:3000});
		}
		
		for (i in 0...15) {
			objs.push( {obj:masterObjs[3].clone(),appTime:23000 + i * 600,rpps:3000,distance: 600});
		}
		
		for (i in 0...15) {
			objs.push( {obj:masterObjs[3].clone(),appTime:23000 + i * 600,rpps:3000,distance: -600});
		}
		
		//横に動く
		for (i in 0...15) {
				objs.push( { obj:masterObjs[7].clone(), appTime:33000 + i * 600, rpps:3000, distance: 600, degree: 270 } );
				objs.push( { obj:masterObjs[7].clone(), appTime:33000 + i * 600, rpps:3000, distance: 600, degree: 370 } );
		}
		
		
		
		
		
		setTimeObject(objs);
		
		//GameMgr.input.addEventListener(EScene.TestScene, function(ev:InputEvent) {
			//var mouseX = (ev.x / GameMgr.WIDTH) * 2 - 1;
			//var mouseY = -(ev.y / GameMgr.HEIGHT) * 2 + 1;
			//
			//var pos = new Vector3(mouseX, mouseY, 1);
			//pos.unproject(camera);
			//
			//// 始点、向きベクトルを渡してレイを作成
			//var ray = new Raycaster(camera.position, pos.sub(camera.position).normalize());
			//var target = [];
			//for ( i in obj3d) {
				//target.push(i);
			//}
			//var objs = ray.intersectObjects(target);
			//if (objs.length > 0) {
				//var obj = cast(objs[0].object, MeshEx);
				//trace(obj.hit());
				//if (obj.hit() == true) {
					//obj3d.remove(obj);
					//scene.remove(obj);
				//}
				//
			//}
		//});
		
	}
	override public function playerShot(player:Player) {
		//trace('x:' + player.getX() + ' y:' + player.getY());
		var x = player.getX();
		var y = player.getY();
		
		var pos = new Vector3((x / GameMgr.WIDTH) * 2 - 1, -(y / GameMgr.HEIGHT) * 2 + 1, 1);
		pos.unproject(camera);
		//trace('player shot');
		// 始点、向きベクトルを渡してレイを作成
		var ray = new Raycaster(camera.position, pos.sub(camera.position).normalize());
		var target = [];
		for ( i in obj3d) {
			target.push(i);
		}
		var objs = ray.intersectObjects(target);
		if (objs.length > 0) {
			var obj = cast(objs[0].object, MeshEx);
			trace(obj.hit());
			if (obj.hit() == true) {
				obj3d.remove(obj);
				scene.remove(obj);
			}
		}
	}
	
	override public function init(args:Array<Dynamic>) {
		super.init(args);
		setDebugTime(0);
		
		//for (i in GameMgr.players) {
			//obj2d.add(i.getObj());
		//}
		// 床の作成
		//var floorShape = new FloorShape('images/checkerboard.jpg');
		// 壊れないオブジェクトとして作成
		//var floor:MeshEx = new InvincibleObject(floorShape);
		// 画面に追加
		//obj3d.add(floor);
		//scene.add(floor);
		

		/*
		// 回転アクション
		var rotateAction = new RotateAction();
		// 壊れないオブジェクトとしてカラー0xff5555のクリスタル作成　回転アクションつき
		var crystal = new BreakObject(new CrystalShape({color: 0xff5555}), [rotateAction]);
		// 画面に追加
		obj3d.add(crystal);
		scene.add(crystal);
		
		
		//var cs1 = new CrystalShape(); // クリスタルの形
		var ud = new JumpAction(); // ジャンプアクション
		var rt = new RotateAction({right: false}); // 回転アクション(逆回転)
		var crystal1 = new BreakObject(new CrystalShape(), [ud, rt]); // 壊れるオブジェクト作成
		crystal1.position.set(0, 0, 400); // クリスタル位置をx:0, y:0, z:400に設置
		obj3d.add(crystal1); // 画面に追加
		scene.add(crystal1); // 画面に追加
		
		
		var cube = new BreakObject( // 壊れるオブジェクト
			  new CubeShape(),  // 立方体
			  // x方向に移動アクション ランダムに回転
			  [new MoveXSideAction(), new RotateRandomAction()]
		);
		cube.position.z += 1000; // z座標1000
		cube.position.y += 200; // y座標200　高さ
		obj3d.add(cube); // 追加
		scene.add(cube); // 追加
		
		
		var cube2 = new BreakObject(
			  new CubeShape(), // 立方体
			  // z方向に移動アクション ランダムに回転
			  [new MoveZSideAction(), new RotateRandomAction()]
		);
		cube2.position.x -= 1000; // x座標に-1000
		cube2.position.y += 200; // y座標に200
		obj3d.add(cube2); // 追加
		scene.add(cube2); // 追加
		
		//　初期位置高さ600 待ち時間0 ジャンプ方向下
		var jp = new JumpAction({wait_time:0, up:false});
		var moveX = new MoveXSideAction();// x方向移動
		var moveZ = new MoveZSideAction();// z方向移動
		var randamRote = new RotateRandomAction(270);// ランダム回転 最大1秒で270度
		// 壊れるオブジェクトとして黒色のクリスタルに作成
		var violent = new BreakObject(new CrystalShape({color:0x000000}), [jp, moveX, moveZ, randamRote]);
		violent.position.z = 1500; // 初期位置z
		violent.position.x = 1500; // 初期位置x
		violent.position.y = 600; // 初期位置y
		obj3d.add(violent); // 追加
		scene.add(violent);
		
		// 上の位置が違うだけ
		var jp2 = new JumpAction({wait_time:0, up:false});
		var moveX2 = new MoveXSideAction();
		var moveZ2 = new MoveZSideAction();
		var randamRote2 = new RotateRandomAction(360);
		var violent2 = new BreakObject(new CrystalShape({color:0x000000}), [jp2, moveX2, moveZ2, randamRote2]);
		violent2.position.z = -1500;
		violent2.position.x = -1500;
		violent2.position.y = 600;
		obj3d.add(violent2);
		scene.add(violent2);
		
		
		var max = 4000; // 配置位置の最大距離
		var center = 200; // 中央の空間 (カメラの範囲外の為
		for ( i in 0...200 ) {
			var type = i % 5;
			// 適当に位置を決める
			var z = Math.random() * max * 2 - max;
			if ( z < center && z > -center ) {
				z += Math.random() * center * 2 - center;
			}
			var x = Math.random() * max * 2 - max;
			if ( x < center && x > -center ) {
				x += Math.random() * center * 2 - center;
			}
			var crystalShape;
			// 回転アクション 以下クリスタル全てに付与
			var rta = new RotateAction();
			var crystal;
			if ( type == 0 ) {
				var s = Std.int(Math.random() * 50 + 50);
				// サイズが50 ~ 100　色がff8800のクリスタル 黄色
				crystalShape = new CrystalShape({size:s, color: 0xff8800});
				var wait:Int = Std.int(Math.random() * 3 * 1000);
				var iv = Math.random() * 10 + 5;
				// 待ち時間が3秒以内、初速5 ~ 15 のジャンプアクション作成
				var upd = new JumpAction( { wait_time : wait, initialVelocity : iv } );
				// 壊れるオブジェクトとして作成
				crystal = new BreakObject(crystalShape, [rta, upd]);
			} else if ( type == 3 ) {
				// 色が0x0088ffのクリスタル作成 青色
				crystalShape = new CrystalShape( { color: 0x0088ff } );
				// x座標に移動アクション
				var mx = new MoveXSideAction();
				crystal = new BreakObject(crystalShape, [rta, mx]);
			} else if ( type == 2 ) {
				var s = Std.int(Math.random() * 100 + 50);
				var ra = Math.random() + 1.5;
				// サイズが50 ~ 150 高さ比が1.5 ~ 2.5 のクリスタル作成
				crystalShape = new CrystalShape( { size: s, ratio: ra } );
				// z方向に移動のアクション
				var mz = new MoveZSideAction();
				// 壊れるオブジェクトとして作成
				crystal = new BreakObject(crystalShape, [rta, mz]);
			} else if ( type == 1 ) {
				var c = Std.int(Math.random() * 0xffffff );
				// ランダムカラーのクリスタル作成
				crystalShape = new CrystalShape( { color: c } );
				// 壊れるオブジェクトとして作成
				crystal = new BreakObject(crystalShape, [rta]);
			} else {
				// ノーマルクリスタル
				crystalShape = new CrystalShape();
				// 壊れるオブジェクト
				crystal = new BreakObject(crystalShape, [rta]);
			}
			// クリスタルをランダムな場所に設置
			crystal.position.set(x, 0, z);
			// 画面に追加
			obj3d.add(crystal);
			scene.add(crystal);
		}
		*/
		// 光だよ！よくわからないよ！
		var light;
		scene.add( new three.AmbientLight( 0x666666 ) );
		light = new three.DirectionalLight( 0xdfebff, 1.75 );
		light.position.set( -50, 250, -200 );
		light.position.multiplyScalar( 1.3 );
		light.castShadow = true;
		light.shadowCameraVisible = true;
		light.shadowMapWidth = 1024;
		light.shadowMapHeight = 1024;
		//var d = 500;
		//light.shadowCameraLeft = -d;
		//light.shadowCameraRight = d;
		//light.shadowCameraTop = d;
		//light.shadowCameraBottom = -d;
		//light.shadowCameraFar = 1000;
		//light.shadowDarkness = 0.5;
		scene.add( light );
		totalTime = 0 - waitTime;
	}
	
	var w = 5;
	//気にしない
	private var totalTime:Float;
	override public function update(time:Float) {
		super.update(time);
		totalTime += time;
		trace(Std.int(totalTime / 1000));
		
		
		
		player2.addScore(w);
		
				
		context.beginPath();
		context.font = '16px "Times New Roman"';
		context.fillStyle = '#F78181';
		context.fillRect(0, 0, GameMgr.WIDTH / 4, 40);
		context.fillStyle = '#000000';
		context.fillText(player1.getName(), 5, 15);
		context.fillText(player1.getScore() + "点", 5, 30); 
		
		context.fillStyle = '#81DAF5';
		context.fillRect(GameMgr.WIDTH / 4, 0, GameMgr.WIDTH / 4, 40);
		context.fillStyle = '#000000';
		context.fillText(player2.getName(), (GameMgr.WIDTH / 4) + 5, 15);
		context.fillText(player2.getScore() + "点", (GameMgr.WIDTH / 4) + 5, 30); 
		
		context.fillStyle = '#9FF781';
		context.fillRect((GameMgr.WIDTH / 4) * 2, 0, GameMgr.WIDTH / 4, 40);
		context.fillStyle = '#000000';
		context.fillText(player3.getName(), ((GameMgr.WIDTH / 4) * 2) + 5, 15);
		context.fillText(player3.getScore() + "点", ((GameMgr.WIDTH / 4) * 2) + 5, 30); 
		
		context.fillStyle = '#F3F781';
		context.fillRect((GameMgr.WIDTH / 4) * 3, 0, GameMgr.WIDTH / 4, 40);
		context.fillStyle = '#000000';
		context.fillText(player4.getName(), ((GameMgr.WIDTH / 4) * 3) + 5, 15);
		context.fillText(player4.getScore() + "点", ((GameMgr.WIDTH / 4) * 3) + 5, 30); 
		context.closePath();
		
		
	}
}