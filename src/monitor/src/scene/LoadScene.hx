package scene;

import js.html.MessageEvent;
import scene.abs.AbstractFixedScene;
import js.Browser;

class LoadScene extends AbstractFixedScene {
	private var maxScene:Int = 0;
	private var loadScene:Int = 0;
	public function new(context) {
		super(context);
	}
	override public function once():Bool {
		if (!super.once()) return false;
		audio.appendBGM("title", GameMgr.DIRECTORY + "audios/title4.mp3");
		audio.appendBGM("normal", GameMgr.DIRECTORY + "audios/normal.mp3");
		audio.appendBGM("easy", GameMgr.DIRECTORY + "audios/easy.mp3");
		audio.appendBGM("hard", GameMgr.DIRECTORY + "audios/hard.mp3");
		audio.appendBGM("result", GameMgr.DIRECTORY + "audios/result.mp3");
		
		audio.appendSE("entry", GameMgr.DIRECTORY + "se/entry.mp3");
		audio.appendSE("change", GameMgr.DIRECTORY + "se/change.mp3");
		audio.appendSE("ready", GameMgr.DIRECTORY + "se/ready.mp3");
		audio.appendSE("start", GameMgr.DIRECTORY + "se/start.mp3");
		audio.appendSE("break", GameMgr.DIRECTORY + "se/break.mp3");
		audio.appendSE("block", GameMgr.DIRECTORY + "se/block.mp3");
		
		return true;
	}
	override public function init(args:Array<Dynamic>) {
		super.init(args);
		maxScene = args[0];
	}
	//public function addLoad() {
		//loadScene++;
		//str = '読み込み中(' + loadScene + ' / ' + maxScene + ')';
	//}
	
	var str:String = '読み込み中';
	override public function update(time:Float) {
		context.beginPath();
		context.font = GameMgr.WIDTH / 24 + 'px "Times New Roman"';
		context.fillStyle = 'rgb(0, 0, 0)';
		context.textAlign = 'center';
		context.fillText(str, GameMgr.WIDTH / 2, GameMgr.HEIGHT / 2);
		context.closePath();
	}
}