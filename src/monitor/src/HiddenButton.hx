package;
import js.html.CanvasRenderingContext2D;

class HiddenButton {
	private var cnt = 0;
	private var widht:Float = 0;
	private var height:Float = 0;
	private var x:Float;
	private var y:Float;
	public function new(x:Float, y:Float, w:Float, h:Float) {
		this.x = x;
		this.y = y;
		this.widht = w;
		this.height = h;
	}
	public function checkHit(px:Float, py:Float):Bool {
		if(!GameMgr.isOpenRandom()) {
			if (px >= x && px <= x + widht
				&& py >= y && py <= y + height) {
				cnt++;
				if (cnt >= 10) {
					GameMgr.setOpenRandom(true);
					var audio = Audio.getInstance();
					audio.playSE('start');
					return true;
				}
			}
		}
		return false;
	}
}