package position;
import three.Vector3;
import position.Route;

typedef RouteParameter = {
	var time : Float; // ミリ秒
	var point : Vector3; // 絶対位置
	//@:optional var look : Bool; //カメラをルートに合わせるか
	@:optional var degree : Float; // カメラの向く角度
}

class RoutePosition extends AbstractPosition {
	private var routes: Array<Route>;
	private var nextRoute: Route;
	private var nextTime: Float = 0;
	private var lastTime: Float = 0;
	private var d: Float = 90; // 初期角度
	private var nowPoint: Vector3;
	private var roteTime: Float = 2000; // 180度を曲がる時間
	private var np: Int = 0; // 現在のポイント
	private var look: Bool;
	private var routeData:Array<RouteParameter>;
	private var maxTime: Float = 0;
	public function new(?wait:Float, ?rt:Float ) {
		super(wait);
		if (rt != null && rt >= 0) roteTime = rt;
		routeData = getDefaultRoute();
	}
	override public function init() {
		super.init();
		if (!setRoute()) {
			setRouteData(getDefaultRoute());
			setRoute();
		}
	}
	override public function getAfterPosition(time:Float) {
		var mp = nowPoint.clone(); // 元となる位置
		var ad = deg; // 
		var nt = nextTime;
		var lt = lastTime;
		var ap = np;
		while ( progress + time > nt && routes.length > ap + 1 ) {
			mp.add(routes[ap].getPoint());
			if(routes[ap].isLook()){
				ad = (360 + routes[ap].getDeg()) % 360;
				//ap = routes[ap].getDeg();
			}
			ap++;
			lt = nt;
			nt += routes[ap].getTime();
		}
		var dif = if (progress + time > nt) nt - lt else (progress + time) - lt;
		mp.add(routes[ap].getPosi(dif));
		var nd = routes[ap].getDeg();
		//trace(nd);
		if (routes[ap].isLook()) {
			var addD = (nd - ad + 540) % 360 - 180;
			ad = (ad + addD * (dif / routes[ap].getTime()) + 360) % 360;
			//trace(ad);
			//if ( nd - ad < ad + 360 - nd ) {
				//ad = ad + (nd - ad) * (dif / routes[ap].getTime());
			//} else {
				//ad = ad + 360 - (ad - nd + 360) * ((dif / routes[ap].getTime()));
			//}
		}

		return { point:mp, deg: ad };
	}
	override public function update(t:Float) {
		progress += t;
		while ( progress > nextTime && routes.length > np + 1 ) {
			setNext();
		}
		position = nowPoint.clone();
		deg = d;
		if (progress >= 0) { 
			var time = progress - lastTime;
			position.add(nextRoute.getPosi(time));
			var nd = (360 + nextRoute.getDeg()) % 360;
			if (look) {
				var addD = (nd - deg + 540) % 360 - 180;
				deg = (deg + addD * (time / nextRoute.getTime()) + 360) % 360;
				//if ( nd - deg < deg + 360 - nd ) {
					//deg = deg + (nd - deg) * (time / nextRoute.getTime());
				//} else {
					//deg = deg + 360 - (deg - nd + 360) * (time / nextRoute.getTime());
				//}
			}
		}
	}
	override public function setRouteData(data) {
		routeData = data;
	}
	override public function getMaxTime() {
		return Std.int(maxTime);
	}
	private function setRoute() : Bool {
		routes = [];
		np = 0;
		progress = 0;
		progress = -startWait;
		// 初期位置
		nowPoint = new Vector3(0, 0, 0);
		d = 90;
		if ( routeData.length == 0 ) {
			// データがなければ初期値
			routeData = getDefaultRoute();
		}
		var routeArray:Array<Route> = [];
		var now = nowPoint.clone(); // 現在位置
		maxTime = 0;
		// 絶対位置から相対位置に変換
		for ( i in routeData ) {
			var p = new Vector3(i.point.x - now.x, i.point.y - now.y, i.point.z - now.z);
			now.copy(i.point);
			routeArray.push(new Route(i.time, p, i.degree == null,i.degree));
			//totalTime += i.time;
			maxTime += i.time;
		}
		var t = 0; // ターゲット
		var next:Route = routeArray[t];
		var nextDeg = next.getDeg();
		if ( nextDeg != deg ) { // 角度が違うとき 周り始めのポイントを追加する
			//var d = Math.min(Math.abs(nextDeg - deg), Math.abs(deg - nextDeg));
			var d = Math.abs((nextDeg - deg + 540) % 360 - 180);
			var time = d / 180 * roteTime;
			// 時間の調整 時間が足りなかった場合falseを返す
			if (next.subTime(time) == false) {
				trace('曲がるための時間が足りません　必要：' + Std.int(time) + 'ミリ秒'); 
				return false;
			}
			var speed = next.getSpeed();
			var curve = new Vector3(speed.x * time, speed.y * time, speed.z * time);
			routes.push(new Route(time, curve, next.isLook(), nextDeg));
		}
		routes.push(next);
		var old:Route;
		while (routeArray.length > t + 1) {
			old = routes[routes.length - 1];
			t++;
			next = routeArray[t];
			var nextDeg = (360 + next.getDeg()) % 360;
			var oldDeg = (360 + old.getDeg()) % 360;
			if (nextDeg != oldDeg) { // 角度が違うとき 周り始めのポイントを追加する
				// 時間の調整 曲がる前後の移動を回る時間分減らす
				var d = Math.abs((nextDeg - oldDeg + 540) % 360 - 180);
				//var d = Math.min(Math.abs(nextDeg - oldDeg), Math.abs(oldDeg + 360 - nextDeg));
				var time = d / 180 * roteTime;
				if (next.subTime(time) == false) return false;
				if (old.subTime(time) == false) return false;
				var ns:Speed = next.getSpeed();
				var os:Speed = old.getSpeed();
				var x = ns.x * time + os.x * time;
				var y = ns.y * time + os.y * time;
				var z = ns.z * time + os.z * time;
				var curve = new Vector3(x, y, z);
				routes.push(new Route(time * 2, curve, next.isLook(), nextDeg));
			}
			routes.push(next);
		}
		nextRoute = routes[np];
		nextTime = nextRoute.getTime();
		lastTime = 0;
		look = nextRoute.isLook();
		return true;
	}
	private function setNext() {
		// 基準位置の調整
		nowPoint.add(nextRoute.getPoint());
		lastTime = nextTime;
		if (look) {
			d = (360 + nextRoute.getDeg()) % 360;
		}
		np++;
		nextRoute = routes[np];
		nextTime += nextRoute.getTime();
		look = nextRoute.isLook();
	}
	private function getDefaultRoute():Array<RouteParameter> {
		var array2:Array<RouteParameter> = [
			{ time: 4000, point: new Vector3(0, 0, 1000) },
			{ time: 6000, point: new Vector3(1732, 0, 1000) },
			{ time: 6000, point: new Vector3(1732, 0, -1000) },
			{ time: 6000, point: new Vector3(0, 0, -2000) },
			{ time: 6000, point: new Vector3(-1732, 0, -1000) },
			{ time: 6000, point: new Vector3(-1732, 0, 1000) },
			{ time: 6000, point: new Vector3(0, 0, 2000) },
			{ time: 6000, point: new Vector3(1732, 0, 1000) },
			{ time: 6000, point: new Vector3(1732, 0, -1000) },
			{ time: 4000, point: new Vector3(0, 0, -1000) }
		];
		var array:Array<RouteParameter> = [
			{ time: 8000, point: new Vector3(0, 0, 2000) },
			{ time: 8000, point: new Vector3(2000, 0, 2000) },
			{ time: 16000, point: new Vector3(2000, 0, -2000) },
			{ time: 16000, point: new Vector3( -2000, 0, -2000) },
			{ time: 8000, point: new Vector3( -2000, 0, 0) },
			//{ time: 8000, point: new Vector3(0, 0, 0), look: false },
			//{ time: 8000, point: new Vector3(2000, 0, 2000) },
			//{ time: 8000, point: new Vector3(0, 0, 0) }
		];	
		return array;
	}
}