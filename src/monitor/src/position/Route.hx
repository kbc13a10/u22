package position;
import three.Vector3;

typedef Speed = {
	x:Float,
	y:Float,
	z:Float
}

class Route {
	private var time:Float; // 時間 ミリ秒
	private var point:Vector3; // 位置(相対
	private var look:Bool; // カメラを自動調整
	private var deg:Float; // 角度
	public function new(t:Float, p:Vector3, l:Bool = true, ?d:Float) {
		time = t;
		point = p;
		look = l;
		if (d == null) {
			deg = (Math.atan2(point.z, point.x)) * 180 / Math.PI;
		} else {
			deg = d;
		}
		deg = (360 + deg) % 360;
	}
	public function getSpeed() : Speed {
		var speed = {
			x:point.x / time,
			y:point.y / time,
			z:point.z / time
		}
		return speed;
	}
	public function getPosi(t) : Vector3 {
		var p:Vector3;
		if (time > t) {
			var ratio = t / time;
			p = new Vector3(point.x * ratio, point.y * ratio, point.z * ratio);
		} else {
			p = point.clone();
		}
		return p;
	}

	public function subTime(t:Float ) : Bool {
		if (t > time) return false;
		var ratio = (time - t) / time;
		var p = new Vector3(point.x * ratio, point.y * ratio, point.z * ratio);
		point.copy(p);
		time -= t;
		return true;
	}
	public function isLook() : Bool {
		return look;
	}
	public function getTime() : Float {
		return time;
	}
	public function getPoint() : Vector3 {
		return point.clone();
	}
	public function getDeg() : Float {
		return deg;
	}
	public function clone() : Route {
		return new Route(time, point.clone(), look, deg);
	}
}

