package position;
import three.Vector3;
import position.RoutePosition.RouteParameter;

typedef PositionParameter = {
	var point:Vector3; 
	var deg:Float;
}

class AbstractPosition {
	private var position:Vector3 = new Vector3();
	private var deg:Float = 90;
	private var progress: Float = 0; // 経過時間
	private var startWait: Float = 3000; // 最初3秒待機
	public function new(startWait:Float = 3000) {
		this.startWait = startWait;
	}
	public function update(time:Float) {}
	public function getPosition():PositionParameter {
		return {point:position, deg:deg};
	}
	public function getAfterPosition(time:Float):PositionParameter {
		return {point:position, deg:deg};
	}
	public function init() {
		position = new Vector3();
		deg = 90;
		progress = 0 - startWait;
	}
	public function setRouteData(data:Dynamic) { }
	public function getMaxTime() : Int {
		return 60 * 1000;
	}
	public function getNowTime() {
		return this.progress;
	}
	public function changeRoute() { }
}