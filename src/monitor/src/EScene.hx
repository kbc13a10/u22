package;

/**
 * 列挙型 シーン
 * @author kbc13a14
 */
enum EScene {
	EasyGameScene;
	NormalGameScene;
	HardGameScene;
	RandomGameScene;
	LoadScene;
	EndScene;
	TitleScene;
	ResultSingleScene;
	//TestScene;
	ResultTeamScene;
	ResultRankScene;
}