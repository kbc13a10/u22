package obj2d;
import js.html.CanvasRenderingContext2D;

class RankResult extends AbstractObj2d {
	private var textSize:Float = GameMgr.HEIGHT / 17;
	private var beforeSize:Float = GameMgr.HEIGHT / 16;
	private var beforeText:String;
	private var width:Float = GameMgr.WIDTH / 2;
	private var height:Float = GameMgr.HEIGHT / 16 * 1.6;
	private var text:String;
	private var endX:Float = 0;
	private var start:Bool = false;
	private var move:Bool;
	public function new(x:Float, y:Float, before:String, text:String, ?width:Float, ?move:Bool = false) {
		super(x, y);
		this.text = text;
		this.beforeText = before;
		if (width != null) this.width = width;
		this.move = move;
	}
	override public function draw(context:CanvasRenderingContext2D, time) {
		super.draw(context, time);
		if (!start && move) {
			endX = x;
			x = GameMgr.WIDTH;
			dx = -40;
		}
		start = true;
		if (x <= endX) dx = 0;
		context.beginPath();
		context.fillStyle = '#ffffff';
		context.globalAlpha = 0.7;
		context.fillRect(x - 30, y, width + 60, GameMgr.HEIGHT / 10 + 10);
		context.globalAlpha = 1;
		context.closePath();
		context.beginPath();
		context.textAlign = 'left';
		context.fillStyle = '#000000';
		context.font = beforeSize + 'px bold "Times New Roman"';
		context.fillText(beforeText, x, y + beforeSize * 1.4);
		context.font = textSize + 'px bold "Times New Roman"';
		context.fillText(text, x + width / 2 , y + beforeSize * 1.4);
		context.closePath();
		context.beginPath();
		context.strokeStyle = '#000000';
		context.lineWidth = 3;
		context.moveTo(x, y + beforeSize * 1.6);
		context.lineTo(x + width, y + beforeSize * 1.6);
		context.closePath();
		context.stroke();
		context.lineWidth = 1;
	}
}