package obj2d;
import js.html.CanvasRenderingContext2D;

class MoveText extends AbstractObj2d{
	private var textSize:Float;
	private var texts:Array<String>;
	private var nowIndex:Int = -1;
	private var elapsedTime:Float = 0;
	private var waitTime:Float;
	private var speed:Float;
	private var move:Bool = false;
	private var initX:Float;
	private var centerStopTime:Float = 3000;
	private var show:Bool = true;
	public function new(x:Float, y:Float, texts:Array<String>, textSize:Float = 17, speed:Float = -2, waitTime:Float = 3000) {
		super(x, y);
		initX = x;
		this.texts = texts;
		this.waitTime = waitTime;
		this.speed = if (speed > 0) -speed else speed;
		this.textSize = textSize;
	}
	override public function draw(context:CanvasRenderingContext2D, time) {
		if (!show) return;
		super.draw(context, time);
		if (nowIndex == -1) nowIndex = 0;
		if (!move) {
			elapsedTime += time;
			if (elapsedTime > waitTime) {
				setMove();
			}
		}
		if (x <= GameMgr.WIDTH / 2 - textSize * texts[nowIndex].length / 2) {
			elapsedTime += time;
			centerStop();
		}
		if (x < -textSize * texts[nowIndex].length) initMove();
		context.textAlign = "left";
		context.fillStyle = "#ff0000";
		context.font = textSize + 'px bold "Times New Roman"';
		context.fillText(texts[nowIndex], x, y);
	}
	private function centerStop() {
		if (elapsedTime < centerStopTime) {
			dx = 0;
		} else {
			dx = speed;
		}
	}
	private function setMove() {
		this.dx = this.speed;
		this.move = true;
		this.elapsedTime = 0;
	}
	public function initMove() {
		this.dx = 0;
		this.x = this.initX;
		this.elapsedTime = 0;
		this.move = false;
		this.nowIndex = (this.nowIndex + 1) % this.texts.length;
	}
	public function isShow() {
		return this.show;
	}
	public function showText() {
		this.show = true;
	}
	public function hideText() {
		this.show = false;
	}
}