package obj2d;
import js.html.CanvasRenderingContext2D;
import three.Vector2;

class TeamResult extends AbstractObj2d {
	private var player:Player;
	private var endY:Float = GameMgr.HEIGHT / 2 - (GameMgr.HEIGHT / 6 + 20) / 2;
	private var color:String;
	private var elapsedTime:Float; //経過時間
	private var startPoint:Vector2; //開始位置
	private var endPoint:Vector2; //終了位置
	private var result:String;//勝ち負け
	private var score:Float = 0; //スコア
	private var teamcolor:String;
	private var names:Array<String>;
	private var type:String;
	public function new(result:String, x:Float, y:Float, color:String, score:Float, names:Array<String>, type:String) {
		super(x, y);
		this.result = result;
		this.type = type;
		this.elapsedTime = 0;
		this.color = color;
		this.score = score;
		this.names = names;
		
		if (type == "0") {
			dy = -40;
		}else {
			dy = 40;
		}
		
	}
	override public function draw(context:CanvasRenderingContext2D, time) {
		super.draw(context, time);
		
		if(type == "0"){
			if (y < endY) { //今のYが終了のYより小さい場合
				y = endY; //今のYに終了のYを入れる
				dy = 0;
			}
		}else {
			if (y > endY) {
				y = endY;
				dy = 0;
			}
		}
		
			
		context.beginPath();
		context.textAlign = "left";
		context.fillStyle = color;
		context.globalAlpha = 0.7;
		context.fillRect(x, y, GameMgr.WIDTH / 3, GameMgr.HEIGHT / 6 + 20 + GameMgr.HEIGHT / 18 * 4);
		context.globalAlpha = 1;
		context.beginPath();
		context.strokeStyle = '#000000';
		context.fillStyle = '#000000';
		context.strokeRect(x, y, GameMgr.WIDTH / 3, GameMgr.HEIGHT / 6 + 20 + GameMgr.HEIGHT / 18 * 4);
		context.beginPath();
		context.textAlign = "center";
		context.font = GameMgr.HEIGHT / 18 + 'px bold "Times New Roman"';
		//context.fillText(result + score + " 点", x + 30, y + GameMgr.HEIGHT / 12);
		context.fillText(result + score + " 点", x + (GameMgr.WIDTH / 6), y + GameMgr.HEIGHT / 12);
		for (i in 0...names.length) {
			//context.fillText(names[i], x + 40, y + GameMgr.HEIGHT / 6 + GameMgr.HEIGHT / 18 * i);
			context.fillText(names[i], x + (GameMgr.WIDTH / 6), y + GameMgr.HEIGHT / 6 + GameMgr.HEIGHT / 18 * i);
		}
		context.textAlign = "left";
		context.closePath();
	}
}