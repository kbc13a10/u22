package obj2d;

import js.html.CanvasRenderingContext2D;
/**
 * ...
 * @author hoshi
 */
class Reticle extends AbstractObj2d{
	private var pointX:Int;
	private var pointY:Int;
	private var color:String;
	private var r1:Float;
	private var r2:Float;
	private var height:Float;
	private var width:Float;
	
	// ショット後に250ミリ秒のゲージ発生
	private var gauge:Float = 250;
	private var max:Float = 250;
	
	public function new(x:Float, y:Float, c:String) {
		super(x, y);
		r1 =  GameMgr.WIDTH * 0.02;
		r2 = r1 * 0.4;
		height = r1; 
		width = height * 0.1;
		color = c;
	}
	
	override public function draw(context:CanvasRenderingContext2D, time:Float) {
		super.draw(context, time);
		
		if (x < 0) x = 0;
		if (x > GameMgr.WIDTH) x = GameMgr.WIDTH;
		if (y < 0) y = 0;
		if (y > GameMgr.HEIGHT) y = GameMgr.HEIGHT;
		
		if (gauge < max) gauge += time;
		if (gauge > max) gauge = max;
		
		context.beginPath();
		context.lineWidth = 2;
		context.globalAlpha = 0.8;
		context.strokeStyle = "#888888"; // 
		context.arc(x, y, r1 + 3, -Math.PI / 2, Math.PI * 2);//半径20
		context.stroke();
		context.closePath();
		
		context.beginPath();
		context.lineWidth = 4;
		context.globalAlpha = 0.5;
		context.strokeStyle = color; // 
		context.arc(x, y, r1, -Math.PI / 2, -Math.PI / 2 + Math.PI * 2 * (gauge / max));//半径20
		context.stroke();
		context.closePath();
		
		context.beginPath();
		context.fillStyle = color;
		context.globalAlpha = 0.5;
		context.arc(x, y, r2, 0.0, Math.PI * 2);//半径5
		context.fill();
		context.closePath();
		
		context.beginPath();
		context.fillStyle = color;
		context.globalAlpha = 0.5;
		context.fillRect(x - ((width) / 2), y - (height+((height)/2)), width, height);//上
		context.fillRect(x - ((width) / 2), y + (height) / 2, width, height);//下
		context.fillRect(x - (height+((height)/2)), y - ((width) / 2), height, width);//左
		context.fillRect(x + (height) / 2, y - ((width) / 2), height, width);//右
		
		context.lineWidth = 1;
		context.closePath();
		context.globalAlpha = 1;
	}
	override public function action() {
		gauge = 0;
	}
}