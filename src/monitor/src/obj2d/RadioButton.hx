package obj2d;
import js.html.CanvasRenderingContext2D;

class RadioButton extends TargetRect {
	private static var checks:Map<String, Array<Bool>>;
	private static var change:Bool = false;
	private var id:String;
	private var index:Int;
	public function new(x:Float, y:Float, id:String, text:String, ?fontSize:Float, ?width:Float, check:Bool = true) {
		super(x, y, text, fontSize, width);
		if (id == null) throw 'RadioButton not Id';
		this.id = id;
		if (checks == null) checks = new Map<String, Array<Bool>>();
		if (checks.get(id) != null) {
			var ids:Array<Bool> = checks.get(id);
			index = ids.length;
			ids.push(false);
		} else {
			checks.set(id, [check]);
			index = 0;
		}
	}
	override public function draw(context:CanvasRenderingContext2D, time) {
		if (!show) return;
		var radioGroup = checks.get(id);
		if(radioGroup[index] == true) {
			context.beginPath();
			context.fillStyle = '#ffffff';
			context.globalAlpha = 0.7;
			context.fillRect(x, y, width, height);
			context.globalAlpha = 1;
			context.closePath();
		}
		super.draw(context, time);
	}
	public function isChecked() {
		var radioGroup = checks.get(id);
		return radioGroup[index];
	}
	public function checked() : Bool{
		if (change) return false;
		change = true;
		var radioGroup = checks.get(id);
		for (i in 0...radioGroup.length) {
			if (i == index) {
				radioGroup[i] = true;
			} else {
				radioGroup[i] = false;
			}
		}
		change = false;
		return true;
	}
	override public function checkHit(px:Float, py:Float) : Bool {
		if (change) return false;
		if (!super.checkHit(px, py)) return false;
		change = true;
		var radioGroup = checks.get(id);
		for (i in 0...radioGroup.length) {
			if (i == index) {
				radioGroup[i] = true;
			} else {
				radioGroup[i] = false;
			}
		}
		change = false;
		return true;
	}
	public static function getCheckedIndex(key:String) : Int {
		var radioGroup = checks.get(key);
		var index = -1;
		for (i in 0...radioGroup.length) {
			if (radioGroup[i]) {
				index = i;
				break;
			}
		}
		return index;
	}
}