package obj2d;
import js.html.CanvasRenderingContext2D;

class QrcodeObj extends AbstractObj2d {
	private var qrcode:Qrcode;
	private var tileW:Float = 128;
	private var tileH:Float = 128;
	private var width:Float = 0;
	private var height:Float = 0;
	private var scale:Float;
	public function new(x:Float, y:Float, type:Int, level:Int, url:String, scale:Float) {
		super(x, y);
		qrcode = new Qrcode(type, level);
		qrcode.addData(url);
		qrcode.make();
		this.scale = scale;
		this.width = tileW * scale;
		this.height = tileH * scale;
		tileW = tileW / qrcode.getModuleCount();
		tileH = tileH / qrcode.getModuleCount();
	}
	override public function draw(context:CanvasRenderingContext2D, time) {
		super.draw(context, time);
		// QRコード配置位置
		for( row in 0...qrcode.getModuleCount()){
			for( col in 0...qrcode.getModuleCount()){
				context.fillStyle = if (qrcode.isDark(row, col)) '#000000' else '#ffffff';
				var w = (Math.ceil((col+1)*tileW) - Math.floor(col*tileW));
				var h = (Math.ceil((row+1)*tileH) - Math.floor(row*tileH));
				context.fillRect(x + (Math.round(col * tileW)) * scale, y + (Math.round(row * tileH)) * scale, w * scale, h * scale);
			}	
		}
	}
	public function getWidth() {
		return width;
	}
	public function getHeight() {
		return height;
	}
}