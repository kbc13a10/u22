package obj2d;

import js.html.CanvasRenderingContext2D;

/**
 * ...
 * @author kbc13a14
 */
class Rectangle extends AbstractObj2d {

	public function new(x:Float, y:Float) {
		super(x, y);
	}
	
	override public function draw(context:CanvasRenderingContext2D) {
		super.draw(context);
		context.beginPath();
		context.fillStyle = 'rgb(192, 80, 77)'; // 赤
		context.fillRect(x - 10.0, y - 10.0, 20.0, 20.0);
		context.fill();
		context.closePath();
	}
	
}