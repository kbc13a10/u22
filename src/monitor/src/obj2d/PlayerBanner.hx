package obj2d;
import js.html.CanvasRenderingContext2D;


class PlayerBanner extends AbstractObj2d {
	private var lineW = 2;
	private var width:Float = 0;
	private var height:Float = 0;
	private var textSize:Float = GameMgr.HEIGHT / 80 * 3;
	private var readyWidth:Float;
	private var readysColors:Array<String> = ['#F78181', '#81DAF5', '#9FF781', '#d584e0'];
	
	public function new(x:Float, y:Float) {
		super(x, y);
		width = GameMgr.WIDTH / 1.5;
		height = textSize * 1.6 * 2 + lineW * 3;
		readyWidth = textSize * 4;
	}
	override public function draw(context:CanvasRenderingContext2D, time) {
		super.draw(context, time);
		
		context.beginPath();
		context.fillStyle = '#ffffff';
		context.fillRect(lineW + x, y + lineW, width, height);
		//枠線
		context.fillStyle = '#000000';
		context.fillRect(x, y, width + lineW, lineW);//バー上辺
		context.fillRect(x, y + lineW, lineW, height);//バー左辺
		context.fillRect(x + width / 2, y + lineW, lineW, height);//バー中央線 縦
		context.fillRect(x, y + height / 2, width + lineW, lineW);//バー中央線　横
		context.fillRect(x, y + height + lineW, width + lineW, lineW);//バー下辺
		context.fillRect(x + width, y + lineW, lineW, height);//バー右辺
		context.closePath();
		
		for (p in GameMgr.players) {
			context.beginPath();
			var nameX = x + textSize;
			var nameY = y + textSize * 1.2 + lineW;
			var readyX = x + width / 2 - readyWidth;
			var readyY = y + lineW;
			var readyText = if (p.isReady()) '完了' else '準備';
			var readyHeight = height / 2 - lineW;
			switch(p.getNumber()) {
				case 1:
					nameY += height / 2 + lineW;
					readyY += height / 2;
					readyHeight += lineW;
				case 2:
					nameX += width / 2;
					readyX += width / 2;
				case 3:
					nameY += height / 2 + lineW;
					readyY += height / 2;
					nameX += width / 2;
					readyX += width / 2;
					readyHeight += lineW;
			}
			if (GameMgr.isTeamBattle()) {
				context.fillStyle = GameMgr.teamColor.get(p.getTeamColor());
			} else {
				context.fillStyle = readysColors[p.getNumber()];
			}
			context.fillRect(readyX, readyY, readyWidth, readyHeight);
			context.fillStyle = '#000000';
			context.textAlign = 'left';
			context.font = textSize + 'px bold "Times New Roman"';
			context.fillText(readyText, readyX + textSize, nameY);
			context.fillText(p.getName(), nameX, nameY);
			context.closePath();
		}
	}
	public function getHeight() {
		return height;
	}
}