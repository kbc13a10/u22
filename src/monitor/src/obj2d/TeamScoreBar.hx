package obj2d;
import js.html.CanvasRenderingContext2D;

class TeamScoreBar extends AbstractObj2d {
	private var width = 0;
	private var textSize = 17;
	private var space = 40;
	private var players:Array<Player>;
	private var colors:Map<ETeamType, String>;
	public function new(textSize:Int, width:Int, players:Array<Player>, colors:Map<ETeamType, String>) {
		super(0, 0);
		this.textSize = textSize;
		this.width = width;
		this.players = players;
		this.colors = colors;
	}
	override public function draw(context:CanvasRenderingContext2D, time) {
		super.draw(context, time);
		context.beginPath();
		context.globalAlpha = 0.7;
		
		context.fillStyle = colors.get(ETeamType.red);
		context.fillRect(0, 0, width / 2, textSize * 2 + 8);
		
		context.fillStyle = colors.get(ETeamType.blue);
		context.fillRect(width / 2, 0, width, textSize * 2 + 8);
		
		context.globalAlpha = 1;
		context.fillStyle = '#000000';
		context.font = textSize + 'px bold "Times New Roman"';
		
		var redName = "";
		var blueName = "";
		
		var redScore:Float = 0;
		var blueScore:Float = 0;
		
		for (player in players) {
			switch(player.getTeamColor()) {
				case red:
					redName += player.getName() + " ";
					redScore += player.getScore();
				case blue:
					blueName += player.getName() + " ";
					blueScore += player.getScore();
			}
		}
		
		context.fillText(redName, space, textSize);
		context.fillText(redScore + "点", space, textSize * 2);
		context.fillText(blueName, (width / 2) + space, textSize);
		context.fillText(blueScore + "点", (width / 2) + space, textSize * 2); 
		context.closePath();
	}
}