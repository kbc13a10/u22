package obj2d;
import js.html.CanvasRenderingContext2D;

class PlayerResult extends AbstractObj2d {
	private var player:Player;
	private var endX:Float = GameMgr.WIDTH / 4;
	private var color:String;
	private var startTime:Float;
	private var elapsedTime:Float;
	private var text:String;
	private var score:Float;
	private var name:String;
	private var width:Float;
	private var heigth:Float;
	private var space:Float;
	public function new(text:String, arrayNum:Int, maxNum:Int, pNum:Int, score:Float, name:String, startTime:Float) {
		super(GameMgr.WIDTH, 0);
		this.text = text;
		this.startTime = startTime;
		this.elapsedTime = 0;
		this.color = '#' + GameMgr.playerColor[pNum];
		this.score = score;
		this.name = name;
		dx = -40;
		this.heigth = GameMgr.HEIGHT / 7;
		this.width = GameMgr.WIDTH / 3;
		this.space = heigth / 3;
		
		switch(maxNum) {
			case 5:
			case 4:
				switch(arrayNum) {
					case 1:
						y = GameMgr.HEIGHT / 2 - (space * 1.5 + heigth * 2);
					case 2:
						y = GameMgr.HEIGHT / 2 - (space / 2 + heigth);
					case 3:
						y = GameMgr.HEIGHT / 2 + space / 2;
					case 4:
						y = GameMgr.HEIGHT / 2 + space * 1.5 + heigth;
				}
			case 3:
				switch(arrayNum) {
					case 1:
						y = GameMgr.HEIGHT / 2 - (space + heigth * 1.5);
					case 2:
						y = GameMgr.HEIGHT / 2 - heigth / 2;
					case 3:
						y = GameMgr.HEIGHT / 2 + (space + heigth / 2);
				}
			case 2:
				switch(arrayNum) {
					case 1:
						y = GameMgr.HEIGHT / 2 - (space + heigth * 1.5);
					case 2:
						y = GameMgr.HEIGHT / 2 + (space + heigth / 2);
				}
			case 1:
				y = GameMgr.HEIGHT / 2 - heigth / 2;
		}
	}
	override public function draw(context:CanvasRenderingContext2D, time) {
		elapsedTime += time;
		if (elapsedTime < startTime) return;
		super.draw(context, time);
		if (x < endX) {
			x = endX;
			dx = 0;
		}
		context.beginPath();
		context.textAlign = "left";
		context.fillStyle = color;
		context.globalAlpha = 0.7;
		context.fillRect(x, y, width, heigth);
		context.globalAlpha = 1;
		context.beginPath();
		context.strokeStyle = '#000000';
		context.fillStyle = '#000000';
		context.strokeRect(x, y, width, heigth);
		context.beginPath();
		context.font = heigth * 0.4 + 'px bold "Times New Roman"';
		context.fillText(text + ' ' + score + " 点", x + heigth * 0.2, y + heigth * 0.5);
		context.fillText(name, x + heigth * 0.4, y + heigth * 0.9);
		context.closePath();
	}
}