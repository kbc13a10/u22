package obj2d;

import js.html.CanvasRenderingContext2D;

/**
 * ...
 * @author kbc13a14
 */
class AbstractObj2d {
	
	public var x(default, null):Float;
	public var y(default, null):Float;
	
	private var dx:Float = 0;
	private var dy:Float = 0;
	private var fps:Int = 60;
	
	public function new(x:Float, y:Float) {
		this.x = x;
		this.y = y;
	}
	
	public function draw(context:CanvasRenderingContext2D, time:Float) {
		//x += 1.0;
		x += dx * (time / (1000 / fps));
		y += dy * (time / (1000 / fps));
	}
	
	public function add(dx:Float, dy:Float) {
		this.dx = dx;
		this.dy = dy;
	}
	
	public function action() { }

	public function checkHit(px:Float, py:Float) : Bool { return false; }
	public function setX(x:Float) {
		this.x = x;
	}
}