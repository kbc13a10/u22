package obj2d;
import js.Browser;
import js.html.CanvasRenderingContext2D;
import js.html.ImageElement;
import js.Cookie;

class AudioButton extends AbstractObj2d {
	private var audioImages:Array<ImageElement> = [];
	private var audio:Audio = Audio.getInstance();
	private var width:Float;
	private var height:Float;
	public function new(x:Float, y:Float) {
		super(x, y);
		for (i in 0...2) {
			var image:ImageElement = Browser.document.createImageElement();
			image.src = GameMgr.DIRECTORY + "images/audio_" + i + ".png";
			audioImages[i] = image;
		}
		width = GameMgr.WIDTH / 12;
		height = GameMgr.WIDTH / 12;
		var cookieData = Cookie.get('audio');
		if (cookieData != null && cookieData == 'off') {
			audio.setMuteMain(true);
		}
	}
	override public function draw(context:CanvasRenderingContext2D, time) {
		super.draw(context, time);
		if (!audio.isMuteMain()) {
			context.drawImage(audioImages[0], x, y, width, height);
		} else {
			context.drawImage(audioImages[1], x, y, width, height);
		}
	}
	override public function checkHit(px:Float, py:Float):Bool {
		if (px >= x && px <= x + width
			&& py >= y && py <= y + height) {
			var value = if (audio.isMuteMain()) 'on' else 'off';
			Cookie.set('audio', value, 60 * 60 * 24 * 7);
			audio.setMuteMain(!audio.isMuteMain());
			return true;
		}
		return false;
	}
}