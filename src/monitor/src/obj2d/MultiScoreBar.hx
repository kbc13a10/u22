package obj2d;
import js.html.CanvasRenderingContext2D;

class MultiScoreBar extends AbstractObj2d{
	private var width = 0;
	private var textSize = 17;
	private var space = 40;
	private var players:Array<Player>;
	public function new(textSize:Int, width:Int, players:Array<Player>) {
		super(0, 0);
		this.textSize = textSize;
		this.width = width;
		this.players = players;
	}
	override public function draw(context:CanvasRenderingContext2D, time) {
		super.draw(context, time);
		context.beginPath();
		context.globalAlpha = 0.7;
		
		context.fillStyle = '#F78181';
		context.fillRect(0, 0, width / 4, textSize * 2 + 8);
		
		context.fillStyle = '#81DAF5';
		context.fillRect(width / 4, 0, width / 4, textSize * 2 + 8);
		
		context.fillStyle = '#9FF781';
		context.fillRect(width / 2, 0, width / 4, textSize * 2 + 8);
		
		context.fillStyle = '#d584e0';
		context.fillRect((width / 4) * 3, 0, width / 4, textSize * 2 + 8);
		
		context.globalAlpha = 1;
		context.fillStyle = '#000000';
		context.font = textSize + 'px bold "Times New Roman"';
		
		for (player in players) {
			switch(player.getNumber()) {
				case 0:
					context.fillText(player.getName(), space, textSize);
					context.fillText(player.getScore() + "点", space, textSize * 2);
				case 1:
					context.fillText(player.getName(), (width / 4) + space, textSize);
					context.fillText(player.getScore() + "点", (width / 4) + space, textSize * 2); 
				case 2:
					context.fillText(player.getName(), (width / 2) + 40, textSize);
					context.fillText(player.getScore() + "点", (width / 2) + space, textSize * 2); 
				case 3:
					context.fillText(player.getName(), ((width / 4) * 3) + 40, textSize);
					context.fillText(player.getScore() + "点", ((width / 4) * 3) + space, textSize * 2); 
			}
		}
		context.closePath();
	}
}