package obj2d;
import js.html.CanvasRenderingContext2D;

class TargetRect extends AbstractObj2d {
	private var text:String;
	private var width:Float;
	private var height:Float;
	private var fontSize:Float;
	private var show:Bool = true;
	private var color:String;
	public function new(x:Float, y:Float, text:String, ?fontSize:Float, ?width:Float, ?color:String) {
		super(x, y);
		this.fontSize = if (fontSize == null) GameMgr.HEIGHT / 20 else fontSize;
		this.text = text;
		this.width = if (width == null) (text.length + 2) * this.fontSize else width;
		this.height = this.fontSize * 1.6;
		this.color = color;
	}
	override public function draw(context:CanvasRenderingContext2D, time) {
		if (!show) return;
		super.draw(context, time);
		context.beginPath();
		if (color != null) {
			context.fillStyle = color;
			context.globalAlpha = 0.7;
			context.fillRect(x, y, width, height);
			context.globalAlpha = 1;
		}
		context.strokeStyle = '#000000';
		context.font = fontSize + 'px bold "Times New Roman"';
		context.textAlign = 'center';
		context.strokeRect(x, y, width, height);
		context.fillStyle = '#000000';
		context.fillText(text, x + width / 2, y + fontSize * 1.2);
		context.closePath();
	}
	public function showText() {
		this.show = true;
	}
	public function hideText() {
		this.show = false;
	}
	public function isShow() {
		return this.show;
	}
	override public function checkHit(px:Float, py:Float) : Bool {
		if (!show) return false;
		if (px >= x && px <= x + width
			&& py >= y && py <= y + height) {
			return true;	
		} else {
			return false;
		}
	}
	public function getHeight() {
		return this.height;
	}
	public function getWidth() {
		return this.width;
	}
	public function getText() {
		return this.text;
	}
}