package;

/**
 * Xorshift XOR128
 * @author kbc13a14
 */
class Random
{
	private var t:UInt;
	private var w:UInt;
	
	private var x:UInt;
	private var y:UInt;
	private var z:UInt;
	
	public function new (w:UInt = 88675123, x:UInt = 123456789, y:UInt = 362436069, z:UInt = 521288629) {
//		trace(w, x, y, z);
		if (x <= 0 && y <= 0 && z <= 0 && w <= 0) {
			throw "すべて0では乱数が生成できません";
		}
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = w;
		
//		シード値が近い値だと最初は似たような値が出てくるから
		for (i in 0...100) {
			nextUInt();
		}
	}
	
	public function nextUInt():UInt {
		t = x ^ (x << 11);
		x = y;
		y = z;
		z = w;
		return w = (w ^ (w >>> 19)) ^ (t ^ (t >>> 8));
	}
	
	public function nextFloat():Float {
		return nextUInt() / 4294967296.0;
	}
	
}