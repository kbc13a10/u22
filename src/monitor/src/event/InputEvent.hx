package event;

/**
 * ...
 * @author kbc13a14
 */
class InputEvent {
	
	public var x(default, null):Float;
	public var y(default, null):Float;
	
	@:allow(event.Input)
	private function new(x: Float, y:Float) {
		this.x = x;
		this.y = y;
	}
	
}