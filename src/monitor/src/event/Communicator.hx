package event;

import js.html.MessageEvent;
import js.html.WebSocket;
import js.Browser;
import haxe.Timer;

class Communicator{
	//private var wsurl:String = "ws://192.168.5.113:8080/shooting_websocket_servlet";
	//private var wsurl:String = "ws://192.168.2.106:8080/shooting_websocket_servlet";
	private var wsurl:String = "ws://" + Reflect.field(Browser.window, "WS_SERVER_HOST") + "/shooting_websocket_servlet";
	//private var wsurl:String = "ws://" + Browser.location.hostname + ":" + Browser.location.port + "/shooting_websocket_servlet";
	private var ws:WebSocket;
	public function new() {
		ws = new WebSocket(wsurl);
		ws.onopen = open;
		ws.onerror = error;
		ws.onclose = close;
		ws.onmessage = message;
	}
	private function message(message:MessageEvent) {
		var msg:String = message.data;
		var sp:Int = msg.indexOf(",");
		try {
			switch(msg.substring(0, sp)) {
				case "CONN":
					if (msg.substring(sp + 1) != 'failre') {
						GameMgr.load(msg.substring(sp + 1));
					} else {
						GameMgr.changeScene(EScene.EndScene, ["部屋を作るスペースがありませんでした。。。"]);
					}
				case "ENTR":
					var np = msg.indexOf(",", sp + 1);
					var id = msg.substring(sp + 1, np);
					var result = GameMgr.playerEntry(id, msg.substring(np + 1));
					ws.send('ENTR,$id,$result');
				case "SHOT":
					GameMgr.playerShot(msg.substring(sp + 1));
				case "MOVE":
					var msgArray = msg.split(",");
					GameMgr.playerMove(msgArray[1], msgArray[2], msgArray[3]);
				case "PRDY":
					var np = msg.indexOf(",", sp + 1);
					GameMgr.playerReady(msg.substring(sp + 1, np), msg.substring(np + 1));
				case "EXIT":
					GameMgr.playerExit(msg.substring(sp + 1));
				case "RANK":
					GameMgr.setRank(msg.substring(sp + 1));
				default:
					trace("message error:" + msg);
			}
		} catch (unknown:Dynamic) {
			trace("message error:" + Std.string(unknown));
		}
	}
	private function close() {
		GameMgr.changeScene(EScene.EndScene);
	}
	private function error() {
		GameMgr.changeScene(EScene.EndScene, ["エラーが発生したため接続が切れました"]);
	}
	private function open() {
		ws.send("CONN,parent");
	}
	public function allReady() {
		ws.send("ARDY,on");
	}
	public function reset() {
		ws.send("ARDY,off");
	}
	public function sendRank(playerId:String, stageId:Int, score:Float) {
		ws.send('RANK,$playerId,$stageId,$score');
	}
}