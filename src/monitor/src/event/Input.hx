package event;
import js.Browser;
import js.html.Element;
import js.html.Event;
import js.html.MouseEvent;
import js.html.TouchEvent;

/**
 * ...
 * @author kbc13a14
 */
class Input {
	public var div:Element;
	var listeners = new Map<EScene, List<InputEvent->Void>>();

	public function new() {
		div = Browser.document.createElement('div');
		div.style.width = GameMgr.WIDTH+'px';
		div.style.height = GameMgr.HEIGHT+'px';
		div.style.position = 'absolute';
		div.style.zIndex = '100';
		//div.addEventListener('touchstart', fire);
		div.addEventListener('click', fire);
		Browser.document.body.appendChild(div);
	}
	
	private function fire(htmlEvent:Event) {
		var htmlInputEvent:MouseEvent = cast(htmlEvent, MouseEvent);
		//var htmlInputEvent:TouchEvent = cast(htmlEvent, TouchEvent);
		var ev = new InputEvent(htmlInputEvent.pageX - div.offsetLeft, htmlInputEvent.pageY - div.offsetTop);
		
		var ls = listeners.get(GameMgr.nowScene);
		if (ls != null) {
			for (l in ls) {
				l(ev);
			}
		}
	}
	
	public function addEventListener(?scene:EScene, listener:InputEvent->Void):Bool {
		if (scene == null) scene = GameMgr.nowScene;
		if (listeners.get(scene) == null) listeners.set(scene, new List());
		listeners.get(scene).add(listener);
		return true;
	}
	
	public function removeEventListener(?scene:EScene, listener:InputEvent->Void):Bool {
		if (scene == null) scene = GameMgr.nowScene;
		if (listeners.get(scene) == null) return false;
		return listeners.get(scene).remove(listener);
	}
	
}