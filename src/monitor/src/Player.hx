package;
import obj2d.AbstractObj2d;

/**
 * ...
 * @author kbc13a14
 */
class Player {
	private var id:String;
	private var name:String;
	private var score:Float;
	private var reticle:AbstractObj2d;
	private var ready:Bool;
	private var number:Int;
	private var color:Int;
	private var teamcolor:ETeamType = ETeamType.red;
	private var retire:Bool = false;	
	public function new(id:String, n:String = '名無し', o:AbstractObj2d, num:Int, c:Int) {
		this.id = id;
		name = n;
		score = 0;
		reticle = o;
		ready = false;
		number = num;
		color = c;
	}
	public function getId() {
		return this.id;
	}
	public function setRetire() {
		retire = true;
	}
	public function isRetire() {
		return retire;
	}
	public function getObj() {
		return reticle;
	}
	public function addScore(s:Float) {
		score += s;
	}
	public function getScore() : Float{
		return score;
	}
	public function reset() {
		score = 0;
		ready = false;
	}
	public function isReady() {
		return this.ready;
	}
	public function setTeamColor(tc:ETeamType) {
		this.teamcolor = tc;
	}
	public function getTeamColor() {
		return teamcolor;
	}
	public function getName() : String {
		return name;
	}
	public function move(r:Float, d:Float) {
		reticle.add(d * Math.cos(r), d * Math.sin(r));
	}
	public function changeReady(msg:String) {
		switch(msg) {
			case 'on':
				ready = true;
			case 'off':
				ready = false;
		}
	}
	public function getX() : Float{
		return reticle.x;
	}
	public function getY() : Float{
		return reticle.y;
	}
	public function getNumber() {
		return this.number;
	}
	public function getColor() : Int {
		return this.color;
	}
	public function shot() {
		reticle.action();
	}
	public function setScore(s:Float) {
		this.score = s;
	}
}