package field;
import obj3d.InvincibleObject;
import shape.TreeShape;
import three.Vector3;

class FieldTree {
	private var appTime:Float = 1000;
	private var appNums = [1, 1, 2];
	private var nowIndex = 0;
	private var distance:Int;
	private var right:Bool = true;
	private var random:Random;
	public function new(distance:Int, ?randomNum:Int = 1) {
		this.distance = distance;
		this.random = new Random(randomNum);
	}
	//public function createTree(baseTime:Float, points:Array<Vector3>, maxTime:Float){
	public function createTree(routes:Array<{time: Float, point: Vector3}>){
		var trees = [];
		for (i in 0...routes.length - 1) {
			var nowPoint = routes[i].point;
			var toPoint = routes[i + 1].point;
			var maxTime = routes[i].time;
			
			var radian = Math.atan2(toPoint.z - nowPoint.z, toPoint.x - nowPoint.x);
			var routeDistance = Math.sqrt(Math.pow(nowPoint.z - toPoint.z, 2) + Math.pow(nowPoint.x - toPoint.x, 2));
			var speed = routeDistance / maxTime;
			var circleTime = distance / speed;
			appTime = if(i == 0) circleTime else 0;
			var beforeDirection:Float = 0;
			var afterDirection:Float = 0;
			
			if (i > 0) {
				var beforeRadian = Math.atan2(nowPoint.z - routes[i - 1].point.z, nowPoint.x - routes[i - 1].point.x);
				beforeDirection = (radian - beforeRadian + Math.PI * 3) % (Math.PI * 2) - Math.PI;
			}
			if (i < routes.length - 2) {
				var afterRadian = Math.atan2(routes[i + 2].point.z - toPoint.z, routes[i + 2].point.x - toPoint.x);
				afterDirection = (radian - afterRadian + Math.PI * 3) % (Math.PI * 2) - Math.PI;
			}
			
			while (appTime < maxTime) {
				for (n in 0...appNums[nowIndex]) {
					var tree = new InvincibleObject(new TreeShape());
					var centerPoint = 
						new Vector3(
							nowPoint.x + (routeDistance * (appTime / maxTime)) * Math.cos(radian),
							0,
							nowPoint.z + (routeDistance * (appTime / maxTime)) * Math.sin(radian)
						);
					centerPoint.x += n * (tree.getWidth() * Math.cos(radian));
					centerPoint.z += n * (tree.getWidth() * Math.sin(radian));
					if(appTime > circleTime && appTime < maxTime - circleTime || (appTime > circleTime && afterDirection == 0)){
						if(right) {
							tree.position.x = centerPoint.x + (distance * Math.cos(radian + Math.PI / 2));
							tree.position.z = centerPoint.z + (distance * Math.sin(radian + Math.PI / 2));
						} else {
							tree.position.x = centerPoint.x + (distance * Math.cos(radian - Math.PI / 2));
							tree.position.z = centerPoint.z + (distance * Math.sin(radian - Math.PI / 2));
						}
					} else {
						if (appTime <= circleTime) {
							if (beforeDirection < 0) {
								tree.position.x = centerPoint.x + (distance * Math.cos(radian + Math.PI / 2));
								tree.position.z = centerPoint.z + (distance * Math.sin(radian + Math.PI / 2));
							} else {
								tree.position.x = centerPoint.x + (distance * Math.cos(radian - Math.PI / 2));
								tree.position.z = centerPoint.z + (distance * Math.sin(radian - Math.PI / 2));
							}
						} else {
							if (afterDirection > 0) {
								tree.position.x = centerPoint.x + (distance * Math.cos(radian + Math.PI / 2));
								tree.position.z = centerPoint.z + (distance * Math.sin(radian + Math.PI / 2));
							} else {
								tree.position.x = centerPoint.x + (distance * Math.cos(radian - Math.PI / 2));
								tree.position.z = centerPoint.z + (distance * Math.sin(radian - Math.PI / 2));
							}
						}
					}
					trees.push(tree);
				}
				nowIndex = (nowIndex + 1) % appNums.length;
				right = !right;
				appTime += this.random.nextUInt() % 2000 + 1000;
			}
			
		}
		
		
		
		//while (addTime < maxTime) {
			//var cnt = 0;
			//for (n in 0...appNums[nowIndex]) {
				//cnt++;
				//var tree = new InvincibleObject(new TreeShape());
				//var centerPoint = 
					//new Vector3(
						//op.x + (routeDistance * (addTime / maxTime)) * Math.cos(radian),
						//0,
						//op.z + (routeDistance * (addTime / maxTime)) * Math.sin(radian)
					//);
				//centerPoint.x += n * (tree.getWidth() * Math.cos(radian));
				//centerPoint.z += n * (tree.getWidth() * Math.sin(radian));
				//if(right) {
					//tree.position.x = centerPoint.x + (distance * Math.cos(radian + Math.PI / 2));
					//tree.position.z = centerPoint.z + (distance * Math.sin(radian + Math.PI / 2));
				//} else {
					//tree.position.x = centerPoint.x + (distance * Math.cos(radian - Math.PI / 2));
					//tree.position.z = centerPoint.z + (distance * Math.sin(radian - Math.PI / 2));
				//}
				//trees.push(tree);
			//}
			//nowIndex = (nowIndex + 1) % appNums.length;
			//right = !right;
			//addTime += this.random.nextUInt() % 2700 + 500;
		//}
		return trees;
	}
}