package field;
import obj3d.InvincibleObject;
import shape.FloorShape;
import three.Mesh;
import three.MeshPhongMaterial;
import position.RoutePosition.RouteParameter;
import three.PlaneBufferGeometry;
import three.CircleGeometry;
import three.Vector3;

class FieldFloor {
	private var material:MeshPhongMaterial;
	private var routeData:Array<RouteParameter>;
	private var radius:Int;
	private var correctionValue:Float = 0;
	private var addValue:Float = 0.3;
	private var addField:Int = 200;
	public function new(cameraRange:Int) {
		createMaterial(GameMgr.DIRECTORY + "images/floor01.jpg");
		this.radius = cameraRange + addField;
	}
	public function setTexture(texture:String, ?pattarn:Int) {
		createMaterial(texture, pattarn);
	}
	public function setRouteData(data) {
		routeData = data;
	}
	public function getFloorObjects(randomNum:Int) {
		return createRouteObjects(randomNum);
	}
	private function createMaterial(texture:String, ?pattern:Int) {
		var floorTexture = three.ImageUtils.loadTexture( texture );
		floorTexture.wrapS = floorTexture.wrapT = Three.WrappingMode.RepeatWrapping;
		floorTexture.anisotropy = GameMgr.getAnisotropy();
		if(pattern != null) floorTexture.repeat.set( pattern, pattern);
		material = new three.MeshPhongMaterial( { color: 0xffffff, map: floorTexture, shininess:10 } );
	}
	private function createRouteObjects(randomNum:Int) {
		var floors = [];
		var routes = [];
		for (i in routeData) {
			routes.push(i);
		}
		routes.unshift( { time: 0, point: new Vector3() } );
		for (i in 0...routes.length - 1) {
			var floor = new InvincibleObject(new FloorShape(material, [routes[i].point, routes[i + 1].point], radius, addValue * 3 - correctionValue));
			floors.push(floor);
			var middle = new InvincibleObject(new FloorShape(material, [routes[i + 1].point], radius, i % 2 * 0.1));
			floors.push(middle);
			correctionValue = (correctionValue + addValue) % (addValue * 3);
		}
		
		var treeManager = new FieldTree(radius, randomNum);
		var trees = treeManager.createTree(routes);
		for (t in trees) {
			floors.push(t);
		}
		return floors;
	}
}