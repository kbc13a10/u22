package field;
import obj3d.MeshEx;
import three.Object3D;
import three.Vector3;

typedef FieldObjectParameter = {
	@:optional var rpps : Float; // relational position per sec 出現時間から何秒後の位置に出現させるか
	var obj:Object3D; // 表示するオブジェクト
	var appTime:Float; // この時間に出現させる
	@:optional var distance:Float; // カメラが見ている位置からの距離
	@:optional var degree:Float; // カメラが見ている位置からのxz軸の角度 前:0 左:270 右:90
	@:optional var positionY: Float; // カメラが見ている位置からの相対的な高さ
}
class FieldObject {
	private var rpps:Float = 0;
	private var obj:Object3D;
	private var distance(null, null):Float; // カメラが見ている位置からの距離
	private var degree(null,null):Float; // カメラが見ている位置からのxz軸の角度
	private var positionY(null, null): Float; // カメラが見ている位置からの高さ
	private var appTime:Float; // この時間に出現させる
	private var first:Bool = true;
	private var initObj:Object3D;
	public function new(data:FieldObjectParameter) {
		appTime = data.appTime;
		obj = data.obj;
		distance = if (data.distance != null) data.distance else 0;
		degree = if (data.degree != null) data.degree else 90;
		positionY = if (data.positionY != null) data.positionY else 0;
		rpps = if (data.rpps != null) data.rpps else 0;
		initObj = obj.clone();
	}
	public function getObj() : Object3D {
		return obj;
	}
	public function init() {
		obj = initObj.clone();
	}
	public function setObjPosition(target:Vector3, d:Float) {
		var radian = Math.PI / 180 * ((d + degree) % 360);
		var x = distance * Math.cos(radian) + target.x;
		var z = distance * Math.sin(radian) + target.z;
		var y = target.y + obj.position.y + positionY;
		obj.position.set(x, y, z);
	}
	public function getAfterTime() {
		return rpps;
	}
	public function getAppTime() {
		return appTime;
	}
}