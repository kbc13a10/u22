package field;

import action.RotateAction;
import field.FieldObject;
import obj3d.BreakObject;
import shape.CrystalShape;
import three.Object3D;

class ObjectPerTime {
	private var objects:Array<FieldObject>;
	private var targetIndex:Int = 0;
	private var progress:Float = 0;
	private var waitTime:Float = 0;
	private var setObj:Array<FieldObject>;
	public function new(wait:Float = 3000) {
		waitTime = wait;
		setTimeObjects(getDefaultObjects());
	}
	public function setTimeObjects(data:Array<FieldObjectParameter>) {
		data.sort(function(a:FieldObjectParameter, b:FieldObjectParameter):Int {
			if (a.appTime < b.appTime) return -1;
			if (a.appTime > b.appTime) return 1;
			return 0;
		});
		objects = [];
		for (i in data) {
			objects[objects.length] = new FieldObject(i);
		}
	}
	public function init() {
		progress = 0 - waitTime;
		targetIndex = 0;
		setObj = [];
		for (o in objects) {
			o.init();
		}
	}
	public function update(time:Float) {
		progress += time;
		while (objects.length > targetIndex && objects[targetIndex].getAppTime() < progress) {
			setObj[setObj.length] = objects[targetIndex];
			targetIndex++;
		}
	}
	public function getSetObject() {
		var copy = setObj.copy();
		setObj = [];
		return copy;
	}
	private function getDefaultObjects() {
		var maxTime = 10; // 1分
		var interval = 500;
		var array:Array<FieldObjectParameter> = [];
		var time = 0;
		while (time < maxTime) {
			time += interval;
			var obj = new BreakObject(new CrystalShape(), [new RotateAction()]);
			array[array.length] = { rpps:time + 2000, appTime:time, obj:obj };
		}
		
		return array;
	}
	
}