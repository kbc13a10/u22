package;

import js.Browser;
import js.html.ButtonElement;
import js.html.CanvasElement;
import js.html.CanvasRenderingContext2D;
import event.Input;
import js.html.MessageEvent;
import js.html.WebSocket;
import obj2d.Reticle;
import obj3d.DefaultGround;
import scene.abs.AbstractScene;
import scene.LoadScene;
import scene.EasyGameScene;
//import scene.TestScene;
import scene.EndScene;
import scene.HardGameScene;
import scene.NormalGameScene;
import scene.RandomGameScene;
import scene.ResultSingleScene;
import scene.ResultTeamScene;
import scene.ResultRankScene;
import scene.TitleScene;
import three.WebGLRenderer;
import three.Color;
import event.Communicator;
import three.Vector3;
import haxe.Timer;

class GameMgr {
	
	public static var DIRECTORY(default, null):String = Reflect.field(Browser.window, "DIRECTORY");
	
	public static var WIDTH:Int = 320*3;
	public static var HEIGHT:Int = 240*2;
	public static var input(default, null):Input;
	public static var nowScene(default, null):EScene;
	public static var players(default, null):Map<String, Player> = new Map<String, Player>();
	public static var entryNum(default, null):Int = 0;
	public static var playerColor:Array<String> = ["ff0000", "0000ff", "009600", "ff00ff", "ffffff"];
	public static var teamColor:Map<ETeamType, String> = new Map<ETeamType, String>();
	public static var openRandom:Bool = false;
	public static var hardClear:Bool = false;
	
	static var scene:scene.abs.AbstractScene;
	static var sceneMap = new Map<EScene,  scene.abs.AbstractScene>();
	
	static var startTimestamp:Float;
	static var lastTimestamp:Float;
	
	static var stats:Stats;
	static var context:CanvasRenderingContext2D;
	static var renderer:WebGLRenderer;
	static var com:Communicator;
	static var maxPlayer:Int = 4;
	static var playing:Bool = false;
	static var teamBattle:Bool = false;
	static var audio:Audio = Audio.getInstance();
	public static function init() {
		WIDTH = Browser.window.innerWidth;
		HEIGHT = Browser.window.innerHeight;
		teamColor.set(ETeamType.red, "#F78181");
		teamColor.set(ETeamType.blue, "#81DAF5");
		
		input = new Input();
		
		//	Canvas2D
		var ce = cast(Browser.document.createElement('canvas'), CanvasElement);
		ce.setAttribute('width', '$WIDTH');
		ce.setAttribute('height', '$HEIGHT');
		ce.style.position = 'absolute';
		ce.style.zIndex = '60';
		context = ce.getContext2d();
		Browser.document.body.appendChild(ce);
		
		//	Threeレンダラー
		renderer = new WebGLRenderer();
		renderer.setSize(WIDTH, HEIGHT);
		renderer.autoClear = false;
		renderer.domElement.style.position = 'absolute';
		renderer.domElement.style.zIndex = '50';
		Browser.document.body.appendChild(renderer.domElement);
		
		//	シーン登録
		nowScene = EScene.LoadScene;
		var sceneLength = 0;
		for (s in Type.allEnums(EScene)) {
			var inst = Type.createInstance(
					Type.resolveClass('scene.'+Std.string(s)), [context]);
			sceneMap.set(s, inst);
			sceneLength++;
		}
		scene = sceneMap.get(nowScene);
		scene.init([sceneLength]);
		
		//	開始時間
		startTimestamp = lastTimestamp = Date.now().getTime();
		//	ループ開始
		raf = Browser.window.requestAnimationFrame(update);
		// WebSocket 初期化
		com = new Communicator();
	}
	
	public static function load(msg:String) {
		// 床に使用するテクスチャを先に読み込み(タイトルで使うため)
		var loadTexture = new DefaultGround(GameMgr.DIRECTORY + "images/grasslight-big.jpg", function() {
			// 各シーンの最初に呼ぶメソッドを呼ぶ
			for (s in Type.allEnums(EScene)) {
				sceneMap.get(s).once();
			}
			// シーンの切り替え
			GameMgr.changeScene(EScene.TitleScene, [msg]);
		});	
		
	}
	
	static var raf:Int;
	private static function update(timestamp:Float) {
		context.clearRect(0, 0, WIDTH, HEIGHT);
		var t = timestamp - lastTimestamp;
		if (t < 0) t = 0;
		scene.update(t);
		
		renderer.clear();
		renderer.setViewport(0, 0, WIDTH, HEIGHT);
		renderer.render(scene.scene, scene.camera);
		
		//stats.update();
		lastTimestamp = timestamp;
		raf = Browser.window.requestAnimationFrame(update);
	}
	
	/**
	 * シーンを変更する
	 * @param nextScene 次のシーン
	 * @param args シーンに渡す値の配列(null可)
	 */
	public static function changeScene(nextScene:EScene, ?args:Array<Dynamic>) {
		args = if (args == null) [] else args;	//	argsがnullなら空の配列
		scene.clear();
		nowScene = nextScene;
		scene = sceneMap.get(nextScene);
		scene.init(args);
	}
	
	public static function setBackColor(rgb) {
		renderer.setClearColor( new three.Color( rgb ) );
	}
	
	public static function getAnisotropy() {
		return renderer.getMaxAnisotropy();
	}
	
	private static var numbers:Array<Bool> = [false, false, false, false];
	public static function playerEntry(id:String, name:String):String {
		if (entryNum >= maxPlayer) {
			return "max";
		} else if (playing) {
			return "play";
		} else {
			entryNum++;
			var number = 0;
			for ( i in 0...numbers.length) {
				if (!numbers[i]) {
					numbers[i] = true;
					number = i;
					break;
				}
			}
			// プレイヤーの作成・追加
			var reticle = new Reticle(WIDTH / 2, HEIGHT / 2, "#" + playerColor[number]);
			scene.addObj2D(reticle);
			players.set(id, new Player(id, name, reticle, number, Std.parseInt("0x" + playerColor[number])));
			audio.playSE("entry");
			return "" + number;
		}
	}
	
	public static function playerShot(id:String) {
		scene.playerShot(players.get(id));
	}
	
	public static function playerMove(id:String, radian:String, distance:String) {
		var player:Player = players.get(id);
		if (player != null) player.move(Std.parseFloat(radian), Std.parseFloat(distance));
	}
	
	public static function playerReady(id:String, msg:String) {
		var player = players.get(id);
		if (player != null) {
			player.changeReady(msg);
		}
		audio.playSE("ready");
	}
	
	public static function isReadyAll() :Bool {
		var readys = 0;
		for (p in players) {
			if (p.isReady()) readys++;
		}
		if (readys == entryNum && readys != 0) return true else return false;
	}
	
	public static function playerExit(id:String) {
		var player = players.get(id);
		if (player != null) {
			player.setRetire();
			numbers[player.getNumber()] = false;
			scene.removeObj2D(player.getObj());
			players.remove(id);
			entryNum--;
		}
	}
	public static function allReady() {
		playing = true;
		com.allReady();
	}
	public static function resetReady() {
		for (p in GameMgr.players) {
			p.reset();
		}
		playing = false;
		com.reset();
	}
	public static function isTeamBattle() : Bool{
		return teamBattle;
	}
	public static function setTeamBattle(flag:Bool) {
		teamBattle = flag;
	}
	public static function setRank(rank:String) {
		var absScene = sceneMap.get(EScene.ResultRankScene);
		var rankScene = cast(absScene, ResultRankScene);
		rankScene.setRank(rank);
	}
	public static function sendPlayerRank(player:Player, stageId:Int) {
		com.sendRank(player.getId(), stageId, player.getScore());
	}
	public static function isOpenRandom() {
		return openRandom;
	}
	public static function setOpenRandom(flag:Bool) {
		openRandom = flag;
	}
	public static function isHardClear() {
		return hardClear;
	}
	public static function setHardClear(flag:Bool) {
		hardClear = flag;
	}
}