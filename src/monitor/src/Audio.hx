package;

import js.Browser;
import js.html.AudioElement;

/**
 * 音楽用クラス
 * @author kbc13a14
 * @version 1.1
 */
class Audio {
	private static var inst:Audio = null;
	
	
	//	同時に鳴らせるSEの数
	private static inline var SE_ELEMENT_NUM = 4; // 必ず1以上
	
	//	状態
	private static inline var STATE_NOT_INITIALIZED = 0;
	private static inline var STATE_PLAY = 1;
	private static inline var STATE_STOP = 2;
	private static inline var STATE_PAUSE = 3;
	
	private var bgmAudioElement:AudioElement;
	private var seAudioElements:Array<AudioElement> = new Array<AudioElement>();
	private var seCount:Int = 0;
	
	private var bgms:Map<String, String> = new Map<String, String>();
	private var ses:Map<String, String> = new Map<String, String>();
	
	private var bgmState = STATE_NOT_INITIALIZED;
	
	private var mainMute = false;
	private var bgmMute = false;
	private var seMute = false;
	private var mainVolume = 0.5;
	private var bgmVolume = 0.5;
	private var seVolume = 0.5;
	
	public static function getInstance() {
		if (inst == null) {
			inst = new Audio();
		}
		return inst;
	}
	
	private function new() {
		bgmAudioElement = cast(Browser.document.createElement('audio'), AudioElement);
		bgmAudioElement.id = 'bgm';
		Browser.document.body.appendChild(bgmAudioElement);
		for (i in 0...SE_ELEMENT_NUM) {
			seAudioElements[i] = cast(Browser.document.createElement('audio'), AudioElement);
			seAudioElements[i].id = 'se' + i;
			Browser.document.body.appendChild(seAudioElements[i]);
		}
		changeVolume();
	}
	
	//	BGM
	
	public function appendBGM(name:String, src:String):Void {
		bgms.set(name, src);
	}
	
	public function playBGM(name:String):Void {
		var src = bgms.get(name);
		if (src == null) throw "Unknown BGM : " + name;
		bgmAudioElement.src = src;
		bgmAudioElement.loop = true;
		bgmAudioElement.play();
		bgmState = STATE_PLAY;
	}
	
	public function stopBGM():Void {
		if (bgmState == STATE_PLAY || bgmState == STATE_PAUSE) {
			bgmAudioElement.pause();
			bgmAudioElement.currentTime = 0;
			bgmState = STATE_STOP;
		}
	}
	
	public function pauseBGM():Void {
		if (bgmState == STATE_PLAY) {
			bgmAudioElement.pause();
			bgmState = STATE_PAUSE;
		}
	}
	
	public function resumeBGM():Void {
		if (bgmState == STATE_PAUSE) {
			bgmAudioElement.play();
			bgmState = STATE_PLAY;
		}
	}
	
	public function isPlayBGM():Bool {
		return bgmState == STATE_PLAY;
	}
	
	public function isStopBGM() {
		return bgmState == STATE_STOP || bgmState == STATE_NOT_INITIALIZED;
	}
	
	public function isPauseBGM() {
		return bgmState == STATE_PAUSE;
	}
	
	//	SE
	
	public function appendSE(name:String, src:String):Void {
		ses.set(name, src);
	}
	
	public function playSE(name:String):Void {
		var src = ses.get(name);
		if (src == null) throw "Unknown SE : " + name;
		seAudioElements[seCount].src = src;
		seAudioElements[seCount].play();
		seCount = (seCount + 1) % 4;
	}
	
	//	volume
	
	public function setMuteMain(mute:Bool):Void {
		mainMute = mute;
		changeVolume();
	}
	public function setMuteBGM(mute:Bool):Void {
		bgmMute = mute;
		changeVolume();
	}
	public function setMuteSE(mute:Bool):Void {
		seMute = mute;
		changeVolume();
	}
	
	public function isMuteMain():Bool {
		return mainMute;
	}
	public function isMuteBGM():Bool {
		return bgmMute;
	}
	public function isMuteSE():Bool {
		return seMute;
	}
	
	public function setVolumeMain(volume:Float) {
		if (volume < 0.0) volume = 0.0;
		if (volume > 1.0) volume = 1.0;
		mainVolume = volume;
		changeVolume();
	}
	public function setVolumeBGM(volume:Float) {
		if (volume < 0.0) volume = 0.0;
		if (volume > 1.0) volume = 1.0;
		bgmVolume = volume;
		changeVolume();
	}
	public function setVolumeSE(volume:Float) {
		if (volume < 0.0) volume = 0.0;
		if (volume > 1.0) volume = 1.0;
		seVolume = volume;
		changeVolume();
	}
	
	public function getVolumeMain():Float {
		return mainVolume;
	}
	public function getVolumeBGM():Float {
		return bgmVolume;
	}
	public function getVolumeSE():Float {
		return seVolume;
	}
	
	private function changeVolume() {
		bgmAudioElement.volume = mainMute ? 0.0 : bgmMute ? 0.0 : mainVolume * bgmVolume;
		for (i in 0...seAudioElements.length) {
			seAudioElements[i].volume = mainMute ? 0.0 : seMute ? 0.0 : mainVolume * seVolume;	
		}
		
	}
}