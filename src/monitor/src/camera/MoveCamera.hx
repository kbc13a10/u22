package camera;

import position.AbstractPosition;
import position.AbstractPosition.PositionParameter;
import three.Vector3;

class MoveCamera extends AbstractCamera {
	private var visualRange:Int; // カメラの視点距離
	private var positionY:Float; // 床からの距離
	private var target:Vector3; // 視点位置
	public function new(?vr:Int = 1500, ?py:Float = 350) {
		super();
		visualRange = vr;
		target = new Vector3(0, 0, visualRange);
		positionY = py;
		position.y = positionY;
	}
	override public function setPosition(data:PositionParameter) {
		var point = data.point;
		var radian = Math.PI / 180 * data.deg;
		position.copy(point);
		position.y += positionY;
		target.copy(point);
		target.x += Math.cos(radian) * visualRange;
		target.z += Math.sin(radian) * visualRange;
		lookAt(target);
	}
	override public function getTarget(data:PositionParameter) : PositionParameter {
		var radian = Math.PI / 180 * data.deg;
		var point = data.point.clone();
		point.x += Math.cos(radian) * visualRange;
		point.z += Math.sin(radian) * visualRange;
		return {point:point, deg:data.deg};
	}
}