package camera;

import position.AbstractPosition.PositionParameter;
import three.PerspectiveCamera;
import three.Vector3;

class AbstractCamera extends PerspectiveCamera {
	public function new() {
		super(60, GameMgr.WIDTH / GameMgr.HEIGHT, 1, 50000);
	}
	public function setPosition(data:PositionParameter) {}
	public function getTarget(data:PositionParameter) : PositionParameter {
		return { point:data.point.clone(), deg:data.deg };
	}
	//public function setPosition(data:{point:Vector3, deg:Float}) {}
}