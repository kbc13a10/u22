package shape;
import shape.AbstractShape;
import three.CylinderGeometry;
import three.Euler;
import three.Face3;
import three.Geometry;
import three.Material;
import three.MeshFaceMaterial;
import three.Vector3;
import three.ConvexGeometry;
import three.MeshLambertMaterial;
import three.MeshPhongMaterial;
import three.Mesh;
import three.GeometryUtils;

typedef CrystalParameters = {
    @:optional var size : Int;		// 大きさ
	@:optional var ratio : Float;	// 高さと幅の比率
	@:optional var color : Int;		// 16進数の色
	@:optional var randomColor : Bool; // 色をランダムに
}
// クリスタル
class CrystalShape extends AbstractShape {
	private var size:Int;
	private var ratio:Float;
	private var c:Int;
	private var random:Bool = false;
	public function new(?score:Int = 750, ?data:CrystalParameters) {
		super(score);
		name = 'crystal';
		if (data == null) data = {};
		size = if (data.size != null) data.size else 50;
		ratio = if (data.ratio != null) data.ratio else 1.5;
		c = if (data.color != null) data.color else 0xff015c;
		if (data.randomColor == true) {
			c = Std.int(0xffffff * Math.random());
			random = true;
		}
		
		var group = new Geometry();
		
		var leftSin = Math.sin(Math.PI / 6);
		var leftCos = Math.cos(Math.PI / 6);
		var rightSin = Math.sin(Math.PI / 6 * 5);
		var rightCos = Math.cos(Math.PI / 6 * 5);
		var points = [
			new three.Vector3( 0, 0, 0 ),
			new three.Vector3( 0, size * ratio, size ),
			new three.Vector3( -size, size * ratio, 0 ),
			new three.Vector3( 0, size * ratio, -size ),
			new three.Vector3( size, size * ratio, 0 ),
			new three.Vector3( 0, size * ratio * 2, 0 ),
		];
		
		shapeWidth = size * 2;
		shapeHeight = size * ratio * 2;
		
		group.vertices = points;
		group.faces = [];
		for (i in 1...5) {
			group.faces.push(
				new Face3(0, i, if(i != 4) i + 1 else 1)
			);
		}
		for (i in 1...5) {
			var face = new Face3(5, i, if (i != 4) i + 1 else 1);
			face.materialIndex = 1;
			group.faces.push(face);
		}
		group.computeFaceNormals();
		geometry = group;
		
		var material1 = new MeshLambertMaterial( 
			{ 
				  color: c
				, transparent:true
				, opacity: 0.6
				, ambient:c
				, side: 0
			}
		);
		var material2 = new MeshLambertMaterial( 
			{ 
				  color: c
				, transparent:true
				, opacity: 0.6
				, ambient:0x990000
				, side: 1
			}
		);
		var materialArray:Array<Material> = [material1, material2];
		material = new MeshFaceMaterial(materialArray);
	}
	override public function clone():AbstractShape {
		return new CrystalShape(score, {size:size, ratio:ratio, color: c, randomColor: random});
	}
}