package shape;
import three.Geometry;
import three.Geo;
import three.Material;
import three.Mesh;
import three.Vector3;
import three.Euler;

class AbstractShape {
	private var geometry:Geo = new Geometry();
	private var material:Material = new Material();
	private var name:String = 'abstract';
	private var score:Int;
	private var shapeWidth:Float = 0;
	private var shapeHeight:Float = 0;
	public function new(s:Int = 0) {
		score = s;
	}
	public function getGeometry() {
		return geometry;
	}
	public function getMaterial() {
		return material != null ? material : null;
	}
	public function init(posi:Vector3, rote:Euler) { }
	public function getName() {
		return name;
	}
	public function clone() : AbstractShape {
		return new AbstractShape();
	}
	public function getScore() {
		return this.score;
	}
	public function getWidth() {
		return shapeWidth;
	}
	public function getHeight() {
		return shapeHeight;
	}
}