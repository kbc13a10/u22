package shape;
import shape.AbstractShape;
import three.Euler;
import three.Mesh;
import three.PlaneBufferGeometry;
import three.MeshPhongMaterial;
import three.Vector3;
import three.CircleGeometry;

// 床
class FloorShape extends AbstractShape {
	private var points:Array<Vector3>;
	private var corectionValue:Float;
	private var radius:Int;
	private var correctionValue:Float;
	public function new(mat, points:Array<Vector3>, radius:Int, ?correctionValue = 0.0)  {
		super();
		name = 'floor';
		material = mat;
		this.points = points;
		this.correctionValue = correctionValue;
		this.radius = radius;
		if (points.length == 2) {
			var sp = points[0]; // startPoint
			var ep = points[1]; // endPoint
			var disX = ep.x - sp.x;
			var disZ = ep.z - sp.z;
			var distance = Math.sqrt(disX * disX + disZ * disZ);
			geometry = new PlaneBufferGeometry(radius * 2, distance);
		} else {
			geometry = new CircleGeometry(radius, 16);
		}
	}
	override public function init(posi:Vector3, rote:Euler) {
		rote.x = -Math.PI / 2;
		if (points.length == 2) {
			var sp = points[0]; // startPoint
			var ep = points[1]; // endPoint
			var radian = Math.atan2(sp.z - ep.z, ep.x - sp.x);
			posi.set(
				sp.x + (ep.x - sp.x) / 2, 
				0 - correctionValue,
				sp.z + (ep.z - sp.z) / 2
			);
			rote.z = radian - Math.PI / 2;
		} else {
			posi.copy(points[0]);
			posi.y = 0 - correctionValue;
		}
	}
	override public function clone():AbstractShape {
		return new FloorShape(material, points, radius, correctionValue);
	}
}