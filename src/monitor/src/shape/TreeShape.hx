package shape;
import three.CylinderGeometry;
import three.Euler;
import three.Mesh;
import three.Vector3;
import three.LatheGeometry;
import three.MeshLambertMaterial;
import three.MeshFaceMaterial;
import three.Material;

class TreeShape extends AbstractShape {
	private var radius = 150;
	private var height = 500;
	private var greenColor = 0x00ff00;
	private var trunkColor = 0x9A6D4B;
	public function new() {
		super(0);
		var bottom = height / 5;
		var green = height - bottom;
		var points = [
			new Vector3(radius / 5, 0, -height / 2 + bottom),
			new Vector3(radius, 0, -height / 2 + bottom - height / 20),
			new Vector3(radius / 3, 0, -height / 12),
			new Vector3(radius / 5 * 4, 0, -height / 12 - height / 20),
			new Vector3(radius / 3, 0, height / 10 * 1.5),
			new Vector3(radius / 3 * 2, 0, height / 10 * 1.5  - height / 20),
			new Vector3(0, 0, height / 2),
		];
		var greenGeo = new LatheGeometry(points, 6);
		var trunk = new CylinderGeometry(radius / 5, radius / 5, bottom, 6);
		for (f in trunk.faces) {
			f.materialIndex = 1;
		}
		var trunkMesh = new Mesh(trunk);
		trunkMesh.rotation.z -= Math.PI / 2;
		trunkMesh.position.z -= height / 2 - bottom / 2;
		trunkMesh.rotation.y -= Math.PI / 2;
		
		shapeWidth = radius * 2;
		shapeHeight = height;
		
		greenGeo.mergeMesh(trunkMesh);
		geometry = greenGeo;
		var colorMaterials:Array<Material> = [
			new MeshLambertMaterial( { color:greenColor } ),
			new MeshLambertMaterial( { color:trunkColor } ),
		];
		material = new MeshFaceMaterial(colorMaterials);
	}
	override public function init(posi:Vector3, rote:Euler) {
		rote.x = -Math.PI / 2;
		posi.y += height / 2;
	}
}