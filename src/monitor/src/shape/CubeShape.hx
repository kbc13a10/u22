package shape;
import shape.AbstractShape;
import three.BoxGeometry;
import three.Euler;
import three.Vector3;

typedef CubeParameters = {
	@:optional var size : Float; // 大きさ
	@:optional var color : Int; // 16進数の色
	@:optional var opacity : Float; // 透過度
	@:optional var name : String;
}
class CubeShape extends AbstractShape {
	private var size:Float;
	private var c:Int;
	private var op:Float;
	public function new(?score:Int = 500,?data:CubeParameters) {
		super(score);
		if (data == null) data = { };
		name = if(data.name != null) data.name else 'cube';
		size = if (data.size != null) data.size else 100;
		c = if (data.color != null) data.color else 0x00ff00;
		op = if (data.opacity != null) data.opacity else 0.7;
		
		shapeWidth = size;
		shapeHeight = size;
		
		geometry = new BoxGeometry(size, size, size);
		material = new three.MeshLambertMaterial( 
			{ 
				  color: c
				, transparent:true
				, opacity: op
				, ambient:0x990000 
			}
		);
	}
	override public function init(posi:Vector3, rote:Euler) {
		posi.y += size / 2;
	}
	override public function clone():AbstractShape {
		return new CubeShape(score, {size:size, color:c, opacity: op});
	}
}