package shape;
import shape.AbstractShape;
import three.CylinderGeometry;
import three.Euler;
import three.MeshLambertMaterial;
import three.Vector3;

typedef CylinderShapeParameter = {
	@:optional var radiusTop : Float; // 上面の半径
	@:optional var radiusBottom : Float; // 下面の半径
	@:optional var height : Float; // 高さ
	@:optional var color : Int; // 16進数の色
	@:optional var opacity : Float; // 透過度
	@:optional var defaultRotaY : Float; // デフォルトの角度
}

class CylinderShape extends AbstractShape {
	private var radiusTop : Float = 50;
	private var radiusBottom : Float = 50;
	private var height : Float = 20;
	private var segments : Float = 50;
	private var color : Int = 0xFFD700;
	private var opacity : Float = 0.7;
	private var defaultRotaY : Float = 0;
	public function new(?score:Int = 800,?params:CylinderShapeParameter) {
		super(score);
		name = 'cylinder';
		if (params == null) params = { };
		if (params.radiusTop != null) radiusTop = params.radiusTop;
		if (params.radiusBottom != null) radiusBottom = params.radiusBottom;
		if (params.height != null) height = params.height;
		if (params.color != null) color = params.color;
		if (params.opacity != null) opacity = params.opacity;
		if (params.defaultRotaY != null) this.defaultRotaY = params.defaultRotaY;
		
		shapeWidth = (Math.max(radiusTop, radiusBottom) * 2);
		shapeHeight = (Math.max(radiusTop, radiusBottom) * 2);
		
		geometry = new CylinderGeometry(radiusTop, radiusBottom, height, segments);
		material = new MeshLambertMaterial( 
			{ 
				  color: color
				, transparent:true
				, opacity: opacity
				, ambient:0x990000 
			}
		);
	}
	override public function init(posi:Vector3, rote:Euler) {
		rote.z = -Math.PI / 2;
		posi.y += if (radiusTop > radiusBottom) radiusTop else radiusBottom;
		rote.y = defaultRotaY;
	}
	override public function clone():AbstractShape {
		return new CylinderShape(score, {
			radiusTop : radiusTop,
			radiusBottom : radiusBottom,
			height : height,
			color : color,
			opacity : opacity,
			defaultRotaY : defaultRotaY
		});
	}
}