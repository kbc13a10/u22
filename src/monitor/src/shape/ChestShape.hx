package shape;

import shape.AbstractShape;
import three.BoxGeometry;
import three.Euler;
import three.Vector3;
import three.ImageUtils;
import three.MeshPhongMaterial;


/**
 * ...
 * @author hoshi
 */

typedef ChestParameters = {
	@:optional var Xsize : Float; // 大きさ
	@:optional var Ysize : Float;
	@:optional var Zsize : Float;
	@:optional var opacity : Float; // 透過度
}
class ChestShape extends AbstractShape {
	private var Xsize:Float;
	private var Ysize:Float;
	private var Zsize:Float;
	
	private var op:Float;
	
	public function new(?score:Int = 0,?data:ChestParameters) 
	{
		super(score);
		name = 'chest';
		if (data == null) data = { };
		Xsize = if (data.Xsize != null) data.Xsize else 100;
		Ysize = if (data.Ysize != null) data.Ysize else 100;
		Zsize = if (data.Zsize != null) data.Zsize else 100;
		op = if (data.opacity != null) data.opacity else 0.7;
		
		shapeWidth = Xsize;
		shapeHeight = Ysize;
		
		var chestTexture = three.ImageUtils.loadTexture( GameMgr.DIRECTORY + "images/Brick256.jpg" );
		chestTexture.wrapS = chestTexture.wrapT = Three.WrappingMode.RepeatWrapping;
		//groundTexture.repeat.set( 256, 256 );
		chestTexture.anisotropy = GameMgr.getAnisotropy();
		//chestTexture.anisotropy = 16;
		geometry = new BoxGeometry(Xsize, Ysize, Zsize);
		material = new three.MeshPhongMaterial( { map: chestTexture, shininess:10 } );
	}
	override public function init(posi:Vector3, rote:Euler) {
		posi.y += Ysize / 2;
	}
	override public function clone():AbstractShape {
		return new ChestShape(score, { Xsize:Xsize, Ysize:Ysize, Zsize:Zsize, opacity: op } );
	}
}