package shape;
import js.html.Text;
import shape.AbstractShape;
import three.Euler;
import three.MeshBasicMaterial;
import three.TextGeometry;
import three.MeshFaceMaterial;
import three.Material;
import three.Vector3;

typedef TextParameter = {
	var text:String;	// 表示する文字
	@:optional var frontColor:Int;	// 全面カラー
	@:optional var SideColor:Int;	// 側面カラー
	@:optional var size:Int;		// 大きさ
	@:optional var height:Int;		// 高さ
	@:optional var weight:String;	// 字の太さ(normal, bold)
	@:optional var style:String;	// スタイル(normal, italics)
	@:optional var bevelThickness:Int;	// 押し出し量。厚さ
	@:optional var opacity:Float;
}
class TextShape extends AbstractShape {
	private var text:String = 'text';
	private var frontColor:Int = 0x00ff00;
	private var sideColor:Int = 0x0088ff;
	private var size:Int = 30;
	private var height:Int = 4;
	private var weight:String = "bold";
	private var font:String = "helvetiker";
	private var style:String = "normal";
	private var bevelThickness:Int = 1;
	private var opacity:Float = 1;
	public function new(param:TextParameter) {
		super();
		name = 'text';
		text = param.text;
		if (param.SideColor != null) sideColor = param.SideColor;
		if (param.frontColor != null) frontColor = param.frontColor;
		if (param.size != null) size = param.size;
		if (param.height != null) height = param.height;
		if (param.weight != null) weight = param.weight;
		if (param.style != null) style = param.style;
		if (param.bevelThickness != null) bevelThickness = param.bevelThickness;
		if (param.opacity != null) opacity = param.opacity;
		var materialFront = new MeshBasicMaterial( { color:frontColor, transparent:true, opacity: opacity } );
		var materialSide = new MeshBasicMaterial( { color:sideColor, transparent:true, opacity: opacity } );
		var materialArray:Array<Material> = [materialFront, materialSide];
		geometry = new TextGeometry(text, {
			size:size, height: height, curveSegments:3,
			font:font, weight:weight, style:style,
			bevelThickness:bevelThickness, bevelSize:2, bevelEnabled:true,
			material:0, extrudeMaterial:1
		});
		material = new MeshFaceMaterial(materialArray);
	}
	override public function clone():AbstractShape {
		return new TextShape( {
			text:text,
			frontColor:frontColor,
			SideColor:sideColor,
			size:size,
			height:height,
			weight:weight,
			style:style,
			bevelThickness:bevelThickness
		});
	}
}