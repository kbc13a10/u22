package shape;
import shape.AbstractShape;
import three.CylinderGeometry;
import three.MeshLambertMaterial;
import three.MeshNormalMaterial;
import three.Vector3;
import three.Euler;

typedef HexagonalParameter = {
	@:optional var color : Int; // 16進数の色
	@:optional var opacity : Float; // 透過度
	@:optional var defaultRotaY : Float; // デフォルトの角度
}
class HexagonalShape extends AbstractShape {
	private var height: Float = 3;
	private var radius: Float = 75;
	private var segments : Float = 6;
	private var color : Int = 0xffffff;
	private var opacity : Float = 1;
	private var defaultRotaY : Float = Math.PI / 2;
	public function new(?s:Int=0, ?params:HexagonalParameter) {
		super(s);
		name = 'Hexagonal';
		if (params == null) params = { };
		if (params.color != null) color = params.color;
		if (params.opacity != null) opacity = params.opacity;
		if (params.defaultRotaY != null) this.defaultRotaY = params.defaultRotaY;
		
		shapeWidth = radius * 2;
		shapeHeight = radius * 2;
		
		geometry = new CylinderGeometry(radius, radius, height, segments);
		material = new MeshNormalMaterial( 
		);
	}
	override public function init(posi:Vector3, rote:Euler) {
		rote.z = -Math.PI / 2;
		posi.y += radius;
		rote.y = defaultRotaY;
	}
	override public function clone() {
		return new HexagonalShape(score, { color: color, opacity:opacity, defaultRotaY:defaultRotaY } );
	}
}