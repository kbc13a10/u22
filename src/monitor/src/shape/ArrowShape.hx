package shape;
import shape.AbstractShape;
import three.ConvexGeometry;
import three.Euler;
import three.MeshPhongMaterial;
import three.Vector3;
import three.Mesh;

typedef ArrowShapeParameter = {
	@:optional var size : Float; //一辺の長さ
	@:optional var height : Float; // 高さ
	@:optional var color : Int; // 16進数の色
	@:optional var opacity : Float; // 透過度
	@:optional var direction : Float; // 方向
}
 
class ArrowShape extends AbstractShape {
	private var size :Float = 5;
	private var color : Int = 0x029FDD;
	private var height : Float = 25;
	private var direction : Float = 0;
	
	public function new(score:Int=0, ?params:ArrowShapeParameter) {
		super(score);
		if (params == null) params = { };
		if (params.size != null) size = params.size;
		if (params.height != null) height = params.height;
		if (params.color != null) color = params.color;
		if (params.direction != null) direction = params.direction;
		
		var points;
		//Math.cos(0 - Math.PI / 2) * radius - (Math.cos(0 + Math.PI / 2) * radius);
		
		 points = [
			new Vector3(0, 0, 0),
			new Vector3(0, size, 0),
			//new Vector3(Math.cos(Math.PI / 180 * direction) * height, 0, Math.sin(Math.PI / 180 * direction) * height),
			//new Vector3(Math.cos(Math.PI / 180 * (direction + 90)) * size, 0, Math.sin(Math.PI / 180 * (direction + 90)) * size),
			//new Vector3(Math.cos(Math.PI / 180 * (direction - 90)) * size, 0, Math.sin(Math.PI / 180 * (direction - 90)) * size),
			//new Vector3(Math.cos(Math.PI / 180 * (direction + 180)) * size, 0, Math.sin(Math.PI / 180 * (direction + 180)) * size),
			new Vector3(height, 0, 0),
			new Vector3(0, 0, size),
			new Vector3(0, 0, -size),
			new Vector3(-size, 0, 0),
			new Vector3(0, -size, 0)
		];
	  
		geometry = new ConvexGeometry(points);
		material = new MeshPhongMaterial( { color: color, shininess:5, specular:0xB3D1DD, metal:true, ambient:0xBED5DD } );
		//material = new MeshPhongMaterial( { color: 0x9DCCE0, shininess:30, specular:0, metal:true, emissive:0x9DCCE0 } );
	}
	override public function init(posi:Vector3, rote:Euler) {
		//rote.z = Math.PI / 2;
		posi.y = size;
		rote.y = -Math.PI / 180 * direction;
	}
	override function clone() : AbstractShape {
		return new ArrowShape({size: size, color: color, height: height, direction: direction});
	}
}