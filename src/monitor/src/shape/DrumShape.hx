package shape;
import haxe.ds.Vector;
import three.Geometry;
import three.GeometryUtils;
import three.LatheGeometry;
import three.Mesh;
import three.MeshLambertMaterial;
import three.Vector3;
import three.Euler;
import three.CylinderGeometry;

typedef DrumParameter = {
	@:optional var radius : Float; // 円の半径
	@:optional var height : Float; // 高さ
	@:optional var sideColor : Int; // 色
}

class DrumShape extends AbstractShape {
	private var radius : Float = 60;
	private var height : Float = 150;
	private var sideColor : Int = 0xA0522D;
	public function new(s:Int=0, ?params:DrumParameter) {
		super(s);
		if (params == null) params = { };
		if (params.radius != null) radius = params.radius;
		if (params.height != null) height = params.height;
		if (params.sideColor != null) sideColor = params.sideColor;
		
		shapeWidth = radius * 2;
		shapeHeight = height;
		
		var convex = height / 50;
		var points = [
			new Vector3(0, 0, -height / 2 ),
			new Vector3(radius + convex, 0, -height / 2 ),
			new Vector3(radius + convex, 0, -height / 2 + convex ),
			new Vector3(radius, 0, -height / 2 + convex ),
			new Vector3(radius, 0, -convex / 2 ),
			new Vector3(radius + convex, 0, -convex / 2 ),
			new Vector3(radius + convex, 0, convex / 2 ),
			new Vector3(radius, 0, convex / 2 ),
			new Vector3(radius, 0, height / 2 - convex ),
			new Vector3(radius + convex, 0, height / 2 - convex ),
			new Vector3(radius + convex, 0, height / 2 ),
			new Vector3(radius - convex, 0, height / 2 ),
			new Vector3(radius - convex, 0, height / 2 - convex),
			new Vector3(0, 0, height / 2 - convex),
		];
		geometry = new LatheGeometry(points, 16);
		material = new MeshLambertMaterial({color:sideColor});
	}
	override public function init(posi:Vector3, rote:Euler) {
		rote.x = -Math.PI / 2;
		posi.y += height / 2;
	}
	override public function clone():AbstractShape {
		return new DrumShape(score, {
			radius : radius,
			height : height,
			sideColor : sideColor
		});
	}
}