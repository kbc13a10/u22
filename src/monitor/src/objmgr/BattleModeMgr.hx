package objmgr;
import js.html.CanvasRenderingContext2D;
import obj2d.AbstractObj2d;
import obj2d.TargetRect;
import obj2d.RadioButton;

class BattleModeMgr {
	private var modes:List<RadioButton> = new List<RadioButton>();
	private var startButton:TargetRect;
	private var selectMode = EBattleMode.none;
	private var radioId:String = 'modeType';
	private var y:Float;
	private var hardMode:RadioButton;
	private var randomMode:RadioButton;
	private var modeWidth:Float;
	
	public function new(y:Float) {
		this.y = y;
		modeWidth = GameMgr.WIDTH / 20 * 3;
		var hardX = GameMgr.isOpenRandom() ? GameMgr.WIDTH / 3 - modeWidth / 2 : GameMgr.WIDTH / 2 - modeWidth / 2;
		hardMode = new RadioButton(hardX, y, radioId, 'Hard', null, modeWidth, false);
		var normalMode = new RadioButton(GameMgr.WIDTH / 4 * 3 - modeWidth / 2, hardMode.y + hardMode.getHeight() * 2, radioId, 'Normal', null, modeWidth, false);
		var easyMode = new RadioButton(GameMgr.WIDTH / 4 - modeWidth / 2, hardMode.y + hardMode.getHeight() * 2, radioId, 'Easy', null, modeWidth, false);
		modes.add(hardMode);
		modes.add(normalMode);
		modes.add(easyMode);
		randomMode = new RadioButton(GameMgr.WIDTH / 3 * 2 - modeWidth / 2, y, radioId, 'Random', null, modeWidth, false);

		startButton = new TargetRect(GameMgr.WIDTH / 2 - (modeWidth * 1.5) / 2, GameMgr.HEIGHT / 5 * 4, 'GameStart', null, modeWidth * 1.5, '#ffff00');
	}
	
	public function draw(context:CanvasRenderingContext2D, time) {
		if (GameMgr.isReadyAll() && GameMgr.entryNum > 0) {
			if (GameMgr.isOpenRandom()) {
				hardMode.setX(GameMgr.WIDTH / 3 - modeWidth / 2);
				randomMode.draw(context, time);
			} else {
				hardMode.setX(GameMgr.WIDTH / 2 - modeWidth / 2);
			}
			for (m in modes) {
				m.draw(context, time);
			}
			if (!Type.enumEq(selectMode, EBattleMode.none)) {
				startButton.draw(context, time);
			}
		}
	}
	
	public function checkHit(px:Float, py:Float):Bool {
		if (!GameMgr.isReadyAll()) return false;
		var lists = modes;
		if (GameMgr.isOpenRandom()) {
			lists.add(randomMode);
		}
		for (m in lists) {
			if (m.checkHit(px, py)) {
				switch(m.getText()) {
					case 'Hard':
						selectMode = EBattleMode.Hard;
					case 'Normal':
						selectMode = EBattleMode.Normal;
					case 'Easy':
						selectMode = EBattleMode.Easy;
					case 'Random':
						selectMode = EBattleMode.Ran;
				}
				var audio = Audio.getInstance();
				audio.playSE('start');
				return true;
			}
		}
		return false;
	}
	
	public function getSelectMode(px:Float, py:Float) : EBattleMode {
		if (GameMgr.isReadyAll() && !Type.enumEq(selectMode, EBattleMode.none)) {
			if (startButton.checkHit(px, py)) {
				return selectMode;
			}
		}
		return EBattleMode.none;
	}
	
}