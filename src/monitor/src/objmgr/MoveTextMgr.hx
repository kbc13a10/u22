package objmgr;

import obj2d.MoveText;
import js.html.CanvasRenderingContext2D;

enum ETextType {
	noPlayer;
	inPlayer;
	teamBattle;
}

class MoveTextMgr {
	public var y(default, null):Float;
	private var texts:Map<ETextType, MoveText> = new Map<ETextType, MoveText>();
	private var nowType = ETextType.noPlayer;
	private var textSize:Float;
	public function new(y:Float, textSize:Float = 17, speed:Float = -3, waitTime:Float = 2000) {
		this.y = y;
		this.textSize = textSize;
		var noPlayerText = new MoveText(GameMgr.WIDTH, y, 
			["右上のアドレスにスマホでアクセス！"], textSize, speed, waitTime);
		texts.set(ETextType.noPlayer, noPlayerText);
		var inPlayArray = ["設定で移動速度が変更できます", "全員が準備完了すると難易度選択が出来ます"];
		if (GameMgr.isHardClear()) inPlayArray.unshift("QRコードを10回打つと新しいステージが・・・？");
		var inPlayerText = new MoveText(GameMgr.WIDTH, y, 
			inPlayArray, textSize, speed, waitTime);
		inPlayerText.hideText();
		texts.set(ETextType.inPlayer, inPlayerText);
		var teamBattleArray = ["青ブロックを撃つと青チームになります", "赤ブロックを撃つと赤チームになります", "全員が準備完了すると難易度選択が出来ます"];
		if (GameMgr.isHardClear()) teamBattleArray.unshift("QRコードを10回打つと新しいステージが・・・？");
		var teamBattleText = new MoveText(GameMgr.WIDTH, y, 
			teamBattleArray, textSize, speed, waitTime);
		teamBattleText.hideText();
		texts.set(ETextType.teamBattle, teamBattleText);
	}
	public function update(context:CanvasRenderingContext2D, time:Float) {
		var textType = ETextType.noPlayer;
		if (GameMgr.isTeamBattle()) {
			textType = ETextType.teamBattle;
		} else if (GameMgr.entryNum > 0) {
			textType = ETextType.inPlayer;
		}
		if (!Type.enumEq(textType, nowType)) {
			for (t in texts) {
				t.hideText();
			}
			var showText:MoveText = texts.get(textType);
			showText.initMove();
			showText.showText();
			nowType = textType;
		}
		for (t in texts) {
			t.draw(context, time);
		}
	}
	public function getTextSize() {
		return this.textSize;
	}
}