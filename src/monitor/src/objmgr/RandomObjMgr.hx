package objmgr;

import action.RotateAction;
import action.RotateRandomAction;
import action.SetMoveAction;
import field.FieldObject.FieldObjectParameter;
import obj3d.BreakObject;
import obj3d.random.ChangeCircleObj;
import obj3d.random.ConvexObj;
import obj3d.random.RightLeftObj;
import obj3d.random.RotaObjs;
import obj3d.random.RotaShieldObj;
import obj3d.random.ShieldObj;
import obj3d.random.UpDownObj;
import obj3d.random.VortexObj;
import shape.AbstractShape;
import shape.CrystalShape;
import shape.CubeShape;
import shape.CylinderShape;
import obj3d.random.AbstractRandomObj;

/**
 * ランダムシーンで使用するランダムオブジェクトを管理するクラス
 * 
 */
class RandomObjMgr {
	private var randomObjs:Array<AbstractRandomObj> = [];
	private var random:Random;
	private var seed:UInt = 0;
	/**
	 * x,yの最大距離
	 * @param	visualRange
	 */
	public function new(visualRange:Int) {
		randomObjs.push(new UpDownObj(visualRange));
		randomObjs.push(new RightLeftObj(visualRange));
		randomObjs.push(new VortexObj(visualRange));
		randomObjs.push(new ShieldObj(visualRange));
		randomObjs.push(new RotaShieldObj(visualRange));
		randomObjs.push(new ConvexObj(visualRange));
		randomObjs.push(new ChangeCircleObj(visualRange));
		randomObjs.push(new RotaObjs(visualRange));
	}
	/**
	 * 乱数の初期化
	 */
	public function init() {
		seed = Std.int((Date.now().getTime()));
		random = new Random(seed);
	}
	public function again() {
		random = new Random(seed);
	}
	/**
	 * オブジェクトの取得
	 * @param	appTime　出現時間
	 * @param	radian 向き
	 * @param	displace 調整時間
	 * @return	オブジェクト配列
	 */
	public function getRandomObjs(appTime:Float, radian:Float, ?displace:Float = 0) : Array<FieldObjectParameter> {
		var r = random.nextUInt();
		var objNum = r % randomObjs.length;
		var objs = randomObjs[objNum].getObjs(random.nextUInt(), appTime, radian);
		for ( o in objs) {
			o.rpps += displace;
			o.appTime -= displace;
		}
		return objs;
	}
}