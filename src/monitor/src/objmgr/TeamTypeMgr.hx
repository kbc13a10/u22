package objmgr;

import obj3d.InvincibleObject;
import obj3d.MeshEx;
import three.Object3D;
import three.Scene;
import js.html.CanvasRenderingContext2D;
import obj2d.RadioButton;
import shape.CubeShape;

class TeamTypeMgr {
	private var teamButtons:List<RadioButton> = new List<RadioButton>();
	private var teamBox:List<InvincibleObject> = new List<InvincibleObject>();
	private var show:Bool = false;
	private var redName = 'red';
	private var blueName = 'blue';
	private var y:Float;
	private var singleButton:RadioButton;
	public function new(y:Float) {
		this.y = y;
		// チームボックス
		var redbox = new InvincibleObject(new CubeShape(0, { color:0xFF0000, size:100, name:redName } ));
		redbox.position.x = - 120;
		redbox.position.z = -170;
		var bluebox = new InvincibleObject(new CubeShape(0, { color:0x0000FF, size:100, name:blueName } ));
		bluebox.position.x = 120;
		bluebox.position.z = -170;
		teamBox.add(redbox);
		teamBox.add(bluebox);
		
		// 個人戦ボタン
		singleButton = new RadioButton(GameMgr.WIDTH / 2 - GameMgr.WIDTH / 20 * 3 - 20, y, 'battleType', '個人戦', null, GameMgr.WIDTH / 20 * 3);
		teamButtons.add(singleButton);
		
		// チーム戦ボタン
		var teamButton = new RadioButton(GameMgr.WIDTH / 2 + 20, y, 'battleType', 'チーム戦', null, GameMgr.WIDTH / 20 * 3);
		teamButtons.add(teamButton);
	}
	public function getY() {
		return this.y;
	}
	public function checkType(px:Float, py:Float) {
		// バトルモード選択
		if(!GameMgr.isReadyAll() && GameMgr.entryNum > 2) {
			for (c in teamButtons) {
				if (c.checkHit(px, py)) {
					switch(c.getText()) {
						case '個人戦':
							GameMgr.setTeamBattle(false);
						case 'チーム戦':
							var redTeam = Std.int(GameMgr.entryNum / 2);
							var seted = '';
							var inNumber = '';
							for (p in GameMgr.players) {
								inNumber += p.getNumber();
							}
							while (redTeam > 0) {
								var target = Std.int(Math.random() * GameMgr.entryNum);
								while (seted.indexOf('' + target) != -1 || inNumber.indexOf('' + target) == -1) {
									target = Std.int(Math.random() * GameMgr.entryNum);
								}
								seted += target;
								redTeam--;
							}
							for (p in GameMgr.players) {
								if (seted.indexOf('' + p.getNumber()) != -1) {
									p.setTeamColor(ETeamType.red);
								} else {
									p.setTeamColor(ETeamType.blue);
								}
							}
							GameMgr.setTeamBattle(true);
					}
					var audio = Audio.getInstance();
					audio.playSE('change');
				}
			}
		}
	}
	public function update(obj3d:List<Object3D>, scene:Scene, context:CanvasRenderingContext2D, time:Float) {
		// 個人かチームか選択 表示
		if (!GameMgr.isReadyAll() && GameMgr.entryNum > 2) {
			for (c in teamButtons) {
				c.draw(context, time);
			}
		}
		// チームカラーブロック 追加・削除
		if (GameMgr.isTeamBattle() && !GameMgr.isReadyAll() && GameMgr.entryNum > 2) {
			if (!show) {
				for (b in teamBox) {
					obj3d.add(b);
					scene.add(b);
				}
				show = true;
			}
		} else {
			if (show) {
				for (b in teamBox) {
					obj3d.remove(b);
					scene.remove(b);
				}
				show = false;
			}
		}
		if (GameMgr.isTeamBattle() && GameMgr.entryNum <= 2) {
			GameMgr.setTeamBattle(false);
			singleButton.checked();
		}
	}
	public function checkTeam(player:Player, obj:MeshEx) {
		if (obj.getShapeName() == redName) {
			player.setTeamColor(ETeamType.red);
		} else if (obj.getShapeName() == blueName) {
			player.setTeamColor(ETeamType.blue);
		}
	}
}