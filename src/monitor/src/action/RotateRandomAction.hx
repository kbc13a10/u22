package action;
import action.AbstractAction;
import three.Euler;
import three.Vector3;

/**
 * ...
 * @author taka
 */
class RotateRandomAction extends AbstractAction {
	private var dx : Float = 0;
	private var dy : Float = 0;
	private var speedX : Float;
	private var speedY : Float;
	private var mSpeed : Int;
	public function new(?maxSpeed : Int = 90) {
		super();
		mSpeed = maxSpeed;
		speedX = Math.random() * maxSpeed / 1000;
		speedY = Math.random() * maxSpeed / 1000;
	}
	override public function clone():AbstractAction {
		return new RotateRandomAction(mSpeed);
	}
	override public function update(time:Float, posi:Vector3, rote:Euler) {
		rote.x = Math.PI / 180 * dx;
		rote.y = Math.PI / 180 * dy;
		dx = (dx + speedX * time) % 360;
		dy = (dy + speedY * time) % 360;
	}
}