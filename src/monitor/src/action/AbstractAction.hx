package action;
import three.Mesh;
import three.Vector3;
import three.Euler;

/**
 * ...
 * @author taka
 */
class AbstractAction {
	public function new() {}
	public function update(time:Float, posi:Vector3, rote:Euler) {}
	public function init(posi:Vector3, rote:Euler) { }
	public function clone() : AbstractAction {
		return new AbstractAction();
	}
}