package action;
import action.AbstractAction;
import three.Euler;
import three.Vector3;

enum MoveType {
	x;
	y;
	z;
}
typedef MoveParameter = {
	@:optional var distance : Float;
	@:optional var speed : Float;
	@:optional var reverse : Bool;
	@:optional var moveType : MoveType;
}
class MoveAction extends AbstractAction {
	private var distance : Float = 1000;
	private var speed : Float = 40;
	private var deg : Float = 0;
	private var add : Float = 0;
	private var reverse : Bool = false;
	private var moveType : MoveType = y;
	public function new(?data:MoveParameter) {
		super();
		if (data == null) data = { };
		if (data.distance != null) distance = data.distance;
		if (data.speed != null) speed = data.speed;
		if (data.reverse != null) reverse = data.reverse;
		if (data.moveType != null) moveType = data.moveType;
	}
	override public function update(time:Float, posi:Vector3, rote:Euler) {
		var move = if (!reverse) Math.sin(Math.PI / 180 * deg) * distance else Math.sin(Math.PI / 180 * (360 - deg)) * distance;
		switch(moveType) {
			case x:
				posi.x -= add;
				add = move;
				posi.x += add;
			case y:
				posi.y -= add;
				add = move;
				posi.y += add;
			case z:
				posi.z -= add;
				add = move;
				posi.z += add;
		}
		deg = (deg + (speed * time / 1000)) % 360;
	}
	override public function clone():AbstractAction {
		return new MoveAction({distance: distance, speed: speed, reverse: reverse, moveType: moveType});
	}
}