package action;
import action.AbstractAction;
import three.Euler;
import three.Vector3;
import action.RotateAction.EDirection;

typedef SetRotaParameter = {
	var time:Int; // 持続時間
	var rotation:Int; // 角度
	var direction:EDirection; // 方向
}

class SetRotaAction extends AbstractAction {
	private var rotas:Array<SetRotaParameter>;
	private var splitNum:Int;
	private var elapsedTime:Float = 0;
	private var nowIndex:Int = 0;
	private var cnt:Int = 0;
	private var dRota:Float = 0;
	public function new(?params:Array<SetRotaParameter>, ?splitNum:Int = 20) {
		super();
		this.rotas = [];
		if (params != null && params.length > 0) {
			this.rotas = params;
		} else {
			setDefaultRota();
		}
		this.splitNum = splitNum;
	}
	override public function clone():AbstractAction {
		var array = [];
		for (r in rotas) {
			array.push(r);
		}
		return new SetRotaAction(array, splitNum);
	}
	override public function update(time:Float, posi:Vector3, rota:Euler) {
		elapsedTime += time;
		if (elapsedTime > rotas[nowIndex].time) {
			setDRota(rota);
		}
		if (cnt < splitNum) {
			switch(rotas[nowIndex].direction) {
				case x:
					rota.x += Math.PI / 180 * dRota;
				case y:
					rota.y += Math.PI / 180 * dRota;
				case z:
					rota.z += Math.PI / 180 * dRota;
			}
			cnt++;
			if (cnt == splitNum) {
				switch(rotas[nowIndex].direction) {
					case x:
						rota.x = Math.PI / 180 * rotas[nowIndex].rotation;
					case y:
						rota.y = Math.PI / 180 * rotas[nowIndex].rotation;
					case z:
						rota.z = Math.PI / 180 * rotas[nowIndex].rotation;
				}
			}
		}
	}
	override public function init(posi:Vector3, rota:Euler) {
		setDRota(rota);
	}
	private function setDRota(euler:Euler) {
		elapsedTime = 0;
		nowIndex = (nowIndex + 1) % rotas.length;
		var oldRota = 0;
		switch(rotas[nowIndex].direction) {
			case x:
				oldRota = Std.int(euler.x / Math.PI * 180);
			case y:
				oldRota = Std.int(euler.y / Math.PI * 180);
			case z:
				oldRota = Std.int(euler.z / Math.PI * 180);
		}
		
		cnt = 0;
		var diff:Float = (rotas[nowIndex].rotation - oldRota + 540) % 360 - 180;
		dRota = diff / splitNum;
	}
	private function setDefaultRota() {
		this.rotas = [
			{time : 2000, rotation : 0, direction : y},
			{time : 1000, rotation : 90, direction : y},
			{time : 2000, rotation : 180, direction : y },
			{time : 1000, rotation : 270, direction : y},
		];
	}
}