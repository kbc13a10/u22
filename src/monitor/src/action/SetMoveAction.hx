package action;
import action.AbstractAction;
import three.Euler;
import three.Vector3;

typedef SetMoveParameter = {
	var time:Int; // 持続時間
	var point:Vector3; // 相対位置
}
class SetMoveAction extends AbstractAction{
	private var moveArray:Array<SetMoveParameter>;
	private var nowPoint:Vector3;
	private var elapsedTime:Float = 0;
	private var nowIndex:Int = 0;
	private var loop:Bool;
	private var oldPoint:Vector3;
	private var end:Bool = false;
	public function new(moveArray:Array<SetMoveParameter>, loop:Bool = false) {
		super();
		if (moveArray == null || moveArray.length == 0) throw 'setMove No data';
		this.moveArray = moveArray;
		this.loop = loop;
		nowPoint = new Vector3();
		oldPoint = new Vector3();
	}
	override public function update(time:Float, posi:Vector3, rote:Euler) {
		super.update(time, posi, rote);
		if (end) return;
		elapsedTime += time;
		if (elapsedTime >= moveArray[nowIndex].time) {
			setNext();
		}
		posi.x -= nowPoint.x;
		posi.y -= nowPoint.y;
		posi.z -= nowPoint.z;
		var nextPoint = moveArray[nowIndex].point;
		var nextTime = moveArray[nowIndex].time;
		nowPoint.x = oldPoint.x + nextPoint.x * (elapsedTime / nextTime);
		nowPoint.y = oldPoint.y + nextPoint.y * (elapsedTime / nextTime);
		nowPoint.z = oldPoint.z + nextPoint.z * (elapsedTime / nextTime);
		posi.x += nowPoint.x;
		posi.y += nowPoint.y;
		posi.z += nowPoint.z;
	}
	private function setNext() {
		if (nowIndex + 1 >= moveArray.length && !loop) {
			elapsedTime = moveArray[nowIndex].time;
			end = true;
		} else {
			elapsedTime -= moveArray[nowIndex].time;
			var nextPoint = moveArray[nowIndex].point;
			oldPoint.x += nextPoint.x;
			oldPoint.y += nextPoint.y;
			oldPoint.z += nextPoint.z;
			nowIndex = (nowIndex + 1) % moveArray.length;
		}
		
	}
	override public function clone():AbstractAction {
		var array = [];
		for (m in moveArray) {
			array.push(m);
		}
		return new SetMoveAction(array, loop);
	}
}