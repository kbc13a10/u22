package action;
import action.AbstractAction;
import three.Euler;
import three.Mesh;
import three.Geometry;
import three.Vector3;

enum EDirection {
	x;
	y;
	z;
}

typedef RotateParameters = {
	@:optional var speed : Float;		// 速度
	@:optional var right : Bool;	// 時計回り
	@:optional var direction : EDirection; // 方向
}
class RotateAction extends AbstractAction {
	private var d:Float = 0;
	private var speed:Float;
	private var right:Bool;
	private var direction:EDirection;
	public function new(?data:RotateParameters) {
		super();
		data = if (data != null) data else {} ;
		speed = if (data.speed != null) data.speed else 90;
		right = if (data.right != null) data.right else true;
		direction = if (data.direction != null) data.direction else EDirection.y;
	}
	override public function update(time:Float, posi:Vector3, rote:Euler) {
		
		switch(direction) {
			case x:
				rote.x = Math.PI / 180 * d;
			case y:
				rote.y = Math.PI / 180 * d;
			case z:
				rote.z = Math.PI / 180 * d;
		}
		
		if (right == true) {
			d = (d - (speed * time / 1000)) % 360;
		} else {
			d = (d + (speed * time / 1000)) % 360;
		}
	}
	override public function clone():AbstractAction {
		return new RotateAction({speed: speed, right: right, direction:direction});
	}
}