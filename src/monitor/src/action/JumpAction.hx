package action;
import action.AbstractAction;
import three.Euler;
import three.Mesh;
import three.Geometry;
import three.Vector3;

typedef JumpParameters = {
	@:optional var waitTime : Float;	// 待ち時間
	@:optional var gravity : Float;	//　落下速度
	@:optional var initialVelocity : Float;	// 初速
	//@:optional var initialPositionY : Float;	// 初期位置
	@:optional var up : Bool;	//	上昇するか
}
class JumpAction extends AbstractAction {
	private var waitTime:Float;	// 待機時間 ミリ秒
	private var g:Float;	// 落下速度
	private var vy:Float = 0;	// 速度
	private var iv:Float;	// 初期速度
	private var progress:Float = 0;
	//private var sp:Float;	// 初期ポイント
	private var up:Bool;	// 上がっていくのか
	private var add:Float = 0;	// 増加距離
	private var elapsedTime:Float = 0;
	
	public function new(?data:JumpParameters) {
		super();
		if (data == null) data = {};
		waitTime = if (data.waitTime != null) data.waitTime else 3000;
		g = if (data.gravity != null) data.gravity else 0.08;
		iv = if (data.initialVelocity != null) data.initialVelocity else 8;
		vy = iv;
		up = if (data.up != null) data.up else true;
	}
	override public function clone():AbstractAction {
		return new JumpAction({waitTime: waitTime, gravity: g, initialVelocity:iv, up:up});
	}
	override public function update(time:Float, posi:Vector3, rote:Euler) {
		if (progress >= waitTime) {
			elapsedTime += time;
			var py = posi.y;
			var sign = up ? 1 : -1;
			py += vy * sign;
			add += vy * sign;
			var cnt = 0;
			while (elapsedTime >= 1000 / 60) {
				if (cnt > 0) {
					py += vy * sign;
					add += vy;
				}
				vy -= g;
				elapsedTime -= 1000 / 60;
				cnt++;
			}
			if ((up == true && add <= 0) || (up == false && add >= 0)) {
				progress = 0;
				vy = iv;
				py -= add;
				add = 0;
			}
			posi.setY(py);
		}
		progress += time;
	}
}