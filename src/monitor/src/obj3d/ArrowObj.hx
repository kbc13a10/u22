package obj3d;

import position.AbstractPosition.PositionParameter;
import shape.AbstractShape;
import shape.ArrowShape;
import three.Color;
import three.Quaternion;
import three.Vector3;
import shape.ArrowShape.ArrowShapeParameter;
import three.MeshPhongMaterial;

class ArrowObj extends MeshEx {
	private var visualRange:Int;
	public function new(visualRange:Int, point:Vector3) {
		super(new ArrowShape( { direction: 0 } ));
		this.visualRange = visualRange;
		position.copy(point);
		position.z += 100;
		position.y -= 115 - visualRange / 50;
	}
	override public function hit():Bool {
		return false;
	}
	public function setPosi(point:Vector3, deg:Float) {
		position.copy(point);
		//trace(data);
		var radian = Math.PI / 180 * deg;
		position.x += Math.cos(radian) * 100;
		position.z += Math.sin(radian) * 100;
		position.y -= 115 - visualRange / 50;
		//trace("x:" + Math.cos(radian) * 100);
		//trace("z:" + Math.sin(radian) * 100);
	}
	public function setDirection(nowPoint:Vector3, nextPoint:Vector3) {
		var radian = Math.atan2(nowPoint.z - nextPoint.z, nextPoint.x - nowPoint.x);
		rotation.y = radian;
		//if (nowPoint.z == nextPoint.z && nowPoint.x == nextPoint.x) {
			//rotation.y = Math.PI / 2;
			//trace("equals");
		//}
	}
}