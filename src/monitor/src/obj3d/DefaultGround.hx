package obj3d;
import three.Mesh;
import three.PlaneBufferGeometry;
import three.MeshPhongMaterial;
import three.ImageUtils;
import three.Texture;

class DefaultGround {
	private static var texture:Texture;
	private var mesh:Mesh;
	public function new(?url:String, ?load:Void->Void) {
		// ground
		if (texture == null) {
			if (url == null) throw 'not DefaultGround url';
			texture = ImageUtils.loadTexture( url, null, load);
			texture.wrapS = texture.wrapT = Three.WrappingMode.RepeatWrapping;
			texture.repeat.set( 128, 128 );
			texture.anisotropy = GameMgr.getAnisotropy();
		}
		var groundMaterial = new MeshPhongMaterial( { color: 0xffffff, map: texture, shininess:10 } );
		
		var size:Float = 20000;
		mesh = new three.Mesh( new PlaneBufferGeometry( size,size ), groundMaterial );
		mesh.position.y = -0.5;
		mesh.rotation.x = - Math.PI / 2;
		mesh.receiveShadow = true;
	}
	public function getMesh() {
		return mesh;
	}
}