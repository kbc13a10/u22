package obj3d.random;
import field.FieldObject.FieldObjectParameter;
import three.Vector3;
import action.RotateAction;
import action.SetMoveAction;

class UpDownObj extends AbstractRandomObj {
	public function new(visualRange:Int) {
		super(visualRange);
	}
	override function getActionObjs(num:UInt, radian:Float):Array<FieldObjectParameter> {
		var objs:Array<FieldObjectParameter> = [];
		var maxCnt = 11;
		var shape = getRandomShape(num, 760, true);
		var upActionArray = [ { time:500, point:new Vector3() }, { time:1000, point:new Vector3(0, maxY, 0) } ];
		var downActionArray = [ { time:500, point:new Vector3() }, { time:1000, point:new Vector3(0, -maxY, 0) } ];
		if (num % 3 == 0) {
			upActionArray = [ { time:400, point:new Vector3() }, { time:900, point:new Vector3(0, maxY, 0) },
				{ time:400, point:new Vector3() }, { time:900, point:new Vector3(0, -maxY, 0) }];
			downActionArray = [ { time:400, point:new Vector3() }, { time:900, point:new Vector3(0, -maxY, 0) },
				{ time:400, point:new Vector3() }, { time:900, point:new Vector3(0, maxY, 0) }];
		}
		var upMove = new SetMoveAction(upActionArray);
		var downMove = new SetMoveAction(downActionArray);
		var upObj = new BreakObject(shape, [new RotateAction(), upMove]);
		var downObj = new BreakObject(shape, [new RotateAction({right: false}), downMove]);
		var dis = (visualRange / 3 * 2) * 2 / maxCnt;
		for (i in 0...maxCnt) {
			var x = dis * i - dis * (maxCnt / 2) + dis / 2;
			objs.push( { 
				rpps:maxTime / 3 * 2, obj:upObj, appTime:0, degree:90, distance:dis * i - dis * (maxCnt / 2) + dis / 2	
			});
			if(i < maxCnt - 1) {
				objs.push( {
					rpps:maxTime / 3 * 2,obj:downObj,appTime:0,degree:90,distance:dis * i - dis * (maxCnt / 2) + dis,positionY:maxY
				});
			}
		}
		return objs;
	}
}