package obj3d.random;
import action.SetRotaAction;
import field.FieldObject.FieldObjectParameter;
import obj3d.BreakObject;
import obj3d.InvincibleObject;
import shape.HexagonalShape;

class ShieldObj extends AbstractRandomObj {
	public function new(visualRange:Int) {
		super(visualRange);
		
	}
	override function getActionObjs(num:UInt, radian:Float):Array<FieldObjectParameter> {
		var objs:Array<FieldObjectParameter> = [];
		var color = Std.int(0xffffff * Math.random());
		var shape:shape.AbstractShape  = new shape.CubeShape(900, {size: 120, color: color});
		switch(num % 3) {
			case 1:
				shape = new shape.CylinderShape(900, {radiusTop: 70, radiusBottom: 70, defaultRotaY:radian, color: color});
			case 2:
				shape = new shape.CrystalShape(900, { size: 60, color: color } );
		}
		var maxCnt = 16;
		var obj = new BreakObject(shape);
		var rightShield = new HexagonalShape();
		var rightObj = new InvincibleObject(rightShield, [new SetRotaAction([
			{time : 1000, rotation : 90, direction : y},
			{time : 1000, rotation : 180, direction : y },
			{time : 1000, rotation : 270, direction : y },
			{time : 1000, rotation : 0, direction : y},
		])]);
		var leftShield = new HexagonalShape(0, {defaultRotaY:Math.PI});
		var leftObj = new InvincibleObject(leftShield, [new SetRotaAction([
			{time : 1000, rotation : 0, direction : y },
			{time : 1000, rotation : 90, direction : y},
			{time : 1000, rotation : 180, direction : y },
			{time : 1000, rotation : 270, direction : y },
		])]);
		var dis = visualRange / (maxCnt / 2) * 1.5;
		for (i in 0...maxCnt) {
			var d = dis * 1.25 - dis * (i % 4) + dis / 2 * (Std.int(i / 4) % 2);
			objs.push({
				rpps:maxTime - 800,
				obj:(Std.int(i / 4) % 2) == 0 ? leftObj : rightObj,
				appTime:0,
				degree:270,
				distance:d,
				positionY:(Std.int(i / 4)) * maxY / 3
			});
			objs.push({
				rpps:maxTime - 300,
				obj:obj,
				appTime:0,
				degree:270,
				distance:d,
				positionY:(Std.int(i / 4)) * maxY / 3
			});
		}
		
		
		return objs;
	}
}