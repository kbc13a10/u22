package obj3d.random;
import field.FieldObject.FieldObjectParameter;
import shape.AbstractShape;
import shape.CubeShape;
import shape.CylinderShape;
import shape.CrystalShape;

class AbstractRandomObj {
	private var maxTime:Int = 3000;
	private var visualRange:Int;
	private var maxY:Int = 600;
	public function new(visualRange:Int) {
		this.visualRange = visualRange;
	}
	public function getObjs(num:UInt, appTime:Float, radian:Float) : Array<FieldObjectParameter> {
		var objs:Array<FieldObjectParameter> = [];
		objs = getActionObjs(num, radian);
		for (o in objs) {
			o.appTime += appTime;
		}
		return objs;
	}
	private function getActionObjs(num:UInt, radian:Float) : Array<FieldObjectParameter> {
		return [];
	}
	private function getRandomShape(num:UInt, ?score:Int, ?random:Bool = false, ?color:Int) : AbstractShape{
		if (random) color = Std.int(0xffffff * Math.random());
		var shape:AbstractShape = new CrystalShape(score, { color: color} );
		num = num % 3;
		switch(num) {
			case 1:
				shape = new CubeShape(score, { color: color } );
			case 2:
				shape = new CylinderShape(score, { color: color } );
		}
		return shape;
	}
	private function getX(distance:Float, radian:Float) : Float {
		return distance * Math.sin(radian);
	}
	private function getZ(distance:Float, radian:Float) : Float{
		return -distance * Math.cos(radian);
	}
}