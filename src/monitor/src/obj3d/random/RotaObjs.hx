package obj3d.random;
import action.MoveAction;
import action.RotateAction;
import field.FieldObject.FieldObjectParameter;
import obj3d.BreakObject;

class RotaObjs extends AbstractRandomObj {

	public function new(visualRange:Int) {
		super(visualRange);
		
	}
	override function getActionObjs(num:UInt, radian:Float):Array<FieldObjectParameter> {
		var objs:Array<FieldObjectParameter> = [];
		var shape = getRandomShape(num, 900, true);
		var maxCnt = 15;
		
		for (i in 0...maxCnt) {
			var r = i % 2 == 0 ? true : false;
			var xR = false;
			var zR = false;
			switch(i % 4) {
				case 0:
					xR = true;
				case 1:
					zR = true;
				case 2:
					xR = true;
					zR = true;
			}
			var obj = new BreakObject(shape, [
				new MoveAction( { moveType:x, distance:visualRange / 3, reverse:xR} ), 
				new MoveAction( { moveType:z, distance:visualRange / 3, reverse:zR } ),
				new RotateAction( { right:r } )
			]);	
			objs.push({
				rpps:maxTime - i * 100 - 1000,
				obj:obj,
				appTime:i * 100,
				degree:90,
				distance:i * 30 * (r ? -1 : 1),
				positionY:0
			});
		}
		
		return objs;
	}
}