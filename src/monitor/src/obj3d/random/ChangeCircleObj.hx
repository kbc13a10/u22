package obj3d.random;
import action.RotateAction;
import action.SetMoveAction;
import field.FieldObject.FieldObjectParameter;
import obj3d.BreakObject;
import three.Vector3;

class ChangeCircleObj extends AbstractRandomObj{

	public function new(visualRange:Int) {
		super(visualRange);
	}
	override function getActionObjs(num:UInt, radian:Float):Array<FieldObjectParameter> {
		var objs:Array<FieldObjectParameter> = [];
		var shape = getRandomShape(num, 800, true);
		var maxCnt = 15;
		var rad = Math.PI * 2 / maxCnt;
		var r = num % 2 == 0 ? 1:-1;
		for (i in 0...maxCnt) {
			var x = Math.cos(i * rad) * maxY / 2 + visualRange * r;
			var obj = new BreakObject(shape, [new SetMoveAction([
				{ time:200, point:new Vector3() }, 
				{ time:1000, point:new Vector3(getX(x, radian), Math.sin(i * rad) * maxY / 2 + maxY / 2, getZ(x, radian)) } 
			]), new RotateAction()]);
			objs.push({
				rpps:maxTime - 1000,
				obj:obj,
				appTime:500,
				degree:90,
				distance:visualRange * r,
				positionY:0
			});
		}
		return objs;
	}
}