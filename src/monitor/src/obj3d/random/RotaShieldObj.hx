package obj3d.random;
import action.RotateAction;
import field.FieldObject.FieldObjectParameter;
import action.SetMoveAction;
import obj3d.InvincibleObject;
import three.Vector3;
import shape.HexagonalShape;

class RotaShieldObj extends AbstractRandomObj {

	public function new(visualRange:Int) {
		super(visualRange);
		
	}
	override function getActionObjs(num:UInt, radian:Float):Array<FieldObjectParameter> {
		var objs:Array<FieldObjectParameter> = [];
		var color = Std.int(0xffffff * Math.random());
		var shape:shape.AbstractShape  = new shape.CubeShape(1000, {size: 120, color: color});
		switch(num % 3) {
			case 1:
				shape = new shape.CylinderShape(1000, {radiusTop: 70, radiusBottom: 70, defaultRotaY:radian, color: color});
			case 2:
				shape = new shape.CrystalShape(1000, { size: 60, color: color } );
		}
		var r = num % 2 == 0 ?  true : false;
		var obj = new BreakObject(shape, [new RotateAction({right:r})]);
		
		var maxCnt = 20;
		var dis = visualRange / (maxCnt / 2) * 1.5;
		var smallRang = dis * 2;
		var bigRang = dis * 4;
		var smallMove:SetMoveAction;
		var rSmallMove:SetMoveAction;
		var bigMove:SetMoveAction;
		var rBigMove:SetMoveAction;
		if(r) {
			smallMove = new SetMoveAction([
				{ time:500, point:new Vector3(getX(smallRang, radian), 0, getZ(smallRang, radian)) },
				{ time:500, point:new Vector3(0, -maxY / 3, 0) },
				{ time:500, point:new Vector3(getX(-smallRang, radian), 0, getZ(-smallRang, radian)) },
				{ time:500, point:new Vector3(0, maxY / 3, 0) },
			], true);
			rSmallMove = new SetMoveAction([
				{ time:500, point:new Vector3(getX(-smallRang, radian), 0, getZ(-smallRang, radian)) },
				{ time:500, point:new Vector3(0, maxY / 3, 0) },
				{ time:500, point:new Vector3(getX(smallRang, radian), 0, getZ(smallRang, radian)) },
				{ time:500, point:new Vector3(0, -maxY / 3, 0) },
			], true);
			bigMove = new SetMoveAction([
				{ time:500, point:new Vector3(getX(bigRang, radian), 0, getZ(bigRang, radian)) },
				{ time:500, point:new Vector3(0, -maxY, 0) },
				{ time:500, point:new Vector3(getX(-bigRang, radian), 0, getZ(-bigRang, radian)) },
				{ time:500, point:new Vector3(0, maxY, 0) },
			], true);
			rBigMove = new SetMoveAction([
				{ time:500, point:new Vector3(getX(-bigRang, radian), 0, getZ(-bigRang, radian)) },
				{ time:500, point:new Vector3(0, maxY, 0) },
				{ time:500, point:new Vector3(getX(bigRang, radian), 0, getZ(bigRang, radian)) },
				{ time:500, point:new Vector3(0, -maxY, 0) },
			], true);
		} else {
			smallMove = new SetMoveAction([
				{ time:500, point:new Vector3(0, -maxY / 3, 0) },
				{ time:500, point:new Vector3(getX(smallRang, radian), 0, getZ(smallRang, radian)) },
				{ time:500, point:new Vector3(0, maxY / 3, 0) },
				{ time:500, point:new Vector3(getX(-smallRang, radian), 0, getZ(-smallRang, radian)) },
			], true);
			rSmallMove = new SetMoveAction([
				{ time:500, point:new Vector3(0, maxY / 3, 0) },
				{ time:500, point:new Vector3(getX(-smallRang, radian), 0, getZ(-smallRang, radian)) },
				{ time:500, point:new Vector3(0, -maxY / 3, 0) },
				{ time:500, point:new Vector3(getX(smallRang, radian), 0, getZ(smallRang, radian)) },
				
			], true);
			bigMove = new SetMoveAction([
				{ time:500, point:new Vector3(0, -maxY, 0) },
				{ time:500, point:new Vector3(getX(bigRang, radian), 0, getZ(bigRang, radian)) },
				{ time:500, point:new Vector3(0, maxY, 0) },
				{ time:500, point:new Vector3(getX(-bigRang, radian), 0, getZ(-bigRang, radian)) },
			], true);
			rBigMove = new SetMoveAction([
				{ time:500, point:new Vector3(0, maxY, 0) },
				{ time:500, point:new Vector3(getX(-bigRang, radian), 0, getZ(-bigRang, radian)) },
				{ time:500, point:new Vector3(0, -maxY, 0) },
				{ time:500, point:new Vector3(getX(bigRang, radian), 0, getZ(bigRang, radian)) },
			], true);
		}
		var hexa = new HexagonalShape(0, { defaultRotaY:radian } );
		var smallHexa = new InvincibleObject(hexa, [smallMove]);
		var bigHexa = new InvincibleObject(hexa, [bigMove]);
		var rSmallHexa = new InvincibleObject(hexa, [rSmallMove]);
		var rBigHexa = new InvincibleObject(hexa, [rBigMove]);
		objs.push({
			rpps:maxTime - 800,
			obj:smallHexa,
			appTime:0,
			degree:270,
			distance:-dis,
			positionY:maxY / 3 * 2
		});
		objs.push({
			rpps:maxTime - 800,
			obj:bigHexa,
			appTime:0,
			degree:270,
			distance:-dis * 2,
			positionY:maxY
		});
		objs.push({
			rpps:maxTime - 800,
			obj:rSmallHexa,
			appTime:0,
			degree:270,
			distance:dis,
			positionY:maxY / 3
		});
		objs.push({
			rpps:maxTime - 800,
			obj:rBigHexa,
			appTime:0,
			degree:270,
			distance:dis * 2,
			positionY:0
		});
		for (i in 0...maxCnt) {
			var d = dis * 2 - dis * (i % 5);
			objs.push({
				rpps:maxTime - 300,
				obj:obj,
				appTime:0,
				degree:270,
				distance:d,
				positionY:(Std.int(i / 5)) * maxY / 3
			});
		}
		
		return objs;
	}
}