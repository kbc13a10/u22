package obj3d.random;
import action.RotateAction;
import field.FieldObject.FieldObjectParameter;
import obj3d.BreakObject;

class VortexObj extends AbstractRandomObj  {

	public function new(visualRange:Int) {
		super(visualRange);
	}
	override function getActionObjs(num:UInt, radian:Float):Array<FieldObjectParameter> {
		var objs:Array<FieldObjectParameter> = [];
		var shape = getRandomShape(num, 420, true);
		var leftObj = new BreakObject(shape, [new RotateAction()]);
		var rightObj = new BreakObject(shape, [new RotateAction({right:false})]);
		var maxCnt = 15;
		var rad = Math.PI / (maxCnt / 2);
		var reverse = num % 2 == 0 ? true : false;
		var quadruple = num % 4 == 0 ? true : false;
		for (i in 0...maxCnt) {
			var d = reverse ? maxCnt - i : i;
			objs.push({
				rpps:maxTime - maxCnt * 100,
				obj:leftObj,
				appTime:i * 100,
				degree:90,
				distance:Math.cos(d * rad) * maxY / 2,
				positionY:Math.sin(d * rad) * maxY / 2 + maxY / 2
			});
			objs.push({
				rpps:maxTime - maxCnt * 100,
				obj:leftObj,
				appTime:i * 100,
				degree:90,
				distance:Math.cos(d * rad + Math.PI) * maxY / 2,
				positionY:Math.sin(d * rad + Math.PI) * maxY / 2 + maxY / 2
			});
			if (quadruple) {
				objs.push({
					rpps:maxTime - maxCnt * 100,
					obj:leftObj,
					appTime:i * 100,
					degree:90,
					distance:Math.cos(d * rad + Math.PI / 2) * maxY / 2,
					positionY:Math.sin(d * rad + Math.PI / 2) * maxY / 2 + maxY / 2
				});
				objs.push({
					rpps:maxTime - maxCnt * 100,
					obj:leftObj,
					appTime:i * 100,
					degree:90,
					distance:Math.cos(d * rad - Math.PI / 2) * maxY / 2,
					positionY:Math.sin(d * rad - Math.PI / 2) * maxY / 2 + maxY / 2
				});
			}
		}
		return objs;
	}
}