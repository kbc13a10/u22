package obj3d.random;
import field.FieldObject.FieldObjectParameter;
import three.Vector3;
import action.RotateAction;
import action.SetMoveAction;

class RightLeftObj extends AbstractRandomObj {
	public function new(visualRange:Int) {
		super(visualRange);
		
	}
	override function getActionObjs(num:UInt, radian:Float):Array<FieldObjectParameter> {
		var objs:Array<FieldObjectParameter> = [];
		var shape = getRandomShape(num, 930, true);
		var h = shape.getHeight();
		var maxCnt = Std.int(maxY / (h + 10));
		var rightMove:SetMoveAction;
		var moveRightArray = [];
		var leftMove:SetMoveAction;
		var moveLeftArray = [];
		var rang = visualRange / 2 * 1.5;
		if(num % 3 != 0) {
			moveRightArray = [{ time:500, point:new Vector3() }, { time:1000, point:new Vector3(getX(rang, radian), 0, getZ(rang, radian)) }];
			rightMove = new SetMoveAction(moveRightArray);
			moveLeftArray = [ { time:500, point:new Vector3() }, { time:1000, point:new Vector3(getX( -rang, radian), 0, getZ( -rang, radian)) } ];
			leftMove = new SetMoveAction(moveLeftArray);
		} else {
			moveRightArray = [
				{ time:600, point:new Vector3() }, 
				{ time:800, point:new Vector3(getX(rang, radian), 0, getZ(rang, radian)) },
				{ time:600, point:new Vector3() }, 
				{ time:800, point:new Vector3(getX(-rang, radian), 0, getZ(-rang, radian)) }
			];
			rightMove = new SetMoveAction(moveRightArray, true);
			moveLeftArray = [ 
				{ time:600, point:new Vector3() }, 
				{ time:800, point:new Vector3(getX( -rang, radian), 0, getZ( -rang, radian)) },
				{ time:600, point:new Vector3() }, 
				{ time:800, point:new Vector3(getX( rang, radian), 0, getZ( rang, radian)) } 
			];
			leftMove = new SetMoveAction(moveLeftArray, true);
		}
		var rightObj = new BreakObject(shape, [new RotateAction(), rightMove]);
		var leftObj = new BreakObject(shape, [new RotateAction(), leftMove]);
		for (i in 0...maxCnt) {
			objs.push({
				rpps:maxTime / 3,
				obj:rightObj,
				appTime:0,
				degree:90,
				distance:rang / 2,
				positionY:i * maxY / maxCnt
			});
			objs.push({
				rpps:maxTime / 30 * 16,
				obj:leftObj,
				appTime:300,
				degree:270,
				distance:rang / 2,
				positionY:i * maxY / maxCnt
			});
			objs.push({
				rpps:maxTime / 30 * 22,
				obj:rightObj,
				appTime:600,
				degree:90,
				distance:rang / 2,
				positionY:i * maxY / maxCnt
			});
			objs.push({
				rpps:maxTime / 3,
				obj:rightObj,
				appTime:0,
				degree:90,
				distance:rang / 3,
				positionY:i * maxY / maxCnt
			});
			objs.push({
				rpps:maxTime / 30 * 16,
				obj:leftObj,
				appTime:300,
				degree:270,
				distance:rang / 3,
				positionY:i * maxY / maxCnt
			});
			objs.push({
				rpps:maxTime / 30 * 22,
				obj:rightObj,
				appTime:600,
				degree:90,
				distance:rang / 3,
				positionY:i * maxY / maxCnt
			});
		}
		return objs;
	}
}