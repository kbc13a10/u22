package obj3d.random;
import action.MoveAction;
import action.RotateAction;
import action.SetMoveAction;
import field.FieldObject.FieldObjectParameter;
import obj3d.BreakObject;
import three.Vector3;

class ConvexObj extends AbstractRandomObj {

	public function new(visualRange:Int) {
		super(visualRange);
		
	}
	override function getActionObjs(num:UInt, radian:Float):Array<FieldObjectParameter> {
		var objs:Array<FieldObjectParameter> = [];
		var maxCnt = 20;
		var shape = getRandomShape(num, 820, true);
		var r = num % 2 == 0 ? true : false;
		var addY = r ? -maxY / (maxCnt / 2) : maxY / (maxCnt / 2);
		var objY: Float = r ? -addY * maxCnt / 2 : 0;
		for (i in 0...maxCnt) {
			objY += (i < maxCnt / 2) ? addY : -addY;
			var obj = new BreakObject(shape, [new SetMoveAction([
				{ time:500, point:new Vector3() }, 
				{ time:1000, point:new Vector3(0, (maxY / 2 - objY) * 2, 0) } 
			]), new RotateAction({right:r})]);
			objs.push({
				rpps:maxTime / 3 * 2,
				obj:obj,
				appTime:0,
				degree:270,
				distance:(visualRange / 3 * 2) - i * ((visualRange / 3 * 2) * 2 / maxCnt) - ((visualRange / 3 * 2) * 2 / maxCnt),
				positionY:objY
			});
		}
		
		return objs;
	}
}