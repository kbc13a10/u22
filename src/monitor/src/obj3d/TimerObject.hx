package obj3d;

import shape.AbstractShape;
import action.AbstractAction;
import three.Object3D;

typedef TimerParameter = {
	@:optional var limit:Float;
}

class TimerObject extends MeshEx {
	private var limit:Float = 1000;
	private var total:Float = 0;
	public function new(shape:AbstractShape, ?args:Array<AbstractAction>, ?timer:TimerParameter) {
		super(shape, args);
		if (timer == null) timer = { };
		if (timer.limit != null) limit = timer.limit;
	}
	override public function update(time:Float) {
		super.update(time);
		total += time;
		if (total >= limit) {
			life = false;
		}
	}
	override public function hit():Bool {
		return false;
	}
	override public function clone():Object3D {
		var actionArray = [];
		for (i in actions) {
			actionArray.push(i.clone());
		}
		return new TimerObject(shape.clone(), actionArray);
	}
}