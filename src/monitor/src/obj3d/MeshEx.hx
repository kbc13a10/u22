package obj3d;

import action.AbstractAction;
import three.Geo;
import three.Material;
import three.Mesh;
import shape.AbstractShape;
import three.Object3D;

/**
 * ...
 * @author taka
 */
class MeshEx extends Mesh {
	private var actions:Array<AbstractAction>;
	private var shapeName:String = 'meshEx';
	private var shape:AbstractShape;
	private var life:Bool = true;
	public function new( shape:AbstractShape, ?args:Array<AbstractAction> ) {
		super(shape.getGeometry(), shape.getMaterial());
		actions = if (args != null) args else [];
		shape.init(position, rotation);
		shapeName = shape.getName();
		this.shape = shape;
	}
	override public function update(time:Float) {
		for (action in actions) {
			action.update(time, position, rotation);
		}
	}
	public function getShapeName() {
		return shapeName;
	}
	public function hit() : Bool {
		return true;
	}
	override public function clone() : Object3D {
		var actionArray = [];
		for (i in actions) {
			actionArray.push(i.clone());
		}
		return new MeshEx(shape.clone(), actionArray);
	}
	override public function isLife() : Bool {
		return life;
	}
	public function getScore() {
		return shape.getScore();
	}
	public function getWidth() {
		return shape.getWidth();
	}
	public function getHeight() {
		return shape.getHeight();
	}
}