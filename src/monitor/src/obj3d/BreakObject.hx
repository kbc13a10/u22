package obj3d;
import action.AbstractAction;
import shape.AbstractShape;
import three.Object3D;

/**
 * ...
 * @author taka
 */
class BreakObject extends MeshEx {
	public function new(shape:AbstractShape, ?args:Array<AbstractAction>) {
		super(shape, args);
	}
	override public function update(time:Float) {
		super.update(time);		
	}
	override public function hit() {
		life = false;
		return true;
	}
	override public function clone():Object3D {
		var actionArray = [];
		for (i in actions) {
			actionArray.push(i.clone());
		}
		return new BreakObject(shape.clone(), actionArray);
	}
}