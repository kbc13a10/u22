package obj3d;
import three.DirectionalLight;

class Lignt extends DirectionalLight{
	public function new( hex : Int, ?intensity : Float ) {
		super(hex, intensity);
		position.set( 50, 200, 100 );
		position.multiplyScalar( 1.3 );
		castShadow = true;
		shadowMapWidth = 1024;
		shadowMapHeight = 1024;
		var d = 500;
		shadowCameraLeft = -d;
		shadowCameraRight = d;
		shadowCameraTop = d;
		shadowCameraBottom = -d;
		shadowCameraFar = 1000;
		shadowDarkness = 0.5;
	}
}