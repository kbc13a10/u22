package obj3d;

import shape.AbstractShape;
import action.AbstractAction;
import three.Object3D;

/**
 * ...
 * @author taka
 */
class InvincibleObject extends MeshEx {
	public function new(shape:AbstractShape, ?args:Array<AbstractAction>) {
		super(shape, args);
	}
	override public function update(time:Float) {
		super.update(time);		
	}
	override public function hit():Bool {
		return false;
	}
	override public function clone():Object3D {
		var actionArray = [];
		for (i in actions) {
			actionArray.push(i.clone());
		}
		return new InvincibleObject(shape.clone(), actionArray);
	}
}