(function (console) { "use strict";
var $hxClasses = {},$estr = function() { return js_Boot.__string_rec(this,''); };
function $extend(from, fields) {
	function Inherit() {} Inherit.prototype = from; var proto = new Inherit();
	for (var name in fields) proto[name] = fields[name];
	if( fields.toString !== Object.prototype.toString ) proto.toString = fields.toString;
	return proto;
}
var Audio = function() {
	this.seVolume = 0.5;
	this.bgmVolume = 0.5;
	this.mainVolume = 0.5;
	this.seMute = false;
	this.bgmMute = false;
	this.mainMute = false;
	this.bgmState = 0;
	this.ses = new haxe_ds_StringMap();
	this.bgms = new haxe_ds_StringMap();
	this.seCount = 0;
	this.seAudioElements = [];
	this.bgmAudioElement = js_Boot.__cast(window.document.createElement("audio") , HTMLAudioElement);
	this.bgmAudioElement.id = "bgm";
	window.document.body.appendChild(this.bgmAudioElement);
	var _g = 0;
	while(_g < 4) {
		var i = _g++;
		this.seAudioElements[i] = js_Boot.__cast(window.document.createElement("audio") , HTMLAudioElement);
		this.seAudioElements[i].id = "se" + i;
		window.document.body.appendChild(this.seAudioElements[i]);
	}
	this.changeVolume();
};
$hxClasses["Audio"] = Audio;
Audio.__name__ = true;
Audio.getInstance = function() {
	if(Audio.inst == null) Audio.inst = new Audio();
	return Audio.inst;
};
Audio.prototype = {
	appendBGM: function(name,src) {
		this.bgms.set(name,src);
	}
	,playBGM: function(name) {
		var src = this.bgms.get(name);
		if(src == null) throw new js__$Boot_HaxeError("Unknown BGM : " + name);
		this.bgmAudioElement.src = src;
		this.bgmAudioElement.loop = true;
		this.bgmAudioElement.play();
		this.bgmState = 1;
	}
	,stopBGM: function() {
		if(this.bgmState == 1 || this.bgmState == 3) {
			this.bgmAudioElement.pause();
			this.bgmAudioElement.currentTime = 0;
			this.bgmState = 2;
		}
	}
	,pauseBGM: function() {
		if(this.bgmState == 1) {
			this.bgmAudioElement.pause();
			this.bgmState = 3;
		}
	}
	,resumeBGM: function() {
		if(this.bgmState == 3) {
			this.bgmAudioElement.play();
			this.bgmState = 1;
		}
	}
	,isPlayBGM: function() {
		return this.bgmState == 1;
	}
	,isStopBGM: function() {
		return this.bgmState == 2 || this.bgmState == 0;
	}
	,isPauseBGM: function() {
		return this.bgmState == 3;
	}
	,appendSE: function(name,src) {
		this.ses.set(name,src);
	}
	,playSE: function(name) {
		var src = this.ses.get(name);
		if(src == null) throw new js__$Boot_HaxeError("Unknown SE : " + name);
		this.seAudioElements[this.seCount].src = src;
		this.seAudioElements[this.seCount].play();
		this.seCount = (this.seCount + 1) % 4;
	}
	,setMuteMain: function(mute) {
		this.mainMute = mute;
		this.changeVolume();
	}
	,setMuteBGM: function(mute) {
		this.bgmMute = mute;
		this.changeVolume();
	}
	,setMuteSE: function(mute) {
		this.seMute = mute;
		this.changeVolume();
	}
	,isMuteMain: function() {
		return this.mainMute;
	}
	,isMuteBGM: function() {
		return this.bgmMute;
	}
	,isMuteSE: function() {
		return this.seMute;
	}
	,setVolumeMain: function(volume) {
		if(volume < 0.0) volume = 0.0;
		if(volume > 1.0) volume = 1.0;
		this.mainVolume = volume;
		this.changeVolume();
	}
	,setVolumeBGM: function(volume) {
		if(volume < 0.0) volume = 0.0;
		if(volume > 1.0) volume = 1.0;
		this.bgmVolume = volume;
		this.changeVolume();
	}
	,setVolumeSE: function(volume) {
		if(volume < 0.0) volume = 0.0;
		if(volume > 1.0) volume = 1.0;
		this.seVolume = volume;
		this.changeVolume();
	}
	,getVolumeMain: function() {
		return this.mainVolume;
	}
	,getVolumeBGM: function() {
		return this.bgmVolume;
	}
	,getVolumeSE: function() {
		return this.seVolume;
	}
	,changeVolume: function() {
		if(this.mainMute) this.bgmAudioElement.volume = 0.0; else if(this.bgmMute) this.bgmAudioElement.volume = 0.0; else this.bgmAudioElement.volume = this.mainVolume * this.bgmVolume;
		var _g1 = 0;
		var _g = this.seAudioElements.length;
		while(_g1 < _g) {
			var i = _g1++;
			if(this.mainMute) this.seAudioElements[i].volume = 0.0; else if(this.seMute) this.seAudioElements[i].volume = 0.0; else this.seAudioElements[i].volume = this.mainVolume * this.seVolume;
		}
	}
	,__class__: Audio
};
var DateTools = function() { };
$hxClasses["DateTools"] = DateTools;
DateTools.__name__ = true;
DateTools.delta = function(d,t) {
	var t1 = d.getTime() + t;
	var d1 = new Date();
	d1.setTime(t1);
	return d1;
};
var EBattleMode = { __ename__ : true, __constructs__ : ["Easy","Normal","Hard","Ran","none"] };
EBattleMode.Easy = ["Easy",0];
EBattleMode.Easy.toString = $estr;
EBattleMode.Easy.__enum__ = EBattleMode;
EBattleMode.Normal = ["Normal",1];
EBattleMode.Normal.toString = $estr;
EBattleMode.Normal.__enum__ = EBattleMode;
EBattleMode.Hard = ["Hard",2];
EBattleMode.Hard.toString = $estr;
EBattleMode.Hard.__enum__ = EBattleMode;
EBattleMode.Ran = ["Ran",3];
EBattleMode.Ran.toString = $estr;
EBattleMode.Ran.__enum__ = EBattleMode;
EBattleMode.none = ["none",4];
EBattleMode.none.toString = $estr;
EBattleMode.none.__enum__ = EBattleMode;
EBattleMode.__empty_constructs__ = [EBattleMode.Easy,EBattleMode.Normal,EBattleMode.Hard,EBattleMode.Ran,EBattleMode.none];
var EScene = { __ename__ : true, __constructs__ : ["EasyGameScene","NormalGameScene","HardGameScene","RandomGameScene","LoadScene","EndScene","TitleScene","ResultSingleScene","ResultTeamScene","ResultRankScene"] };
EScene.EasyGameScene = ["EasyGameScene",0];
EScene.EasyGameScene.toString = $estr;
EScene.EasyGameScene.__enum__ = EScene;
EScene.NormalGameScene = ["NormalGameScene",1];
EScene.NormalGameScene.toString = $estr;
EScene.NormalGameScene.__enum__ = EScene;
EScene.HardGameScene = ["HardGameScene",2];
EScene.HardGameScene.toString = $estr;
EScene.HardGameScene.__enum__ = EScene;
EScene.RandomGameScene = ["RandomGameScene",3];
EScene.RandomGameScene.toString = $estr;
EScene.RandomGameScene.__enum__ = EScene;
EScene.LoadScene = ["LoadScene",4];
EScene.LoadScene.toString = $estr;
EScene.LoadScene.__enum__ = EScene;
EScene.EndScene = ["EndScene",5];
EScene.EndScene.toString = $estr;
EScene.EndScene.__enum__ = EScene;
EScene.TitleScene = ["TitleScene",6];
EScene.TitleScene.toString = $estr;
EScene.TitleScene.__enum__ = EScene;
EScene.ResultSingleScene = ["ResultSingleScene",7];
EScene.ResultSingleScene.toString = $estr;
EScene.ResultSingleScene.__enum__ = EScene;
EScene.ResultTeamScene = ["ResultTeamScene",8];
EScene.ResultTeamScene.toString = $estr;
EScene.ResultTeamScene.__enum__ = EScene;
EScene.ResultRankScene = ["ResultRankScene",9];
EScene.ResultRankScene.toString = $estr;
EScene.ResultRankScene.__enum__ = EScene;
EScene.__empty_constructs__ = [EScene.EasyGameScene,EScene.NormalGameScene,EScene.HardGameScene,EScene.RandomGameScene,EScene.LoadScene,EScene.EndScene,EScene.TitleScene,EScene.ResultSingleScene,EScene.ResultTeamScene,EScene.ResultRankScene];
var ETeamType = { __ename__ : true, __constructs__ : ["red","blue"] };
ETeamType.red = ["red",0];
ETeamType.red.toString = $estr;
ETeamType.red.__enum__ = ETeamType;
ETeamType.blue = ["blue",1];
ETeamType.blue.toString = $estr;
ETeamType.blue.__enum__ = ETeamType;
ETeamType.__empty_constructs__ = [ETeamType.red,ETeamType.blue];
var Reflect = function() { };
$hxClasses["Reflect"] = Reflect;
Reflect.__name__ = true;
Reflect.field = function(o,field) {
	try {
		return o[field];
	} catch( e ) {
		if (e instanceof js__$Boot_HaxeError) e = e.val;
		return null;
	}
};
Reflect.compare = function(a,b) {
	if(a == b) return 0; else if(a > b) return 1; else return -1;
};
Reflect.isEnumValue = function(v) {
	return v != null && v.__enum__ != null;
};
var GameMgr = function() { };
$hxClasses["GameMgr"] = GameMgr;
GameMgr.__name__ = true;
GameMgr.init = function() {
	GameMgr.WIDTH = window.innerWidth;
	GameMgr.HEIGHT = window.innerHeight;
	GameMgr.teamColor.set(ETeamType.red,"#F78181");
	GameMgr.teamColor.set(ETeamType.blue,"#81DAF5");
	GameMgr.input = new event_Input();
	var ce;
	ce = js_Boot.__cast(window.document.createElement("canvas") , HTMLCanvasElement);
	ce.setAttribute("width","" + GameMgr.WIDTH);
	ce.setAttribute("height","" + GameMgr.HEIGHT);
	ce.style.position = "absolute";
	ce.style.zIndex = "60";
	GameMgr.context = ce.getContext("2d",null);
	window.document.body.appendChild(ce);
	GameMgr.renderer = new THREE.WebGLRenderer();
	GameMgr.renderer.setSize(GameMgr.WIDTH,GameMgr.HEIGHT);
	GameMgr.renderer.autoClear = false;
	GameMgr.renderer.domElement.style.position = "absolute";
	GameMgr.renderer.domElement.style.zIndex = "50";
	window.document.body.appendChild(GameMgr.renderer.domElement);
	GameMgr.nowScene = EScene.LoadScene;
	var sceneLength = 0;
	var _g = 0;
	var _g1 = Type.allEnums(EScene);
	while(_g < _g1.length) {
		var s = _g1[_g];
		++_g;
		var inst = Type.createInstance(Type.resolveClass("scene." + Std.string(s)),[GameMgr.context]);
		GameMgr.sceneMap.set(s,inst);
		sceneLength++;
	}
	GameMgr.scene = GameMgr.sceneMap.get(GameMgr.nowScene);
	GameMgr.scene.init([sceneLength]);
	GameMgr.startTimestamp = GameMgr.lastTimestamp = new Date().getTime();
	GameMgr.raf = window.requestAnimationFrame(GameMgr.update);
	GameMgr.com = new event_Communicator();
};
GameMgr.load = function(msg) {
	var loadTexture = new obj3d_DefaultGround(GameMgr.DIRECTORY + "images/grasslight-big.jpg",function() {
		var _g = 0;
		var _g1 = Type.allEnums(EScene);
		while(_g < _g1.length) {
			var s = _g1[_g];
			++_g;
			GameMgr.sceneMap.get(s).once();
		}
		GameMgr.changeScene(EScene.TitleScene,[msg]);
	});
};
GameMgr.update = function(timestamp) {
	GameMgr.context.clearRect(0,0,GameMgr.WIDTH,GameMgr.HEIGHT);
	var t = timestamp - GameMgr.lastTimestamp;
	if(t < 0) t = 0;
	GameMgr.scene.update(t);
	GameMgr.renderer.clear();
	GameMgr.renderer.setViewport(0,0,GameMgr.WIDTH,GameMgr.HEIGHT);
	GameMgr.renderer.render(GameMgr.scene.scene,GameMgr.scene.camera);
	GameMgr.lastTimestamp = timestamp;
	GameMgr.raf = window.requestAnimationFrame(GameMgr.update);
};
GameMgr.changeScene = function(nextScene,args) {
	if(args == null) args = []; else args = args;
	GameMgr.scene.clear();
	GameMgr.nowScene = nextScene;
	GameMgr.scene = GameMgr.sceneMap.get(nextScene);
	GameMgr.scene.init(args);
};
GameMgr.setBackColor = function(rgb) {
	GameMgr.renderer.setClearColor(new THREE.Color(rgb));
};
GameMgr.getAnisotropy = function() {
	return GameMgr.renderer.getMaxAnisotropy();
};
GameMgr.playerEntry = function(id,name) {
	if(GameMgr.entryNum >= GameMgr.maxPlayer) return "max"; else if(GameMgr.playing) return "play"; else {
		GameMgr.entryNum++;
		var number = 0;
		var _g1 = 0;
		var _g = GameMgr.numbers.length;
		while(_g1 < _g) {
			var i = _g1++;
			if(!GameMgr.numbers[i]) {
				GameMgr.numbers[i] = true;
				number = i;
				break;
			}
		}
		var reticle = new obj2d_Reticle(GameMgr.WIDTH / 2,GameMgr.HEIGHT / 2,"#" + GameMgr.playerColor[number]);
		GameMgr.scene.addObj2D(reticle);
		var value = new Player(id,name,reticle,number,Std.parseInt("0x" + GameMgr.playerColor[number]));
		GameMgr.players.set(id,value);
		GameMgr.audio.playSE("entry");
		return "" + number;
	}
};
GameMgr.playerShot = function(id) {
	GameMgr.scene.playerShot(GameMgr.players.get(id));
};
GameMgr.playerMove = function(id,radian,distance) {
	var player = GameMgr.players.get(id);
	if(player != null) player.move(parseFloat(radian),parseFloat(distance));
};
GameMgr.playerReady = function(id,msg) {
	var player = GameMgr.players.get(id);
	if(player != null) player.changeReady(msg);
	GameMgr.audio.playSE("ready");
};
GameMgr.isReadyAll = function() {
	var readys = 0;
	var $it0 = GameMgr.players.iterator();
	while( $it0.hasNext() ) {
		var p = $it0.next();
		if(p.isReady()) readys++;
	}
	if(readys == GameMgr.entryNum && readys != 0) return true; else return false;
};
GameMgr.playerExit = function(id) {
	var player = GameMgr.players.get(id);
	if(player != null) {
		player.setRetire();
		GameMgr.numbers[player.getNumber()] = false;
		GameMgr.scene.removeObj2D(player.getObj());
		GameMgr.players.remove(id);
		GameMgr.entryNum--;
	}
};
GameMgr.allReady = function() {
	GameMgr.playing = true;
	GameMgr.com.allReady();
};
GameMgr.resetReady = function() {
	var $it0 = GameMgr.players.iterator();
	while( $it0.hasNext() ) {
		var p = $it0.next();
		p.reset();
	}
	GameMgr.playing = false;
	GameMgr.com.reset();
};
GameMgr.isTeamBattle = function() {
	return GameMgr.teamBattle;
};
GameMgr.setTeamBattle = function(flag) {
	GameMgr.teamBattle = flag;
};
GameMgr.setRank = function(rank) {
	var absScene = GameMgr.sceneMap.get(EScene.ResultRankScene);
	var rankScene;
	rankScene = js_Boot.__cast(absScene , scene_ResultRankScene);
	rankScene.setRank(rank);
};
GameMgr.sendPlayerRank = function(player,stageId) {
	GameMgr.com.sendRank(player.getId(),stageId,player.getScore());
};
GameMgr.isOpenRandom = function() {
	return GameMgr.openRandom;
};
GameMgr.setOpenRandom = function(flag) {
	GameMgr.openRandom = flag;
};
GameMgr.isHardClear = function() {
	return GameMgr.hardClear;
};
GameMgr.setHardClear = function(flag) {
	GameMgr.hardClear = flag;
};
var HiddenButton = function(x,y,w,h) {
	this.height = 0;
	this.widht = 0;
	this.cnt = 0;
	this.x = x;
	this.y = y;
	this.widht = w;
	this.height = h;
};
$hxClasses["HiddenButton"] = HiddenButton;
HiddenButton.__name__ = true;
HiddenButton.prototype = {
	checkHit: function(px,py) {
		if(!GameMgr.isOpenRandom()) {
			if(px >= this.x && px <= this.x + this.widht && py >= this.y && py <= this.y + this.height) {
				this.cnt++;
				if(this.cnt >= 10) {
					GameMgr.setOpenRandom(true);
					var audio = Audio.getInstance();
					audio.playSE("start");
					return true;
				}
			}
		}
		return false;
	}
	,__class__: HiddenButton
};
var HxOverrides = function() { };
$hxClasses["HxOverrides"] = HxOverrides;
HxOverrides.__name__ = true;
HxOverrides.cca = function(s,index) {
	var x = s.charCodeAt(index);
	if(x != x) return undefined;
	return x;
};
HxOverrides.substr = function(s,pos,len) {
	if(pos != null && pos != 0 && len != null && len < 0) return "";
	if(len == null) len = s.length;
	if(pos < 0) {
		pos = s.length + pos;
		if(pos < 0) pos = 0;
	} else if(len < 0) len = s.length + len - pos;
	return s.substr(pos,len);
};
HxOverrides.iter = function(a) {
	return { cur : 0, arr : a, hasNext : function() {
		return this.cur < this.arr.length;
	}, next : function() {
		return this.arr[this.cur++];
	}};
};
var List = function() {
	this.length = 0;
};
$hxClasses["List"] = List;
List.__name__ = true;
List.prototype = {
	add: function(item) {
		var x = [item];
		if(this.h == null) this.h = x; else this.q[1] = x;
		this.q = x;
		this.length++;
	}
	,clear: function() {
		this.h = null;
		this.q = null;
		this.length = 0;
	}
	,remove: function(v) {
		var prev = null;
		var l = this.h;
		while(l != null) {
			if(l[0] == v) {
				if(prev == null) this.h = l[1]; else prev[1] = l[1];
				if(this.q == l) this.q = prev;
				this.length--;
				return true;
			}
			prev = l;
			l = l[1];
		}
		return false;
	}
	,__class__: List
};
var Main = function() { };
$hxClasses["Main"] = Main;
Main.__name__ = true;
Main.main = function() {
	window.onload = GameMgr.init;
};
Math.__name__ = true;
var Player = function(id,n,o,num,c) {
	if(n == null) n = "名無し";
	this.retire = false;
	this.teamcolor = ETeamType.red;
	this.id = id;
	this.name = n;
	this.score = 0;
	this.reticle = o;
	this.ready = false;
	this.number = num;
	this.color = c;
};
$hxClasses["Player"] = Player;
Player.__name__ = true;
Player.prototype = {
	getId: function() {
		return this.id;
	}
	,setRetire: function() {
		this.retire = true;
	}
	,isRetire: function() {
		return this.retire;
	}
	,getObj: function() {
		return this.reticle;
	}
	,addScore: function(s) {
		this.score += s;
	}
	,getScore: function() {
		return this.score;
	}
	,reset: function() {
		this.score = 0;
		this.ready = false;
	}
	,isReady: function() {
		return this.ready;
	}
	,setTeamColor: function(tc) {
		this.teamcolor = tc;
	}
	,getTeamColor: function() {
		return this.teamcolor;
	}
	,getName: function() {
		return this.name;
	}
	,move: function(r,d) {
		this.reticle.add(d * Math.cos(r),d * Math.sin(r));
	}
	,changeReady: function(msg) {
		switch(msg) {
		case "on":
			this.ready = true;
			break;
		case "off":
			this.ready = false;
			break;
		}
	}
	,getX: function() {
		return this.reticle.x;
	}
	,getY: function() {
		return this.reticle.y;
	}
	,getNumber: function() {
		return this.number;
	}
	,getColor: function() {
		return this.color;
	}
	,shot: function() {
		this.reticle.action();
	}
	,setScore: function(s) {
		this.score = s;
	}
	,__class__: Player
};
var Random = function(w,x,y,z) {
	if(z == null) z = 521288629;
	if(y == null) y = 362436069;
	if(x == null) x = 123456789;
	if(w == null) w = 88675123;
	if(_$UInt_UInt_$Impl_$.gte(0,x) && _$UInt_UInt_$Impl_$.gte(0,y) && _$UInt_UInt_$Impl_$.gte(0,z) && _$UInt_UInt_$Impl_$.gte(0,w)) throw new js__$Boot_HaxeError("すべて0では乱数が生成できません");
	this.x = x;
	this.y = y;
	this.z = z;
	this.w = w;
	var _g = 0;
	while(_g < 100) {
		var i = _g++;
		this.nextUInt();
	}
};
$hxClasses["Random"] = Random;
Random.__name__ = true;
Random.prototype = {
	nextUInt: function() {
		this.t = this.x ^ this.x << 11;
		this.x = this.y;
		this.y = this.z;
		this.z = this.w;
		return this.w = this.w ^ this.w >>> 19 ^ (this.t ^ this.t >>> 8);
	}
	,nextFloat: function() {
		var a = this.nextUInt();
		return _$UInt_UInt_$Impl_$.toFloat(a) / 4294967296.0;
	}
	,__class__: Random
};
var Std = function() { };
$hxClasses["Std"] = Std;
Std.__name__ = true;
Std.string = function(s) {
	return js_Boot.__string_rec(s,"");
};
Std["int"] = function(x) {
	return x | 0;
};
Std.parseInt = function(x) {
	var v = parseInt(x,10);
	if(v == 0 && (HxOverrides.cca(x,1) == 120 || HxOverrides.cca(x,1) == 88)) v = parseInt(x);
	if(isNaN(v)) return null;
	return v;
};
var StringTools = function() { };
$hxClasses["StringTools"] = StringTools;
StringTools.__name__ = true;
StringTools.isSpace = function(s,pos) {
	var c = HxOverrides.cca(s,pos);
	return c > 8 && c < 14 || c == 32;
};
StringTools.ltrim = function(s) {
	var l = s.length;
	var r = 0;
	while(r < l && StringTools.isSpace(s,r)) r++;
	if(r > 0) return HxOverrides.substr(s,r,l - r); else return s;
};
var Three = function() { };
$hxClasses["Three"] = Three;
Three.__name__ = true;
var Type = function() { };
$hxClasses["Type"] = Type;
Type.__name__ = true;
Type.resolveClass = function(name) {
	var cl = $hxClasses[name];
	if(cl == null || !cl.__name__) return null;
	return cl;
};
Type.createInstance = function(cl,args) {
	var _g = args.length;
	switch(_g) {
	case 0:
		return new cl();
	case 1:
		return new cl(args[0]);
	case 2:
		return new cl(args[0],args[1]);
	case 3:
		return new cl(args[0],args[1],args[2]);
	case 4:
		return new cl(args[0],args[1],args[2],args[3]);
	case 5:
		return new cl(args[0],args[1],args[2],args[3],args[4]);
	case 6:
		return new cl(args[0],args[1],args[2],args[3],args[4],args[5]);
	case 7:
		return new cl(args[0],args[1],args[2],args[3],args[4],args[5],args[6]);
	case 8:
		return new cl(args[0],args[1],args[2],args[3],args[4],args[5],args[6],args[7]);
	default:
		throw new js__$Boot_HaxeError("Too many arguments");
	}
	return null;
};
Type.enumEq = function(a,b) {
	if(a == b) return true;
	try {
		if(a[0] != b[0]) return false;
		var _g1 = 2;
		var _g = a.length;
		while(_g1 < _g) {
			var i = _g1++;
			if(!Type.enumEq(a[i],b[i])) return false;
		}
		var e = a.__enum__;
		if(e != b.__enum__ || e == null) return false;
	} catch( e1 ) {
		if (e1 instanceof js__$Boot_HaxeError) e1 = e1.val;
		return false;
	}
	return true;
};
Type.allEnums = function(e) {
	return e.__empty_constructs__;
};
var _$UInt_UInt_$Impl_$ = {};
$hxClasses["_UInt.UInt_Impl_"] = _$UInt_UInt_$Impl_$;
_$UInt_UInt_$Impl_$.__name__ = true;
_$UInt_UInt_$Impl_$.gte = function(a,b) {
	var aNeg = a < 0;
	var bNeg = b < 0;
	if(aNeg != bNeg) return aNeg; else return a >= b;
};
_$UInt_UInt_$Impl_$.toFloat = function(this1) {
	var $int = this1;
	if($int < 0) return 4294967296.0 + $int; else return $int + 0.0;
};
var action_AbstractAction = function() {
};
$hxClasses["action.AbstractAction"] = action_AbstractAction;
action_AbstractAction.__name__ = true;
action_AbstractAction.prototype = {
	update: function(time,posi,rote) {
	}
	,init: function(posi,rote) {
	}
	,clone: function() {
		return new action_AbstractAction();
	}
	,__class__: action_AbstractAction
};
var action_JumpAction = function(data) {
	this.elapsedTime = 0;
	this.add = 0;
	this.progress = 0;
	this.vy = 0;
	action_AbstractAction.call(this);
	if(data == null) data = { };
	if(data.waitTime != null) this.waitTime = data.waitTime; else this.waitTime = 3000;
	if(data.gravity != null) this.g = data.gravity; else this.g = 0.08;
	if(data.initialVelocity != null) this.iv = data.initialVelocity; else this.iv = 8;
	this.vy = this.iv;
	if(data.up != null) this.up = data.up; else this.up = true;
};
$hxClasses["action.JumpAction"] = action_JumpAction;
action_JumpAction.__name__ = true;
action_JumpAction.__super__ = action_AbstractAction;
action_JumpAction.prototype = $extend(action_AbstractAction.prototype,{
	clone: function() {
		return new action_JumpAction({ waitTime : this.waitTime, gravity : this.g, initialVelocity : this.iv, up : this.up});
	}
	,update: function(time,posi,rote) {
		if(this.progress >= this.waitTime) {
			this.elapsedTime += time;
			var py = posi.y;
			var sign;
			if(this.up) sign = 1; else sign = -1;
			py += this.vy * sign;
			this.add += this.vy * sign;
			var cnt = 0;
			while(this.elapsedTime >= 16.666666666666668) {
				if(cnt > 0) {
					py += this.vy * sign;
					this.add += this.vy;
				}
				this.vy -= this.g;
				this.elapsedTime -= 16.666666666666668;
				cnt++;
			}
			if(this.up == true && this.add <= 0 || this.up == false && this.add >= 0) {
				this.progress = 0;
				this.vy = this.iv;
				py -= this.add;
				this.add = 0;
			}
			posi.setY(py);
		}
		this.progress += time;
	}
	,__class__: action_JumpAction
});
var action_MoveType = { __ename__ : true, __constructs__ : ["x","y","z"] };
action_MoveType.x = ["x",0];
action_MoveType.x.toString = $estr;
action_MoveType.x.__enum__ = action_MoveType;
action_MoveType.y = ["y",1];
action_MoveType.y.toString = $estr;
action_MoveType.y.__enum__ = action_MoveType;
action_MoveType.z = ["z",2];
action_MoveType.z.toString = $estr;
action_MoveType.z.__enum__ = action_MoveType;
action_MoveType.__empty_constructs__ = [action_MoveType.x,action_MoveType.y,action_MoveType.z];
var action_MoveAction = function(data) {
	this.moveType = action_MoveType.y;
	this.reverse = false;
	this.add = 0;
	this.deg = 0;
	this.speed = 40;
	this.distance = 1000;
	action_AbstractAction.call(this);
	if(data == null) data = { };
	if(data.distance != null) this.distance = data.distance;
	if(data.speed != null) this.speed = data.speed;
	if(data.reverse != null) this.reverse = data.reverse;
	if(data.moveType != null) this.moveType = data.moveType;
};
$hxClasses["action.MoveAction"] = action_MoveAction;
action_MoveAction.__name__ = true;
action_MoveAction.__super__ = action_AbstractAction;
action_MoveAction.prototype = $extend(action_AbstractAction.prototype,{
	update: function(time,posi,rote) {
		var move;
		if(!this.reverse) move = Math.sin(Math.PI / 180 * this.deg) * this.distance; else move = Math.sin(Math.PI / 180 * (360 - this.deg)) * this.distance;
		var _g = this.moveType;
		switch(_g[1]) {
		case 0:
			posi.x -= this.add;
			this.add = move;
			posi.x += this.add;
			break;
		case 1:
			posi.y -= this.add;
			this.add = move;
			posi.y += this.add;
			break;
		case 2:
			posi.z -= this.add;
			this.add = move;
			posi.z += this.add;
			break;
		}
		this.deg = (this.deg + this.speed * time / 1000) % 360;
	}
	,clone: function() {
		return new action_MoveAction({ distance : this.distance, speed : this.speed, reverse : this.reverse, moveType : this.moveType});
	}
	,__class__: action_MoveAction
});
var action_EDirection = { __ename__ : true, __constructs__ : ["x","y","z"] };
action_EDirection.x = ["x",0];
action_EDirection.x.toString = $estr;
action_EDirection.x.__enum__ = action_EDirection;
action_EDirection.y = ["y",1];
action_EDirection.y.toString = $estr;
action_EDirection.y.__enum__ = action_EDirection;
action_EDirection.z = ["z",2];
action_EDirection.z.toString = $estr;
action_EDirection.z.__enum__ = action_EDirection;
action_EDirection.__empty_constructs__ = [action_EDirection.x,action_EDirection.y,action_EDirection.z];
var action_RotateAction = function(data) {
	this.d = 0;
	action_AbstractAction.call(this);
	if(data != null) data = data; else data = { };
	if(data.speed != null) this.speed = data.speed; else this.speed = 90;
	if(data.right != null) this.right = data.right; else this.right = true;
	if(data.direction != null) this.direction = data.direction; else this.direction = action_EDirection.y;
};
$hxClasses["action.RotateAction"] = action_RotateAction;
action_RotateAction.__name__ = true;
action_RotateAction.__super__ = action_AbstractAction;
action_RotateAction.prototype = $extend(action_AbstractAction.prototype,{
	update: function(time,posi,rote) {
		var _g = this.direction;
		switch(_g[1]) {
		case 0:
			rote.x = Math.PI / 180 * this.d;
			break;
		case 1:
			rote.y = Math.PI / 180 * this.d;
			break;
		case 2:
			rote.z = Math.PI / 180 * this.d;
			break;
		}
		if(this.right == true) this.d = (this.d - this.speed * time / 1000) % 360; else this.d = (this.d + this.speed * time / 1000) % 360;
	}
	,clone: function() {
		return new action_RotateAction({ speed : this.speed, right : this.right, direction : this.direction});
	}
	,__class__: action_RotateAction
});
var action_RotateRandomAction = function(maxSpeed) {
	if(maxSpeed == null) maxSpeed = 90;
	this.dy = 0;
	this.dx = 0;
	action_AbstractAction.call(this);
	this.mSpeed = maxSpeed;
	this.speedX = Math.random() * maxSpeed / 1000;
	this.speedY = Math.random() * maxSpeed / 1000;
};
$hxClasses["action.RotateRandomAction"] = action_RotateRandomAction;
action_RotateRandomAction.__name__ = true;
action_RotateRandomAction.__super__ = action_AbstractAction;
action_RotateRandomAction.prototype = $extend(action_AbstractAction.prototype,{
	clone: function() {
		return new action_RotateRandomAction(this.mSpeed);
	}
	,update: function(time,posi,rote) {
		rote.x = Math.PI / 180 * this.dx;
		rote.y = Math.PI / 180 * this.dy;
		this.dx = (this.dx + this.speedX * time) % 360;
		this.dy = (this.dy + this.speedY * time) % 360;
	}
	,__class__: action_RotateRandomAction
});
var action_SetMoveAction = function(moveArray,loop) {
	if(loop == null) loop = false;
	this.end = false;
	this.nowIndex = 0;
	this.elapsedTime = 0;
	action_AbstractAction.call(this);
	if(moveArray == null || moveArray.length == 0) throw new js__$Boot_HaxeError("setMove No data");
	this.moveArray = moveArray;
	this.loop = loop;
	this.nowPoint = new THREE.Vector3();
	this.oldPoint = new THREE.Vector3();
};
$hxClasses["action.SetMoveAction"] = action_SetMoveAction;
action_SetMoveAction.__name__ = true;
action_SetMoveAction.__super__ = action_AbstractAction;
action_SetMoveAction.prototype = $extend(action_AbstractAction.prototype,{
	update: function(time,posi,rote) {
		action_AbstractAction.prototype.update.call(this,time,posi,rote);
		if(this.end) return;
		this.elapsedTime += time;
		if(this.elapsedTime >= this.moveArray[this.nowIndex].time) this.setNext();
		posi.x -= this.nowPoint.x;
		posi.y -= this.nowPoint.y;
		posi.z -= this.nowPoint.z;
		var nextPoint = this.moveArray[this.nowIndex].point;
		var nextTime = this.moveArray[this.nowIndex].time;
		this.nowPoint.x = this.oldPoint.x + nextPoint.x * (this.elapsedTime / nextTime);
		this.nowPoint.y = this.oldPoint.y + nextPoint.y * (this.elapsedTime / nextTime);
		this.nowPoint.z = this.oldPoint.z + nextPoint.z * (this.elapsedTime / nextTime);
		posi.x += this.nowPoint.x;
		posi.y += this.nowPoint.y;
		posi.z += this.nowPoint.z;
	}
	,setNext: function() {
		if(this.nowIndex + 1 >= this.moveArray.length && !this.loop) {
			this.elapsedTime = this.moveArray[this.nowIndex].time;
			this.end = true;
		} else {
			this.elapsedTime -= this.moveArray[this.nowIndex].time;
			var nextPoint = this.moveArray[this.nowIndex].point;
			this.oldPoint.x += nextPoint.x;
			this.oldPoint.y += nextPoint.y;
			this.oldPoint.z += nextPoint.z;
			this.nowIndex = (this.nowIndex + 1) % this.moveArray.length;
		}
	}
	,clone: function() {
		var array = [];
		var _g = 0;
		var _g1 = this.moveArray;
		while(_g < _g1.length) {
			var m = _g1[_g];
			++_g;
			array.push(m);
		}
		return new action_SetMoveAction(array,this.loop);
	}
	,__class__: action_SetMoveAction
});
var action_SetRotaAction = function(params,splitNum) {
	if(splitNum == null) splitNum = 20;
	this.dRota = 0;
	this.cnt = 0;
	this.nowIndex = 0;
	this.elapsedTime = 0;
	action_AbstractAction.call(this);
	this.rotas = [];
	if(params != null && params.length > 0) this.rotas = params; else this.setDefaultRota();
	this.splitNum = splitNum;
};
$hxClasses["action.SetRotaAction"] = action_SetRotaAction;
action_SetRotaAction.__name__ = true;
action_SetRotaAction.__super__ = action_AbstractAction;
action_SetRotaAction.prototype = $extend(action_AbstractAction.prototype,{
	clone: function() {
		var array = [];
		var _g = 0;
		var _g1 = this.rotas;
		while(_g < _g1.length) {
			var r = _g1[_g];
			++_g;
			array.push(r);
		}
		return new action_SetRotaAction(array,this.splitNum);
	}
	,update: function(time,posi,rota) {
		this.elapsedTime += time;
		if(this.elapsedTime > this.rotas[this.nowIndex].time) this.setDRota(rota);
		if(this.cnt < this.splitNum) {
			var _g = this.rotas[this.nowIndex].direction;
			switch(_g[1]) {
			case 0:
				rota.x += Math.PI / 180 * this.dRota;
				break;
			case 1:
				rota.y += Math.PI / 180 * this.dRota;
				break;
			case 2:
				rota.z += Math.PI / 180 * this.dRota;
				break;
			}
			this.cnt++;
			if(this.cnt == this.splitNum) {
				var _g1 = this.rotas[this.nowIndex].direction;
				switch(_g1[1]) {
				case 0:
					rota.x = Math.PI / 180 * this.rotas[this.nowIndex].rotation;
					break;
				case 1:
					rota.y = Math.PI / 180 * this.rotas[this.nowIndex].rotation;
					break;
				case 2:
					rota.z = Math.PI / 180 * this.rotas[this.nowIndex].rotation;
					break;
				}
			}
		}
	}
	,init: function(posi,rota) {
		this.setDRota(rota);
	}
	,setDRota: function(euler) {
		this.elapsedTime = 0;
		this.nowIndex = (this.nowIndex + 1) % this.rotas.length;
		var oldRota = 0;
		var _g = this.rotas[this.nowIndex].direction;
		switch(_g[1]) {
		case 0:
			oldRota = euler.x / Math.PI * 180 | 0;
			break;
		case 1:
			oldRota = euler.y / Math.PI * 180 | 0;
			break;
		case 2:
			oldRota = euler.z / Math.PI * 180 | 0;
			break;
		}
		this.cnt = 0;
		var diff = (this.rotas[this.nowIndex].rotation - oldRota + 540) % 360 - 180;
		this.dRota = diff / this.splitNum;
	}
	,setDefaultRota: function() {
		this.rotas = [{ time : 2000, rotation : 0, direction : action_EDirection.y},{ time : 1000, rotation : 90, direction : action_EDirection.y},{ time : 2000, rotation : 180, direction : action_EDirection.y},{ time : 1000, rotation : 270, direction : action_EDirection.y}];
	}
	,__class__: action_SetRotaAction
});
var camera_AbstractCamera = function() {
	THREE.PerspectiveCamera.call(this,60,GameMgr.WIDTH / GameMgr.HEIGHT,1,50000);
};
$hxClasses["camera.AbstractCamera"] = camera_AbstractCamera;
camera_AbstractCamera.__name__ = true;
camera_AbstractCamera.__super__ = THREE.PerspectiveCamera;
camera_AbstractCamera.prototype = $extend(THREE.PerspectiveCamera.prototype,{
	setPosition: function(data) {
	}
	,getTarget: function(data) {
		return { point : data.point.clone(), deg : data.deg};
	}
	,__class__: camera_AbstractCamera
});
var camera_FixedCamera = function() {
	camera_AbstractCamera.call(this);
};
$hxClasses["camera.FixedCamera"] = camera_FixedCamera;
camera_FixedCamera.__name__ = true;
camera_FixedCamera.__super__ = camera_AbstractCamera;
camera_FixedCamera.prototype = $extend(camera_AbstractCamera.prototype,{
	__class__: camera_FixedCamera
});
var camera_MoveCamera = function(vr,py) {
	if(py == null) py = 350;
	if(vr == null) vr = 1500;
	camera_AbstractCamera.call(this);
	this.visualRange = vr;
	this.target = new THREE.Vector3(0,0,this.visualRange);
	this.positionY = py;
	this.position.y = this.positionY;
};
$hxClasses["camera.MoveCamera"] = camera_MoveCamera;
camera_MoveCamera.__name__ = true;
camera_MoveCamera.__super__ = camera_AbstractCamera;
camera_MoveCamera.prototype = $extend(camera_AbstractCamera.prototype,{
	setPosition: function(data) {
		var point = data.point;
		var radian = Math.PI / 180 * data.deg;
		this.position.copy(point);
		this.position.y += this.positionY;
		this.target.copy(point);
		this.target.x += Math.cos(radian) * this.visualRange;
		this.target.z += Math.sin(radian) * this.visualRange;
		this.lookAt(this.target);
	}
	,getTarget: function(data) {
		var radian = Math.PI / 180 * data.deg;
		var point = data.point.clone();
		point.x += Math.cos(radian) * this.visualRange;
		point.z += Math.sin(radian) * this.visualRange;
		return { point : point, deg : data.deg};
	}
	,__class__: camera_MoveCamera
});
var event_Communicator = function() {
	this.wsurl = "ws://" + Std.string(Reflect.field(window,"WS_SERVER_HOST")) + "/shooting_websocket_servlet";
	this.ws = new WebSocket(this.wsurl);
	this.ws.onopen = $bind(this,this.open);
	this.ws.onerror = $bind(this,this.error);
	this.ws.onclose = $bind(this,this.close);
	this.ws.onmessage = $bind(this,this.message);
};
$hxClasses["event.Communicator"] = event_Communicator;
event_Communicator.__name__ = true;
event_Communicator.prototype = {
	message: function(message) {
		var msg = message.data;
		var sp = msg.indexOf(",");
		try {
			var _g = msg.substring(0,sp);
			switch(_g) {
			case "CONN":
				if(msg.substring(sp + 1) != "failre") GameMgr.load(msg.substring(sp + 1)); else GameMgr.changeScene(EScene.EndScene,["部屋を作るスペースがありませんでした。。。"]);
				break;
			case "ENTR":
				var np = msg.indexOf(",",sp + 1);
				var id = msg.substring(sp + 1,np);
				var result = GameMgr.playerEntry(id,msg.substring(np + 1));
				this.ws.send("ENTR," + id + "," + result);
				break;
			case "SHOT":
				GameMgr.playerShot(msg.substring(sp + 1));
				break;
			case "MOVE":
				var msgArray = msg.split(",");
				GameMgr.playerMove(msgArray[1],msgArray[2],msgArray[3]);
				break;
			case "PRDY":
				var np1 = msg.indexOf(",",sp + 1);
				GameMgr.playerReady(msg.substring(sp + 1,np1),msg.substring(np1 + 1));
				break;
			case "EXIT":
				GameMgr.playerExit(msg.substring(sp + 1));
				break;
			case "RANK":
				GameMgr.setRank(msg.substring(sp + 1));
				break;
			default:
				console.log("message error:" + msg);
			}
		} catch( unknown ) {
			if (unknown instanceof js__$Boot_HaxeError) unknown = unknown.val;
			console.log("message error:" + Std.string(unknown));
		}
	}
	,close: function() {
		GameMgr.changeScene(EScene.EndScene);
	}
	,error: function() {
		GameMgr.changeScene(EScene.EndScene,["エラーが発生したため接続が切れました"]);
	}
	,open: function() {
		this.ws.send("CONN,parent");
	}
	,allReady: function() {
		this.ws.send("ARDY,on");
	}
	,reset: function() {
		this.ws.send("ARDY,off");
	}
	,sendRank: function(playerId,stageId,score) {
		this.ws.send("RANK," + playerId + "," + stageId + "," + score);
	}
	,__class__: event_Communicator
};
var event_Input = function() {
	this.listeners = new haxe_ds_EnumValueMap();
	this.div = window.document.createElement("div");
	this.div.style.width = GameMgr.WIDTH + "px";
	this.div.style.height = GameMgr.HEIGHT + "px";
	this.div.style.position = "absolute";
	this.div.style.zIndex = "100";
	this.div.addEventListener("click",$bind(this,this.fire));
	window.document.body.appendChild(this.div);
};
$hxClasses["event.Input"] = event_Input;
event_Input.__name__ = true;
event_Input.prototype = {
	fire: function(htmlEvent) {
		var htmlInputEvent;
		htmlInputEvent = js_Boot.__cast(htmlEvent , MouseEvent);
		var ev = new event_InputEvent(htmlInputEvent.pageX - this.div.offsetLeft,htmlInputEvent.pageY - this.div.offsetTop);
		var ls = this.listeners.get(GameMgr.nowScene);
		if(ls != null) {
			var _g_head = ls.h;
			var _g_val = null;
			while(_g_head != null) {
				var l;
				l = (function($this) {
					var $r;
					_g_val = _g_head[0];
					_g_head = _g_head[1];
					$r = _g_val;
					return $r;
				}(this));
				l(ev);
			}
		}
	}
	,addEventListener: function(scene,listener) {
		if(scene == null) scene = GameMgr.nowScene;
		if(this.listeners.get(scene) == null) {
			var value = new List();
			this.listeners.set(scene,value);
		}
		this.listeners.get(scene).add(listener);
		return true;
	}
	,removeEventListener: function(scene,listener) {
		if(scene == null) scene = GameMgr.nowScene;
		if(this.listeners.get(scene) == null) return false;
		return this.listeners.get(scene).remove(listener);
	}
	,__class__: event_Input
};
var event_InputEvent = function(x,y) {
	this.x = x;
	this.y = y;
};
$hxClasses["event.InputEvent"] = event_InputEvent;
event_InputEvent.__name__ = true;
event_InputEvent.prototype = {
	__class__: event_InputEvent
};
var field_FieldFloor = function(cameraRange) {
	this.addField = 200;
	this.addValue = 0.3;
	this.correctionValue = 0;
	this.createMaterial(GameMgr.DIRECTORY + "images/floor01.jpg");
	this.radius = cameraRange + this.addField;
};
$hxClasses["field.FieldFloor"] = field_FieldFloor;
field_FieldFloor.__name__ = true;
field_FieldFloor.prototype = {
	setTexture: function(texture,pattarn) {
		this.createMaterial(texture,pattarn);
	}
	,setRouteData: function(data) {
		this.routeData = data;
	}
	,getFloorObjects: function(randomNum) {
		return this.createRouteObjects(randomNum);
	}
	,createMaterial: function(texture,pattern) {
		var floorTexture = THREE.ImageUtils.loadTexture(texture);
		floorTexture.wrapS = floorTexture.wrapT = 1000;
		floorTexture.anisotropy = GameMgr.getAnisotropy();
		if(pattern != null) floorTexture.repeat.set(pattern,pattern);
		this.material = new THREE.MeshPhongMaterial({ color : 16777215, map : floorTexture, shininess : 10});
	}
	,createRouteObjects: function(randomNum) {
		var floors = [];
		var routes = [];
		var _g = 0;
		var _g1 = this.routeData;
		while(_g < _g1.length) {
			var i = _g1[_g];
			++_g;
			routes.push(i);
		}
		routes.unshift({ time : 0, point : new THREE.Vector3()});
		var _g11 = 0;
		var _g2 = routes.length - 1;
		while(_g11 < _g2) {
			var i1 = _g11++;
			var floor = new obj3d_InvincibleObject(new shape_FloorShape(this.material,[routes[i1].point,routes[i1 + 1].point],this.radius,this.addValue * 3 - this.correctionValue));
			floors.push(floor);
			var middle = new obj3d_InvincibleObject(new shape_FloorShape(this.material,[routes[i1 + 1].point],this.radius,i1 % 2 * 0.1));
			floors.push(middle);
			this.correctionValue = (this.correctionValue + this.addValue) % (this.addValue * 3);
		}
		var treeManager = new field_FieldTree(this.radius,randomNum);
		var trees = treeManager.createTree(routes);
		var _g3 = 0;
		while(_g3 < trees.length) {
			var t = trees[_g3];
			++_g3;
			floors.push(t);
		}
		return floors;
	}
	,__class__: field_FieldFloor
};
var field_FieldObject = function(data) {
	this.first = true;
	this.rpps = 0;
	this.appTime = data.appTime;
	this.obj = data.obj;
	if(data.distance != null) this.distance = data.distance; else this.distance = 0;
	if(data.degree != null) this.degree = data.degree; else this.degree = 90;
	if(data.positionY != null) this.positionY = data.positionY; else this.positionY = 0;
	if(data.rpps != null) this.rpps = data.rpps; else this.rpps = 0;
	this.initObj = this.obj.clone();
};
$hxClasses["field.FieldObject"] = field_FieldObject;
field_FieldObject.__name__ = true;
field_FieldObject.prototype = {
	getObj: function() {
		return this.obj;
	}
	,init: function() {
		this.obj = this.initObj.clone();
	}
	,setObjPosition: function(target,d) {
		var radian = Math.PI / 180 * ((d + this.degree) % 360);
		var x = this.distance * Math.cos(radian) + target.x;
		var z = this.distance * Math.sin(radian) + target.z;
		var y = target.y + this.obj.position.y + this.positionY;
		this.obj.position.set(x,y,z);
	}
	,getAfterTime: function() {
		return this.rpps;
	}
	,getAppTime: function() {
		return this.appTime;
	}
	,__class__: field_FieldObject
};
var field_FieldTree = function(distance,randomNum) {
	if(randomNum == null) randomNum = 1;
	this.right = true;
	this.nowIndex = 0;
	this.appNums = [1,1,2];
	this.appTime = 1000;
	this.distance = distance;
	this.random = new Random(randomNum);
};
$hxClasses["field.FieldTree"] = field_FieldTree;
field_FieldTree.__name__ = true;
field_FieldTree.prototype = {
	createTree: function(routes) {
		var trees = [];
		var _g1 = 0;
		var _g = routes.length - 1;
		while(_g1 < _g) {
			var i = _g1++;
			var nowPoint = routes[i].point;
			var toPoint = routes[i + 1].point;
			var maxTime = routes[i].time;
			var radian = Math.atan2(toPoint.z - nowPoint.z,toPoint.x - nowPoint.x);
			var routeDistance = Math.sqrt(Math.pow(nowPoint.z - toPoint.z,2) + Math.pow(nowPoint.x - toPoint.x,2));
			var speed = routeDistance / maxTime;
			var circleTime = this.distance / speed;
			if(i == 0) this.appTime = circleTime; else this.appTime = 0;
			var beforeDirection = 0;
			var afterDirection = 0;
			if(i > 0) {
				var beforeRadian = Math.atan2(nowPoint.z - routes[i - 1].point.z,nowPoint.x - routes[i - 1].point.x);
				beforeDirection = (radian - beforeRadian + Math.PI * 3) % (Math.PI * 2) - Math.PI;
			}
			if(i < routes.length - 2) {
				var afterRadian = Math.atan2(routes[i + 2].point.z - toPoint.z,routes[i + 2].point.x - toPoint.x);
				afterDirection = (radian - afterRadian + Math.PI * 3) % (Math.PI * 2) - Math.PI;
			}
			while(this.appTime < maxTime) {
				var _g3 = 0;
				var _g2 = this.appNums[this.nowIndex];
				while(_g3 < _g2) {
					var n = _g3++;
					var tree = new obj3d_InvincibleObject(new shape_TreeShape());
					var centerPoint = new THREE.Vector3(nowPoint.x + routeDistance * (this.appTime / maxTime) * Math.cos(radian),0,nowPoint.z + routeDistance * (this.appTime / maxTime) * Math.sin(radian));
					centerPoint.x += n * (tree.getWidth() * Math.cos(radian));
					centerPoint.z += n * (tree.getWidth() * Math.sin(radian));
					if(this.appTime > circleTime && this.appTime < maxTime - circleTime || this.appTime > circleTime && afterDirection == 0) {
						if(this.right) {
							tree.position.x = centerPoint.x + this.distance * Math.cos(radian + Math.PI / 2);
							tree.position.z = centerPoint.z + this.distance * Math.sin(radian + Math.PI / 2);
						} else {
							tree.position.x = centerPoint.x + this.distance * Math.cos(radian - Math.PI / 2);
							tree.position.z = centerPoint.z + this.distance * Math.sin(radian - Math.PI / 2);
						}
					} else if(this.appTime <= circleTime) {
						if(beforeDirection < 0) {
							tree.position.x = centerPoint.x + this.distance * Math.cos(radian + Math.PI / 2);
							tree.position.z = centerPoint.z + this.distance * Math.sin(radian + Math.PI / 2);
						} else {
							tree.position.x = centerPoint.x + this.distance * Math.cos(radian - Math.PI / 2);
							tree.position.z = centerPoint.z + this.distance * Math.sin(radian - Math.PI / 2);
						}
					} else if(afterDirection > 0) {
						tree.position.x = centerPoint.x + this.distance * Math.cos(radian + Math.PI / 2);
						tree.position.z = centerPoint.z + this.distance * Math.sin(radian + Math.PI / 2);
					} else {
						tree.position.x = centerPoint.x + this.distance * Math.cos(radian - Math.PI / 2);
						tree.position.z = centerPoint.z + this.distance * Math.sin(radian - Math.PI / 2);
					}
					trees.push(tree);
				}
				this.nowIndex = (this.nowIndex + 1) % this.appNums.length;
				this.right = !this.right;
				var _g21 = this.appTime;
				var _g31;
				var a;
				var a1 = this.random.nextUInt();
				a = Std["int"](_$UInt_UInt_$Impl_$.toFloat(a1) % _$UInt_UInt_$Impl_$.toFloat(2000));
				_g31 = a + 1000;
				this.appTime = _$UInt_UInt_$Impl_$.toFloat(_g31) + _g21;
			}
		}
		return trees;
	}
	,__class__: field_FieldTree
};
var field_ObjectPerTime = function(wait) {
	if(wait == null) wait = 3000;
	this.waitTime = 0;
	this.progress = 0;
	this.targetIndex = 0;
	this.waitTime = wait;
	this.setTimeObjects(this.getDefaultObjects());
};
$hxClasses["field.ObjectPerTime"] = field_ObjectPerTime;
field_ObjectPerTime.__name__ = true;
field_ObjectPerTime.prototype = {
	setTimeObjects: function(data) {
		data.sort(function(a,b) {
			if(a.appTime < b.appTime) return -1;
			if(a.appTime > b.appTime) return 1;
			return 0;
		});
		this.objects = [];
		var _g = 0;
		while(_g < data.length) {
			var i = data[_g];
			++_g;
			this.objects[this.objects.length] = new field_FieldObject(i);
		}
	}
	,init: function() {
		this.progress = 0 - this.waitTime;
		this.targetIndex = 0;
		this.setObj = [];
		var _g = 0;
		var _g1 = this.objects;
		while(_g < _g1.length) {
			var o = _g1[_g];
			++_g;
			o.init();
		}
	}
	,update: function(time) {
		this.progress += time;
		while(this.objects.length > this.targetIndex && this.objects[this.targetIndex].getAppTime() < this.progress) {
			this.setObj[this.setObj.length] = this.objects[this.targetIndex];
			this.targetIndex++;
		}
	}
	,getSetObject: function() {
		var copy = this.setObj.slice();
		this.setObj = [];
		return copy;
	}
	,getDefaultObjects: function() {
		var maxTime = 10;
		var interval = 500;
		var array = [];
		var time = 0;
		while(time < maxTime) {
			time += interval;
			var obj = new obj3d_BreakObject(new shape_CrystalShape(),[new action_RotateAction()]);
			array[array.length] = { rpps : time + 2000, appTime : time, obj : obj};
		}
		return array;
	}
	,__class__: field_ObjectPerTime
};
var haxe_IMap = function() { };
$hxClasses["haxe.IMap"] = haxe_IMap;
haxe_IMap.__name__ = true;
var haxe_ds_BalancedTree = function() {
};
$hxClasses["haxe.ds.BalancedTree"] = haxe_ds_BalancedTree;
haxe_ds_BalancedTree.__name__ = true;
haxe_ds_BalancedTree.prototype = {
	set: function(key,value) {
		this.root = this.setLoop(key,value,this.root);
	}
	,get: function(key) {
		var node = this.root;
		while(node != null) {
			var c = this.compare(key,node.key);
			if(c == 0) return node.value;
			if(c < 0) node = node.left; else node = node.right;
		}
		return null;
	}
	,iterator: function() {
		var ret = [];
		this.iteratorLoop(this.root,ret);
		return HxOverrides.iter(ret);
	}
	,setLoop: function(k,v,node) {
		if(node == null) return new haxe_ds_TreeNode(null,k,v,null);
		var c = this.compare(k,node.key);
		if(c == 0) return new haxe_ds_TreeNode(node.left,k,v,node.right,node == null?0:node._height); else if(c < 0) {
			var nl = this.setLoop(k,v,node.left);
			return this.balance(nl,node.key,node.value,node.right);
		} else {
			var nr = this.setLoop(k,v,node.right);
			return this.balance(node.left,node.key,node.value,nr);
		}
	}
	,iteratorLoop: function(node,acc) {
		if(node != null) {
			this.iteratorLoop(node.left,acc);
			acc.push(node.value);
			this.iteratorLoop(node.right,acc);
		}
	}
	,balance: function(l,k,v,r) {
		var hl;
		if(l == null) hl = 0; else hl = l._height;
		var hr;
		if(r == null) hr = 0; else hr = r._height;
		if(hl > hr + 2) {
			if((function($this) {
				var $r;
				var _this = l.left;
				$r = _this == null?0:_this._height;
				return $r;
			}(this)) >= (function($this) {
				var $r;
				var _this1 = l.right;
				$r = _this1 == null?0:_this1._height;
				return $r;
			}(this))) return new haxe_ds_TreeNode(l.left,l.key,l.value,new haxe_ds_TreeNode(l.right,k,v,r)); else return new haxe_ds_TreeNode(new haxe_ds_TreeNode(l.left,l.key,l.value,l.right.left),l.right.key,l.right.value,new haxe_ds_TreeNode(l.right.right,k,v,r));
		} else if(hr > hl + 2) {
			if((function($this) {
				var $r;
				var _this2 = r.right;
				$r = _this2 == null?0:_this2._height;
				return $r;
			}(this)) > (function($this) {
				var $r;
				var _this3 = r.left;
				$r = _this3 == null?0:_this3._height;
				return $r;
			}(this))) return new haxe_ds_TreeNode(new haxe_ds_TreeNode(l,k,v,r.left),r.key,r.value,r.right); else return new haxe_ds_TreeNode(new haxe_ds_TreeNode(l,k,v,r.left.left),r.left.key,r.left.value,new haxe_ds_TreeNode(r.left.right,r.key,r.value,r.right));
		} else return new haxe_ds_TreeNode(l,k,v,r,(hl > hr?hl:hr) + 1);
	}
	,compare: function(k1,k2) {
		return Reflect.compare(k1,k2);
	}
	,__class__: haxe_ds_BalancedTree
};
var haxe_ds_TreeNode = function(l,k,v,r,h) {
	if(h == null) h = -1;
	this.left = l;
	this.key = k;
	this.value = v;
	this.right = r;
	if(h == -1) this._height = ((function($this) {
		var $r;
		var _this = $this.left;
		$r = _this == null?0:_this._height;
		return $r;
	}(this)) > (function($this) {
		var $r;
		var _this1 = $this.right;
		$r = _this1 == null?0:_this1._height;
		return $r;
	}(this))?(function($this) {
		var $r;
		var _this2 = $this.left;
		$r = _this2 == null?0:_this2._height;
		return $r;
	}(this)):(function($this) {
		var $r;
		var _this3 = $this.right;
		$r = _this3 == null?0:_this3._height;
		return $r;
	}(this))) + 1; else this._height = h;
};
$hxClasses["haxe.ds.TreeNode"] = haxe_ds_TreeNode;
haxe_ds_TreeNode.__name__ = true;
haxe_ds_TreeNode.prototype = {
	__class__: haxe_ds_TreeNode
};
var haxe_ds_EnumValueMap = function() {
	haxe_ds_BalancedTree.call(this);
};
$hxClasses["haxe.ds.EnumValueMap"] = haxe_ds_EnumValueMap;
haxe_ds_EnumValueMap.__name__ = true;
haxe_ds_EnumValueMap.__interfaces__ = [haxe_IMap];
haxe_ds_EnumValueMap.__super__ = haxe_ds_BalancedTree;
haxe_ds_EnumValueMap.prototype = $extend(haxe_ds_BalancedTree.prototype,{
	compare: function(k1,k2) {
		var d = k1[1] - k2[1];
		if(d != 0) return d;
		var p1 = k1.slice(2);
		var p2 = k2.slice(2);
		if(p1.length == 0 && p2.length == 0) return 0;
		return this.compareArgs(p1,p2);
	}
	,compareArgs: function(a1,a2) {
		var ld = a1.length - a2.length;
		if(ld != 0) return ld;
		var _g1 = 0;
		var _g = a1.length;
		while(_g1 < _g) {
			var i = _g1++;
			var d = this.compareArg(a1[i],a2[i]);
			if(d != 0) return d;
		}
		return 0;
	}
	,compareArg: function(v1,v2) {
		if(Reflect.isEnumValue(v1) && Reflect.isEnumValue(v2)) return this.compare(v1,v2); else if((v1 instanceof Array) && v1.__enum__ == null && ((v2 instanceof Array) && v2.__enum__ == null)) return this.compareArgs(v1,v2); else return Reflect.compare(v1,v2);
	}
	,__class__: haxe_ds_EnumValueMap
});
var haxe_ds__$StringMap_StringMapIterator = function(map,keys) {
	this.map = map;
	this.keys = keys;
	this.index = 0;
	this.count = keys.length;
};
$hxClasses["haxe.ds._StringMap.StringMapIterator"] = haxe_ds__$StringMap_StringMapIterator;
haxe_ds__$StringMap_StringMapIterator.__name__ = true;
haxe_ds__$StringMap_StringMapIterator.prototype = {
	hasNext: function() {
		return this.index < this.count;
	}
	,next: function() {
		return this.map.get(this.keys[this.index++]);
	}
	,__class__: haxe_ds__$StringMap_StringMapIterator
};
var haxe_ds_StringMap = function() {
	this.h = { };
};
$hxClasses["haxe.ds.StringMap"] = haxe_ds_StringMap;
haxe_ds_StringMap.__name__ = true;
haxe_ds_StringMap.__interfaces__ = [haxe_IMap];
haxe_ds_StringMap.prototype = {
	set: function(key,value) {
		if(__map_reserved[key] != null) this.setReserved(key,value); else this.h[key] = value;
	}
	,get: function(key) {
		if(__map_reserved[key] != null) return this.getReserved(key);
		return this.h[key];
	}
	,setReserved: function(key,value) {
		if(this.rh == null) this.rh = { };
		this.rh["$" + key] = value;
	}
	,getReserved: function(key) {
		if(this.rh == null) return null; else return this.rh["$" + key];
	}
	,remove: function(key) {
		if(__map_reserved[key] != null) {
			key = "$" + key;
			if(this.rh == null || !this.rh.hasOwnProperty(key)) return false;
			delete(this.rh[key]);
			return true;
		} else {
			if(!this.h.hasOwnProperty(key)) return false;
			delete(this.h[key]);
			return true;
		}
	}
	,arrayKeys: function() {
		var out = [];
		for( var key in this.h ) {
		if(this.h.hasOwnProperty(key)) out.push(key);
		}
		if(this.rh != null) {
			for( var key in this.rh ) {
			if(key.charCodeAt(0) == 36) out.push(key.substr(1));
			}
		}
		return out;
	}
	,iterator: function() {
		return new haxe_ds__$StringMap_StringMapIterator(this,this.arrayKeys());
	}
	,__class__: haxe_ds_StringMap
};
var js__$Boot_HaxeError = function(val) {
	Error.call(this);
	this.val = val;
	this.message = String(val);
	if(Error.captureStackTrace) Error.captureStackTrace(this,js__$Boot_HaxeError);
};
$hxClasses["js._Boot.HaxeError"] = js__$Boot_HaxeError;
js__$Boot_HaxeError.__name__ = true;
js__$Boot_HaxeError.__super__ = Error;
js__$Boot_HaxeError.prototype = $extend(Error.prototype,{
	__class__: js__$Boot_HaxeError
});
var js_Boot = function() { };
$hxClasses["js.Boot"] = js_Boot;
js_Boot.__name__ = true;
js_Boot.getClass = function(o) {
	if((o instanceof Array) && o.__enum__ == null) return Array; else {
		var cl = o.__class__;
		if(cl != null) return cl;
		var name = js_Boot.__nativeClassName(o);
		if(name != null) return js_Boot.__resolveNativeClass(name);
		return null;
	}
};
js_Boot.__string_rec = function(o,s) {
	if(o == null) return "null";
	if(s.length >= 5) return "<...>";
	var t = typeof(o);
	if(t == "function" && (o.__name__ || o.__ename__)) t = "object";
	switch(t) {
	case "object":
		if(o instanceof Array) {
			if(o.__enum__) {
				if(o.length == 2) return o[0];
				var str2 = o[0] + "(";
				s += "\t";
				var _g1 = 2;
				var _g = o.length;
				while(_g1 < _g) {
					var i1 = _g1++;
					if(i1 != 2) str2 += "," + js_Boot.__string_rec(o[i1],s); else str2 += js_Boot.__string_rec(o[i1],s);
				}
				return str2 + ")";
			}
			var l = o.length;
			var i;
			var str1 = "[";
			s += "\t";
			var _g2 = 0;
			while(_g2 < l) {
				var i2 = _g2++;
				str1 += (i2 > 0?",":"") + js_Boot.__string_rec(o[i2],s);
			}
			str1 += "]";
			return str1;
		}
		var tostr;
		try {
			tostr = o.toString;
		} catch( e ) {
			if (e instanceof js__$Boot_HaxeError) e = e.val;
			return "???";
		}
		if(tostr != null && tostr != Object.toString && typeof(tostr) == "function") {
			var s2 = o.toString();
			if(s2 != "[object Object]") return s2;
		}
		var k = null;
		var str = "{\n";
		s += "\t";
		var hasp = o.hasOwnProperty != null;
		for( var k in o ) {
		if(hasp && !o.hasOwnProperty(k)) {
			continue;
		}
		if(k == "prototype" || k == "__class__" || k == "__super__" || k == "__interfaces__" || k == "__properties__") {
			continue;
		}
		if(str.length != 2) str += ", \n";
		str += s + k + " : " + js_Boot.__string_rec(o[k],s);
		}
		s = s.substring(1);
		str += "\n" + s + "}";
		return str;
	case "function":
		return "<function>";
	case "string":
		return o;
	default:
		return String(o);
	}
};
js_Boot.__interfLoop = function(cc,cl) {
	if(cc == null) return false;
	if(cc == cl) return true;
	var intf = cc.__interfaces__;
	if(intf != null) {
		var _g1 = 0;
		var _g = intf.length;
		while(_g1 < _g) {
			var i = _g1++;
			var i1 = intf[i];
			if(i1 == cl || js_Boot.__interfLoop(i1,cl)) return true;
		}
	}
	return js_Boot.__interfLoop(cc.__super__,cl);
};
js_Boot.__instanceof = function(o,cl) {
	if(cl == null) return false;
	switch(cl) {
	case Int:
		return (o|0) === o;
	case Float:
		return typeof(o) == "number";
	case Bool:
		return typeof(o) == "boolean";
	case String:
		return typeof(o) == "string";
	case Array:
		return (o instanceof Array) && o.__enum__ == null;
	case Dynamic:
		return true;
	default:
		if(o != null) {
			if(typeof(cl) == "function") {
				if(o instanceof cl) return true;
				if(js_Boot.__interfLoop(js_Boot.getClass(o),cl)) return true;
			} else if(typeof(cl) == "object" && js_Boot.__isNativeObj(cl)) {
				if(o instanceof cl) return true;
			}
		} else return false;
		if(cl == Class && o.__name__ != null) return true;
		if(cl == Enum && o.__ename__ != null) return true;
		return o.__enum__ == cl;
	}
};
js_Boot.__cast = function(o,t) {
	if(js_Boot.__instanceof(o,t)) return o; else throw new js__$Boot_HaxeError("Cannot cast " + Std.string(o) + " to " + Std.string(t));
};
js_Boot.__nativeClassName = function(o) {
	var name = js_Boot.__toStr.call(o).slice(8,-1);
	if(name == "Object" || name == "Function" || name == "Math" || name == "JSON") return null;
	return name;
};
js_Boot.__isNativeObj = function(o) {
	return js_Boot.__nativeClassName(o) != null;
};
js_Boot.__resolveNativeClass = function(name) {
	return (Function("return typeof " + name + " != \"undefined\" ? " + name + " : null"))();
};
var js_Cookie = function() { };
$hxClasses["js.Cookie"] = js_Cookie;
js_Cookie.__name__ = true;
js_Cookie.set = function(name,value,expireDelay,path,domain) {
	var s = name + "=" + encodeURIComponent(value);
	if(expireDelay != null) {
		var d = DateTools.delta(new Date(),expireDelay * 1000);
		s += ";expires=" + d.toGMTString();
	}
	if(path != null) s += ";path=" + path;
	if(domain != null) s += ";domain=" + domain;
	window.document.cookie = s;
};
js_Cookie.all = function() {
	var h = new haxe_ds_StringMap();
	var a = window.document.cookie.split(";");
	var _g = 0;
	while(_g < a.length) {
		var e = a[_g];
		++_g;
		e = StringTools.ltrim(e);
		var t = e.split("=");
		if(t.length < 2) continue;
		h.set(t[0],decodeURIComponent(t[1].split("+").join(" ")));
	}
	return h;
};
js_Cookie.get = function(name) {
	return js_Cookie.all().get(name);
};
var obj2d_AbstractObj2d = function(x,y) {
	this.fps = 60;
	this.dy = 0;
	this.dx = 0;
	this.x = x;
	this.y = y;
};
$hxClasses["obj2d.AbstractObj2d"] = obj2d_AbstractObj2d;
obj2d_AbstractObj2d.__name__ = true;
obj2d_AbstractObj2d.prototype = {
	draw: function(context,time) {
		this.x += this.dx * (time / (1000 / this.fps));
		this.y += this.dy * (time / (1000 / this.fps));
	}
	,add: function(dx,dy) {
		this.dx = dx;
		this.dy = dy;
	}
	,action: function() {
	}
	,checkHit: function(px,py) {
		return false;
	}
	,setX: function(x) {
		this.x = x;
	}
	,__class__: obj2d_AbstractObj2d
};
var obj2d_AudioButton = function(x,y) {
	this.audio = Audio.getInstance();
	this.audioImages = [];
	obj2d_AbstractObj2d.call(this,x,y);
	var _g = 0;
	while(_g < 2) {
		var i = _g++;
		var image;
		var _this = window.document;
		image = _this.createElement("img");
		image.src = GameMgr.DIRECTORY + "images/audio_" + i + ".png";
		this.audioImages[i] = image;
	}
	this.width = GameMgr.WIDTH / 12;
	this.height = GameMgr.WIDTH / 12;
	var cookieData = js_Cookie.get("audio");
	if(cookieData != null && cookieData == "off") this.audio.setMuteMain(true);
};
$hxClasses["obj2d.AudioButton"] = obj2d_AudioButton;
obj2d_AudioButton.__name__ = true;
obj2d_AudioButton.__super__ = obj2d_AbstractObj2d;
obj2d_AudioButton.prototype = $extend(obj2d_AbstractObj2d.prototype,{
	draw: function(context,time) {
		obj2d_AbstractObj2d.prototype.draw.call(this,context,time);
		if(!this.audio.isMuteMain()) context.drawImage(this.audioImages[0],this.x,this.y,this.width,this.height); else context.drawImage(this.audioImages[1],this.x,this.y,this.width,this.height);
	}
	,checkHit: function(px,py) {
		if(px >= this.x && px <= this.x + this.width && py >= this.y && py <= this.y + this.height) {
			var value;
			if(this.audio.isMuteMain()) value = "on"; else value = "off";
			js_Cookie.set("audio",value,604800);
			this.audio.setMuteMain(!this.audio.isMuteMain());
			return true;
		}
		return false;
	}
	,__class__: obj2d_AudioButton
});
var obj2d_MoveText = function(x,y,texts,textSize,speed,waitTime) {
	if(waitTime == null) waitTime = 3000;
	if(speed == null) speed = -2;
	if(textSize == null) textSize = 17;
	this.show = true;
	this.centerStopTime = 3000;
	this.move = false;
	this.elapsedTime = 0;
	this.nowIndex = -1;
	obj2d_AbstractObj2d.call(this,x,y);
	this.initX = x;
	this.texts = texts;
	this.waitTime = waitTime;
	if(speed > 0) this.speed = -speed; else this.speed = speed;
	this.textSize = textSize;
};
$hxClasses["obj2d.MoveText"] = obj2d_MoveText;
obj2d_MoveText.__name__ = true;
obj2d_MoveText.__super__ = obj2d_AbstractObj2d;
obj2d_MoveText.prototype = $extend(obj2d_AbstractObj2d.prototype,{
	draw: function(context,time) {
		if(!this.show) return;
		obj2d_AbstractObj2d.prototype.draw.call(this,context,time);
		if(this.nowIndex == -1) this.nowIndex = 0;
		if(!this.move) {
			this.elapsedTime += time;
			if(this.elapsedTime > this.waitTime) this.setMove();
		}
		if(this.x <= GameMgr.WIDTH / 2 - this.textSize * this.texts[this.nowIndex].length / 2) {
			this.elapsedTime += time;
			this.centerStop();
		}
		if(this.x < -this.textSize * this.texts[this.nowIndex].length) this.initMove();
		context.textAlign = "left";
		context.fillStyle = "#ff0000";
		context.font = this.textSize + "px bold \"Times New Roman\"";
		context.fillText(this.texts[this.nowIndex],this.x,this.y);
	}
	,centerStop: function() {
		if(this.elapsedTime < this.centerStopTime) this.dx = 0; else this.dx = this.speed;
	}
	,setMove: function() {
		this.dx = this.speed;
		this.move = true;
		this.elapsedTime = 0;
	}
	,initMove: function() {
		this.dx = 0;
		this.x = this.initX;
		this.elapsedTime = 0;
		this.move = false;
		this.nowIndex = (this.nowIndex + 1) % this.texts.length;
	}
	,isShow: function() {
		return this.show;
	}
	,showText: function() {
		this.show = true;
	}
	,hideText: function() {
		this.show = false;
	}
	,__class__: obj2d_MoveText
});
var obj2d_MultiScoreBar = function(textSize,width,players) {
	this.space = 40;
	this.textSize = 17;
	this.width = 0;
	obj2d_AbstractObj2d.call(this,0,0);
	this.textSize = textSize;
	this.width = width;
	this.players = players;
};
$hxClasses["obj2d.MultiScoreBar"] = obj2d_MultiScoreBar;
obj2d_MultiScoreBar.__name__ = true;
obj2d_MultiScoreBar.__super__ = obj2d_AbstractObj2d;
obj2d_MultiScoreBar.prototype = $extend(obj2d_AbstractObj2d.prototype,{
	draw: function(context,time) {
		obj2d_AbstractObj2d.prototype.draw.call(this,context,time);
		context.beginPath();
		context.globalAlpha = 0.7;
		context.fillStyle = "#F78181";
		context.fillRect(0,0,this.width / 4,this.textSize * 2 + 8);
		context.fillStyle = "#81DAF5";
		context.fillRect(this.width / 4,0,this.width / 4,this.textSize * 2 + 8);
		context.fillStyle = "#9FF781";
		context.fillRect(this.width / 2,0,this.width / 4,this.textSize * 2 + 8);
		context.fillStyle = "#d584e0";
		context.fillRect(this.width / 4 * 3,0,this.width / 4,this.textSize * 2 + 8);
		context.globalAlpha = 1;
		context.fillStyle = "#000000";
		context.font = this.textSize + "px bold \"Times New Roman\"";
		var _g = 0;
		var _g1 = this.players;
		while(_g < _g1.length) {
			var player = _g1[_g];
			++_g;
			var _g2 = player.getNumber();
			switch(_g2) {
			case 0:
				context.fillText(player.getName(),this.space,this.textSize);
				context.fillText(player.getScore() + "点",this.space,this.textSize * 2);
				break;
			case 1:
				context.fillText(player.getName(),this.width / 4 + this.space,this.textSize);
				context.fillText(player.getScore() + "点",this.width / 4 + this.space,this.textSize * 2);
				break;
			case 2:
				context.fillText(player.getName(),this.width / 2 + 40,this.textSize);
				context.fillText(player.getScore() + "点",this.width / 2 + this.space,this.textSize * 2);
				break;
			case 3:
				context.fillText(player.getName(),this.width / 4 * 3 + 40,this.textSize);
				context.fillText(player.getScore() + "点",this.width / 4 * 3 + this.space,this.textSize * 2);
				break;
			}
		}
		context.closePath();
	}
	,__class__: obj2d_MultiScoreBar
});
var obj2d_PlayerBanner = function(x,y) {
	this.readysColors = ["#F78181","#81DAF5","#9FF781","#d584e0"];
	this.textSize = GameMgr.HEIGHT / 80 * 3;
	this.height = 0;
	this.width = 0;
	this.lineW = 2;
	obj2d_AbstractObj2d.call(this,x,y);
	this.width = GameMgr.WIDTH / 1.5;
	this.height = this.textSize * 1.6 * 2 + this.lineW * 3;
	this.readyWidth = this.textSize * 4;
};
$hxClasses["obj2d.PlayerBanner"] = obj2d_PlayerBanner;
obj2d_PlayerBanner.__name__ = true;
obj2d_PlayerBanner.__super__ = obj2d_AbstractObj2d;
obj2d_PlayerBanner.prototype = $extend(obj2d_AbstractObj2d.prototype,{
	draw: function(context,time) {
		obj2d_AbstractObj2d.prototype.draw.call(this,context,time);
		context.beginPath();
		context.fillStyle = "#ffffff";
		context.fillRect(this.lineW + this.x,this.y + this.lineW,this.width,this.height);
		context.fillStyle = "#000000";
		context.fillRect(this.x,this.y,this.width + this.lineW,this.lineW);
		context.fillRect(this.x,this.y + this.lineW,this.lineW,this.height);
		context.fillRect(this.x + this.width / 2,this.y + this.lineW,this.lineW,this.height);
		context.fillRect(this.x,this.y + this.height / 2,this.width + this.lineW,this.lineW);
		context.fillRect(this.x,this.y + this.height + this.lineW,this.width + this.lineW,this.lineW);
		context.fillRect(this.x + this.width,this.y + this.lineW,this.lineW,this.height);
		context.closePath();
		var $it0 = GameMgr.players.iterator();
		while( $it0.hasNext() ) {
			var p = $it0.next();
			context.beginPath();
			var nameX = this.x + this.textSize;
			var nameY = this.y + this.textSize * 1.2 + this.lineW;
			var readyX = this.x + this.width / 2 - this.readyWidth;
			var readyY = this.y + this.lineW;
			var readyText;
			if(p.isReady()) readyText = "完了"; else readyText = "準備";
			var readyHeight = this.height / 2 - this.lineW;
			var _g = p.getNumber();
			switch(_g) {
			case 1:
				nameY += this.height / 2 + this.lineW;
				readyY += this.height / 2;
				readyHeight += this.lineW;
				break;
			case 2:
				nameX += this.width / 2;
				readyX += this.width / 2;
				break;
			case 3:
				nameY += this.height / 2 + this.lineW;
				readyY += this.height / 2;
				nameX += this.width / 2;
				readyX += this.width / 2;
				readyHeight += this.lineW;
				break;
			}
			if(GameMgr.isTeamBattle()) {
				var key = p.getTeamColor();
				context.fillStyle = GameMgr.teamColor.get(key);
			} else context.fillStyle = this.readysColors[p.getNumber()];
			context.fillRect(readyX,readyY,this.readyWidth,readyHeight);
			context.fillStyle = "#000000";
			context.textAlign = "left";
			context.font = this.textSize + "px bold \"Times New Roman\"";
			context.fillText(readyText,readyX + this.textSize,nameY);
			context.fillText(p.getName(),nameX,nameY);
			context.closePath();
		}
	}
	,getHeight: function() {
		return this.height;
	}
	,__class__: obj2d_PlayerBanner
});
var obj2d_PlayerResult = function(text,arrayNum,maxNum,pNum,score,name,startTime) {
	this.endX = GameMgr.WIDTH / 4;
	obj2d_AbstractObj2d.call(this,GameMgr.WIDTH,0);
	this.text = text;
	this.startTime = startTime;
	this.elapsedTime = 0;
	this.color = "#" + GameMgr.playerColor[pNum];
	this.score = score;
	this.name = name;
	this.dx = -40;
	this.heigth = GameMgr.HEIGHT / 7;
	this.width = GameMgr.WIDTH / 3;
	this.space = this.heigth / 3;
	switch(maxNum) {
	case 5:
		break;
	case 4:
		switch(arrayNum) {
		case 1:
			this.y = GameMgr.HEIGHT / 2 - (this.space * 1.5 + this.heigth * 2);
			break;
		case 2:
			this.y = GameMgr.HEIGHT / 2 - (this.space / 2 + this.heigth);
			break;
		case 3:
			this.y = GameMgr.HEIGHT / 2 + this.space / 2;
			break;
		case 4:
			this.y = GameMgr.HEIGHT / 2 + this.space * 1.5 + this.heigth;
			break;
		}
		break;
	case 3:
		switch(arrayNum) {
		case 1:
			this.y = GameMgr.HEIGHT / 2 - (this.space + this.heigth * 1.5);
			break;
		case 2:
			this.y = GameMgr.HEIGHT / 2 - this.heigth / 2;
			break;
		case 3:
			this.y = GameMgr.HEIGHT / 2 + (this.space + this.heigth / 2);
			break;
		}
		break;
	case 2:
		switch(arrayNum) {
		case 1:
			this.y = GameMgr.HEIGHT / 2 - (this.space + this.heigth * 1.5);
			break;
		case 2:
			this.y = GameMgr.HEIGHT / 2 + (this.space + this.heigth / 2);
			break;
		}
		break;
	case 1:
		this.y = GameMgr.HEIGHT / 2 - this.heigth / 2;
		break;
	}
};
$hxClasses["obj2d.PlayerResult"] = obj2d_PlayerResult;
obj2d_PlayerResult.__name__ = true;
obj2d_PlayerResult.__super__ = obj2d_AbstractObj2d;
obj2d_PlayerResult.prototype = $extend(obj2d_AbstractObj2d.prototype,{
	draw: function(context,time) {
		this.elapsedTime += time;
		if(this.elapsedTime < this.startTime) return;
		obj2d_AbstractObj2d.prototype.draw.call(this,context,time);
		if(this.x < this.endX) {
			this.x = this.endX;
			this.dx = 0;
		}
		context.beginPath();
		context.textAlign = "left";
		context.fillStyle = this.color;
		context.globalAlpha = 0.7;
		context.fillRect(this.x,this.y,this.width,this.heigth);
		context.globalAlpha = 1;
		context.beginPath();
		context.strokeStyle = "#000000";
		context.fillStyle = "#000000";
		context.strokeRect(this.x,this.y,this.width,this.heigth);
		context.beginPath();
		context.font = this.heigth * 0.4 + "px bold \"Times New Roman\"";
		context.fillText(this.text + " " + this.score + " 点",this.x + this.heigth * 0.2,this.y + this.heigth * 0.5);
		context.fillText(this.name,this.x + this.heigth * 0.4,this.y + this.heigth * 0.9);
		context.closePath();
	}
	,__class__: obj2d_PlayerResult
});
var obj2d_QrcodeObj = function(x,y,type,level,url,scale) {
	this.height = 0;
	this.width = 0;
	this.tileH = 128;
	this.tileW = 128;
	obj2d_AbstractObj2d.call(this,x,y);
	this.qrcode = new QRCode(type,level);
	this.qrcode.addData(url);
	this.qrcode.make();
	this.scale = scale;
	this.width = this.tileW * scale;
	this.height = this.tileH * scale;
	this.tileW = this.tileW / this.qrcode.getModuleCount();
	this.tileH = this.tileH / this.qrcode.getModuleCount();
};
$hxClasses["obj2d.QrcodeObj"] = obj2d_QrcodeObj;
obj2d_QrcodeObj.__name__ = true;
obj2d_QrcodeObj.__super__ = obj2d_AbstractObj2d;
obj2d_QrcodeObj.prototype = $extend(obj2d_AbstractObj2d.prototype,{
	draw: function(context,time) {
		obj2d_AbstractObj2d.prototype.draw.call(this,context,time);
		var _g1 = 0;
		var _g = this.qrcode.getModuleCount();
		while(_g1 < _g) {
			var row = _g1++;
			var _g3 = 0;
			var _g2 = this.qrcode.getModuleCount();
			while(_g3 < _g2) {
				var col = _g3++;
				if(this.qrcode.isDark(row,col)) context.fillStyle = "#000000"; else context.fillStyle = "#ffffff";
				var w = Math.ceil((col + 1) * this.tileW) - Math.floor(col * this.tileW);
				var h = Math.ceil((row + 1) * this.tileH) - Math.floor(row * this.tileH);
				context.fillRect(this.x + Math.round(col * this.tileW) * this.scale,this.y + Math.round(row * this.tileH) * this.scale,w * this.scale,h * this.scale);
			}
		}
	}
	,getWidth: function() {
		return this.width;
	}
	,getHeight: function() {
		return this.height;
	}
	,__class__: obj2d_QrcodeObj
});
var obj2d_TargetRect = function(x,y,text,fontSize,width,color) {
	this.show = true;
	obj2d_AbstractObj2d.call(this,x,y);
	if(fontSize == null) this.fontSize = GameMgr.HEIGHT / 20; else this.fontSize = fontSize;
	this.text = text;
	if(width == null) this.width = (text.length + 2) * this.fontSize; else this.width = width;
	this.height = this.fontSize * 1.6;
	this.color = color;
};
$hxClasses["obj2d.TargetRect"] = obj2d_TargetRect;
obj2d_TargetRect.__name__ = true;
obj2d_TargetRect.__super__ = obj2d_AbstractObj2d;
obj2d_TargetRect.prototype = $extend(obj2d_AbstractObj2d.prototype,{
	draw: function(context,time) {
		if(!this.show) return;
		obj2d_AbstractObj2d.prototype.draw.call(this,context,time);
		context.beginPath();
		if(this.color != null) {
			context.fillStyle = this.color;
			context.globalAlpha = 0.7;
			context.fillRect(this.x,this.y,this.width,this.height);
			context.globalAlpha = 1;
		}
		context.strokeStyle = "#000000";
		context.font = this.fontSize + "px bold \"Times New Roman\"";
		context.textAlign = "center";
		context.strokeRect(this.x,this.y,this.width,this.height);
		context.fillStyle = "#000000";
		context.fillText(this.text,this.x + this.width / 2,this.y + this.fontSize * 1.2);
		context.closePath();
	}
	,showText: function() {
		this.show = true;
	}
	,hideText: function() {
		this.show = false;
	}
	,isShow: function() {
		return this.show;
	}
	,checkHit: function(px,py) {
		if(!this.show) return false;
		if(px >= this.x && px <= this.x + this.width && py >= this.y && py <= this.y + this.height) return true; else return false;
	}
	,getHeight: function() {
		return this.height;
	}
	,getWidth: function() {
		return this.width;
	}
	,getText: function() {
		return this.text;
	}
	,__class__: obj2d_TargetRect
});
var obj2d_RadioButton = function(x,y,id,text,fontSize,width,check) {
	if(check == null) check = true;
	obj2d_TargetRect.call(this,x,y,text,fontSize,width);
	if(id == null) throw new js__$Boot_HaxeError("RadioButton not Id");
	this.id = id;
	if(obj2d_RadioButton.checks == null) obj2d_RadioButton.checks = new haxe_ds_StringMap();
	if(obj2d_RadioButton.checks.get(id) != null) {
		var ids = obj2d_RadioButton.checks.get(id);
		this.index = ids.length;
		ids.push(false);
	} else {
		obj2d_RadioButton.checks.set(id,[check]);
		this.index = 0;
	}
};
$hxClasses["obj2d.RadioButton"] = obj2d_RadioButton;
obj2d_RadioButton.__name__ = true;
obj2d_RadioButton.getCheckedIndex = function(key) {
	var radioGroup = obj2d_RadioButton.checks.get(key);
	var index = -1;
	var _g1 = 0;
	var _g = radioGroup.length;
	while(_g1 < _g) {
		var i = _g1++;
		if(radioGroup[i]) {
			index = i;
			break;
		}
	}
	return index;
};
obj2d_RadioButton.__super__ = obj2d_TargetRect;
obj2d_RadioButton.prototype = $extend(obj2d_TargetRect.prototype,{
	draw: function(context,time) {
		if(!this.show) return;
		var radioGroup = obj2d_RadioButton.checks.get(this.id);
		if(radioGroup[this.index] == true) {
			context.beginPath();
			context.fillStyle = "#ffffff";
			context.globalAlpha = 0.7;
			context.fillRect(this.x,this.y,this.width,this.height);
			context.globalAlpha = 1;
			context.closePath();
		}
		obj2d_TargetRect.prototype.draw.call(this,context,time);
	}
	,isChecked: function() {
		var radioGroup = obj2d_RadioButton.checks.get(this.id);
		return radioGroup[this.index];
	}
	,checked: function() {
		if(obj2d_RadioButton.change) return false;
		obj2d_RadioButton.change = true;
		var radioGroup = obj2d_RadioButton.checks.get(this.id);
		var _g1 = 0;
		var _g = radioGroup.length;
		while(_g1 < _g) {
			var i = _g1++;
			if(i == this.index) radioGroup[i] = true; else radioGroup[i] = false;
		}
		obj2d_RadioButton.change = false;
		return true;
	}
	,checkHit: function(px,py) {
		if(obj2d_RadioButton.change) return false;
		if(!obj2d_TargetRect.prototype.checkHit.call(this,px,py)) return false;
		obj2d_RadioButton.change = true;
		var radioGroup = obj2d_RadioButton.checks.get(this.id);
		var _g1 = 0;
		var _g = radioGroup.length;
		while(_g1 < _g) {
			var i = _g1++;
			if(i == this.index) radioGroup[i] = true; else radioGroup[i] = false;
		}
		obj2d_RadioButton.change = false;
		return true;
	}
	,__class__: obj2d_RadioButton
});
var obj2d_RankResult = function(x,y,before,text,width,move) {
	if(move == null) move = false;
	this.start = false;
	this.endX = 0;
	this.height = GameMgr.HEIGHT / 16 * 1.6;
	this.width = GameMgr.WIDTH / 2;
	this.beforeSize = GameMgr.HEIGHT / 16;
	this.textSize = GameMgr.HEIGHT / 17;
	obj2d_AbstractObj2d.call(this,x,y);
	this.text = text;
	this.beforeText = before;
	if(width != null) this.width = width;
	this.move = move;
};
$hxClasses["obj2d.RankResult"] = obj2d_RankResult;
obj2d_RankResult.__name__ = true;
obj2d_RankResult.__super__ = obj2d_AbstractObj2d;
obj2d_RankResult.prototype = $extend(obj2d_AbstractObj2d.prototype,{
	draw: function(context,time) {
		obj2d_AbstractObj2d.prototype.draw.call(this,context,time);
		if(!this.start && this.move) {
			this.endX = this.x;
			this.x = GameMgr.WIDTH;
			this.dx = -40;
		}
		this.start = true;
		if(this.x <= this.endX) this.dx = 0;
		context.beginPath();
		context.fillStyle = "#ffffff";
		context.globalAlpha = 0.7;
		context.fillRect(this.x - 30,this.y,this.width + 60,GameMgr.HEIGHT / 10 + 10);
		context.globalAlpha = 1;
		context.closePath();
		context.beginPath();
		context.textAlign = "left";
		context.fillStyle = "#000000";
		context.font = this.beforeSize + "px bold \"Times New Roman\"";
		context.fillText(this.beforeText,this.x,this.y + this.beforeSize * 1.4);
		context.font = this.textSize + "px bold \"Times New Roman\"";
		context.fillText(this.text,this.x + this.width / 2,this.y + this.beforeSize * 1.4);
		context.closePath();
		context.beginPath();
		context.strokeStyle = "#000000";
		context.lineWidth = 3;
		context.moveTo(this.x,this.y + this.beforeSize * 1.6);
		context.lineTo(this.x + this.width,this.y + this.beforeSize * 1.6);
		context.closePath();
		context.stroke();
		context.lineWidth = 1;
	}
	,__class__: obj2d_RankResult
});
var obj2d_Reticle = function(x,y,c) {
	this.max = 250;
	this.gauge = 250;
	obj2d_AbstractObj2d.call(this,x,y);
	this.r1 = GameMgr.WIDTH * 0.02;
	this.r2 = this.r1 * 0.4;
	this.height = this.r1;
	this.width = this.height * 0.1;
	this.color = c;
};
$hxClasses["obj2d.Reticle"] = obj2d_Reticle;
obj2d_Reticle.__name__ = true;
obj2d_Reticle.__super__ = obj2d_AbstractObj2d;
obj2d_Reticle.prototype = $extend(obj2d_AbstractObj2d.prototype,{
	draw: function(context,time) {
		obj2d_AbstractObj2d.prototype.draw.call(this,context,time);
		if(this.x < 0) this.x = 0;
		if(this.x > GameMgr.WIDTH) this.x = GameMgr.WIDTH;
		if(this.y < 0) this.y = 0;
		if(this.y > GameMgr.HEIGHT) this.y = GameMgr.HEIGHT;
		if(this.gauge < this.max) this.gauge += time;
		if(this.gauge > this.max) this.gauge = this.max;
		context.beginPath();
		context.lineWidth = 2;
		context.globalAlpha = 0.8;
		context.strokeStyle = "#888888";
		context.arc(this.x,this.y,this.r1 + 3,-Math.PI / 2,Math.PI * 2);
		context.stroke();
		context.closePath();
		context.beginPath();
		context.lineWidth = 4;
		context.globalAlpha = 0.5;
		context.strokeStyle = this.color;
		context.arc(this.x,this.y,this.r1,-Math.PI / 2,-Math.PI / 2 + Math.PI * 2 * (this.gauge / this.max));
		context.stroke();
		context.closePath();
		context.beginPath();
		context.fillStyle = this.color;
		context.globalAlpha = 0.5;
		context.arc(this.x,this.y,this.r2,0.0,Math.PI * 2);
		context.fill();
		context.closePath();
		context.beginPath();
		context.fillStyle = this.color;
		context.globalAlpha = 0.5;
		context.fillRect(this.x - this.width / 2,this.y - (this.height + this.height / 2),this.width,this.height);
		context.fillRect(this.x - this.width / 2,this.y + this.height / 2,this.width,this.height);
		context.fillRect(this.x - (this.height + this.height / 2),this.y - this.width / 2,this.height,this.width);
		context.fillRect(this.x + this.height / 2,this.y - this.width / 2,this.height,this.width);
		context.lineWidth = 1;
		context.closePath();
		context.globalAlpha = 1;
	}
	,action: function() {
		this.gauge = 0;
	}
	,__class__: obj2d_Reticle
});
var obj2d_TeamResult = function(result,x,y,color,score,names,type) {
	this.score = 0;
	this.endY = GameMgr.HEIGHT / 2 - (GameMgr.HEIGHT / 6 + 20) / 2;
	obj2d_AbstractObj2d.call(this,x,y);
	this.result = result;
	this.type = type;
	this.elapsedTime = 0;
	this.color = color;
	this.score = score;
	this.names = names;
	if(type == "0") this.dy = -40; else this.dy = 40;
};
$hxClasses["obj2d.TeamResult"] = obj2d_TeamResult;
obj2d_TeamResult.__name__ = true;
obj2d_TeamResult.__super__ = obj2d_AbstractObj2d;
obj2d_TeamResult.prototype = $extend(obj2d_AbstractObj2d.prototype,{
	draw: function(context,time) {
		obj2d_AbstractObj2d.prototype.draw.call(this,context,time);
		if(this.type == "0") {
			if(this.y < this.endY) {
				this.y = this.endY;
				this.dy = 0;
			}
		} else if(this.y > this.endY) {
			this.y = this.endY;
			this.dy = 0;
		}
		context.beginPath();
		context.textAlign = "left";
		context.fillStyle = this.color;
		context.globalAlpha = 0.7;
		context.fillRect(this.x,this.y,GameMgr.WIDTH / 3,GameMgr.HEIGHT / 6 + 20 + GameMgr.HEIGHT / 18 * 4);
		context.globalAlpha = 1;
		context.beginPath();
		context.strokeStyle = "#000000";
		context.fillStyle = "#000000";
		context.strokeRect(this.x,this.y,GameMgr.WIDTH / 3,GameMgr.HEIGHT / 6 + 20 + GameMgr.HEIGHT / 18 * 4);
		context.beginPath();
		context.textAlign = "center";
		context.font = GameMgr.HEIGHT / 18 + "px bold \"Times New Roman\"";
		context.fillText(this.result + this.score + " 点",this.x + GameMgr.WIDTH / 6,this.y + GameMgr.HEIGHT / 12);
		var _g1 = 0;
		var _g = this.names.length;
		while(_g1 < _g) {
			var i = _g1++;
			context.fillText(this.names[i],this.x + GameMgr.WIDTH / 6,this.y + GameMgr.HEIGHT / 6 + GameMgr.HEIGHT / 18 * i);
		}
		context.textAlign = "left";
		context.closePath();
	}
	,__class__: obj2d_TeamResult
});
var obj2d_TeamScoreBar = function(textSize,width,players,colors) {
	this.space = 40;
	this.textSize = 17;
	this.width = 0;
	obj2d_AbstractObj2d.call(this,0,0);
	this.textSize = textSize;
	this.width = width;
	this.players = players;
	this.colors = colors;
};
$hxClasses["obj2d.TeamScoreBar"] = obj2d_TeamScoreBar;
obj2d_TeamScoreBar.__name__ = true;
obj2d_TeamScoreBar.__super__ = obj2d_AbstractObj2d;
obj2d_TeamScoreBar.prototype = $extend(obj2d_AbstractObj2d.prototype,{
	draw: function(context,time) {
		obj2d_AbstractObj2d.prototype.draw.call(this,context,time);
		context.beginPath();
		context.globalAlpha = 0.7;
		context.fillStyle = this.colors.get(ETeamType.red);
		context.fillRect(0,0,this.width / 2,this.textSize * 2 + 8);
		context.fillStyle = this.colors.get(ETeamType.blue);
		context.fillRect(this.width / 2,0,this.width,this.textSize * 2 + 8);
		context.globalAlpha = 1;
		context.fillStyle = "#000000";
		context.font = this.textSize + "px bold \"Times New Roman\"";
		var redName = "";
		var blueName = "";
		var redScore = 0;
		var blueScore = 0;
		var _g = 0;
		var _g1 = this.players;
		while(_g < _g1.length) {
			var player = _g1[_g];
			++_g;
			var _g2 = player.getTeamColor();
			switch(_g2[1]) {
			case 0:
				redName += player.getName() + " ";
				redScore += player.getScore();
				break;
			case 1:
				blueName += player.getName() + " ";
				blueScore += player.getScore();
				break;
			}
		}
		context.fillText(redName,this.space,this.textSize);
		context.fillText(redScore + "点",this.space,this.textSize * 2);
		context.fillText(blueName,this.width / 2 + this.space,this.textSize);
		context.fillText(blueScore + "点",this.width / 2 + this.space,this.textSize * 2);
		context.closePath();
	}
	,__class__: obj2d_TeamScoreBar
});
var obj3d_MeshEx = function(shape,args) {
	this.life = true;
	this.shapeName = "meshEx";
	THREE.Mesh.call(this,shape.getGeometry(),shape.getMaterial());
	if(args != null) this.actions = args; else this.actions = [];
	shape.init(this.position,this.rotation);
	this.shapeName = shape.getName();
	this.shape = shape;
};
$hxClasses["obj3d.MeshEx"] = obj3d_MeshEx;
obj3d_MeshEx.__name__ = true;
obj3d_MeshEx.__super__ = THREE.Mesh;
obj3d_MeshEx.prototype = $extend(THREE.Mesh.prototype,{
	update: function(time) {
		var _g = 0;
		var _g1 = this.actions;
		while(_g < _g1.length) {
			var action = _g1[_g];
			++_g;
			action.update(time,this.position,this.rotation);
		}
	}
	,getShapeName: function() {
		return this.shapeName;
	}
	,hit: function() {
		return true;
	}
	,clone: function() {
		var actionArray = [];
		var _g = 0;
		var _g1 = this.actions;
		while(_g < _g1.length) {
			var i = _g1[_g];
			++_g;
			actionArray.push(i.clone());
		}
		return new obj3d_MeshEx(this.shape.clone(),actionArray);
	}
	,isLife: function() {
		return this.life;
	}
	,getScore: function() {
		return this.shape.getScore();
	}
	,getWidth: function() {
		return this.shape.getWidth();
	}
	,getHeight: function() {
		return this.shape.getHeight();
	}
	,__class__: obj3d_MeshEx
});
var obj3d_ArrowObj = function(visualRange,point) {
	obj3d_MeshEx.call(this,new shape_ArrowShape(null,{ direction : 0}));
	this.visualRange = visualRange;
	this.position.copy(point);
	this.position.z += 100;
	this.position.y -= 115 - visualRange / 50;
};
$hxClasses["obj3d.ArrowObj"] = obj3d_ArrowObj;
obj3d_ArrowObj.__name__ = true;
obj3d_ArrowObj.__super__ = obj3d_MeshEx;
obj3d_ArrowObj.prototype = $extend(obj3d_MeshEx.prototype,{
	hit: function() {
		return false;
	}
	,setPosi: function(point,deg) {
		this.position.copy(point);
		var radian = Math.PI / 180 * deg;
		this.position.x += Math.cos(radian) * 100;
		this.position.z += Math.sin(radian) * 100;
		this.position.y -= 115 - this.visualRange / 50;
	}
	,setDirection: function(nowPoint,nextPoint) {
		var radian = Math.atan2(nowPoint.z - nextPoint.z,nextPoint.x - nowPoint.x);
		this.rotation.y = radian;
	}
	,__class__: obj3d_ArrowObj
});
var obj3d_BreakObject = function(shape,args) {
	obj3d_MeshEx.call(this,shape,args);
};
$hxClasses["obj3d.BreakObject"] = obj3d_BreakObject;
obj3d_BreakObject.__name__ = true;
obj3d_BreakObject.__super__ = obj3d_MeshEx;
obj3d_BreakObject.prototype = $extend(obj3d_MeshEx.prototype,{
	update: function(time) {
		obj3d_MeshEx.prototype.update.call(this,time);
	}
	,hit: function() {
		this.life = false;
		return true;
	}
	,clone: function() {
		var actionArray = [];
		var _g = 0;
		var _g1 = this.actions;
		while(_g < _g1.length) {
			var i = _g1[_g];
			++_g;
			actionArray.push(i.clone());
		}
		return new obj3d_BreakObject(this.shape.clone(),actionArray);
	}
	,__class__: obj3d_BreakObject
});
var obj3d_DefaultGround = function(url,load) {
	if(obj3d_DefaultGround.texture == null) {
		if(url == null) throw new js__$Boot_HaxeError("not DefaultGround url");
		obj3d_DefaultGround.texture = THREE.ImageUtils.loadTexture(url,null,load);
		obj3d_DefaultGround.texture.wrapS = obj3d_DefaultGround.texture.wrapT = 1000;
		obj3d_DefaultGround.texture.repeat.set(128,128);
		obj3d_DefaultGround.texture.anisotropy = GameMgr.getAnisotropy();
	}
	var groundMaterial = new THREE.MeshPhongMaterial({ color : 16777215, map : obj3d_DefaultGround.texture, shininess : 10});
	var size = 20000;
	this.mesh = new THREE.Mesh(new THREE.PlaneBufferGeometry(size,size),groundMaterial);
	this.mesh.position.y = -0.5;
	this.mesh.rotation.x = -Math.PI / 2;
	this.mesh.receiveShadow = true;
};
$hxClasses["obj3d.DefaultGround"] = obj3d_DefaultGround;
obj3d_DefaultGround.__name__ = true;
obj3d_DefaultGround.prototype = {
	getMesh: function() {
		return this.mesh;
	}
	,__class__: obj3d_DefaultGround
};
var obj3d_InvincibleObject = function(shape,args) {
	obj3d_MeshEx.call(this,shape,args);
};
$hxClasses["obj3d.InvincibleObject"] = obj3d_InvincibleObject;
obj3d_InvincibleObject.__name__ = true;
obj3d_InvincibleObject.__super__ = obj3d_MeshEx;
obj3d_InvincibleObject.prototype = $extend(obj3d_MeshEx.prototype,{
	update: function(time) {
		obj3d_MeshEx.prototype.update.call(this,time);
	}
	,hit: function() {
		return false;
	}
	,clone: function() {
		var actionArray = [];
		var _g = 0;
		var _g1 = this.actions;
		while(_g < _g1.length) {
			var i = _g1[_g];
			++_g;
			actionArray.push(i.clone());
		}
		return new obj3d_InvincibleObject(this.shape.clone(),actionArray);
	}
	,__class__: obj3d_InvincibleObject
});
var obj3d_Lignt = function(hex,intensity) {
	THREE.DirectionalLight.call(this,hex,intensity);
	this.position.set(50,200,100);
	this.position.multiplyScalar(1.3);
	this.castShadow = true;
	this.shadowMapWidth = 1024;
	this.shadowMapHeight = 1024;
	var d = 500;
	this.shadowCameraLeft = -d;
	this.shadowCameraRight = d;
	this.shadowCameraTop = d;
	this.shadowCameraBottom = -d;
	this.shadowCameraFar = 1000;
	this.shadowDarkness = 0.5;
};
$hxClasses["obj3d.Lignt"] = obj3d_Lignt;
obj3d_Lignt.__name__ = true;
obj3d_Lignt.__super__ = THREE.DirectionalLight;
obj3d_Lignt.prototype = $extend(THREE.DirectionalLight.prototype,{
	__class__: obj3d_Lignt
});
var obj3d_TimerObject = function(shape,args,timer) {
	this.total = 0;
	this.limit = 1000;
	obj3d_MeshEx.call(this,shape,args);
	if(timer == null) timer = { };
	if(timer.limit != null) this.limit = timer.limit;
};
$hxClasses["obj3d.TimerObject"] = obj3d_TimerObject;
obj3d_TimerObject.__name__ = true;
obj3d_TimerObject.__super__ = obj3d_MeshEx;
obj3d_TimerObject.prototype = $extend(obj3d_MeshEx.prototype,{
	update: function(time) {
		obj3d_MeshEx.prototype.update.call(this,time);
		this.total += time;
		if(this.total >= this.limit) this.life = false;
	}
	,hit: function() {
		return false;
	}
	,clone: function() {
		var actionArray = [];
		var _g = 0;
		var _g1 = this.actions;
		while(_g < _g1.length) {
			var i = _g1[_g];
			++_g;
			actionArray.push(i.clone());
		}
		return new obj3d_TimerObject(this.shape.clone(),actionArray);
	}
	,__class__: obj3d_TimerObject
});
var obj3d_random_AbstractRandomObj = function(visualRange) {
	this.maxY = 600;
	this.maxTime = 3000;
	this.visualRange = visualRange;
};
$hxClasses["obj3d.random.AbstractRandomObj"] = obj3d_random_AbstractRandomObj;
obj3d_random_AbstractRandomObj.__name__ = true;
obj3d_random_AbstractRandomObj.prototype = {
	getObjs: function(num,appTime,radian) {
		var objs = [];
		objs = this.getActionObjs(num,radian);
		var _g = 0;
		while(_g < objs.length) {
			var o = objs[_g];
			++_g;
			o.appTime += appTime;
		}
		return objs;
	}
	,getActionObjs: function(num,radian) {
		return [];
	}
	,getRandomShape: function(num,score,random,color) {
		if(random == null) random = false;
		if(random) color = Std["int"](16777215 * Math.random());
		var shape1 = new shape_CrystalShape(score,{ color : color});
		num = Std["int"](_$UInt_UInt_$Impl_$.toFloat(num) % _$UInt_UInt_$Impl_$.toFloat(3));
		switch(num) {
		case 1:
			shape1 = new shape_CubeShape(score,{ color : color});
			break;
		case 2:
			shape1 = new shape_CylinderShape(score,{ color : color});
			break;
		}
		return shape1;
	}
	,getX: function(distance,radian) {
		return distance * Math.sin(radian);
	}
	,getZ: function(distance,radian) {
		return -distance * Math.cos(radian);
	}
	,__class__: obj3d_random_AbstractRandomObj
};
var obj3d_random_ChangeCircleObj = function(visualRange) {
	obj3d_random_AbstractRandomObj.call(this,visualRange);
};
$hxClasses["obj3d.random.ChangeCircleObj"] = obj3d_random_ChangeCircleObj;
obj3d_random_ChangeCircleObj.__name__ = true;
obj3d_random_ChangeCircleObj.__super__ = obj3d_random_AbstractRandomObj;
obj3d_random_ChangeCircleObj.prototype = $extend(obj3d_random_AbstractRandomObj.prototype,{
	getActionObjs: function(num,radian) {
		var objs = [];
		var shape = this.getRandomShape(num,800,true);
		var maxCnt = 15;
		var rad = Math.PI * 2 / maxCnt;
		var r;
		if((function($this) {
			var $r;
			var a = Std["int"](_$UInt_UInt_$Impl_$.toFloat(num) % _$UInt_UInt_$Impl_$.toFloat(2));
			$r = a == 0;
			return $r;
		}(this))) r = 1; else r = -1;
		var _g = 0;
		while(_g < maxCnt) {
			var i = _g++;
			var x = Math.cos(i * rad) * this.maxY / 2 + this.visualRange * r;
			var obj = new obj3d_BreakObject(shape,[new action_SetMoveAction([{ time : 200, point : new THREE.Vector3()},{ time : 1000, point : new THREE.Vector3(this.getX(x,radian),Math.sin(i * rad) * this.maxY / 2 + this.maxY / 2,this.getZ(x,radian))}]),new action_RotateAction()]);
			objs.push({ rpps : this.maxTime - 1000, obj : obj, appTime : 500, degree : 90, distance : this.visualRange * r, positionY : 0});
		}
		return objs;
	}
	,__class__: obj3d_random_ChangeCircleObj
});
var obj3d_random_ConvexObj = function(visualRange) {
	obj3d_random_AbstractRandomObj.call(this,visualRange);
};
$hxClasses["obj3d.random.ConvexObj"] = obj3d_random_ConvexObj;
obj3d_random_ConvexObj.__name__ = true;
obj3d_random_ConvexObj.__super__ = obj3d_random_AbstractRandomObj;
obj3d_random_ConvexObj.prototype = $extend(obj3d_random_AbstractRandomObj.prototype,{
	getActionObjs: function(num,radian) {
		var objs = [];
		var maxCnt = 20;
		var shape = this.getRandomShape(num,820,true);
		var r;
		if((function($this) {
			var $r;
			var a = Std["int"](_$UInt_UInt_$Impl_$.toFloat(num) % _$UInt_UInt_$Impl_$.toFloat(2));
			$r = a == 0;
			return $r;
		}(this))) r = true; else r = false;
		var addY;
		if(r) addY = -this.maxY / (maxCnt / 2); else addY = this.maxY / (maxCnt / 2);
		var objY;
		if(r) objY = -addY * maxCnt / 2; else objY = 0;
		var _g = 0;
		while(_g < maxCnt) {
			var i = _g++;
			if(i < maxCnt / 2) objY += addY; else objY += -addY;
			var obj = new obj3d_BreakObject(shape,[new action_SetMoveAction([{ time : 500, point : new THREE.Vector3()},{ time : 1000, point : new THREE.Vector3(0,(this.maxY / 2 - objY) * 2,0)}]),new action_RotateAction({ right : r})]);
			objs.push({ rpps : this.maxTime / 3 * 2, obj : obj, appTime : 0, degree : 270, distance : this.visualRange / 3 * 2 - i * (this.visualRange / 3 * 2 * 2 / maxCnt) - this.visualRange / 3 * 2 * 2 / maxCnt, positionY : objY});
		}
		return objs;
	}
	,__class__: obj3d_random_ConvexObj
});
var obj3d_random_RightLeftObj = function(visualRange) {
	obj3d_random_AbstractRandomObj.call(this,visualRange);
};
$hxClasses["obj3d.random.RightLeftObj"] = obj3d_random_RightLeftObj;
obj3d_random_RightLeftObj.__name__ = true;
obj3d_random_RightLeftObj.__super__ = obj3d_random_AbstractRandomObj;
obj3d_random_RightLeftObj.prototype = $extend(obj3d_random_AbstractRandomObj.prototype,{
	getActionObjs: function(num,radian) {
		var objs = [];
		var shape = this.getRandomShape(num,930,true);
		var h = shape.getHeight();
		var maxCnt = this.maxY / (h + 10) | 0;
		var rightMove;
		var moveRightArray = [];
		var leftMove;
		var moveLeftArray = [];
		var rang = this.visualRange / 2 * 1.5;
		if((function($this) {
			var $r;
			var a = Std["int"](_$UInt_UInt_$Impl_$.toFloat(num) % _$UInt_UInt_$Impl_$.toFloat(3));
			$r = a != 0;
			return $r;
		}(this))) {
			moveRightArray = [{ time : 500, point : new THREE.Vector3()},{ time : 1000, point : new THREE.Vector3(this.getX(rang,radian),0,this.getZ(rang,radian))}];
			rightMove = new action_SetMoveAction(moveRightArray);
			moveLeftArray = [{ time : 500, point : new THREE.Vector3()},{ time : 1000, point : new THREE.Vector3(this.getX(-rang,radian),0,this.getZ(-rang,radian))}];
			leftMove = new action_SetMoveAction(moveLeftArray);
		} else {
			moveRightArray = [{ time : 600, point : new THREE.Vector3()},{ time : 800, point : new THREE.Vector3(this.getX(rang,radian),0,this.getZ(rang,radian))},{ time : 600, point : new THREE.Vector3()},{ time : 800, point : new THREE.Vector3(this.getX(-rang,radian),0,this.getZ(-rang,radian))}];
			rightMove = new action_SetMoveAction(moveRightArray,true);
			moveLeftArray = [{ time : 600, point : new THREE.Vector3()},{ time : 800, point : new THREE.Vector3(this.getX(-rang,radian),0,this.getZ(-rang,radian))},{ time : 600, point : new THREE.Vector3()},{ time : 800, point : new THREE.Vector3(this.getX(rang,radian),0,this.getZ(rang,radian))}];
			leftMove = new action_SetMoveAction(moveLeftArray,true);
		}
		var rightObj = new obj3d_BreakObject(shape,[new action_RotateAction(),rightMove]);
		var leftObj = new obj3d_BreakObject(shape,[new action_RotateAction(),leftMove]);
		var _g = 0;
		while(_g < maxCnt) {
			var i = _g++;
			objs.push({ rpps : this.maxTime / 3, obj : rightObj, appTime : 0, degree : 90, distance : rang / 2, positionY : i * this.maxY / maxCnt});
			objs.push({ rpps : this.maxTime / 30 * 16, obj : leftObj, appTime : 300, degree : 270, distance : rang / 2, positionY : i * this.maxY / maxCnt});
			objs.push({ rpps : this.maxTime / 30 * 22, obj : rightObj, appTime : 600, degree : 90, distance : rang / 2, positionY : i * this.maxY / maxCnt});
			objs.push({ rpps : this.maxTime / 3, obj : rightObj, appTime : 0, degree : 90, distance : rang / 3, positionY : i * this.maxY / maxCnt});
			objs.push({ rpps : this.maxTime / 30 * 16, obj : leftObj, appTime : 300, degree : 270, distance : rang / 3, positionY : i * this.maxY / maxCnt});
			objs.push({ rpps : this.maxTime / 30 * 22, obj : rightObj, appTime : 600, degree : 90, distance : rang / 3, positionY : i * this.maxY / maxCnt});
		}
		return objs;
	}
	,__class__: obj3d_random_RightLeftObj
});
var obj3d_random_RotaObjs = function(visualRange) {
	obj3d_random_AbstractRandomObj.call(this,visualRange);
};
$hxClasses["obj3d.random.RotaObjs"] = obj3d_random_RotaObjs;
obj3d_random_RotaObjs.__name__ = true;
obj3d_random_RotaObjs.__super__ = obj3d_random_AbstractRandomObj;
obj3d_random_RotaObjs.prototype = $extend(obj3d_random_AbstractRandomObj.prototype,{
	getActionObjs: function(num,radian) {
		var objs = [];
		var shape = this.getRandomShape(num,900,true);
		var maxCnt = 15;
		var _g = 0;
		while(_g < maxCnt) {
			var i = _g++;
			var r;
			if(i % 2 == 0) r = true; else r = false;
			var xR = false;
			var zR = false;
			var _g1 = i % 4;
			switch(_g1) {
			case 0:
				xR = true;
				break;
			case 1:
				zR = true;
				break;
			case 2:
				xR = true;
				zR = true;
				break;
			}
			var obj = new obj3d_BreakObject(shape,[new action_MoveAction({ moveType : action_MoveType.x, distance : this.visualRange / 3, reverse : xR}),new action_MoveAction({ moveType : action_MoveType.z, distance : this.visualRange / 3, reverse : zR}),new action_RotateAction({ right : r})]);
			objs.push({ rpps : this.maxTime - i * 100 - 1000, obj : obj, appTime : i * 100, degree : 90, distance : i * 30 * (r?-1:1), positionY : 0});
		}
		return objs;
	}
	,__class__: obj3d_random_RotaObjs
});
var obj3d_random_RotaShieldObj = function(visualRange) {
	obj3d_random_AbstractRandomObj.call(this,visualRange);
};
$hxClasses["obj3d.random.RotaShieldObj"] = obj3d_random_RotaShieldObj;
obj3d_random_RotaShieldObj.__name__ = true;
obj3d_random_RotaShieldObj.__super__ = obj3d_random_AbstractRandomObj;
obj3d_random_RotaShieldObj.prototype = $extend(obj3d_random_AbstractRandomObj.prototype,{
	getActionObjs: function(num,radian) {
		var objs = [];
		var color = Std["int"](16777215 * Math.random());
		var shape1 = new shape_CubeShape(1000,{ size : 120, color : color});
		var _g = Std["int"](_$UInt_UInt_$Impl_$.toFloat(num) % _$UInt_UInt_$Impl_$.toFloat(3));
		switch(_g) {
		case 1:
			shape1 = new shape_CylinderShape(1000,{ radiusTop : 70, radiusBottom : 70, defaultRotaY : radian, color : color});
			break;
		case 2:
			shape1 = new shape_CrystalShape(1000,{ size : 60, color : color});
			break;
		}
		var r;
		if((function($this) {
			var $r;
			var a = Std["int"](_$UInt_UInt_$Impl_$.toFloat(num) % _$UInt_UInt_$Impl_$.toFloat(2));
			$r = a == 0;
			return $r;
		}(this))) r = true; else r = false;
		var obj = new obj3d_BreakObject(shape1,[new action_RotateAction({ right : r})]);
		var maxCnt = 20;
		var dis = this.visualRange / (maxCnt / 2) * 1.5;
		var smallRang = dis * 2;
		var bigRang = dis * 4;
		var smallMove;
		var rSmallMove;
		var bigMove;
		var rBigMove;
		if(r) {
			smallMove = new action_SetMoveAction([{ time : 500, point : new THREE.Vector3(this.getX(smallRang,radian),0,this.getZ(smallRang,radian))},{ time : 500, point : new THREE.Vector3(0,-this.maxY / 3,0)},{ time : 500, point : new THREE.Vector3(this.getX(-smallRang,radian),0,this.getZ(-smallRang,radian))},{ time : 500, point : new THREE.Vector3(0,this.maxY / 3,0)}],true);
			rSmallMove = new action_SetMoveAction([{ time : 500, point : new THREE.Vector3(this.getX(-smallRang,radian),0,this.getZ(-smallRang,radian))},{ time : 500, point : new THREE.Vector3(0,this.maxY / 3,0)},{ time : 500, point : new THREE.Vector3(this.getX(smallRang,radian),0,this.getZ(smallRang,radian))},{ time : 500, point : new THREE.Vector3(0,-this.maxY / 3,0)}],true);
			bigMove = new action_SetMoveAction([{ time : 500, point : new THREE.Vector3(this.getX(bigRang,radian),0,this.getZ(bigRang,radian))},{ time : 500, point : new THREE.Vector3(0,-this.maxY,0)},{ time : 500, point : new THREE.Vector3(this.getX(-bigRang,radian),0,this.getZ(-bigRang,radian))},{ time : 500, point : new THREE.Vector3(0,this.maxY,0)}],true);
			rBigMove = new action_SetMoveAction([{ time : 500, point : new THREE.Vector3(this.getX(-bigRang,radian),0,this.getZ(-bigRang,radian))},{ time : 500, point : new THREE.Vector3(0,this.maxY,0)},{ time : 500, point : new THREE.Vector3(this.getX(bigRang,radian),0,this.getZ(bigRang,radian))},{ time : 500, point : new THREE.Vector3(0,-this.maxY,0)}],true);
		} else {
			smallMove = new action_SetMoveAction([{ time : 500, point : new THREE.Vector3(0,-this.maxY / 3,0)},{ time : 500, point : new THREE.Vector3(this.getX(smallRang,radian),0,this.getZ(smallRang,radian))},{ time : 500, point : new THREE.Vector3(0,this.maxY / 3,0)},{ time : 500, point : new THREE.Vector3(this.getX(-smallRang,radian),0,this.getZ(-smallRang,radian))}],true);
			rSmallMove = new action_SetMoveAction([{ time : 500, point : new THREE.Vector3(0,this.maxY / 3,0)},{ time : 500, point : new THREE.Vector3(this.getX(-smallRang,radian),0,this.getZ(-smallRang,radian))},{ time : 500, point : new THREE.Vector3(0,-this.maxY / 3,0)},{ time : 500, point : new THREE.Vector3(this.getX(smallRang,radian),0,this.getZ(smallRang,radian))}],true);
			bigMove = new action_SetMoveAction([{ time : 500, point : new THREE.Vector3(0,-this.maxY,0)},{ time : 500, point : new THREE.Vector3(this.getX(bigRang,radian),0,this.getZ(bigRang,radian))},{ time : 500, point : new THREE.Vector3(0,this.maxY,0)},{ time : 500, point : new THREE.Vector3(this.getX(-bigRang,radian),0,this.getZ(-bigRang,radian))}],true);
			rBigMove = new action_SetMoveAction([{ time : 500, point : new THREE.Vector3(0,this.maxY,0)},{ time : 500, point : new THREE.Vector3(this.getX(-bigRang,radian),0,this.getZ(-bigRang,radian))},{ time : 500, point : new THREE.Vector3(0,-this.maxY,0)},{ time : 500, point : new THREE.Vector3(this.getX(bigRang,radian),0,this.getZ(bigRang,radian))}],true);
		}
		var hexa = new shape_HexagonalShape(0,{ defaultRotaY : radian});
		var smallHexa = new obj3d_InvincibleObject(hexa,[smallMove]);
		var bigHexa = new obj3d_InvincibleObject(hexa,[bigMove]);
		var rSmallHexa = new obj3d_InvincibleObject(hexa,[rSmallMove]);
		var rBigHexa = new obj3d_InvincibleObject(hexa,[rBigMove]);
		objs.push({ rpps : this.maxTime - 800, obj : smallHexa, appTime : 0, degree : 270, distance : -dis, positionY : this.maxY / 3 * 2});
		objs.push({ rpps : this.maxTime - 800, obj : bigHexa, appTime : 0, degree : 270, distance : -dis * 2, positionY : this.maxY});
		objs.push({ rpps : this.maxTime - 800, obj : rSmallHexa, appTime : 0, degree : 270, distance : dis, positionY : this.maxY / 3});
		objs.push({ rpps : this.maxTime - 800, obj : rBigHexa, appTime : 0, degree : 270, distance : dis * 2, positionY : 0});
		var _g1 = 0;
		while(_g1 < maxCnt) {
			var i = _g1++;
			var d = dis * 2 - dis * (i % 5);
			objs.push({ rpps : this.maxTime - 300, obj : obj, appTime : 0, degree : 270, distance : d, positionY : (i / 5 | 0) * this.maxY / 3});
		}
		return objs;
	}
	,__class__: obj3d_random_RotaShieldObj
});
var obj3d_random_ShieldObj = function(visualRange) {
	obj3d_random_AbstractRandomObj.call(this,visualRange);
};
$hxClasses["obj3d.random.ShieldObj"] = obj3d_random_ShieldObj;
obj3d_random_ShieldObj.__name__ = true;
obj3d_random_ShieldObj.__super__ = obj3d_random_AbstractRandomObj;
obj3d_random_ShieldObj.prototype = $extend(obj3d_random_AbstractRandomObj.prototype,{
	getActionObjs: function(num,radian) {
		var objs = [];
		var color = Std["int"](16777215 * Math.random());
		var shape1 = new shape_CubeShape(900,{ size : 120, color : color});
		var _g = Std["int"](_$UInt_UInt_$Impl_$.toFloat(num) % _$UInt_UInt_$Impl_$.toFloat(3));
		switch(_g) {
		case 1:
			shape1 = new shape_CylinderShape(900,{ radiusTop : 70, radiusBottom : 70, defaultRotaY : radian, color : color});
			break;
		case 2:
			shape1 = new shape_CrystalShape(900,{ size : 60, color : color});
			break;
		}
		var maxCnt = 16;
		var obj = new obj3d_BreakObject(shape1);
		var rightShield = new shape_HexagonalShape();
		var rightObj = new obj3d_InvincibleObject(rightShield,[new action_SetRotaAction([{ time : 1000, rotation : 90, direction : action_EDirection.y},{ time : 1000, rotation : 180, direction : action_EDirection.y},{ time : 1000, rotation : 270, direction : action_EDirection.y},{ time : 1000, rotation : 0, direction : action_EDirection.y}])]);
		var leftShield = new shape_HexagonalShape(0,{ defaultRotaY : Math.PI});
		var leftObj = new obj3d_InvincibleObject(leftShield,[new action_SetRotaAction([{ time : 1000, rotation : 0, direction : action_EDirection.y},{ time : 1000, rotation : 90, direction : action_EDirection.y},{ time : 1000, rotation : 180, direction : action_EDirection.y},{ time : 1000, rotation : 270, direction : action_EDirection.y}])]);
		var dis = this.visualRange / (maxCnt / 2) * 1.5;
		var _g1 = 0;
		while(_g1 < maxCnt) {
			var i = _g1++;
			var d = dis * 1.25 - dis * (i % 4) + dis / 2 * ((i / 4 | 0) % 2);
			objs.push({ rpps : this.maxTime - 800, obj : (i / 4 | 0) % 2 == 0?leftObj:rightObj, appTime : 0, degree : 270, distance : d, positionY : (i / 4 | 0) * this.maxY / 3});
			objs.push({ rpps : this.maxTime - 300, obj : obj, appTime : 0, degree : 270, distance : d, positionY : (i / 4 | 0) * this.maxY / 3});
		}
		return objs;
	}
	,__class__: obj3d_random_ShieldObj
});
var obj3d_random_UpDownObj = function(visualRange) {
	obj3d_random_AbstractRandomObj.call(this,visualRange);
};
$hxClasses["obj3d.random.UpDownObj"] = obj3d_random_UpDownObj;
obj3d_random_UpDownObj.__name__ = true;
obj3d_random_UpDownObj.__super__ = obj3d_random_AbstractRandomObj;
obj3d_random_UpDownObj.prototype = $extend(obj3d_random_AbstractRandomObj.prototype,{
	getActionObjs: function(num,radian) {
		var objs = [];
		var maxCnt = 11;
		var shape = this.getRandomShape(num,760,true);
		var upActionArray = [{ time : 500, point : new THREE.Vector3()},{ time : 1000, point : new THREE.Vector3(0,this.maxY,0)}];
		var downActionArray = [{ time : 500, point : new THREE.Vector3()},{ time : 1000, point : new THREE.Vector3(0,-this.maxY,0)}];
		if((function($this) {
			var $r;
			var a = Std["int"](_$UInt_UInt_$Impl_$.toFloat(num) % _$UInt_UInt_$Impl_$.toFloat(3));
			$r = a == 0;
			return $r;
		}(this))) {
			upActionArray = [{ time : 400, point : new THREE.Vector3()},{ time : 900, point : new THREE.Vector3(0,this.maxY,0)},{ time : 400, point : new THREE.Vector3()},{ time : 900, point : new THREE.Vector3(0,-this.maxY,0)}];
			downActionArray = [{ time : 400, point : new THREE.Vector3()},{ time : 900, point : new THREE.Vector3(0,-this.maxY,0)},{ time : 400, point : new THREE.Vector3()},{ time : 900, point : new THREE.Vector3(0,this.maxY,0)}];
		}
		var upMove = new action_SetMoveAction(upActionArray);
		var downMove = new action_SetMoveAction(downActionArray);
		var upObj = new obj3d_BreakObject(shape,[new action_RotateAction(),upMove]);
		var downObj = new obj3d_BreakObject(shape,[new action_RotateAction({ right : false}),downMove]);
		var dis = this.visualRange / 3 * 2 * 2 / maxCnt;
		var _g = 0;
		while(_g < maxCnt) {
			var i = _g++;
			var x = dis * i - dis * (maxCnt / 2) + dis / 2;
			objs.push({ rpps : this.maxTime / 3 * 2, obj : upObj, appTime : 0, degree : 90, distance : dis * i - dis * (maxCnt / 2) + dis / 2});
			if(i < maxCnt - 1) objs.push({ rpps : this.maxTime / 3 * 2, obj : downObj, appTime : 0, degree : 90, distance : dis * i - dis * (maxCnt / 2) + dis, positionY : this.maxY});
		}
		return objs;
	}
	,__class__: obj3d_random_UpDownObj
});
var obj3d_random_VortexObj = function(visualRange) {
	obj3d_random_AbstractRandomObj.call(this,visualRange);
};
$hxClasses["obj3d.random.VortexObj"] = obj3d_random_VortexObj;
obj3d_random_VortexObj.__name__ = true;
obj3d_random_VortexObj.__super__ = obj3d_random_AbstractRandomObj;
obj3d_random_VortexObj.prototype = $extend(obj3d_random_AbstractRandomObj.prototype,{
	getActionObjs: function(num,radian) {
		var objs = [];
		var shape = this.getRandomShape(num,420,true);
		var leftObj = new obj3d_BreakObject(shape,[new action_RotateAction()]);
		var rightObj = new obj3d_BreakObject(shape,[new action_RotateAction({ right : false})]);
		var maxCnt = 15;
		var rad = Math.PI / (maxCnt / 2);
		var reverse;
		if((function($this) {
			var $r;
			var a = Std["int"](_$UInt_UInt_$Impl_$.toFloat(num) % _$UInt_UInt_$Impl_$.toFloat(2));
			$r = a == 0;
			return $r;
		}(this))) reverse = true; else reverse = false;
		var quadruple;
		if((function($this) {
			var $r;
			var a1 = Std["int"](_$UInt_UInt_$Impl_$.toFloat(num) % _$UInt_UInt_$Impl_$.toFloat(4));
			$r = a1 == 0;
			return $r;
		}(this))) quadruple = true; else quadruple = false;
		var _g = 0;
		while(_g < maxCnt) {
			var i = _g++;
			var d;
			if(reverse) d = maxCnt - i; else d = i;
			objs.push({ rpps : this.maxTime - maxCnt * 100, obj : leftObj, appTime : i * 100, degree : 90, distance : Math.cos(d * rad) * this.maxY / 2, positionY : Math.sin(d * rad) * this.maxY / 2 + this.maxY / 2});
			objs.push({ rpps : this.maxTime - maxCnt * 100, obj : leftObj, appTime : i * 100, degree : 90, distance : Math.cos(d * rad + Math.PI) * this.maxY / 2, positionY : Math.sin(d * rad + Math.PI) * this.maxY / 2 + this.maxY / 2});
			if(quadruple) {
				objs.push({ rpps : this.maxTime - maxCnt * 100, obj : leftObj, appTime : i * 100, degree : 90, distance : Math.cos(d * rad + Math.PI / 2) * this.maxY / 2, positionY : Math.sin(d * rad + Math.PI / 2) * this.maxY / 2 + this.maxY / 2});
				objs.push({ rpps : this.maxTime - maxCnt * 100, obj : leftObj, appTime : i * 100, degree : 90, distance : Math.cos(d * rad - Math.PI / 2) * this.maxY / 2, positionY : Math.sin(d * rad - Math.PI / 2) * this.maxY / 2 + this.maxY / 2});
			}
		}
		return objs;
	}
	,__class__: obj3d_random_VortexObj
});
var objmgr_BattleModeMgr = function(y) {
	this.radioId = "modeType";
	this.selectMode = EBattleMode.none;
	this.modes = new List();
	this.y = y;
	this.modeWidth = GameMgr.WIDTH / 20 * 3;
	var hardX;
	if(GameMgr.isOpenRandom()) hardX = GameMgr.WIDTH / 3 - this.modeWidth / 2; else hardX = GameMgr.WIDTH / 2 - this.modeWidth / 2;
	this.hardMode = new obj2d_RadioButton(hardX,y,this.radioId,"Hard",null,this.modeWidth,false);
	var normalMode = new obj2d_RadioButton(GameMgr.WIDTH / 4 * 3 - this.modeWidth / 2,this.hardMode.y + this.hardMode.getHeight() * 2,this.radioId,"Normal",null,this.modeWidth,false);
	var easyMode = new obj2d_RadioButton(GameMgr.WIDTH / 4 - this.modeWidth / 2,this.hardMode.y + this.hardMode.getHeight() * 2,this.radioId,"Easy",null,this.modeWidth,false);
	this.modes.add(this.hardMode);
	this.modes.add(normalMode);
	this.modes.add(easyMode);
	this.randomMode = new obj2d_RadioButton(GameMgr.WIDTH / 3 * 2 - this.modeWidth / 2,y,this.radioId,"Random",null,this.modeWidth,false);
	this.startButton = new obj2d_TargetRect(GameMgr.WIDTH / 2 - this.modeWidth * 1.5 / 2,GameMgr.HEIGHT / 5 * 4,"GameStart",null,this.modeWidth * 1.5,"#ffff00");
};
$hxClasses["objmgr.BattleModeMgr"] = objmgr_BattleModeMgr;
objmgr_BattleModeMgr.__name__ = true;
objmgr_BattleModeMgr.prototype = {
	draw: function(context,time) {
		if(GameMgr.isReadyAll() && GameMgr.entryNum > 0) {
			if(GameMgr.isOpenRandom()) {
				this.hardMode.setX(GameMgr.WIDTH / 3 - this.modeWidth / 2);
				this.randomMode.draw(context,time);
			} else this.hardMode.setX(GameMgr.WIDTH / 2 - this.modeWidth / 2);
			var _g_head = this.modes.h;
			var _g_val = null;
			while(_g_head != null) {
				var m;
				m = (function($this) {
					var $r;
					_g_val = _g_head[0];
					_g_head = _g_head[1];
					$r = _g_val;
					return $r;
				}(this));
				m.draw(context,time);
			}
			if(!Type.enumEq(this.selectMode,EBattleMode.none)) this.startButton.draw(context,time);
		}
	}
	,checkHit: function(px,py) {
		if(!GameMgr.isReadyAll()) return false;
		var lists = this.modes;
		if(GameMgr.isOpenRandom()) lists.add(this.randomMode);
		var _g_head = lists.h;
		var _g_val = null;
		while(_g_head != null) {
			var m;
			m = (function($this) {
				var $r;
				_g_val = _g_head[0];
				_g_head = _g_head[1];
				$r = _g_val;
				return $r;
			}(this));
			if(m.checkHit(px,py)) {
				var _g = m.getText();
				switch(_g) {
				case "Hard":
					this.selectMode = EBattleMode.Hard;
					break;
				case "Normal":
					this.selectMode = EBattleMode.Normal;
					break;
				case "Easy":
					this.selectMode = EBattleMode.Easy;
					break;
				case "Random":
					this.selectMode = EBattleMode.Ran;
					break;
				}
				var audio = Audio.getInstance();
				audio.playSE("start");
				return true;
			}
		}
		return false;
	}
	,getSelectMode: function(px,py) {
		if(GameMgr.isReadyAll() && !Type.enumEq(this.selectMode,EBattleMode.none)) {
			if(this.startButton.checkHit(px,py)) return this.selectMode;
		}
		return EBattleMode.none;
	}
	,__class__: objmgr_BattleModeMgr
};
var objmgr_ETextType = { __ename__ : true, __constructs__ : ["noPlayer","inPlayer","teamBattle"] };
objmgr_ETextType.noPlayer = ["noPlayer",0];
objmgr_ETextType.noPlayer.toString = $estr;
objmgr_ETextType.noPlayer.__enum__ = objmgr_ETextType;
objmgr_ETextType.inPlayer = ["inPlayer",1];
objmgr_ETextType.inPlayer.toString = $estr;
objmgr_ETextType.inPlayer.__enum__ = objmgr_ETextType;
objmgr_ETextType.teamBattle = ["teamBattle",2];
objmgr_ETextType.teamBattle.toString = $estr;
objmgr_ETextType.teamBattle.__enum__ = objmgr_ETextType;
objmgr_ETextType.__empty_constructs__ = [objmgr_ETextType.noPlayer,objmgr_ETextType.inPlayer,objmgr_ETextType.teamBattle];
var objmgr_MoveTextMgr = function(y,textSize,speed,waitTime) {
	if(waitTime == null) waitTime = 2000;
	if(speed == null) speed = -3;
	if(textSize == null) textSize = 17;
	this.nowType = objmgr_ETextType.noPlayer;
	this.texts = new haxe_ds_EnumValueMap();
	this.y = y;
	this.textSize = textSize;
	var noPlayerText = new obj2d_MoveText(GameMgr.WIDTH,y,["右上のアドレスにスマホでアクセス！"],textSize,speed,waitTime);
	this.texts.set(objmgr_ETextType.noPlayer,noPlayerText);
	var inPlayArray = ["設定で移動速度が変更できます","全員が準備完了すると難易度選択が出来ます"];
	if(GameMgr.isHardClear()) inPlayArray.unshift("QRコードを10回打つと新しいステージが・・・？");
	var inPlayerText = new obj2d_MoveText(GameMgr.WIDTH,y,inPlayArray,textSize,speed,waitTime);
	inPlayerText.hideText();
	this.texts.set(objmgr_ETextType.inPlayer,inPlayerText);
	var teamBattleArray = ["青ブロックを撃つと青チームになります","赤ブロックを撃つと赤チームになります","全員が準備完了すると難易度選択が出来ます"];
	if(GameMgr.isHardClear()) teamBattleArray.unshift("QRコードを10回打つと新しいステージが・・・？");
	var teamBattleText = new obj2d_MoveText(GameMgr.WIDTH,y,teamBattleArray,textSize,speed,waitTime);
	teamBattleText.hideText();
	this.texts.set(objmgr_ETextType.teamBattle,teamBattleText);
};
$hxClasses["objmgr.MoveTextMgr"] = objmgr_MoveTextMgr;
objmgr_MoveTextMgr.__name__ = true;
objmgr_MoveTextMgr.prototype = {
	update: function(context,time) {
		var textType = objmgr_ETextType.noPlayer;
		if(GameMgr.isTeamBattle()) textType = objmgr_ETextType.teamBattle; else if(GameMgr.entryNum > 0) textType = objmgr_ETextType.inPlayer;
		if(!Type.enumEq(textType,this.nowType)) {
			var $it0 = this.texts.iterator();
			while( $it0.hasNext() ) {
				var t = $it0.next();
				t.hideText();
			}
			var showText = this.texts.get(textType);
			showText.initMove();
			showText.showText();
			this.nowType = textType;
		}
		var $it1 = this.texts.iterator();
		while( $it1.hasNext() ) {
			var t1 = $it1.next();
			t1.draw(context,time);
		}
	}
	,getTextSize: function() {
		return this.textSize;
	}
	,__class__: objmgr_MoveTextMgr
};
var objmgr_RandomObjMgr = function(visualRange) {
	this.seed = 0;
	this.randomObjs = [];
	this.randomObjs.push(new obj3d_random_UpDownObj(visualRange));
	this.randomObjs.push(new obj3d_random_RightLeftObj(visualRange));
	this.randomObjs.push(new obj3d_random_VortexObj(visualRange));
	this.randomObjs.push(new obj3d_random_ShieldObj(visualRange));
	this.randomObjs.push(new obj3d_random_RotaShieldObj(visualRange));
	this.randomObjs.push(new obj3d_random_ConvexObj(visualRange));
	this.randomObjs.push(new obj3d_random_ChangeCircleObj(visualRange));
	this.randomObjs.push(new obj3d_random_RotaObjs(visualRange));
};
$hxClasses["objmgr.RandomObjMgr"] = objmgr_RandomObjMgr;
objmgr_RandomObjMgr.__name__ = true;
objmgr_RandomObjMgr.prototype = {
	init: function() {
		this.seed = Std["int"](new Date().getTime());
		this.random = new Random(this.seed);
	}
	,again: function() {
		this.random = new Random(this.seed);
	}
	,getRandomObjs: function(appTime,radian,displace) {
		if(displace == null) displace = 0;
		var r = this.random.nextUInt();
		var objNum = Std["int"](_$UInt_UInt_$Impl_$.toFloat(r) % _$UInt_UInt_$Impl_$.toFloat(this.randomObjs.length));
		var objs = this.randomObjs[objNum].getObjs(this.random.nextUInt(),appTime,radian);
		var _g = 0;
		while(_g < objs.length) {
			var o = objs[_g];
			++_g;
			o.rpps += displace;
			o.appTime -= displace;
		}
		return objs;
	}
	,__class__: objmgr_RandomObjMgr
};
var objmgr_TeamTypeMgr = function(y) {
	this.blueName = "blue";
	this.redName = "red";
	this.show = false;
	this.teamBox = new List();
	this.teamButtons = new List();
	this.y = y;
	var redbox = new obj3d_InvincibleObject(new shape_CubeShape(0,{ color : 16711680, size : 100, name : this.redName}));
	redbox.position.x = -120;
	redbox.position.z = -170;
	var bluebox = new obj3d_InvincibleObject(new shape_CubeShape(0,{ color : 255, size : 100, name : this.blueName}));
	bluebox.position.x = 120;
	bluebox.position.z = -170;
	this.teamBox.add(redbox);
	this.teamBox.add(bluebox);
	this.singleButton = new obj2d_RadioButton(GameMgr.WIDTH / 2 - GameMgr.WIDTH / 20 * 3 - 20,y,"battleType","個人戦",null,GameMgr.WIDTH / 20 * 3);
	this.teamButtons.add(this.singleButton);
	var teamButton = new obj2d_RadioButton(GameMgr.WIDTH / 2 + 20,y,"battleType","チーム戦",null,GameMgr.WIDTH / 20 * 3);
	this.teamButtons.add(teamButton);
};
$hxClasses["objmgr.TeamTypeMgr"] = objmgr_TeamTypeMgr;
objmgr_TeamTypeMgr.__name__ = true;
objmgr_TeamTypeMgr.prototype = {
	getY: function() {
		return this.y;
	}
	,checkType: function(px,py) {
		if(!GameMgr.isReadyAll() && GameMgr.entryNum > 2) {
			var _g_head = this.teamButtons.h;
			var _g_val = null;
			while(_g_head != null) {
				var c;
				c = (function($this) {
					var $r;
					_g_val = _g_head[0];
					_g_head = _g_head[1];
					$r = _g_val;
					return $r;
				}(this));
				if(c.checkHit(px,py)) {
					var _g = c.getText();
					switch(_g) {
					case "個人戦":
						GameMgr.setTeamBattle(false);
						break;
					case "チーム戦":
						var redTeam = GameMgr.entryNum / 2 | 0;
						var seted = "";
						var inNumber = "";
						var $it0 = GameMgr.players.iterator();
						while( $it0.hasNext() ) {
							var p = $it0.next();
							inNumber += p.getNumber();
						}
						while(redTeam > 0) {
							var target = Std["int"](Math.random() * GameMgr.entryNum);
							while(seted.indexOf("" + target) != -1 || inNumber.indexOf("" + target) == -1) target = Std["int"](Math.random() * GameMgr.entryNum);
							seted += target;
							redTeam--;
						}
						var $it1 = GameMgr.players.iterator();
						while( $it1.hasNext() ) {
							var p1 = $it1.next();
							if(seted.indexOf("" + p1.getNumber()) != -1) p1.setTeamColor(ETeamType.red); else p1.setTeamColor(ETeamType.blue);
						}
						GameMgr.setTeamBattle(true);
						break;
					}
					var audio = Audio.getInstance();
					audio.playSE("change");
				}
			}
		}
	}
	,update: function(obj3d,scene,context,time) {
		if(!GameMgr.isReadyAll() && GameMgr.entryNum > 2) {
			var _g_head = this.teamButtons.h;
			var _g_val = null;
			while(_g_head != null) {
				var c;
				c = (function($this) {
					var $r;
					_g_val = _g_head[0];
					_g_head = _g_head[1];
					$r = _g_val;
					return $r;
				}(this));
				c.draw(context,time);
			}
		}
		if(GameMgr.isTeamBattle() && !GameMgr.isReadyAll() && GameMgr.entryNum > 2) {
			if(!this.show) {
				var _g_head1 = this.teamBox.h;
				var _g_val1 = null;
				while(_g_head1 != null) {
					var b;
					b = (function($this) {
						var $r;
						_g_val1 = _g_head1[0];
						_g_head1 = _g_head1[1];
						$r = _g_val1;
						return $r;
					}(this));
					obj3d.add(b);
					scene.add(b);
				}
				this.show = true;
			}
		} else if(this.show) {
			var _g_head2 = this.teamBox.h;
			var _g_val2 = null;
			while(_g_head2 != null) {
				var b1;
				b1 = (function($this) {
					var $r;
					_g_val2 = _g_head2[0];
					_g_head2 = _g_head2[1];
					$r = _g_val2;
					return $r;
				}(this));
				obj3d.remove(b1);
				scene.remove(b1);
			}
			this.show = false;
		}
		if(GameMgr.isTeamBattle() && GameMgr.entryNum <= 2) {
			GameMgr.setTeamBattle(false);
			this.singleButton.checked();
		}
	}
	,checkTeam: function(player,obj) {
		if(obj.getShapeName() == this.redName) player.setTeamColor(ETeamType.red); else if(obj.getShapeName() == this.blueName) player.setTeamColor(ETeamType.blue);
	}
	,__class__: objmgr_TeamTypeMgr
};
var position_AbstractPosition = function(startWait) {
	if(startWait == null) startWait = 3000;
	this.startWait = 3000;
	this.progress = 0;
	this.deg = 90;
	this.position = new THREE.Vector3();
	this.startWait = startWait;
};
$hxClasses["position.AbstractPosition"] = position_AbstractPosition;
position_AbstractPosition.__name__ = true;
position_AbstractPosition.prototype = {
	update: function(time) {
	}
	,getPosition: function() {
		return { point : this.position, deg : this.deg};
	}
	,getAfterPosition: function(time) {
		return { point : this.position, deg : this.deg};
	}
	,init: function() {
		this.position = new THREE.Vector3();
		this.deg = 90;
		this.progress = 0 - this.startWait;
	}
	,setRouteData: function(data) {
	}
	,getMaxTime: function() {
		return 60000;
	}
	,getNowTime: function() {
		return this.progress;
	}
	,changeRoute: function() {
	}
	,__class__: position_AbstractPosition
};
var position_Route = function(t,p,l,d) {
	if(l == null) l = true;
	this.time = t;
	this.point = p;
	this.look = l;
	if(d == null) this.deg = Math.atan2(this.point.z,this.point.x) * 180 / Math.PI; else this.deg = d;
	this.deg = (360 + this.deg) % 360;
};
$hxClasses["position.Route"] = position_Route;
position_Route.__name__ = true;
position_Route.prototype = {
	getSpeed: function() {
		var speed = { x : this.point.x / this.time, y : this.point.y / this.time, z : this.point.z / this.time};
		return speed;
	}
	,getPosi: function(t) {
		var p;
		if(this.time > t) {
			var ratio = t / this.time;
			p = new THREE.Vector3(this.point.x * ratio,this.point.y * ratio,this.point.z * ratio);
		} else p = this.point.clone();
		return p;
	}
	,subTime: function(t) {
		if(t > this.time) return false;
		var ratio = (this.time - t) / this.time;
		var p = new THREE.Vector3(this.point.x * ratio,this.point.y * ratio,this.point.z * ratio);
		this.point.copy(p);
		this.time -= t;
		return true;
	}
	,isLook: function() {
		return this.look;
	}
	,getTime: function() {
		return this.time;
	}
	,getPoint: function() {
		return this.point.clone();
	}
	,getDeg: function() {
		return this.deg;
	}
	,clone: function() {
		return new position_Route(this.time,this.point.clone(),this.look,this.deg);
	}
	,__class__: position_Route
};
var position_RoutePosition = function(wait,rt) {
	this.maxTime = 0;
	this.np = 0;
	this.roteTime = 2000;
	this.d = 90;
	this.lastTime = 0;
	this.nextTime = 0;
	position_AbstractPosition.call(this,wait);
	if(rt != null && rt >= 0) this.roteTime = rt;
	this.routeData = this.getDefaultRoute();
};
$hxClasses["position.RoutePosition"] = position_RoutePosition;
position_RoutePosition.__name__ = true;
position_RoutePosition.__super__ = position_AbstractPosition;
position_RoutePosition.prototype = $extend(position_AbstractPosition.prototype,{
	init: function() {
		position_AbstractPosition.prototype.init.call(this);
		if(!this.setRoute()) {
			this.setRouteData(this.getDefaultRoute());
			this.setRoute();
		}
	}
	,getAfterPosition: function(time) {
		var mp = this.nowPoint.clone();
		var ad = this.deg;
		var nt = this.nextTime;
		var lt = this.lastTime;
		var ap = this.np;
		while(this.progress + time > nt && this.routes.length > ap + 1) {
			mp.add(this.routes[ap].getPoint());
			if(this.routes[ap].isLook()) ad = (360 + this.routes[ap].getDeg()) % 360;
			ap++;
			lt = nt;
			nt += this.routes[ap].getTime();
		}
		var dif;
		if(this.progress + time > nt) dif = nt - lt; else dif = this.progress + time - lt;
		mp.add(this.routes[ap].getPosi(dif));
		var nd = this.routes[ap].getDeg();
		if(this.routes[ap].isLook()) {
			var addD = (nd - ad + 540) % 360 - 180;
			ad = (ad + addD * (dif / this.routes[ap].getTime()) + 360) % 360;
		}
		return { point : mp, deg : ad};
	}
	,update: function(t) {
		this.progress += t;
		while(this.progress > this.nextTime && this.routes.length > this.np + 1) this.setNext();
		this.position = this.nowPoint.clone();
		this.deg = this.d;
		if(this.progress >= 0) {
			var time = this.progress - this.lastTime;
			this.position.add(this.nextRoute.getPosi(time));
			var nd = (360 + this.nextRoute.getDeg()) % 360;
			if(this.look) {
				var addD = (nd - this.deg + 540) % 360 - 180;
				this.deg = (this.deg + addD * (time / this.nextRoute.getTime()) + 360) % 360;
			}
		}
	}
	,setRouteData: function(data) {
		this.routeData = data;
	}
	,getMaxTime: function() {
		return this.maxTime | 0;
	}
	,setRoute: function() {
		this.routes = [];
		this.np = 0;
		this.progress = 0;
		this.progress = -this.startWait;
		this.nowPoint = new THREE.Vector3(0,0,0);
		this.d = 90;
		if(this.routeData.length == 0) this.routeData = this.getDefaultRoute();
		var routeArray = [];
		var now = this.nowPoint.clone();
		this.maxTime = 0;
		var _g = 0;
		var _g1 = this.routeData;
		while(_g < _g1.length) {
			var i = _g1[_g];
			++_g;
			var p = new THREE.Vector3(i.point.x - now.x,i.point.y - now.y,i.point.z - now.z);
			now.copy(i.point);
			routeArray.push(new position_Route(i.time,p,i.degree == null,i.degree));
			this.maxTime += i.time;
		}
		var t = 0;
		var next = routeArray[t];
		var nextDeg = next.getDeg();
		if(nextDeg != this.deg) {
			var d = Math.abs((nextDeg - this.deg + 540) % 360 - 180);
			var time = d / 180 * this.roteTime;
			if(next.subTime(time) == false) {
				console.log("曲がるための時間が足りません　必要：" + (time | 0) + "ミリ秒");
				return false;
			}
			var speed = next.getSpeed();
			var curve = new THREE.Vector3(speed.x * time,speed.y * time,speed.z * time);
			this.routes.push(new position_Route(time,curve,next.isLook(),nextDeg));
		}
		this.routes.push(next);
		var old;
		while(routeArray.length > t + 1) {
			old = this.routes[this.routes.length - 1];
			t++;
			next = routeArray[t];
			var nextDeg1 = (360 + next.getDeg()) % 360;
			var oldDeg = (360 + old.getDeg()) % 360;
			if(nextDeg1 != oldDeg) {
				var d1 = Math.abs((nextDeg1 - oldDeg + 540) % 360 - 180);
				var time1 = d1 / 180 * this.roteTime;
				if(next.subTime(time1) == false) return false;
				if(old.subTime(time1) == false) return false;
				var ns = next.getSpeed();
				var os = old.getSpeed();
				var x = ns.x * time1 + os.x * time1;
				var y = ns.y * time1 + os.y * time1;
				var z = ns.z * time1 + os.z * time1;
				var curve1 = new THREE.Vector3(x,y,z);
				this.routes.push(new position_Route(time1 * 2,curve1,next.isLook(),nextDeg1));
			}
			this.routes.push(next);
		}
		this.nextRoute = this.routes[this.np];
		this.nextTime = this.nextRoute.getTime();
		this.lastTime = 0;
		this.look = this.nextRoute.isLook();
		return true;
	}
	,setNext: function() {
		this.nowPoint.add(this.nextRoute.getPoint());
		this.lastTime = this.nextTime;
		if(this.look) this.d = (360 + this.nextRoute.getDeg()) % 360;
		this.np++;
		this.nextRoute = this.routes[this.np];
		this.nextTime += this.nextRoute.getTime();
		this.look = this.nextRoute.isLook();
	}
	,getDefaultRoute: function() {
		var array2_0 = { time : 4000, point : new THREE.Vector3(0,0,1000)};
		var array2_1 = { time : 6000, point : new THREE.Vector3(1732,0,1000)};
		var array2_2 = { time : 6000, point : new THREE.Vector3(1732,0,-1000)};
		var array2_3 = { time : 6000, point : new THREE.Vector3(0,0,-2000)};
		var array2_4 = { time : 6000, point : new THREE.Vector3(-1732,0,-1000)};
		var array2_5 = { time : 6000, point : new THREE.Vector3(-1732,0,1000)};
		var array2_6 = { time : 6000, point : new THREE.Vector3(0,0,2000)};
		var array2_7 = { time : 6000, point : new THREE.Vector3(1732,0,1000)};
		var array2_8 = { time : 6000, point : new THREE.Vector3(1732,0,-1000)};
		var array2_9 = { time : 4000, point : new THREE.Vector3(0,0,-1000)};
		var array = [{ time : 8000, point : new THREE.Vector3(0,0,2000)},{ time : 8000, point : new THREE.Vector3(2000,0,2000)},{ time : 16000, point : new THREE.Vector3(2000,0,-2000)},{ time : 16000, point : new THREE.Vector3(-2000,0,-2000)},{ time : 8000, point : new THREE.Vector3(-2000,0,0)}];
		return array;
	}
	,__class__: position_RoutePosition
});
var scene_abs_AbstractScene = function(context) {
	this.doOnce = true;
	this.firstInit = true;
	this.obj3d = new List();
	this.obj2d = new List();
	this.context = context;
	this.scene = new THREE.Scene();
	this.audio = Audio.getInstance();
};
$hxClasses["scene.abs.AbstractScene"] = scene_abs_AbstractScene;
scene_abs_AbstractScene.__name__ = true;
scene_abs_AbstractScene.prototype = {
	init: function(args) {
		this.obj3d.clear();
		GameMgr.setBackColor(13434879);
		if(this.audioName != null) this.audio.playBGM(this.audioName);
	}
	,isFirst: function() {
		if(!this.firstInit) return false;
		this.firstInit = false;
		return true;
	}
	,update: function(time) {
		var _g_head = this.obj2d.h;
		var _g_val = null;
		while(_g_head != null) {
			var o2;
			o2 = (function($this) {
				var $r;
				_g_val = _g_head[0];
				_g_head = _g_head[1];
				$r = _g_val;
				return $r;
			}(this));
			o2.draw(this.context,time);
		}
		var removes = [];
		var _g_head1 = this.obj3d.h;
		var _g_val1 = null;
		while(_g_head1 != null) {
			var o3;
			o3 = (function($this) {
				var $r;
				_g_val1 = _g_head1[0];
				_g_head1 = _g_head1[1];
				$r = _g_val1;
				return $r;
			}(this));
			o3.update(time);
			if(!o3.isLife()) {
				this.scene.remove(o3);
				removes.push(o3);
			}
		}
		var _g = 0;
		while(_g < removes.length) {
			var r = removes[_g];
			++_g;
			this.obj3d.remove(r);
		}
	}
	,clear: function() {
		var _g_head = this.obj3d.h;
		var _g_val = null;
		while(_g_head != null) {
			var o3;
			o3 = (function($this) {
				var $r;
				_g_val = _g_head[0];
				_g_head = _g_head[1];
				$r = _g_val;
				return $r;
			}(this));
			this.scene.remove(o3);
		}
		this.obj3d.clear();
		this.obj2d.clear();
	}
	,addObj2D: function(obj) {
		this.obj2d.add(obj);
	}
	,removeObj2D: function(obj) {
		this.obj2d.remove(obj);
	}
	,playerShot: function(player) {
		player.shot();
	}
	,once: function() {
		if(!this.doOnce) return false;
		this.doOnce = false;
		return true;
	}
	,__class__: scene_abs_AbstractScene
};
var scene_abs_AbstractMoveScene = function(context,param) {
	this.errorTimeCnt = 0;
	this.stageId = 1;
	this.players = [];
	this.end = false;
	this.endTime = 2000;
	this.maxTime = 0;
	this.first = true;
	this.debugTime = 0;
	this.nowTime = 0;
	this.progress = 0;
	scene_abs_AbstractScene.call(this,context);
	if(param == null) param = { };
	if(param.startWait != null) this.waitTime = param.startWait; else this.waitTime = 3000;
	if(param.visualRange != null) this.visualRange = param.visualRange; else this.visualRange = 1500;
	var py;
	if(param.potisionY != null) py = param.potisionY; else py = 350;
	this.camera = new camera_MoveCamera(this.visualRange,py);
	this.opt = new field_ObjectPerTime(this.waitTime);
	this.arrow = new obj3d_ArrowObj(this.visualRange,this.camera.position);
	this.scene.add(this.arrow);
};
$hxClasses["scene.abs.AbstractMoveScene"] = scene_abs_AbstractMoveScene;
scene_abs_AbstractMoveScene.__name__ = true;
scene_abs_AbstractMoveScene.__super__ = scene_abs_AbstractScene;
scene_abs_AbstractMoveScene.prototype = $extend(scene_abs_AbstractScene.prototype,{
	setDebugTime: function(startTime,first) {
		this.debugTime = startTime;
		if(first != null) this.first = first;
	}
	,init: function(args) {
		scene_abs_AbstractScene.prototype.init.call(this,args);
		this.positionMgr.init();
		this.opt.init();
		this.nowTime = (this.waitTime / 1000 | 0) + 1;
		this.progress = 0;
		this.first = true;
		this.maxTime = this.positionMgr.getMaxTime();
		this.end = false;
		this.players = [];
		var $it0 = GameMgr.players.iterator();
		while( $it0.hasNext() ) {
			var p = $it0.next();
			this.players.push(p);
		}
		var scoreBar;
		if(GameMgr.isTeamBattle()) scoreBar = new obj2d_TeamScoreBar(17,GameMgr.WIDTH,this.players,GameMgr.teamColor); else scoreBar = new obj2d_MultiScoreBar(17,GameMgr.WIDTH,this.players);
		this.obj2d.add(scoreBar);
	}
	,update: function(time) {
		if(this.debugTime != 0) {
			time += this.debugTime;
			this.debugTime = 0;
		}
		scene_abs_AbstractScene.prototype.update.call(this,time);
		this.progress += time;
		if(this.isError()) return;
		this.positionMgr.update(time);
		var posiData = this.positionMgr.getPosition();
		this.camera.setPosition(posiData);
		this.addObjs(time);
		if(this.nowTime > 0) this.startCount();
		var arrowTime;
		if(this.nowTime > 0) arrowTime = this.waitTime; else arrowTime = 2000;
		this.arrow.setPosi(this.camera.position,posiData.deg);
		if(this.progress >= this.maxTime + this.waitTime) this.endCount(); else this.arrow.setDirection(this.camera.position,this.positionMgr.getAfterPosition(arrowTime).point);
		this.first = false;
	}
	,setTimeObject: function(timeArray) {
		this.opt.setTimeObjects(timeArray);
	}
	,addObjs: function(time) {
		this.opt.update(time);
		var objs = this.opt.getSetObject();
		if(objs.length > 0 && this.first == false) {
			var _g = 0;
			while(_g < objs.length) {
				var o = objs[_g];
				++_g;
				var ap = this.positionMgr.getAfterPosition(o.getAfterTime());
				ap = this.camera.getTarget(ap);
				o.setObjPosition(ap.point,ap.deg);
				var obj = o.getObj();
				this.obj3d.add(obj);
				this.scene.add(obj);
			}
		}
	}
	,startCount: function() {
		var prInt = this.progress / 1000 | 0;
		var num = (this.waitTime / 1000 | 0) - prInt;
		if(this.nowTime != num) {
			this.nowTime = num;
			if(this.nowTime < 0) return;
			var text;
			if(this.nowTime == 0) text = "Start!"; else text = "" + this.nowTime;
			var positionData = this.positionMgr.getPosition();
			var textObj = new obj3d_TimerObject(new shape_TextShape({ text : text, size : 200}),null,{ limit : Math.min((prInt + 1) * 1000 - this.progress,800)});
			textObj.position.copy(positionData.point);
			var radian = Math.PI / 180 * positionData.deg;
			textObj.position.z += 800 * Math.sin(radian);
			textObj.position.x += 100 * text.length / 2;
			textObj.position.y += 100;
			textObj.rotation.y = Math.PI;
			this.obj3d.add(textObj);
			this.scene.add(textObj);
		}
	}
	,endCount: function() {
		if(!this.end) {
			this.end = true;
			var positionData = this.positionMgr.getPosition();
			positionData = this.camera.getTarget(positionData);
			var text = "FINISH!";
			var textObj = new obj3d_TimerObject(new shape_TextShape({ text : text, size : 200}),null,{ limit : this.endTime});
			textObj.position.copy(positionData.point);
			var radian = Math.PI / 180 * positionData.deg;
			textObj.position.z += 500 * Math.sin(radian - Math.PI + Math.PI / 4);
			textObj.position.x += 500 * Math.cos(radian - Math.PI + Math.PI / 4);
			textObj.position.y += 100;
			textObj.rotation.y = Math.PI / 180 * (90 - positionData.deg) + Math.PI;
			this.obj3d.add(textObj);
			this.scene.add(textObj);
			if(this.players.length == 1 && !Type.enumEq(GameMgr.nowScene,EScene.RandomGameScene)) GameMgr.sendPlayerRank(this.players[0],this.stageId);
		}
		if(this.progress - this.maxTime - this.waitTime >= this.endTime) {
			if(this.players.length == 1) GameMgr.changeScene(EScene.ResultRankScene,[this.players,GameMgr.nowScene]); else if(GameMgr.isTeamBattle()) GameMgr.changeScene(EScene.ResultTeamScene,[this.players,GameMgr.nowScene]); else GameMgr.changeScene(EScene.ResultSingleScene,[this.players,GameMgr.nowScene]);
		}
	}
	,isError: function() {
		if(GameMgr.entryNum > 0) return false;
		if(!this.end) this.progress = this.maxTime + this.waitTime;
		this.endCount();
		return true;
	}
	,__class__: scene_abs_AbstractMoveScene
});
var scene_abs_AbstractMoveRouteScene = function(context,param,roteTime) {
	this.floorObjs = [];
	scene_abs_AbstractMoveScene.call(this,context,param);
	if(param == null) param = { };
	this.positionMgr = new position_RoutePosition(param.startWait,roteTime);
	this.floorMgr = new field_FieldFloor(this.visualRange);
};
$hxClasses["scene.abs.AbstractMoveRouteScene"] = scene_abs_AbstractMoveRouteScene;
scene_abs_AbstractMoveRouteScene.__name__ = true;
scene_abs_AbstractMoveRouteScene.__super__ = scene_abs_AbstractMoveScene;
scene_abs_AbstractMoveRouteScene.prototype = $extend(scene_abs_AbstractMoveScene.prototype,{
	setRouteData: function(data) {
		this.positionMgr.setRouteData(data);
		this.floorMgr.setRouteData(data);
		this.maxTime = this.positionMgr.getMaxTime();
	}
	,setFloorTexture: function(floor,pattern) {
		if(pattern == null) pattern = 16;
		if(floor == null) floor = GameMgr.DIRECTORY + "images/floor01.jpg";
		this.floorMgr.setTexture(floor,pattern);
	}
	,setGroudTexture: function(ground,pattern) {
		if(pattern == null) pattern = 128;
		if(ground == null) ground = GameMgr.DIRECTORY + "images/grasslight-big.jpg";
		var groundTexture = THREE.ImageUtils.loadTexture(ground);
		groundTexture.wrapS = groundTexture.wrapT = 1000;
		groundTexture.repeat.set(pattern,pattern);
		groundTexture.anisotropy = 16;
		this.groundMaterial = new THREE.MeshPhongMaterial({ color : 16777215, specular : 1118481, map : groundTexture});
	}
	,setSkyTexture: function(imagePrefix,directions,imageSuffix,addY) {
		if(addY == null) addY = 0;
		if(imageSuffix == null) imageSuffix = ".png";
		if(imagePrefix == null) imagePrefix = GameMgr.DIRECTORY + "images/dawnmountain-";
		this.skyMaterials = [];
		var imageSuffix1 = ".png";
		if(directions == null) directions = ["xpos","xneg","ypos","yneg","zpos","zneg"];
		var _g = 0;
		while(_g < directions.length) {
			var d = directions[_g];
			++_g;
			this.skyMaterials.push(THREE.ImageUtils.loadTexture(imagePrefix + d + imageSuffix1));
		}
		this.skyBoxAddY = addY;
	}
	,once: function() {
		if(!scene_abs_AbstractMoveScene.prototype.once.call(this)) return false;
		var light;
		this.scene.add(new THREE.AmbientLight(6710886));
		light = new THREE.DirectionalLight(14674943,1.75);
		light.position.set(-50,250,-200);
		light.position.multiplyScalar(1.3);
		light.castShadow = true;
		light.shadowCameraVisible = true;
		light.shadowMapWidth = 1024;
		light.shadowMapHeight = 1024;
		this.scene.add(light);
		this.setSkyTexture();
		this.setFloorTexture();
		this.setGroudTexture();
		return true;
	}
	,init: function(args) {
		scene_abs_AbstractMoveScene.prototype.init.call(this,args);
		if(this.isFirst()) {
			var skyGeometry = new THREE.BoxGeometry(10000,10000,10000);
			var materialArray = [];
			var _g = 0;
			while(_g < 6) {
				var i = _g++;
				materialArray.push(new THREE.MeshBasicMaterial({ map : this.skyMaterials[i], side : 1}));
			}
			var skyMaterial = new THREE.MeshFaceMaterial(materialArray);
			this.skyBox = new THREE.Mesh(skyGeometry,skyMaterial);
			this.scene.add(this.skyBox);
			var size = 200000;
			var mesh = new THREE.Mesh(new THREE.PlaneBufferGeometry(size,size),this.groundMaterial);
			mesh.position.y = -3.5;
			mesh.rotation.x = -Math.PI / 2;
			mesh.receiveShadow = true;
			this.scene.add(mesh);
		}
		var _g1 = 0;
		var _g11 = this.floorObjs;
		while(_g1 < _g11.length) {
			var f = _g11[_g1];
			++_g1;
			this.scene.remove(f);
		}
		this.floorObjs = [];
		var floors = this.floorMgr.getFloorObjects(this.stageId);
		var _g2 = 0;
		while(_g2 < floors.length) {
			var i1 = floors[_g2];
			++_g2;
			this.scene.add(i1);
			this.floorObjs.push(i1);
		}
		var $it0 = GameMgr.players.iterator();
		while( $it0.hasNext() ) {
			var i2 = $it0.next();
			this.obj2d.add(i2.getObj());
		}
	}
	,playerShot: function(player) {
		scene_abs_AbstractMoveScene.prototype.playerShot.call(this,player);
		var x = player.getX();
		var y = player.getY();
		if(this.end) return;
		var pos = new THREE.Vector3(x / GameMgr.WIDTH * 2 - 1,-(y / GameMgr.HEIGHT) * 2 + 1,1);
		pos.unproject(this.camera);
		var ray = new THREE.Raycaster(this.camera.position,pos.sub(this.camera.position).normalize());
		var target = [];
		var _g_head = this.obj3d.h;
		var _g_val = null;
		while(_g_head != null) {
			var i;
			i = (function($this) {
				var $r;
				_g_val = _g_head[0];
				_g_head = _g_head[1];
				$r = _g_val;
				return $r;
			}(this));
			target.push(i);
		}
		var objs = ray.intersectObjects(target);
		if(objs.length > 0) {
			var i1 = 0;
			var obj;
			obj = js_Boot.__cast(objs[i1].object , obj3d_MeshEx);
			while(obj.getShapeName() == "text" && objs.length > i1 + 1) {
				i1++;
				obj = js_Boot.__cast(objs[i1].object , obj3d_MeshEx);
			}
			if(obj.hit() == true) {
				var score = obj.getScore();
				var text = new obj3d_TimerObject(new shape_TextShape({ text : "" + score, frontColor : player.getColor(), SideColor : player.getColor(), opacity : 0.6, size : 60}));
				text.position.copy(obj.position);
				var rad = Math.atan2(this.camera.position.z - obj.position.z,obj.position.x - this.camera.position.x);
				text.rotation.y = rad - Math.PI / 2;
				player.addScore(score);
				this.obj3d.add(text);
				this.scene.add(text);
				this.obj3d.remove(obj);
				this.scene.remove(obj);
				this.audio.playSE("break");
			} else if(obj.getShapeName() != "text") this.audio.playSE("block");
		}
	}
	,update: function(time) {
		scene_abs_AbstractMoveScene.prototype.update.call(this,time);
		if(this.skyBox != null) {
			var p = this.positionMgr.getPosition().point;
			p.y += this.skyBoxAddY;
			this.skyBox.position.copy(p);
		}
	}
	,__class__: scene_abs_AbstractMoveRouteScene
});
var scene_EasyGameScene = function(context) {
	scene_abs_AbstractMoveRouteScene.call(this,context,null,1000);
	this.audioName = "easy";
};
$hxClasses["scene.EasyGameScene"] = scene_EasyGameScene;
scene_EasyGameScene.__name__ = true;
scene_EasyGameScene.__super__ = scene_abs_AbstractMoveRouteScene;
scene_EasyGameScene.prototype = $extend(scene_abs_AbstractMoveRouteScene.prototype,{
	once: function() {
		if(!scene_abs_AbstractMoveRouteScene.prototype.once.call(this)) return false;
		var routes = [{ time : 5000, point : new THREE.Vector3(0,0,2000)},{ time : 10000, point : new THREE.Vector3(6000,0,2000)},{ time : 10000, point : new THREE.Vector3(6000,0,8000)},{ time : 10000, point : new THREE.Vector3(0,0,8000)},{ time : 10000, point : new THREE.Vector3(-2000,0,14000)},{ time : 10000, point : new THREE.Vector3(3000,0,14000)},{ time : 5000, point : new THREE.Vector3(6000,0,14000)}];
		this.setRouteData(routes);
		var roteRightAction = new action_RotateAction({ right : true});
		var jumpAction = new action_JumpAction({ waitTime : 0});
		var randamRoteAction = new action_RotateRandomAction();
		var moveXAction = new action_MoveAction({ moveType : action_MoveType.x});
		var moveXRightAction = new action_MoveAction({ reverse : true, moveType : action_MoveType.x});
		var moveZAction = new action_MoveAction({ moveType : action_MoveType.z});
		var crystalColorShape = new shape_CrystalShape(null,{ color : 16733525});
		var crystalBlackShape = new shape_CrystalShape(null,{ color : 0});
		var crystalSingleShape = new shape_CrystalShape(900);
		var cylinderShape = new shape_CylinderShape();
		var cubeShape = new shape_CubeShape();
		var drumShape = new shape_DrumShape();
		var masterObjs_0 = new obj3d_BreakObject(crystalColorShape,[moveXAction]);
		var masterObjs_1 = new obj3d_BreakObject(crystalBlackShape,[roteRightAction]);
		var masterObjs_2 = new obj3d_BreakObject(cubeShape,[randamRoteAction]);
		var masterObjs_3 = new obj3d_BreakObject(crystalColorShape,[jumpAction]);
		var masterObjs_4 = new obj3d_BreakObject(cylinderShape,[roteRightAction]);
		var masterObjs_5 = new obj3d_BreakObject(cylinderShape,[jumpAction]);
		var masterObjs_6 = new obj3d_InvincibleObject(drumShape);
		var masterObjs_7 = new obj3d_BreakObject(crystalColorShape,[moveXRightAction]);
		var masterObjs_8 = new obj3d_BreakObject(crystalSingleShape,[jumpAction]);
		var objs = [{ obj : masterObjs_6, appTime : 3000, rpps : 4500, distance : -300},{ obj : masterObjs_6, appTime : 3000, rpps : 4500, distance : 300},{ obj : masterObjs_6, appTime : 4000, rpps : 4500, distance : -1000},{ obj : masterObjs_6, appTime : 4000, rpps : 4500, distance : 1000},{ obj : masterObjs_6, appTime : 4000, rpps : 5500, distance : 1000},{ obj : masterObjs_6, appTime : 4000, rpps : 5500, distance : -1000},{ obj : masterObjs_6, appTime : 4000, rpps : 6000, distance : 500},{ obj : masterObjs_6, appTime : 4000, rpps : 6000, distance : -500},{ obj : masterObjs_5, appTime : 3000, rpps : 4600, distance : 300, positionY : -100},{ obj : masterObjs_5, appTime : 3000, rpps : 4600, distance : -300, positionY : -100},{ obj : masterObjs_5, appTime : 4000, rpps : 4600, distance : 1000, positionY : -100},{ obj : masterObjs_5, appTime : 4000, rpps : 4600, distance : -1000, positionY : -100},{ obj : masterObjs_5, appTime : 4000, rpps : 5600, distance : 1000, positionY : -100},{ obj : masterObjs_5, appTime : 4000, rpps : 5600, distance : -1000, positionY : -100},{ obj : masterObjs_5, appTime : 3000, rpps : 7100, distance : 500, positionY : -100},{ obj : masterObjs_5, appTime : 3000, rpps : 7100, distance : -500, positionY : -100},{ obj : masterObjs_1, appTime : 0, rpps : 3000, degree : 1000},{ obj : masterObjs_1, appTime : 0, rpps : 3000, distance : 300, degree : 1000},{ obj : masterObjs_1, appTime : 0, rpps : 3000, distance : 300, degree : -1000},{ obj : masterObjs_2, appTime : 6000, rpps : 3000, positionY : 230},{ obj : masterObjs_2, appTime : 6000, rpps : 3000, positionY : 400, distance : 200},{ obj : masterObjs_2, appTime : 6000, rpps : 3000, distance : -200, positionY : 50},{ obj : masterObjs_2, appTime : 6000, rpps : 3000, positionY : 400, distance : -200},{ obj : masterObjs_2, appTime : 6000, rpps : 3000, positionY : 50, distance : 200},{ obj : masterObjs_1, appTime : 4000, rpps : 2000, positionY : 200, distance : 1000},{ obj : masterObjs_1, appTime : 4000, rpps : 2000, positionY : 200, distance : 800},{ obj : masterObjs_1, appTime : 4000, rpps : 2000, positionY : 200, distance : 600},{ obj : masterObjs_1, appTime : 4000, rpps : 2000, positionY : 200, distance : 400},{ obj : masterObjs_1, appTime : 4000, rpps : 2000, positionY : 200, distance : 200},{ obj : masterObjs_1, appTime : 4000, rpps : 2000, positionY : 200},{ obj : masterObjs_1, appTime : 4000, rpps : 2000, positionY : 200, distance : -200},{ obj : masterObjs_1, appTime : 4000, rpps : 2000, positionY : 200, distance : -400},{ obj : masterObjs_1, appTime : 4000, rpps : 2000, positionY : 200, distance : -600},{ obj : masterObjs_1, appTime : 4000, rpps : 2000, positionY : 200, distance : -800},{ obj : masterObjs_1, appTime : 4000, rpps : 2000, positionY : 200, distance : -1000},{ obj : masterObjs_1, appTime : 4000, rpps : 2000, positionY : 400, distance : 1000},{ obj : masterObjs_1, appTime : 4000, rpps : 2000, positionY : 400, distance : 800},{ obj : masterObjs_1, appTime : 4000, rpps : 2000, positionY : 400, distance : 600},{ obj : masterObjs_1, appTime : 4000, rpps : 2000, positionY : 400, distance : 400},{ obj : masterObjs_1, appTime : 4000, rpps : 2000, positionY : 400, distance : 200},{ obj : masterObjs_1, appTime : 4000, rpps : 2000, positionY : 400},{ obj : masterObjs_1, appTime : 4000, rpps : 2000, positionY : 400, distance : -200},{ obj : masterObjs_1, appTime : 4000, rpps : 2000, positionY : 400, distance : -400},{ obj : masterObjs_1, appTime : 4000, rpps : 2000, positionY : 400, distance : -600},{ obj : masterObjs_1, appTime : 4000, rpps : 2000, positionY : 400, distance : -800},{ obj : masterObjs_1, appTime : 4000, rpps : 2000, positionY : 400, distance : -1000},{ obj : masterObjs_1, appTime : 4000, rpps : 2000, distance : 1000},{ obj : masterObjs_1, appTime : 4000, rpps : 2000, distance : 800},{ obj : masterObjs_1, appTime : 4000, rpps : 2000, distance : 600},{ obj : masterObjs_1, appTime : 4000, rpps : 2000, distance : 400},{ obj : masterObjs_1, appTime : 4000, rpps : 2000, distance : 200},{ obj : masterObjs_1, appTime : 4000, rpps : 2000},{ obj : masterObjs_1, appTime : 4000, rpps : 2000, distance : -200},{ obj : masterObjs_1, appTime : 4000, rpps : 2000, distance : -400},{ obj : masterObjs_1, appTime : 4000, rpps : 2000, distance : -600},{ obj : masterObjs_1, appTime : 4000, rpps : 2000, distance : -800},{ obj : masterObjs_1, appTime : 4000, rpps : 2000, distance : -1000},{ obj : masterObjs_8, appTime : 8000, rpps : 4000}];
		var maxHeight = 500;
		var _g = 0;
		while(_g < 20) {
			var i = _g++;
			objs.push({ obj : masterObjs_2, appTime : 48000 + i * 500, rpps : 1000, distance : 1000, degree : (90 - i * 10 + 360) % 360, positionY : maxHeight * i / 20});
		}
		var _g1 = 0;
		while(_g1 < 20) {
			var i1 = _g1++;
			objs.push({ obj : masterObjs_2, appTime : 48000 + i1 * 500, rpps : 1000, distance : 1000, degree : (270 + i1 * 10) % 360, positionY : maxHeight * i1 / 20});
		}
		var _g2 = 0;
		while(_g2 < 20) {
			var i2 = _g2++;
			objs.push({ obj : masterObjs_2, appTime : 48000 + i2 * 500, rpps : 1000, distance : 1000, degree : (90 - i2 * 10 + 360) % 360, positionY : maxHeight - maxHeight * i2 / 20});
		}
		var _g3 = 0;
		while(_g3 < 20) {
			var i3 = _g3++;
			objs.push({ obj : masterObjs_2, appTime : 48000 + i3 * 500, rpps : 1000, distance : 1000, degree : (270 + i3 * 10) % 360, positionY : maxHeight - maxHeight * i3 / 20});
		}
		var _g4 = 0;
		while(_g4 < 18) {
			var i4 = _g4++;
			objs.push({ obj : masterObjs_4, appTime : 10000 + i4 * 500, rpps : 6000, distance : 0});
		}
		var _g5 = 0;
		while(_g5 < 18) {
			var i5 = _g5++;
			objs.push({ obj : masterObjs_4, appTime : 10000 + i5 * 500, rpps : 6000, distance : -200});
		}
		var _g6 = 0;
		while(_g6 < 18) {
			var i6 = _g6++;
			objs.push({ obj : masterObjs_4, appTime : 10000 + i6 * 500, rpps : 6000, distance : -400});
		}
		var _g7 = 0;
		while(_g7 < 18) {
			var i7 = _g7++;
			objs.push({ obj : masterObjs_4, appTime : 10000 + i7 * 500, rpps : 6000, distance : -600});
		}
		var _g8 = 0;
		while(_g8 < 18) {
			var i8 = _g8++;
			objs.push({ obj : masterObjs_4, appTime : 10000 + i8 * 500, rpps : 6000, distance : -800});
		}
		var _g9 = 0;
		while(_g9 < 18) {
			var i9 = _g9++;
			objs.push({ obj : masterObjs_4, appTime : 10000 + i9 * 500, rpps : 6000, distance : -1000});
		}
		var _g10 = 0;
		while(_g10 < 18) {
			var i10 = _g10++;
			objs.push({ obj : masterObjs_4, appTime : 10000 + i10 * 500, rpps : 6000, distance : 200});
		}
		var _g11 = 0;
		while(_g11 < 18) {
			var i11 = _g11++;
			objs.push({ obj : masterObjs_4, appTime : 10000 + i11 * 500, rpps : 6000, distance : 400});
		}
		var _g12 = 0;
		while(_g12 < 18) {
			var i12 = _g12++;
			objs.push({ obj : masterObjs_4, appTime : 10000 + i12 * 500, rpps : 6000, distance : 600});
		}
		var _g13 = 0;
		while(_g13 < 18) {
			var i13 = _g13++;
			objs.push({ obj : masterObjs_4, appTime : 10000 + i13 * 500, rpps : 6000, distance : 800});
		}
		var _g14 = 0;
		while(_g14 < 18) {
			var i14 = _g14++;
			objs.push({ obj : masterObjs_4, appTime : 10000 + i14 * 500, rpps : 6000, distance : 1000});
		}
		var _g15 = 0;
		while(_g15 < 15) {
			var i15 = _g15++;
			objs.push({ obj : masterObjs_3, appTime : 23000 + i15 * 600, rpps : 3000});
		}
		var _g16 = 0;
		while(_g16 < 15) {
			var i16 = _g16++;
			objs.push({ obj : masterObjs_3, appTime : 23000 + i16 * 600, rpps : 3000, distance : 600});
		}
		var _g17 = 0;
		while(_g17 < 15) {
			var i17 = _g17++;
			objs.push({ obj : masterObjs_3, appTime : 23000 + i17 * 600, rpps : 3000, distance : -600});
		}
		var _g18 = 0;
		while(_g18 < 15) {
			var i18 = _g18++;
			objs.push({ obj : masterObjs_7, appTime : 33000 + i18 * 600, rpps : 3000, distance : 600, degree : 270});
			objs.push({ obj : masterObjs_7, appTime : 33000 + i18 * 600, rpps : 3000, distance : 600, degree : 370});
		}
		this.setTimeObject(objs);
		this.setSkyTexture(GameMgr.DIRECTORY + "images/fulllg_",["px","nx","py","ny","pz","nz"]);
		this.stageId = 1;
		return true;
	}
	,init: function(args) {
		scene_abs_AbstractMoveRouteScene.prototype.init.call(this,args);
	}
	,update: function(time) {
		if(this.first) time = Math.min(time,1000);
		scene_abs_AbstractMoveRouteScene.prototype.update.call(this,time);
	}
	,__class__: scene_EasyGameScene
});
var scene_abs_AbstractFixedScene = function(context) {
	scene_abs_AbstractScene.call(this,context);
	this.camera = new camera_FixedCamera();
};
$hxClasses["scene.abs.AbstractFixedScene"] = scene_abs_AbstractFixedScene;
scene_abs_AbstractFixedScene.__name__ = true;
scene_abs_AbstractFixedScene.__super__ = scene_abs_AbstractScene;
scene_abs_AbstractFixedScene.prototype = $extend(scene_abs_AbstractScene.prototype,{
	__class__: scene_abs_AbstractFixedScene
});
var scene_EndScene = function(context) {
	this.msg = "サーバーとの接続が切れました";
	scene_abs_AbstractFixedScene.call(this,context);
};
$hxClasses["scene.EndScene"] = scene_EndScene;
scene_EndScene.__name__ = true;
scene_EndScene.__super__ = scene_abs_AbstractFixedScene;
scene_EndScene.prototype = $extend(scene_abs_AbstractFixedScene.prototype,{
	init: function(args) {
		scene_abs_AbstractFixedScene.prototype.init.call(this,args);
		if(args.length > 0) this.msg = args[0];
		this.audio.stopBGM();
	}
	,update: function(time) {
		this.context.beginPath();
		this.context.font = GameMgr.WIDTH / 24 + "px \"Times New Roman\"";
		this.context.fillStyle = "rgb(0, 0, 0)";
		this.context.textAlign = "center";
		this.context.fillText(this.msg,GameMgr.WIDTH / 2,GameMgr.HEIGHT / 2);
		this.context.closePath();
	}
	,__class__: scene_EndScene
});
var scene_HardGameScene = function(context) {
	scene_abs_AbstractMoveRouteScene.call(this,context,null,1000);
	this.audioName = "hard";
};
$hxClasses["scene.HardGameScene"] = scene_HardGameScene;
scene_HardGameScene.__name__ = true;
scene_HardGameScene.__super__ = scene_abs_AbstractMoveRouteScene;
scene_HardGameScene.prototype = $extend(scene_abs_AbstractMoveRouteScene.prototype,{
	once: function() {
		if(!scene_abs_AbstractMoveRouteScene.prototype.once.call(this)) return false;
		var routes = [{ time : 5000, point : new THREE.Vector3(0,0,3000)},{ time : 8000, point : new THREE.Vector3(-5000,0,3000)},{ time : 10000, point : new THREE.Vector3(-5000,0,13000)},{ time : 5000, point : new THREE.Vector3(2000,0,13000), degree : 90},{ time : 5000, point : new THREE.Vector3(2000,0,20000)},{ time : 8000, point : new THREE.Vector3(10000,0,20000)},{ time : 5000, point : new THREE.Vector3(10000,0,30000), degree : 0},{ time : 5000, point : new THREE.Vector3(7000,0,30000), degree : 0},{ time : 3000, point : new THREE.Vector3(0,0,30000), degree : 0},{ time : 6000, point : new THREE.Vector3(-3000,0,30000), degree : 0}];
		this.setRouteData(routes);
		var roteRightAction = new action_RotateAction({ right : true});
		var jumpAction = new action_JumpAction({ waitTime : 0});
		var delayJump = new action_JumpAction({ waitTime : 100});
		var lowSpeedJump = new action_JumpAction({ waitTime : 0, gravity : 0.5, initialVelocity : 15});
		var middleSpeedJump = new action_JumpAction({ waitTime : 0, gravity : 1, initialVelocity : 20});
		var hiSpeedJump = new action_JumpAction({ waitTime : 0, gravity : 2, initialVelocity : 25});
		var lowDropAction = new action_JumpAction({ waitTime : 0, gravity : 0.5, initialVelocity : 15, up : false});
		var middleDropAction = new action_JumpAction({ waitTime : 0, gravity : 1, initialVelocity : 20, up : false});
		var hiDropAction = new action_JumpAction({ waitTime : 0, gravity : 2, initialVelocity : 25, up : false});
		var dropAction = new action_JumpAction({ waitTime : 0, gravity : 1, initialVelocity : 50, up : false});
		var delayDrop1 = new action_JumpAction({ waitTime : 200, gravity : 1, initialVelocity : 50, up : false});
		var delayDrop2 = new action_JumpAction({ waitTime : 400, gravity : 1, initialVelocity : 50, up : false});
		var delayDrop3 = new action_JumpAction({ waitTime : 600, gravity : 1, initialVelocity : 50, up : false});
		var randamRoteAction = new action_RotateRandomAction();
		var moveXAction = new action_MoveAction({ moveType : action_MoveType.x});
		var moveRXAction = new action_MoveAction({ distance : 1000, speed : 30, reverse : true, moveType : action_MoveType.x});
		var escapeXAction = new action_MoveAction({ distance : 800, speed : 45, reverse : true, moveType : action_MoveType.x});
		var moveX1 = new action_MoveAction({ distance : 1000, speed : 500, moveType : action_MoveType.x});
		var moveX2 = new action_MoveAction({ distance : 1000, speed : 500, reverse : true, moveType : action_MoveType.x});
		var slowMoveX = new action_MoveAction({ distance : 1000, speed : 10, reverse : true, moveType : action_MoveType.x});
		var moveYAction = new action_MoveAction();
		var escapeYAction = new action_MoveAction({ distance : 800, speed : 45, reverse : true});
		var moveY1 = new action_MoveAction({ distance : 1000, speed : 250});
		var moveY2 = new action_MoveAction({ distance : 1000, speed : 250, reverse : true});
		var slowMoveY = new action_MoveAction({ distance : 1000, speed : 100});
		var moveZAction = new action_MoveAction({ moveType : action_MoveType.z});
		var escapeZAction = new action_MoveAction({ distance : 800, speed : 45, reverse : true, moveType : action_MoveType.z});
		var moveZ1 = new action_MoveAction({ distance : 1000, speed : 500, moveType : action_MoveType.z});
		var moveZ2 = new action_MoveAction({ distance : 1000, speed : 500, reverse : true, moveType : action_MoveType.z});
		var slowMoveZ = new action_MoveAction({ distance : 1000, speed : 5, moveType : action_MoveType.z});
		var slowMoveRZ = new action_MoveAction({ distance : 1000, speed : 5, reverse : true, moveType : action_MoveType.z});
		var lastMoveZ = new action_MoveAction({ distance : 1000, speed : 200, moveType : action_MoveType.z});
		var lastMoveRZ = new action_MoveAction({ distance : 1000, speed : 200, reverse : true, moveType : action_MoveType.z});
		var lastSlowMoveZ = new action_MoveAction({ distance : 1000, speed : 10, moveType : action_MoveType.z});
		var lastSlowMoveRZ = new action_MoveAction({ distance : 1000, speed : 10, reverse : true, moveType : action_MoveType.z});
		var crystalColorShape = new shape_CrystalShape(null,{ color : 16733525});
		var crystalColorYellow = new shape_CrystalShape(850,{ color : 16510005});
		var crystalColorBlue = new shape_CrystalShape(850,{ color : 732119});
		var crystalColorGreen = new shape_CrystalShape(850,{ color : 776785});
		var crystalColorRed = new shape_CrystalShape(null,{ color : 16711735});
		var crystalColorOrange = new shape_CrystalShape(null,{ color : 16735769});
		var crystalColorRandom = new shape_CrystalShape(null,{ randomColor : true});
		var crystalBlackShape = new shape_CrystalShape(null,{ color : 0});
		var cylinderShape = new shape_CylinderShape();
		var cylinderShapeColorBule = new shape_CylinderShape(900,{ color : 732119});
		var cubeShape = new shape_CubeShape();
		var cubeShapeColorRed = new shape_CubeShape(null,{ color : 13369344});
		var cubeShapeColorBlue = new shape_CubeShape(600,{ color : 4425705});
		var drumShape = new shape_DrumShape();
		var masterObjs_0 = new obj3d_BreakObject(crystalColorShape,[moveXAction]);
		var masterObjs_1 = new obj3d_BreakObject(crystalBlackShape,[roteRightAction]);
		var masterObjs_2 = new obj3d_BreakObject(cubeShape,[randamRoteAction]);
		var masterObjs_3 = new obj3d_BreakObject(crystalColorShape,[jumpAction]);
		var masterObjs_4 = new obj3d_BreakObject(cylinderShape,[roteRightAction]);
		var masterObjs_5 = new obj3d_InvincibleObject(drumShape);
		var masterObjs_6 = new obj3d_BreakObject(crystalColorShape,[jumpAction,moveXAction]);
		var masterObjs_7 = new obj3d_BreakObject(cubeShape,[randamRoteAction,lowSpeedJump]);
		var masterObjs_8 = new obj3d_BreakObject(cubeShape,[randamRoteAction,middleSpeedJump]);
		var masterObjs_9 = new obj3d_BreakObject(cubeShape,[randamRoteAction,hiSpeedJump]);
		var masterObjs_10 = new obj3d_BreakObject(cubeShape,[randamRoteAction,lowDropAction]);
		var masterObjs_11 = new obj3d_BreakObject(cubeShape,[randamRoteAction,middleDropAction]);
		var masterObjs_12 = new obj3d_BreakObject(cubeShape,[randamRoteAction,hiDropAction]);
		var masterObjs_13 = new obj3d_BreakObject(cylinderShape,[roteRightAction,moveXAction]);
		var masterObjs_14 = new obj3d_BreakObject(cylinderShape,[roteRightAction,moveZAction]);
		var masterObjs_15 = new obj3d_BreakObject(crystalColorShape,[escapeXAction]);
		var masterObjs_16 = new obj3d_BreakObject(crystalColorShape,[delayJump]);
		var masterObjs_17 = new obj3d_InvincibleObject(drumShape,[moveX1]);
		var masterObjs_18 = new obj3d_InvincibleObject(drumShape,[moveX2]);
		var masterObjs_19 = new obj3d_InvincibleObject(drumShape,[moveY1]);
		var masterObjs_20 = new obj3d_InvincibleObject(drumShape,[moveY2]);
		var masterObjs_21 = new obj3d_InvincibleObject(drumShape,[moveZ1]);
		var masterObjs_22 = new obj3d_InvincibleObject(drumShape,[moveZ2]);
		var masterObjs_23 = new obj3d_InvincibleObject(drumShape,[slowMoveX]);
		var masterObjs_24 = new obj3d_InvincibleObject(drumShape,[slowMoveY]);
		var masterObjs_25 = new obj3d_InvincibleObject(drumShape,[slowMoveZ]);
		var masterObjs_26 = new obj3d_BreakObject(crystalColorShape,[roteRightAction]);
		var masterObjs_27 = new obj3d_BreakObject(crystalColorYellow,[roteRightAction,lowSpeedJump,moveRXAction]);
		var masterObjs_28 = new obj3d_BreakObject(crystalColorBlue,[roteRightAction,middleSpeedJump,moveRXAction]);
		var masterObjs_29 = new obj3d_BreakObject(crystalColorGreen,[roteRightAction,hiSpeedJump,moveRXAction]);
		var masterObjs_30 = new obj3d_BreakObject(crystalColorOrange,[roteRightAction,slowMoveZ]);
		var masterObjs_31 = new obj3d_BreakObject(crystalColorOrange,[roteRightAction,slowMoveRZ]);
		var masterObjs_32 = new obj3d_BreakObject(cylinderShapeColorBule,[roteRightAction,dropAction]);
		var masterObjs_33 = new obj3d_BreakObject(cylinderShapeColorBule,[roteRightAction,delayDrop1]);
		var masterObjs_34 = new obj3d_BreakObject(cylinderShapeColorBule,[roteRightAction,delayDrop2]);
		var masterObjs_35 = new obj3d_BreakObject(cylinderShapeColorBule,[roteRightAction,delayDrop3]);
		var masterObjs_36 = new obj3d_InvincibleObject(drumShape,[lastSlowMoveRZ]);
		var masterObjs_37 = new obj3d_InvincibleObject(drumShape,[lastSlowMoveZ]);
		var masterObjs_38 = new obj3d_BreakObject(crystalColorRed,[roteRightAction]);
		var masterObjs_39 = new obj3d_BreakObject(cubeShapeColorBlue,[randamRoteAction,lowSpeedJump]);
		var objs = [{ obj : masterObjs_4, appTime : 1500, rpps : 500},{ obj : masterObjs_4, appTime : 1500, distance : 200, degree : 270, positionY : 200, rpps : 500},{ obj : masterObjs_4, appTime : 1500, distance : 200, degree : 90, positionY : 200, rpps : 500},{ obj : masterObjs_4, appTime : 1500, positionY : 400, rpps : 500},{ obj : masterObjs_4, appTime : 2000, positionY : 200, rpps : 500},{ obj : masterObjs_4, appTime : 2000, distance : 200, degree : 270, rpps : 500},{ obj : masterObjs_4, appTime : 2000, distance : 200, degree : 90, rpps : 500},{ obj : masterObjs_4, appTime : 2000, positionY : 400, distance : 200, degree : 90, rpps : 500},{ obj : masterObjs_4, appTime : 2000, positionY : 400, distance : 200, degree : 270, rpps : 500},{ obj : masterObjs_4, appTime : 2500, positionY : 200, distance : 400, degree : 270, rpps : 500},{ obj : masterObjs_4, appTime : 2500, positionY : 200, distance : 400, degree : 90, rpps : 500},{ obj : masterObjs_1, appTime : 5500, distance : 400, degree : 90},{ obj : masterObjs_1, appTime : 6000, distance : 400, degree : 90},{ obj : masterObjs_1, appTime : 6500, distance : 400, degree : 90},{ obj : masterObjs_1, appTime : 5500, distance : 400, degree : 270},{ obj : masterObjs_1, appTime : 6000, distance : 400, degree : 270},{ obj : masterObjs_1, appTime : 6500, distance : 400, degree : 270},{ obj : masterObjs_1, appTime : 5500},{ obj : masterObjs_1, appTime : 6000},{ obj : masterObjs_1, appTime : 6500},{ appTime : 6000, obj : masterObjs_5, rpps : 1000},{ appTime : 6000, obj : masterObjs_5, rpps : 1000, distance : 300, degree : 270},{ appTime : 6000, obj : masterObjs_5, rpps : 1000, distance : 300, degree : 90},{ appTime : 6300, obj : masterObjs_7, rpps : 1000},{ appTime : 6300, obj : masterObjs_7, distance : 300, degree : 270, rpps : 1000},{ appTime : 6300, obj : masterObjs_7, distance : 300, degree : 90, rpps : 1000},{ appTime : 6600, obj : masterObjs_10, distance : 150, degree : 90, positionY : 400, rpps : 1000},{ appTime : 6600, obj : masterObjs_10, distance : 150, degree : 270, positionY : 400, rpps : 1000},{ appTime : 6600, obj : masterObjs_10, distance : 450, degree : 90, positionY : 400, rpps : 1000},{ appTime : 6600, obj : masterObjs_10, distance : 450, degree : 270, positionY : 400, rpps : 1000},{ appTime : 7000, obj : masterObjs_5, rpps : 1000},{ appTime : 7000, obj : masterObjs_5, rpps : 1000, distance : 300, degree : 270},{ appTime : 7000, obj : masterObjs_5, rpps : 1000, distance : 300, degree : 90},{ appTime : 7300, obj : masterObjs_8, rpps : 1000},{ appTime : 7300, obj : masterObjs_8, distance : 300, degree : 270, rpps : 1000},{ appTime : 7300, obj : masterObjs_8, distance : 300, degree : 90, rpps : 1000},{ appTime : 7600, obj : masterObjs_11, distance : 150, degree : 90, positionY : 400, rpps : 1000},{ appTime : 7600, obj : masterObjs_11, distance : 150, degree : 270, positionY : 400, rpps : 1000},{ appTime : 7600, obj : masterObjs_11, distance : 450, degree : 90, positionY : 400, rpps : 1000},{ appTime : 7600, obj : masterObjs_11, distance : 450, degree : 270, positionY : 400, rpps : 1000},{ appTime : 8000, obj : masterObjs_5, rpps : 1000},{ appTime : 8000, obj : masterObjs_5, distance : 300, degree : 270, rpps : 1000},{ appTime : 8000, obj : masterObjs_5, distance : 300, degree : 90, rpps : 1000},{ appTime : 8300, obj : masterObjs_9, rpps : 1000},{ appTime : 8300, obj : masterObjs_9, distance : 300, degree : 270, rpps : 1000},{ appTime : 8300, obj : masterObjs_9, distance : 300, degree : 90, rpps : 1000},{ appTime : 8600, obj : masterObjs_12, distance : 150, degree : 90, positionY : 400, rpps : 1000},{ appTime : 8600, obj : masterObjs_12, distance : 150, degree : 270, positionY : 400, rpps : 1000},{ appTime : 8600, obj : masterObjs_12, distance : 450, degree : 90, positionY : 400, rpps : 1000},{ appTime : 8600, obj : masterObjs_12, distance : 450, degree : 270, positionY : 400, rpps : 1000},{ appTime : 10000, obj : masterObjs_5, rpps : 2000},{ appTime : 10000, obj : masterObjs_5, distance : 300, degree : 270, rpps : 2000},{ appTime : 10000, obj : masterObjs_5, distance : 300, degree : 90, rpps : 2000},{ appTime : 11000, obj : masterObjs_15},{ appTime : 11000, obj : masterObjs_15, distance : 300, degree : 270},{ appTime : 11000, obj : masterObjs_15, distance : 300, degree : 90},{ appTime : 11500, obj : masterObjs_16, rpps : 1000},{ appTime : 11500, obj : masterObjs_16, rpps : 1000, distance : 300, degree : 90},{ appTime : 11500, obj : masterObjs_16, rpps : 1000, distance : 300, degree : 270},{ appTime : 13000, obj : masterObjs_17, rpps : 1000},{ appTime : 13000, obj : masterObjs_1, rpps : 1000},{ appTime : 13000, obj : masterObjs_1, rpps : 1000, distance : 200, degree : 90},{ appTime : 13000, obj : masterObjs_1, rpps : 1000, distance : 400, degree : 90},{ appTime : 13000, obj : masterObjs_1, rpps : 1000, distance : 600, degree : 90},{ appTime : 13000, obj : masterObjs_1, rpps : 1000, distance : 800, degree : 90},{ appTime : 13000, obj : masterObjs_1, rpps : 1000, distance : 200, degree : 270},{ appTime : 13000, obj : masterObjs_1, rpps : 1000, distance : 400, degree : 270},{ appTime : 13000, obj : masterObjs_1, rpps : 1000, distance : 600, degree : 270},{ appTime : 13000, obj : masterObjs_1, rpps : 1000, distance : 800, degree : 270},{ appTime : 13000, obj : masterObjs_18, rpps : 2000},{ appTime : 13000, obj : masterObjs_1, rpps : 2000},{ appTime : 13000, obj : masterObjs_1, rpps : 2000, distance : 200, degree : 90},{ appTime : 13000, obj : masterObjs_1, rpps : 2000, distance : 400, degree : 90},{ appTime : 13000, obj : masterObjs_1, rpps : 2000, distance : 600, degree : 90},{ appTime : 13000, obj : masterObjs_1, rpps : 2000, distance : 800, degree : 90},{ appTime : 13000, obj : masterObjs_1, rpps : 2000, distance : 200, degree : 270},{ appTime : 13000, obj : masterObjs_1, rpps : 2000, distance : 400, degree : 270},{ appTime : 13000, obj : masterObjs_1, rpps : 2000, distance : 600, degree : 270},{ appTime : 13000, obj : masterObjs_1, rpps : 2000, distance : 800, degree : 270},{ appTime : 13000, obj : masterObjs_21, rpps : 4000, distance : 600, degree : 90},{ appTime : 13000, obj : masterObjs_1, rpps : 3000, distance : 600, degree : 90},{ appTime : 13000, obj : masterObjs_1, rpps : 3200, distance : 600, degree : 90},{ appTime : 13000, obj : masterObjs_1, rpps : 3400, distance : 600, degree : 90},{ appTime : 13000, obj : masterObjs_1, rpps : 3600, distance : 600, degree : 90},{ appTime : 13000, obj : masterObjs_1, rpps : 3800, distance : 600, degree : 90},{ appTime : 13000, obj : masterObjs_1, rpps : 4000, distance : 600, degree : 90},{ appTime : 13000, obj : masterObjs_1, rpps : 4200, distance : 600, degree : 90},{ appTime : 13000, obj : masterObjs_1, rpps : 4400, distance : 600, degree : 90},{ appTime : 13000, obj : masterObjs_1, rpps : 4600, distance : 600, degree : 90},{ appTime : 13000, obj : masterObjs_1, rpps : 4800, distance : 600, degree : 90},{ appTime : 13000, obj : masterObjs_1, rpps : 5000, distance : 600, degree : 90},{ appTime : 13000, obj : masterObjs_21, rpps : 4000, distance : 600, degree : 270},{ appTime : 13000, obj : masterObjs_1, rpps : 3000, distance : 600, degree : 270},{ appTime : 13000, obj : masterObjs_1, rpps : 3200, distance : 600, degree : 270},{ appTime : 13000, obj : masterObjs_1, rpps : 3400, distance : 600, degree : 270},{ appTime : 13000, obj : masterObjs_1, rpps : 3600, distance : 600, degree : 270},{ appTime : 13000, obj : masterObjs_1, rpps : 3800, distance : 600, degree : 270},{ appTime : 13000, obj : masterObjs_1, rpps : 4000, distance : 600, degree : 270},{ appTime : 13000, obj : masterObjs_1, rpps : 4200, distance : 600, degree : 270},{ appTime : 13000, obj : masterObjs_1, rpps : 4400, distance : 600, degree : 270},{ appTime : 13000, obj : masterObjs_1, rpps : 4600, distance : 600, degree : 270},{ appTime : 13000, obj : masterObjs_1, rpps : 4800, distance : 600, degree : 270},{ appTime : 13000, obj : masterObjs_1, rpps : 5000, distance : 600, degree : 270},{ appTime : 13000, obj : masterObjs_22, rpps : 4000},{ appTime : 13000, obj : masterObjs_1, rpps : 3000},{ appTime : 13000, obj : masterObjs_1, rpps : 3200},{ appTime : 13000, obj : masterObjs_1, rpps : 3400},{ appTime : 13000, obj : masterObjs_1, rpps : 3600},{ appTime : 13000, obj : masterObjs_1, rpps : 3800},{ appTime : 13000, obj : masterObjs_1, rpps : 4000},{ appTime : 13000, obj : masterObjs_1, rpps : 4200},{ appTime : 13000, obj : masterObjs_1, rpps : 4400},{ appTime : 13000, obj : masterObjs_1, rpps : 4600},{ appTime : 13000, obj : masterObjs_1, rpps : 4800},{ appTime : 13000, obj : masterObjs_1, rpps : 5000},{ appTime : 16000, obj : masterObjs_19, rpps : 4000, distance : 600, degree : 90},{ appTime : 16000, obj : masterObjs_1, rpps : 4000, distance : 600, degree : 90},{ appTime : 16000, obj : masterObjs_1, rpps : 4000, positionY : 200, distance : 600, degree : 90},{ appTime : 16000, obj : masterObjs_1, rpps : 4000, positionY : 400, distance : 600, degree : 90},{ appTime : 16000, obj : masterObjs_1, rpps : 4000, positionY : 600, distance : 600, degree : 90},{ appTime : 16000, obj : masterObjs_1, rpps : 4000, positionY : 800, distance : 600, degree : 90},{ appTime : 16000, obj : masterObjs_1, rpps : 4000, positionY : 1000, distance : 600, degree : 90},{ appTime : 16000, obj : masterObjs_19, rpps : 4000, distance : 600, degree : 270},{ appTime : 16000, obj : masterObjs_1, rpps : 4000, distance : 600, degree : 270},{ appTime : 16000, obj : masterObjs_1, rpps : 4000, positionY : 200, distance : 600, degree : 270},{ appTime : 16000, obj : masterObjs_1, rpps : 4000, positionY : 400, distance : 600, degree : 270},{ appTime : 16000, obj : masterObjs_1, rpps : 4000, positionY : 600, distance : 600, degree : 270},{ appTime : 16000, obj : masterObjs_1, rpps : 4000, positionY : 800, distance : 600, degree : 270},{ appTime : 16000, obj : masterObjs_1, rpps : 4000, positionY : 1000, distance : 600, degree : 270},{ appTime : 16000, obj : masterObjs_20, rpps : 4000},{ appTime : 16000, obj : masterObjs_1, rpps : 4000},{ appTime : 16000, obj : masterObjs_1, rpps : 4000, positionY : 200},{ appTime : 16000, obj : masterObjs_1, rpps : 4000, positionY : 400},{ appTime : 16000, obj : masterObjs_1, rpps : 4000, positionY : 600},{ appTime : 16000, obj : masterObjs_1, rpps : 4000, positionY : 800},{ appTime : 16000, obj : masterObjs_1, rpps : 4000, positionY : 1000},{ appTime : 20000, obj : masterObjs_23, rpps : 2000, distance : 300, degree : 270},{ appTime : 20000, obj : masterObjs_23, rpps : 2000, distance : 400, degree : 270},{ appTime : 20000, obj : masterObjs_23, rpps : 2000, distance : 500, degree : 270},{ appTime : 20000, obj : masterObjs_23, rpps : 2000, distance : 600, degree : 270},{ appTime : 20000, obj : masterObjs_23, rpps : 2000, positionY : 150, distance : 300, degree : 270},{ appTime : 20000, obj : masterObjs_23, rpps : 2000, positionY : 150, distance : 400, degree : 270},{ appTime : 20000, obj : masterObjs_23, rpps : 2000, positionY : 150, distance : 500, degree : 270},{ appTime : 20000, obj : masterObjs_23, rpps : 2000, positionY : 150, distance : 600, degree : 270},{ appTime : 20000, obj : masterObjs_23, rpps : 2000, positionY : 300, distance : 300, degree : 270},{ appTime : 20000, obj : masterObjs_23, rpps : 2000, positionY : 300, distance : 400, degree : 270},{ appTime : 20000, obj : masterObjs_23, rpps : 2000, positionY : 300, distance : 500, degree : 270},{ appTime : 20000, obj : masterObjs_23, rpps : 2000, positionY : 300, distance : 600, degree : 270},{ appTime : 20000, obj : masterObjs_23, rpps : 2000, distance : 300, degree : 270},{ appTime : 20000, obj : masterObjs_23, rpps : 2000, distance : 400, degree : 270},{ appTime : 20000, obj : masterObjs_23, rpps : 2000, distance : 500, degree : 270},{ appTime : 20000, obj : masterObjs_23, rpps : 2000, distance : 600, degree : 270},{ appTime : 19000, obj : masterObjs_26, rpps : 3200},{ appTime : 19000, obj : masterObjs_26, rpps : 3200, distance : 100, degree : 270},{ appTime : 19000, obj : masterObjs_26, rpps : 3200, distance : 100, degree : 90},{ appTime : 19000, obj : masterObjs_26, rpps : 3200, positionY : 150},{ appTime : 19000, obj : masterObjs_26, rpps : 3200, positionY : 150, distance : 100, degree : 270},{ appTime : 19000, obj : masterObjs_26, rpps : 3200, positionY : 150, distance : 100, degree : 90},{ appTime : 19000, obj : masterObjs_26, rpps : 3200, positionY : 300},{ appTime : 19000, obj : masterObjs_26, rpps : 3200, positionY : 300, distance : 100, degree : 270},{ appTime : 19000, obj : masterObjs_26, rpps : 3200, positionY : 300, distance : 100, degree : 90},{ appTime : 24000, obj : masterObjs_27, distance : 800, degree : 270},{ appTime : 24000, obj : masterObjs_27, distance : 800, degree : 270, rpps : 250},{ appTime : 24000, obj : masterObjs_27, distance : 800, degree : 270, rpps : 500},{ appTime : 24000, obj : masterObjs_27, distance : 800, degree : 270, rpps : 750},{ appTime : 24000, obj : masterObjs_27, distance : 800, degree : 270, rpps : 1000},{ appTime : 24000, obj : masterObjs_27, distance : 800, degree : 270, positionY : 200},{ appTime : 24000, obj : masterObjs_27, distance : 800, degree : 270, rpps : 250, positionY : 200},{ appTime : 24000, obj : masterObjs_27, distance : 800, degree : 270, rpps : 500, positionY : 200},{ appTime : 24000, obj : masterObjs_27, distance : 800, degree : 270, rpps : 750, positionY : 200},{ appTime : 24000, obj : masterObjs_27, distance : 800, degree : 270, rpps : 1000, positionY : 200},{ appTime : 24000, obj : masterObjs_27, distance : 800, degree : 270, positionY : 400},{ appTime : 24000, obj : masterObjs_27, distance : 800, degree : 270, rpps : 250, positionY : 400},{ appTime : 24000, obj : masterObjs_27, distance : 800, degree : 270, rpps : 500, positionY : 400},{ appTime : 24000, obj : masterObjs_27, distance : 800, degree : 270, rpps : 750, positionY : 400},{ appTime : 24000, obj : masterObjs_27, distance : 800, degree : 270, rpps : 1000, positionY : 400},{ appTime : 25000, obj : masterObjs_28, distance : 1600, degree : 270},{ appTime : 25000, obj : masterObjs_28, distance : 1600, degree : 270, rpps : 250},{ appTime : 25000, obj : masterObjs_28, distance : 1600, degree : 270, rpps : 500},{ appTime : 25000, obj : masterObjs_28, distance : 1600, degree : 270, rpps : 750},{ appTime : 25000, obj : masterObjs_28, distance : 1600, degree : 270, rpps : 1000},{ appTime : 25000, obj : masterObjs_28, distance : 1600, degree : 270, positionY : 200},{ appTime : 25000, obj : masterObjs_28, distance : 1600, degree : 270, rpps : 250, positionY : 200},{ appTime : 25000, obj : masterObjs_28, distance : 1600, degree : 270, rpps : 500, positionY : 200},{ appTime : 25000, obj : masterObjs_28, distance : 1600, degree : 270, rpps : 750, positionY : 200},{ appTime : 25000, obj : masterObjs_28, distance : 1600, degree : 270, rpps : 1000, positionY : 200},{ appTime : 25000, obj : masterObjs_28, distance : 1600, degree : 270, positionY : 400},{ appTime : 25000, obj : masterObjs_28, distance : 1600, degree : 270, rpps : 250, positionY : 400},{ appTime : 25000, obj : masterObjs_28, distance : 1600, degree : 270, rpps : 500, positionY : 400},{ appTime : 25000, obj : masterObjs_28, distance : 1600, degree : 270, rpps : 750, positionY : 400},{ appTime : 25000, obj : masterObjs_28, distance : 1600, degree : 270, rpps : 1000, positionY : 400},{ appTime : 26000, obj : masterObjs_29, distance : 2400, degree : 270},{ appTime : 26000, obj : masterObjs_29, distance : 2400, degree : 270, rpps : 250},{ appTime : 26000, obj : masterObjs_29, distance : 2400, degree : 270, rpps : 500},{ appTime : 26000, obj : masterObjs_29, distance : 2400, degree : 270, rpps : 750},{ appTime : 26000, obj : masterObjs_29, distance : 2400, degree : 270, rpps : 1000},{ appTime : 26000, obj : masterObjs_29, distance : 2400, degree : 270, positionY : 200},{ appTime : 26000, obj : masterObjs_29, distance : 2400, degree : 270, rpps : 250, positionY : 200},{ appTime : 26000, obj : masterObjs_29, distance : 2400, degree : 270, rpps : 500, positionY : 200},{ appTime : 26000, obj : masterObjs_29, distance : 2400, degree : 270, rpps : 750, positionY : 200},{ appTime : 26000, obj : masterObjs_29, distance : 2400, degree : 270, rpps : 1000, positionY : 200},{ appTime : 26000, obj : masterObjs_29, distance : 2400, degree : 270, positionY : 400},{ appTime : 26000, obj : masterObjs_29, distance : 2400, degree : 270, rpps : 250, positionY : 400},{ appTime : 26000, obj : masterObjs_29, distance : 2400, degree : 270, rpps : 500, positionY : 400},{ appTime : 26000, obj : masterObjs_29, distance : 2400, degree : 270, rpps : 750, positionY : 400},{ appTime : 26000, obj : masterObjs_29, distance : 2400, degree : 270, rpps : 1000, positionY : 400},{ appTime : 28000, obj : masterObjs_39, rpps : 1000, positionY : -150},{ appTime : 28000, obj : masterObjs_39, rpps : 1000, positionY : -150, distance : 400, degree : 270},{ appTime : 28000, obj : masterObjs_39, rpps : 1000, positionY : -150, distance : 400, degree : 90},{ appTime : 28000, obj : masterObjs_39, rpps : 1250, distance : 200, degree : 270, positionY : -150},{ appTime : 28000, obj : masterObjs_39, rpps : 1250, distance : 200, degree : 90, positionY : -150},{ appTime : 28500, obj : masterObjs_39, rpps : 1000, positionY : -150},{ appTime : 28500, obj : masterObjs_39, rpps : 1000, positionY : -150, distance : 400, degree : 270},{ appTime : 28500, obj : masterObjs_39, rpps : 1000, positionY : -150, distance : 400, degree : 90},{ appTime : 28500, obj : masterObjs_39, rpps : 1250, distance : 200, degree : 270, positionY : -150},{ appTime : 28500, obj : masterObjs_39, rpps : 1250, distance : 200, degree : 90, positionY : -150},{ appTime : 29000, obj : masterObjs_39, rpps : 1000, positionY : -150},{ appTime : 29000, obj : masterObjs_39, rpps : 1000, positionY : -150, distance : 400, degree : 270},{ appTime : 29000, obj : masterObjs_39, rpps : 1000, positionY : -150, distance : 400, degree : 90},{ appTime : 29000, obj : masterObjs_39, rpps : 1250, distance : 200, degree : 270, positionY : -150},{ appTime : 29000, obj : masterObjs_39, rpps : 1250, distance : 200, degree : 90, positionY : -150},{ appTime : 29500, obj : masterObjs_39, rpps : 1000, positionY : -150},{ appTime : 29500, obj : masterObjs_39, rpps : 1000, positionY : -150, distance : 400, degree : 270},{ appTime : 29500, obj : masterObjs_39, rpps : 1000, positionY : -150, distance : 400, degree : 90},{ appTime : 29500, obj : masterObjs_39, rpps : 1250, distance : 200, degree : 270, positionY : -150},{ appTime : 29500, obj : masterObjs_39, rpps : 1250, distance : 200, degree : 90, positionY : -150},{ appTime : 30000, obj : masterObjs_39, rpps : 1000, positionY : -150},{ appTime : 30000, obj : masterObjs_39, rpps : 1000, positionY : -150, distance : 400, degree : 270},{ appTime : 30000, obj : masterObjs_39, rpps : 1000, positionY : -150, distance : 400, degree : 90},{ appTime : 30000, obj : masterObjs_39, rpps : 1250, distance : 200, degree : 270, positionY : -150},{ appTime : 30000, obj : masterObjs_39, rpps : 1250, distance : 200, degree : 90, positionY : -150},{ appTime : 33000, obj : masterObjs_30, rpps : 1500, distance : 100, degree : 90},{ appTime : 33000, obj : masterObjs_31, rpps : 1500, distance : 100, degree : 270},{ appTime : 33000, obj : masterObjs_30, rpps : 1500, distance : 200, degree : 90},{ appTime : 33000, obj : masterObjs_31, rpps : 1500, distance : 200, degree : 270},{ appTime : 33000, obj : masterObjs_30, rpps : 1500, distance : 300, degree : 90},{ appTime : 33000, obj : masterObjs_31, rpps : 1500, distance : 300, degree : 270},{ appTime : 33500, obj : masterObjs_30, rpps : 1500, distance : 100, degree : 90},{ appTime : 33500, obj : masterObjs_31, rpps : 1500, distance : 100, degree : 270},{ appTime : 33500, obj : masterObjs_30, rpps : 1500, distance : 200, degree : 90},{ appTime : 33500, obj : masterObjs_31, rpps : 1500, distance : 200, degree : 270},{ appTime : 33500, obj : masterObjs_30, rpps : 1500, distance : 300, degree : 90},{ appTime : 33500, obj : masterObjs_31, rpps : 1500, distance : 300, degree : 270},{ appTime : 34000, obj : masterObjs_30, rpps : 1500, distance : 100, degree : 90},{ appTime : 34000, obj : masterObjs_31, rpps : 1500, distance : 100, degree : 270},{ appTime : 34000, obj : masterObjs_30, rpps : 1500, distance : 200, degree : 90},{ appTime : 34000, obj : masterObjs_31, rpps : 1500, distance : 200, degree : 270},{ appTime : 34000, obj : masterObjs_30, rpps : 1500, distance : 300, degree : 90},{ appTime : 34000, obj : masterObjs_31, rpps : 1500, distance : 300, degree : 270},{ appTime : 34500, obj : masterObjs_30, rpps : 1500, distance : 100, degree : 90},{ appTime : 34500, obj : masterObjs_31, rpps : 1500, distance : 100, degree : 270},{ appTime : 34500, obj : masterObjs_30, rpps : 1500, distance : 200, degree : 90},{ appTime : 34500, obj : masterObjs_31, rpps : 1500, distance : 200, degree : 270},{ appTime : 34500, obj : masterObjs_30, rpps : 1500, distance : 300, degree : 90},{ appTime : 34500, obj : masterObjs_31, rpps : 1500, distance : 300, degree : 270},{ appTime : 35000, obj : masterObjs_30, rpps : 1500, distance : 100, degree : 90},{ appTime : 35000, obj : masterObjs_31, rpps : 1500, distance : 100, degree : 270},{ appTime : 35000, obj : masterObjs_30, rpps : 1500, distance : 200, degree : 90},{ appTime : 35000, obj : masterObjs_31, rpps : 1500, distance : 200, degree : 270},{ appTime : 35000, obj : masterObjs_30, rpps : 1500, distance : 300, degree : 90},{ appTime : 35000, obj : masterObjs_31, rpps : 1500, distance : 300, degree : 270},{ appTime : 35500, obj : masterObjs_30, rpps : 1500, distance : 100, degree : 90},{ appTime : 35500, obj : masterObjs_31, rpps : 1500, distance : 100, degree : 270},{ appTime : 35500, obj : masterObjs_30, rpps : 1500, distance : 200, degree : 90},{ appTime : 35500, obj : masterObjs_31, rpps : 1500, distance : 200, degree : 270},{ appTime : 35500, obj : masterObjs_30, rpps : 1500, distance : 300, degree : 90},{ appTime : 35500, obj : masterObjs_31, rpps : 1500, distance : 300, degree : 270},{ appTime : 36000, obj : masterObjs_30, rpps : 1500, distance : 100, degree : 90},{ appTime : 36000, obj : masterObjs_31, rpps : 1500, distance : 100, degree : 270},{ appTime : 36000, obj : masterObjs_30, rpps : 1500, distance : 200, degree : 90},{ appTime : 36000, obj : masterObjs_31, rpps : 1500, distance : 200, degree : 270},{ appTime : 36000, obj : masterObjs_30, rpps : 1500, distance : 300, degree : 90},{ appTime : 36000, obj : masterObjs_31, rpps : 1500, distance : 300, degree : 270},{ appTime : 36500, obj : masterObjs_30, rpps : 1500, distance : 100, degree : 90},{ appTime : 36500, obj : masterObjs_31, rpps : 1500, distance : 100, degree : 270},{ appTime : 36500, obj : masterObjs_30, rpps : 1500, distance : 200, degree : 90},{ appTime : 36500, obj : masterObjs_31, rpps : 1500, distance : 200, degree : 270},{ appTime : 36500, obj : masterObjs_30, rpps : 1500, distance : 300, degree : 90},{ appTime : 36500, obj : masterObjs_31, rpps : 1500, distance : 300, degree : 270},{ appTime : 33000, obj : masterObjs_30, rpps : 1500, distance : 100, degree : 90, positionY : 200},{ appTime : 33000, obj : masterObjs_31, rpps : 1500, distance : 100, degree : 270, positionY : 200},{ appTime : 33000, obj : masterObjs_30, rpps : 1500, distance : 200, degree : 90, positionY : 200},{ appTime : 33000, obj : masterObjs_31, rpps : 1500, distance : 200, degree : 270, positionY : 200},{ appTime : 33000, obj : masterObjs_30, rpps : 1500, distance : 300, degree : 90, positionY : 200},{ appTime : 33000, obj : masterObjs_31, rpps : 1500, distance : 300, degree : 270, positionY : 200},{ appTime : 33500, obj : masterObjs_30, rpps : 1500, distance : 100, degree : 90, positionY : 200},{ appTime : 33500, obj : masterObjs_31, rpps : 1500, distance : 100, degree : 270, positionY : 200},{ appTime : 33500, obj : masterObjs_30, rpps : 1500, distance : 200, degree : 90, positionY : 200},{ appTime : 33500, obj : masterObjs_31, rpps : 1500, distance : 200, degree : 270, positionY : 200},{ appTime : 33500, obj : masterObjs_30, rpps : 1500, distance : 300, degree : 90, positionY : 200},{ appTime : 33500, obj : masterObjs_31, rpps : 1500, distance : 300, degree : 270, positionY : 200},{ appTime : 34000, obj : masterObjs_30, rpps : 1500, distance : 100, degree : 90, positionY : 200},{ appTime : 34000, obj : masterObjs_31, rpps : 1500, distance : 100, degree : 270, positionY : 200},{ appTime : 34000, obj : masterObjs_30, rpps : 1500, distance : 200, degree : 90, positionY : 200},{ appTime : 34000, obj : masterObjs_31, rpps : 1500, distance : 200, degree : 270, positionY : 200},{ appTime : 34000, obj : masterObjs_30, rpps : 1500, distance : 300, degree : 90, positionY : 200},{ appTime : 34000, obj : masterObjs_31, rpps : 1500, distance : 300, degree : 270, positionY : 200},{ appTime : 34500, obj : masterObjs_30, rpps : 1500, distance : 100, degree : 90, positionY : 200},{ appTime : 34500, obj : masterObjs_31, rpps : 1500, distance : 100, degree : 270, positionY : 200},{ appTime : 34500, obj : masterObjs_30, rpps : 1500, distance : 200, degree : 90, positionY : 200},{ appTime : 34500, obj : masterObjs_31, rpps : 1500, distance : 200, degree : 270, positionY : 200},{ appTime : 34500, obj : masterObjs_30, rpps : 1500, distance : 300, degree : 90, positionY : 200},{ appTime : 34500, obj : masterObjs_31, rpps : 1500, distance : 300, degree : 270, positionY : 200},{ appTime : 35000, obj : masterObjs_30, rpps : 1500, distance : 100, degree : 90, positionY : 200},{ appTime : 35000, obj : masterObjs_31, rpps : 1500, distance : 100, degree : 270, positionY : 200},{ appTime : 35000, obj : masterObjs_30, rpps : 1500, distance : 200, degree : 90, positionY : 200},{ appTime : 35000, obj : masterObjs_31, rpps : 1500, distance : 200, degree : 270, positionY : 200},{ appTime : 35000, obj : masterObjs_30, rpps : 1500, distance : 300, degree : 90, positionY : 200},{ appTime : 35000, obj : masterObjs_31, rpps : 1500, distance : 300, degree : 270, positionY : 200},{ appTime : 35500, obj : masterObjs_30, rpps : 1500, distance : 100, degree : 90, positionY : 200},{ appTime : 35500, obj : masterObjs_31, rpps : 1500, distance : 100, degree : 270, positionY : 200},{ appTime : 35500, obj : masterObjs_30, rpps : 1500, distance : 200, degree : 90, positionY : 200},{ appTime : 35500, obj : masterObjs_31, rpps : 1500, distance : 200, degree : 270, positionY : 200},{ appTime : 35500, obj : masterObjs_30, rpps : 1500, distance : 300, degree : 90, positionY : 200},{ appTime : 35500, obj : masterObjs_31, rpps : 1500, distance : 300, degree : 270, positionY : 200},{ appTime : 36000, obj : masterObjs_30, rpps : 1500, distance : 100, degree : 90, positionY : 200},{ appTime : 36000, obj : masterObjs_31, rpps : 1500, distance : 100, degree : 270, positionY : 200},{ appTime : 36000, obj : masterObjs_30, rpps : 1500, distance : 200, degree : 90, positionY : 200},{ appTime : 36000, obj : masterObjs_31, rpps : 1500, distance : 200, degree : 270, positionY : 200},{ appTime : 36000, obj : masterObjs_30, rpps : 1500, distance : 300, degree : 90, positionY : 200},{ appTime : 36000, obj : masterObjs_31, rpps : 1500, distance : 300, degree : 270, positionY : 200},{ appTime : 36500, obj : masterObjs_30, rpps : 1500, distance : 100, degree : 90, positionY : 200},{ appTime : 36500, obj : masterObjs_31, rpps : 1500, distance : 100, degree : 270, positionY : 200},{ appTime : 36500, obj : masterObjs_30, rpps : 1500, distance : 200, degree : 90, positionY : 200},{ appTime : 36500, obj : masterObjs_31, rpps : 1500, distance : 200, degree : 270, positionY : 200},{ appTime : 36500, obj : masterObjs_30, rpps : 1500, distance : 300, degree : 90, positionY : 200},{ appTime : 36500, obj : masterObjs_31, rpps : 1500, distance : 300, degree : 270, positionY : 200},{ appTime : 33000, obj : masterObjs_30, rpps : 1500, distance : 100, degree : 90, positionY : 400},{ appTime : 33000, obj : masterObjs_31, rpps : 1500, distance : 100, degree : 270, positionY : 400},{ appTime : 33000, obj : masterObjs_30, rpps : 1500, distance : 200, degree : 90, positionY : 400},{ appTime : 33000, obj : masterObjs_31, rpps : 1500, distance : 200, degree : 270, positionY : 400},{ appTime : 33000, obj : masterObjs_30, rpps : 1500, distance : 300, degree : 90, positionY : 400},{ appTime : 33000, obj : masterObjs_31, rpps : 1500, distance : 300, degree : 270, positionY : 400},{ appTime : 33500, obj : masterObjs_30, rpps : 1500, distance : 100, degree : 90, positionY : 400},{ appTime : 33500, obj : masterObjs_31, rpps : 1500, distance : 100, degree : 270, positionY : 400},{ appTime : 33500, obj : masterObjs_30, rpps : 1500, distance : 200, degree : 90, positionY : 400},{ appTime : 33500, obj : masterObjs_31, rpps : 1500, distance : 200, degree : 270, positionY : 400},{ appTime : 33500, obj : masterObjs_30, rpps : 1500, distance : 300, degree : 90, positionY : 400},{ appTime : 33500, obj : masterObjs_31, rpps : 1500, distance : 300, degree : 270, positionY : 400},{ appTime : 34000, obj : masterObjs_30, rpps : 1500, distance : 100, degree : 90, positionY : 400},{ appTime : 34000, obj : masterObjs_31, rpps : 1500, distance : 100, degree : 270, positionY : 400},{ appTime : 34000, obj : masterObjs_30, rpps : 1500, distance : 200, degree : 90, positionY : 400},{ appTime : 34000, obj : masterObjs_31, rpps : 1500, distance : 200, degree : 270, positionY : 400},{ appTime : 34000, obj : masterObjs_30, rpps : 1500, distance : 300, degree : 90, positionY : 400},{ appTime : 34000, obj : masterObjs_31, rpps : 1500, distance : 300, degree : 270, positionY : 400},{ appTime : 34500, obj : masterObjs_30, rpps : 1500, distance : 100, degree : 90, positionY : 400},{ appTime : 34500, obj : masterObjs_31, rpps : 1500, distance : 100, degree : 270, positionY : 400},{ appTime : 34500, obj : masterObjs_30, rpps : 1500, distance : 200, degree : 90, positionY : 400},{ appTime : 34500, obj : masterObjs_31, rpps : 1500, distance : 200, degree : 270, positionY : 400},{ appTime : 34500, obj : masterObjs_30, rpps : 1500, distance : 300, degree : 90, positionY : 400},{ appTime : 34500, obj : masterObjs_31, rpps : 1500, distance : 300, degree : 270, positionY : 400},{ appTime : 35000, obj : masterObjs_30, rpps : 1500, distance : 100, degree : 90, positionY : 400},{ appTime : 35000, obj : masterObjs_31, rpps : 1500, distance : 100, degree : 270, positionY : 400},{ appTime : 35000, obj : masterObjs_30, rpps : 1500, distance : 200, degree : 90, positionY : 400},{ appTime : 35000, obj : masterObjs_31, rpps : 1500, distance : 200, degree : 270, positionY : 400},{ appTime : 35000, obj : masterObjs_30, rpps : 1500, distance : 300, degree : 90, positionY : 400},{ appTime : 35000, obj : masterObjs_31, rpps : 1500, distance : 300, degree : 270, positionY : 400},{ appTime : 35500, obj : masterObjs_30, rpps : 1500, distance : 100, degree : 90, positionY : 400},{ appTime : 35500, obj : masterObjs_31, rpps : 1500, distance : 100, degree : 270, positionY : 400},{ appTime : 35500, obj : masterObjs_30, rpps : 1500, distance : 200, degree : 90, positionY : 400},{ appTime : 35500, obj : masterObjs_31, rpps : 1500, distance : 200, degree : 270, positionY : 400},{ appTime : 35500, obj : masterObjs_30, rpps : 1500, distance : 300, degree : 90, positionY : 400},{ appTime : 35500, obj : masterObjs_31, rpps : 1500, distance : 300, degree : 270, positionY : 400},{ appTime : 36000, obj : masterObjs_30, rpps : 1500, distance : 100, degree : 90, positionY : 400},{ appTime : 36000, obj : masterObjs_31, rpps : 1500, distance : 100, degree : 270, positionY : 400},{ appTime : 36000, obj : masterObjs_30, rpps : 1500, distance : 200, degree : 90, positionY : 400},{ appTime : 36000, obj : masterObjs_31, rpps : 1500, distance : 200, degree : 270, positionY : 400},{ appTime : 36000, obj : masterObjs_30, rpps : 1500, distance : 300, degree : 90, positionY : 400},{ appTime : 36000, obj : masterObjs_31, rpps : 1500, distance : 300, degree : 270, positionY : 400},{ appTime : 36500, obj : masterObjs_30, rpps : 1500, distance : 100, degree : 90, positionY : 400},{ appTime : 36500, obj : masterObjs_31, rpps : 1500, distance : 100, degree : 270, positionY : 400},{ appTime : 36500, obj : masterObjs_30, rpps : 1500, distance : 200, degree : 90, positionY : 400},{ appTime : 36500, obj : masterObjs_31, rpps : 1500, distance : 200, degree : 270, positionY : 400},{ appTime : 36500, obj : masterObjs_30, rpps : 1500, distance : 300, degree : 90, positionY : 400},{ appTime : 36500, obj : masterObjs_31, rpps : 1500, distance : 300, degree : 270, positionY : 400},{ appTime : 41500, obj : masterObjs_32, rpps : 500, positionY : 800},{ appTime : 41500, obj : masterObjs_33, rpps : 1000, positionY : 800},{ appTime : 41500, obj : masterObjs_34, rpps : 1500, positionY : 800},{ appTime : 41500, obj : masterObjs_35, rpps : 2000, positionY : 800},{ appTime : 42000, obj : masterObjs_32, rpps : 500, positionY : 800},{ appTime : 42000, obj : masterObjs_33, rpps : 1000, positionY : 800},{ appTime : 42000, obj : masterObjs_34, rpps : 1500, positionY : 800},{ appTime : 42000, obj : masterObjs_35, rpps : 2000, positionY : 800},{ appTime : 42500, obj : masterObjs_35, rpps : 500, positionY : 800},{ appTime : 42500, obj : masterObjs_34, rpps : 1000, positionY : 800},{ appTime : 42500, obj : masterObjs_33, rpps : 1500, positionY : 800},{ appTime : 42500, obj : masterObjs_32, rpps : 2000, positionY : 800},{ appTime : 43000, obj : masterObjs_35, rpps : 500, positionY : 800},{ appTime : 43000, obj : masterObjs_34, rpps : 1000, positionY : 800},{ appTime : 43000, obj : masterObjs_32, rpps : 1500, positionY : 800},{ appTime : 43000, obj : masterObjs_33, rpps : 2000, positionY : 800},{ appTime : 43500, obj : masterObjs_34, rpps : 500, positionY : 800},{ appTime : 43500, obj : masterObjs_35, rpps : 1000, positionY : 800},{ appTime : 43500, obj : masterObjs_33, rpps : 1500, positionY : 800},{ appTime : 43500, obj : masterObjs_32, rpps : 2000, positionY : 800},{ appTime : 44000, obj : masterObjs_33, rpps : 500, positionY : 800},{ appTime : 44000, obj : masterObjs_35, rpps : 1000, positionY : 800},{ appTime : 44000, obj : masterObjs_32, rpps : 1500, positionY : 800},{ appTime : 44000, obj : masterObjs_34, rpps : 2000, positionY : 800},{ appTime : 47000, obj : masterObjs_2, positionY : 200},{ appTime : 47000, obj : masterObjs_2, positionY : 400},{ appTime : 47000, obj : masterObjs_2, positionY : 600},{ appTime : 47000, obj : masterObjs_2, positionY : 200, distance : 200, degree : 90},{ appTime : 47000, obj : masterObjs_2, positionY : 400, distance : 200, degree : 90},{ appTime : 47000, obj : masterObjs_2, positionY : 600, distance : 200, degree : 90},{ appTime : 47000, obj : masterObjs_2, positionY : 200, distance : 200, degree : 270},{ appTime : 47000, obj : masterObjs_2, positionY : 400, distance : 200, degree : 270},{ appTime : 47000, obj : masterObjs_2, positionY : 600, distance : 200, degree : 270},{ appTime : 50000, obj : masterObjs_1, distance : 100, degree : 270, rpps : 1300},{ appTime : 50000, obj : masterObjs_1, positionY : 150, distance : 100, degree : 270, rpps : 1300},{ appTime : 50000, obj : masterObjs_1, positionY : 300, distance : 100, degree : 270, rpps : 1300},{ appTime : 50000, obj : masterObjs_1, distance : 200, degree : 270, rpps : 1300},{ appTime : 50000, obj : masterObjs_1, positionY : 150, distance : 200, degree : 270, rpps : 1300},{ appTime : 50000, obj : masterObjs_1, positionY : 300, distance : 200, degree : 270, rpps : 1300},{ appTime : 50000, obj : masterObjs_1, distance : 100, degree : 90, rpps : 1300},{ appTime : 50000, obj : masterObjs_1, positionY : 150, distance : 100, degree : 90, rpps : 1300},{ appTime : 50000, obj : masterObjs_1, positionY : 300, distance : 100, degree : 90, rpps : 1300},{ appTime : 50000, obj : masterObjs_1, distance : 200, degree : 90, rpps : 1300},{ appTime : 50000, obj : masterObjs_1, positionY : 150, distance : 200, degree : 90, rpps : 1300},{ appTime : 50000, obj : masterObjs_1, positionY : 300, distance : 200, degree : 90, rpps : 1300},{ appTime : 50000, obj : masterObjs_1, rpps : 1300},{ appTime : 50000, obj : masterObjs_1, positionY : 150, rpps : 1300},{ appTime : 50000, obj : masterObjs_1, positionY : 300, rpps : 1300},{ appTime : 50000, obj : masterObjs_1, positionY : 300, distance : 100, degree : 90, rpps : 1300},{ appTime : 50500, obj : masterObjs_5, rpps : 1000},{ appTime : 50500, obj : masterObjs_5, distance : 100, degree : 90, rpps : 1000},{ appTime : 50500, obj : masterObjs_5, distance : 100, degree : 270, rpps : 1000},{ appTime : 50500, obj : masterObjs_5, distance : 200, degree : 90, rpps : 1000},{ appTime : 50500, obj : masterObjs_5, distance : 200, degree : 270, rpps : 1000},{ appTime : 51000, obj : masterObjs_5, positionY : 150, rpps : 500},{ appTime : 51000, obj : masterObjs_5, distance : 100, degree : 90, positionY : 150, rpps : 500},{ appTime : 51000, obj : masterObjs_5, distance : 100, degree : 270, positionY : 150, rpps : 500},{ appTime : 51000, obj : masterObjs_5, distance : 200, degree : 90, positionY : 150, rpps : 500},{ appTime : 51000, obj : masterObjs_5, distance : 200, degree : 270, positionY : 150, rpps : 500},{ appTime : 51500, obj : masterObjs_5, positionY : 300},{ appTime : 51500, obj : masterObjs_5, distance : 100, degree : 90, positionY : 300},{ appTime : 51500, obj : masterObjs_5, distance : 100, degree : 270, positionY : 300},{ appTime : 51500, obj : masterObjs_5, distance : 200, degree : 90, positionY : 300},{ appTime : 51500, obj : masterObjs_5, distance : 200, degree : 270, positionY : 300},{ appTime : 53000, obj : masterObjs_7, distance : 500, degree : 270},{ appTime : 53000, obj : masterObjs_7, distance : 500, degree : 90},{ appTime : 53000, obj : masterObjs_7, distance : 1000, degree : 270},{ appTime : 53000, obj : masterObjs_7, distance : 1000, degree : 90},{ appTime : 53000, obj : masterObjs_7},{ appTime : 53500, obj : masterObjs_8, distance : 250, degree : 270},{ appTime : 53500, obj : masterObjs_8, distance : 250, degree : 90},{ appTime : 53500, obj : masterObjs_8, distance : 750, degree : 270},{ appTime : 53500, obj : masterObjs_8, distance : 750, degree : 90},{ appTime : 53500, obj : masterObjs_8, distance : 1250, degree : 270},{ appTime : 53500, obj : masterObjs_8, distance : 1250, degree : 90},{ appTime : 54000, obj : masterObjs_9, distance : 500, degree : 270},{ appTime : 54000, obj : masterObjs_9, distance : 500, degree : 90},{ appTime : 54000, obj : masterObjs_9, distance : 1000, degree : 270},{ appTime : 54000, obj : masterObjs_9, distance : 1000, degree : 90},{ appTime : 54000, obj : masterObjs_9, distance : 1500, degree : 270},{ appTime : 54000, obj : masterObjs_9, distance : 1500, degree : 90},{ appTime : 54000, obj : masterObjs_9},{ appTime : 56000, obj : masterObjs_38},{ appTime : 56000, obj : masterObjs_38, distance : 100, degree : 270},{ appTime : 56000, obj : masterObjs_38, distance : 100, degree : 90},{ appTime : 56000, obj : masterObjs_38, distance : 200, degree : 270},{ appTime : 56000, obj : masterObjs_38, distance : 200, degree : 90},{ appTime : 56000, obj : masterObjs_38, positionY : 150},{ appTime : 56000, obj : masterObjs_38, distance : 100, degree : 270, positionY : 150},{ appTime : 56000, obj : masterObjs_38, distance : 100, degree : 90, positionY : 150},{ appTime : 56000, obj : masterObjs_38, distance : 200, degree : 270, positionY : 150},{ appTime : 56000, obj : masterObjs_38, distance : 200, degree : 90, positionY : 150},{ appTime : 56000, obj : masterObjs_38, positionY : 300},{ appTime : 56000, obj : masterObjs_38, distance : 100, degree : 270, positionY : 300},{ appTime : 56000, obj : masterObjs_38, distance : 100, degree : 90, positionY : 300},{ appTime : 56000, obj : masterObjs_38, distance : 200, degree : 270, positionY : 300},{ appTime : 56000, obj : masterObjs_38, distance : 200, degree : 90, positionY : 300},{ appTime : 56500, obj : masterObjs_38},{ appTime : 56500, obj : masterObjs_38, distance : 100, degree : 270},{ appTime : 56500, obj : masterObjs_38, distance : 100, degree : 90},{ appTime : 56500, obj : masterObjs_38, distance : 200, degree : 270},{ appTime : 56500, obj : masterObjs_38, distance : 200, degree : 90},{ appTime : 56500, obj : masterObjs_38, positionY : 150},{ appTime : 56500, obj : masterObjs_38, distance : 100, degree : 270, positionY : 150},{ appTime : 56500, obj : masterObjs_38, distance : 100, degree : 90, positionY : 150},{ appTime : 56500, obj : masterObjs_38, distance : 200, degree : 270, positionY : 150},{ appTime : 56500, obj : masterObjs_38, distance : 200, degree : 90, positionY : 150},{ appTime : 56500, obj : masterObjs_38, positionY : 300},{ appTime : 56500, obj : masterObjs_38, distance : 100, degree : 270, positionY : 300},{ appTime : 56500, obj : masterObjs_38, distance : 100, degree : 90, positionY : 300},{ appTime : 56500, obj : masterObjs_38, distance : 200, degree : 270, positionY : 300},{ appTime : 56500, obj : masterObjs_38, distance : 200, degree : 90, positionY : 300},{ appTime : 57000, obj : masterObjs_38},{ appTime : 57000, obj : masterObjs_38, distance : 100, degree : 270},{ appTime : 57000, obj : masterObjs_38, distance : 100, degree : 90},{ appTime : 57000, obj : masterObjs_38, distance : 200, degree : 270},{ appTime : 57000, obj : masterObjs_38, distance : 200, degree : 90},{ appTime : 57000, obj : masterObjs_38, positionY : 150},{ appTime : 57000, obj : masterObjs_38, distance : 100, degree : 270, positionY : 150},{ appTime : 57000, obj : masterObjs_38, distance : 100, degree : 90, positionY : 150},{ appTime : 57000, obj : masterObjs_38, distance : 200, degree : 270, positionY : 150},{ appTime : 57000, obj : masterObjs_38, distance : 200, degree : 90, positionY : 150},{ appTime : 57000, obj : masterObjs_38, positionY : 300},{ appTime : 57000, obj : masterObjs_38, distance : 100, degree : 270, positionY : 300},{ appTime : 57000, obj : masterObjs_38, distance : 100, degree : 90, positionY : 300},{ appTime : 57000, obj : masterObjs_38, distance : 200, degree : 270, positionY : 300},{ appTime : 57000, obj : masterObjs_38, distance : 200, degree : 90, positionY : 300},{ appTime : 57500, obj : masterObjs_38},{ appTime : 57500, obj : masterObjs_38, distance : 100, degree : 270},{ appTime : 57500, obj : masterObjs_38, distance : 100, degree : 90},{ appTime : 57500, obj : masterObjs_38, distance : 200, degree : 270},{ appTime : 57500, obj : masterObjs_38, distance : 200, degree : 90},{ appTime : 57500, obj : masterObjs_38, positionY : 150},{ appTime : 57500, obj : masterObjs_38, distance : 100, degree : 270, positionY : 150},{ appTime : 57500, obj : masterObjs_38, distance : 100, degree : 90, positionY : 150},{ appTime : 57500, obj : masterObjs_38, distance : 200, degree : 270, positionY : 150},{ appTime : 57500, obj : masterObjs_38, distance : 200, degree : 90, positionY : 150},{ appTime : 57500, obj : masterObjs_38, positionY : 300},{ appTime : 57500, obj : masterObjs_38, distance : 100, degree : 270, positionY : 300},{ appTime : 57500, obj : masterObjs_38, distance : 100, degree : 90, positionY : 300},{ appTime : 57500, obj : masterObjs_38, distance : 200, degree : 270, positionY : 300},{ appTime : 57500, obj : masterObjs_38, distance : 200, degree : 90, positionY : 300},{ appTime : 58000, obj : masterObjs_38},{ appTime : 58000, obj : masterObjs_38, distance : 100, degree : 270},{ appTime : 58000, obj : masterObjs_38, distance : 100, degree : 90},{ appTime : 58000, obj : masterObjs_38, distance : 200, degree : 270},{ appTime : 58000, obj : masterObjs_38, distance : 200, degree : 90},{ appTime : 58000, obj : masterObjs_38, positionY : 150},{ appTime : 58000, obj : masterObjs_38, distance : 100, degree : 270, positionY : 150},{ appTime : 58000, obj : masterObjs_38, distance : 100, degree : 90, positionY : 150},{ appTime : 58000, obj : masterObjs_38, distance : 200, degree : 270, positionY : 150},{ appTime : 58000, obj : masterObjs_38, distance : 200, degree : 90, positionY : 150},{ appTime : 58000, obj : masterObjs_38, positionY : 300},{ appTime : 58000, obj : masterObjs_38, distance : 100, degree : 270, positionY : 300},{ appTime : 58000, obj : masterObjs_38, distance : 100, degree : 90, positionY : 300},{ appTime : 58000, obj : masterObjs_38, distance : 200, degree : 270, positionY : 300},{ appTime : 58000, obj : masterObjs_38, distance : 200, degree : 90, positionY : 300},{ appTime : 59000, obj : masterObjs_5},{ appTime : 59000, obj : masterObjs_5, distance : 100, degree : 270},{ appTime : 59000, obj : masterObjs_5, distance : 100, degree : 90},{ appTime : 59000, obj : masterObjs_5, distance : 200, degree : 270},{ appTime : 59000, obj : masterObjs_5, distance : 200, degree : 90},{ appTime : 59000, obj : masterObjs_5, positionY : 150},{ appTime : 59000, obj : masterObjs_5, distance : 100, degree : 270, positionY : 150},{ appTime : 59000, obj : masterObjs_5, distance : 100, degree : 90, positionY : 150},{ appTime : 59000, obj : masterObjs_5, distance : 200, degree : 270, positionY : 150},{ appTime : 59000, obj : masterObjs_5, distance : 200, degree : 90, positionY : 150},{ appTime : 59000, obj : masterObjs_5, positionY : 300},{ appTime : 59000, obj : masterObjs_5, distance : 100, degree : 270, positionY : 300},{ appTime : 59000, obj : masterObjs_5, distance : 100, degree : 90, positionY : 300},{ appTime : 59000, obj : masterObjs_5, distance : 200, degree : 270, positionY : 300},{ appTime : 59000, obj : masterObjs_5, distance : 200, degree : 90, positionY : 300}];
		this.setTimeObject(objs);
		this.stageId = 3;
		return true;
	}
	,init: function(args) {
		scene_abs_AbstractMoveRouteScene.prototype.init.call(this,args);
	}
	,update: function(time) {
		if(this.first) time = Math.min(time,1000);
		scene_abs_AbstractMoveRouteScene.prototype.update.call(this,time);
	}
	,__class__: scene_HardGameScene
});
var scene_LoadScene = function(context) {
	this.str = "読み込み中";
	this.loadScene = 0;
	this.maxScene = 0;
	scene_abs_AbstractFixedScene.call(this,context);
};
$hxClasses["scene.LoadScene"] = scene_LoadScene;
scene_LoadScene.__name__ = true;
scene_LoadScene.__super__ = scene_abs_AbstractFixedScene;
scene_LoadScene.prototype = $extend(scene_abs_AbstractFixedScene.prototype,{
	once: function() {
		if(!scene_abs_AbstractFixedScene.prototype.once.call(this)) return false;
		this.audio.appendBGM("title",GameMgr.DIRECTORY + "audios/title4.mp3");
		this.audio.appendBGM("normal",GameMgr.DIRECTORY + "audios/normal.mp3");
		this.audio.appendBGM("easy",GameMgr.DIRECTORY + "audios/easy.mp3");
		this.audio.appendBGM("hard",GameMgr.DIRECTORY + "audios/hard.mp3");
		this.audio.appendBGM("result",GameMgr.DIRECTORY + "audios/result.mp3");
		this.audio.appendSE("entry",GameMgr.DIRECTORY + "se/entry.mp3");
		this.audio.appendSE("change",GameMgr.DIRECTORY + "se/change.mp3");
		this.audio.appendSE("ready",GameMgr.DIRECTORY + "se/ready.mp3");
		this.audio.appendSE("start",GameMgr.DIRECTORY + "se/start.mp3");
		this.audio.appendSE("break",GameMgr.DIRECTORY + "se/break.mp3");
		this.audio.appendSE("block",GameMgr.DIRECTORY + "se/block.mp3");
		return true;
	}
	,init: function(args) {
		scene_abs_AbstractFixedScene.prototype.init.call(this,args);
		this.maxScene = args[0];
	}
	,update: function(time) {
		this.context.beginPath();
		this.context.font = GameMgr.WIDTH / 24 + "px \"Times New Roman\"";
		this.context.fillStyle = "rgb(0, 0, 0)";
		this.context.textAlign = "center";
		this.context.fillText(this.str,GameMgr.WIDTH / 2,GameMgr.HEIGHT / 2);
		this.context.closePath();
	}
	,__class__: scene_LoadScene
});
var scene_NormalGameScene = function(context) {
	scene_abs_AbstractMoveRouteScene.call(this,context,null,1000);
	this.audioName = "normal";
};
$hxClasses["scene.NormalGameScene"] = scene_NormalGameScene;
scene_NormalGameScene.__name__ = true;
scene_NormalGameScene.__super__ = scene_abs_AbstractMoveRouteScene;
scene_NormalGameScene.prototype = $extend(scene_abs_AbstractMoveRouteScene.prototype,{
	once: function() {
		if(!scene_abs_AbstractMoveRouteScene.prototype.once.call(this)) return false;
		this.setGroudTexture(GameMgr.DIRECTORY + "images/schnee-texture.jpg",128);
		var easy_routes = [{ time : 10000, point : new THREE.Vector3(0,0,4000)},{ time : 10000, point : new THREE.Vector3(-4000,0,4000), degree : 90},{ time : 10000, point : new THREE.Vector3(-4000,0,8000)},{ time : 10000, point : new THREE.Vector3(-8000,0,8000), degree : 90},{ time : 10000, point : new THREE.Vector3(-8000,0,12000)},{ time : 10000, point : new THREE.Vector3(-12000,0,12000), degree : 90}];
		this.setRouteData(easy_routes);
		var roteRightAction = new action_RotateAction({ right : true});
		var jumpAction = new action_JumpAction({ waitTime : 0});
		var randamRoteAction = new action_RotateRandomAction();
		var moveXAction = new action_MoveAction({ moveType : action_MoveType.x});
		var moveXRightAction = new action_MoveAction({ reverse : true, moveType : action_MoveType.x});
		var moveZAction = new action_MoveAction({ moveType : action_MoveType.z});
		var crystalColorShape = new shape_CrystalShape(null,{ color : 16733525});
		var crystalBlackShape = new shape_CrystalShape(null,{ color : 0});
		var cylinderShape = new shape_CylinderShape();
		var cubeShape = new shape_CubeShape();
		var drumShape = new shape_DrumShape();
		var chestShape = new shape_ChestShape();
		var arrowShape = new shape_ArrowShape();
		var masterObjs_0 = new obj3d_BreakObject(crystalColorShape,[moveXAction]);
		var masterObjs_1 = new obj3d_BreakObject(crystalBlackShape,[roteRightAction]);
		var masterObjs_2 = new obj3d_BreakObject(cubeShape,[randamRoteAction]);
		var masterObjs_3 = new obj3d_BreakObject(cylinderShape,[jumpAction,roteRightAction]);
		var masterObjs_4 = new obj3d_BreakObject(cylinderShape,[roteRightAction]);
		var masterObjs_5 = new obj3d_BreakObject(cylinderShape,[new action_MoveAction({ speed : 20, moveType : action_MoveType.x}),roteRightAction]);
		var masterObjs_6 = new obj3d_InvincibleObject(drumShape);
		var masterObjs_7 = new obj3d_BreakObject(cylinderShape,[new action_MoveAction({ moveType : action_MoveType.x}),jumpAction,roteRightAction]);
		var masterObjs_8 = new obj3d_BreakObject(cylinderShape,[new action_JumpAction({ gravity : 0.2, initialVelocity : 10, waitTime : 0}),roteRightAction]);
		var masterObjs_9 = new obj3d_InvincibleObject(chestShape);
		var masterObjs_10 = new obj3d_InvincibleObject(new shape_ChestShape(null,{ Xsize : 200, Ysize : 200, Zsize : 50}));
		var masterObjs_11 = new obj3d_BreakObject(cylinderShape,[new action_JumpAction({ gravity : 0.1, initialVelocity : 10, waitTime : 0, up : true}),roteRightAction]);
		var easy_objs = [{ obj : masterObjs_4, appTime : 0, distance : 1000, degree : 355, positionY : 200},{ obj : masterObjs_4, appTime : 0, distance : 1000, degree : 5, positionY : 200},{ obj : masterObjs_4, appTime : 500, distance : 1100, degree : 345, positionY : 400},{ obj : masterObjs_4, appTime : 500, distance : 1100, degree : 15, positionY : 400},{ obj : masterObjs_4, appTime : 1000, distance : 1200, degree : 330, positionY : 600},{ obj : masterObjs_4, appTime : 1000, distance : 1200, degree : 30, positionY : 600},{ obj : masterObjs_4, appTime : 1500, distance : 1300, degree : 330, positionY : 100},{ obj : masterObjs_4, appTime : 1500, distance : 1300, degree : 30, positionY : 100},{ obj : masterObjs_4, appTime : 2000, distance : 1400, degree : 345, positionY : 300},{ obj : masterObjs_4, appTime : 2000, distance : 1400, degree : 15, positionY : 300},{ obj : masterObjs_4, appTime : 2500, distance : 1500, degree : 345, positionY : 200},{ obj : masterObjs_4, appTime : 2500, distance : 1500, degree : 15, positionY : 200},{ obj : masterObjs_4, appTime : 3000, distance : 1600, degree : 330, positionY : 200},{ obj : masterObjs_4, appTime : 3000, distance : 1600, degree : 30, positionY : 200},{ obj : masterObjs_4, appTime : 3000, distance : 1600, degree : 355, positionY : 300},{ obj : masterObjs_4, appTime : 3000, distance : 1600, degree : 5, positionY : 300},{ obj : masterObjs_4, appTime : 3500, distance : 1700, degree : 330, positionY : 400},{ obj : masterObjs_4, appTime : 3500, distance : 1700, degree : 30, positionY : 400},{ obj : masterObjs_4, appTime : 4000, distance : 1800, degree : 345, positionY : 100},{ obj : masterObjs_4, appTime : 4000, distance : 1800, degree : 15, positionY : 100},{ obj : masterObjs_4, appTime : 4500, distance : 1900, degree : 345, positionY : 600},{ obj : masterObjs_4, appTime : 4500, distance : 1900, degree : 15, positionY : 600},{ obj : masterObjs_4, appTime : 5000, distance : 2000, degree : 330, positionY : 400},{ obj : masterObjs_4, appTime : 5000, distance : 2000, degree : 30, positionY : 400},{ obj : masterObjs_4, appTime : 5500, distance : 2100, degree : 355, positionY : 500},{ obj : masterObjs_4, appTime : 5500, distance : 2100, degree : 5, positionY : 500},{ obj : masterObjs_4, appTime : 10000, rpps : 3000, distance : -200, degree : 0, positionY : 500},{ obj : masterObjs_4, appTime : 10000, rpps : 3500, distance : -200, degree : 0, positionY : 500},{ obj : masterObjs_4, appTime : 10000, rpps : 4000, distance : -200, degree : 0, positionY : 500},{ obj : masterObjs_4, appTime : 10000, rpps : 4500, distance : -200, degree : 0, positionY : 500},{ obj : masterObjs_4, appTime : 10000, rpps : 5000, distance : -200, degree : 0, positionY : 500},{ obj : masterObjs_4, appTime : 10000, rpps : 5500, distance : -200, degree : 0, positionY : 500},{ obj : masterObjs_4, appTime : 10000, rpps : 6000, distance : -200, degree : 0, positionY : 500},{ obj : masterObjs_4, appTime : 10000, rpps : 6500, distance : -200, degree : 0, positionY : 500},{ obj : masterObjs_4, appTime : 10000, rpps : 7000, distance : -200, degree : 0, positionY : 500},{ obj : masterObjs_4, appTime : 10000, rpps : 7500, distance : -200, degree : 0, positionY : 500},{ obj : masterObjs_4, appTime : 10000, rpps : 3000, distance : -400, degree : 0, positionY : 250},{ obj : masterObjs_4, appTime : 10000, rpps : 3500, distance : -400, degree : 0, positionY : 250},{ obj : masterObjs_4, appTime : 10000, rpps : 4000, distance : -400, degree : 0, positionY : 250},{ obj : masterObjs_4, appTime : 10000, rpps : 4500, distance : -400, degree : 0, positionY : 250},{ obj : masterObjs_4, appTime : 10000, rpps : 5000, distance : -400, degree : 0, positionY : 250},{ obj : masterObjs_4, appTime : 10000, rpps : 5500, distance : -400, degree : 0, positionY : 250},{ obj : masterObjs_4, appTime : 10000, rpps : 6000, distance : -400, degree : 0, positionY : 250},{ obj : masterObjs_4, appTime : 10000, rpps : 6500, distance : -400, degree : 0, positionY : 250},{ obj : masterObjs_4, appTime : 10000, rpps : 7000, distance : -400, degree : 0, positionY : 250},{ obj : masterObjs_4, appTime : 10000, rpps : 7500, distance : -400, degree : 0, positionY : 250},{ obj : masterObjs_4, appTime : 10000, rpps : 3000, distance : -600, degree : 0},{ obj : masterObjs_4, appTime : 10000, rpps : 3500, distance : -600, degree : 0},{ obj : masterObjs_4, appTime : 10000, rpps : 4000, distance : -600, degree : 0},{ obj : masterObjs_4, appTime : 10000, rpps : 4500, distance : -600, degree : 0},{ obj : masterObjs_4, appTime : 10000, rpps : 5000, distance : -600, degree : 0},{ obj : masterObjs_4, appTime : 10000, rpps : 5500, distance : -600, degree : 0},{ obj : masterObjs_4, appTime : 10000, rpps : 6000, distance : -600, degree : 0},{ obj : masterObjs_4, appTime : 10000, rpps : 6500, distance : -600, degree : 0},{ obj : masterObjs_4, appTime : 10000, rpps : 7000, distance : -600, degree : 0},{ obj : masterObjs_4, appTime : 10000, rpps : 7500, distance : -600, degree : 0},{ obj : masterObjs_5, appTime : 20000, distance : 1000, degree : 330},{ obj : masterObjs_5, appTime : 20000, distance : 1000, degree : 30},{ obj : masterObjs_5, appTime : 20000, distance : 1100, degree : 355},{ obj : masterObjs_5, appTime : 20000, distance : 1100, degree : 5},{ obj : masterObjs_5, appTime : 21000, distance : 1000, degree : 330},{ obj : masterObjs_5, appTime : 21000, distance : 1000, degree : 30},{ obj : masterObjs_5, appTime : 21000, distance : 1100, degree : 355},{ obj : masterObjs_5, appTime : 21000, distance : 1100, degree : 5},{ obj : masterObjs_5, appTime : 22000, distance : 1100, degree : 345},{ obj : masterObjs_5, appTime : 22000, distance : 1100, degree : 15},{ obj : masterObjs_5, appTime : 22000, distance : 1200, degree : 355},{ obj : masterObjs_5, appTime : 22000, distance : 1200, degree : 5},{ obj : masterObjs_5, appTime : 23000, distance : 1100, degree : 330},{ obj : masterObjs_5, appTime : 23000, distance : 1100, degree : 30},{ obj : masterObjs_5, appTime : 23000, distance : 1300, degree : 355},{ obj : masterObjs_5, appTime : 23000, distance : 1300, degree : 5},{ obj : masterObjs_5, appTime : 24000, distance : 1200, degree : 345},{ obj : masterObjs_5, appTime : 24000, distance : 1200, degree : 15},{ obj : masterObjs_5, appTime : 24000, distance : 1400, degree : 355},{ obj : masterObjs_5, appTime : 24000, distance : 1400, degree : 5},{ obj : masterObjs_5, appTime : 25000, distance : 1300, degree : 330},{ obj : masterObjs_5, appTime : 25000, distance : 1300, degree : 30},{ obj : masterObjs_5, appTime : 25000, distance : 1500, degree : 355},{ obj : masterObjs_5, appTime : 25000, distance : 1500, degree : 5},{ obj : masterObjs_5, appTime : 26000, distance : 1400, degree : 330},{ obj : masterObjs_5, appTime : 26000, distance : 1400, degree : 30},{ obj : masterObjs_5, appTime : 26000, distance : 1500, degree : 355},{ obj : masterObjs_5, appTime : 26000, distance : 1500, degree : 5},{ obj : masterObjs_4, appTime : 30000, rpps : 3000, distance : -200, degree : 0, positionY : 500},{ obj : masterObjs_4, appTime : 30000, rpps : 3500, distance : -200, degree : 0, positionY : 500},{ obj : masterObjs_4, appTime : 30000, rpps : 4000, distance : -200, degree : 0, positionY : 500},{ obj : masterObjs_4, appTime : 30000, rpps : 4500, distance : -200, degree : 0, positionY : 500},{ obj : masterObjs_4, appTime : 30000, rpps : 5000, distance : -200, degree : 0, positionY : 500},{ obj : masterObjs_4, appTime : 30000, rpps : 5500, distance : -200, degree : 0, positionY : 500},{ obj : masterObjs_4, appTime : 30000, rpps : 6000, distance : -200, degree : 0, positionY : 500},{ obj : masterObjs_4, appTime : 30000, rpps : 6500, distance : -200, degree : 0, positionY : 500},{ obj : masterObjs_4, appTime : 30000, rpps : 7000, distance : -200, degree : 0, positionY : 500},{ obj : masterObjs_4, appTime : 30000, rpps : 7500, distance : -200, degree : 0, positionY : 500},{ obj : masterObjs_4, appTime : 30000, rpps : 3000, distance : -400, degree : 0, positionY : 250},{ obj : masterObjs_4, appTime : 30000, rpps : 3500, distance : -400, degree : 0, positionY : 250},{ obj : masterObjs_4, appTime : 30000, rpps : 4000, distance : -400, degree : 0, positionY : 250},{ obj : masterObjs_4, appTime : 30000, rpps : 4500, distance : -400, degree : 0, positionY : 250},{ obj : masterObjs_4, appTime : 30000, rpps : 5000, distance : -400, degree : 0, positionY : 250},{ obj : masterObjs_4, appTime : 30000, rpps : 5500, distance : -400, degree : 0, positionY : 250},{ obj : masterObjs_4, appTime : 30000, rpps : 6000, distance : -400, degree : 0, positionY : 250},{ obj : masterObjs_4, appTime : 30000, rpps : 6500, distance : -400, degree : 0, positionY : 250},{ obj : masterObjs_4, appTime : 30000, rpps : 7000, distance : -400, degree : 0, positionY : 250},{ obj : masterObjs_4, appTime : 30000, rpps : 7500, distance : -400, degree : 0, positionY : 250},{ obj : masterObjs_4, appTime : 30000, rpps : 3000, distance : -600, degree : 0},{ obj : masterObjs_4, appTime : 30000, rpps : 3500, distance : -600, degree : 0},{ obj : masterObjs_4, appTime : 30000, rpps : 4000, distance : -600, degree : 0},{ obj : masterObjs_4, appTime : 30000, rpps : 4500, distance : -600, degree : 0},{ obj : masterObjs_4, appTime : 30000, rpps : 5000, distance : -600, degree : 0},{ obj : masterObjs_4, appTime : 30000, rpps : 5500, distance : -600, degree : 0},{ obj : masterObjs_4, appTime : 30000, rpps : 6000, distance : -600, degree : 0},{ obj : masterObjs_4, appTime : 30000, rpps : 6500, distance : -600, degree : 0},{ obj : masterObjs_4, appTime : 30000, rpps : 7000, distance : -600, degree : 0},{ obj : masterObjs_4, appTime : 30000, rpps : 7500, distance : -600, degree : 0},{ obj : masterObjs_8, appTime : 40000, rpps : 2000, distance : 200, degree : 80},{ obj : masterObjs_10, appTime : 0, rpps : 42000, distance : -200, degree : 80},{ obj : masterObjs_8, appTime : 40000, rpps : 2000, distance : 200, degree : 280},{ obj : masterObjs_10, appTime : 0, rpps : 42000, distance : -200, degree : 280},{ obj : masterObjs_8, appTime : 41000, rpps : 2000, distance : -500, degree : 90},{ obj : masterObjs_10, appTime : 0, rpps : 43000, distance : -500, degree : 80},{ obj : masterObjs_8, appTime : 41000, rpps : 2000, distance : -500, degree : 270},{ obj : masterObjs_10, appTime : 0, rpps : 43000, distance : -500, degree : 280},{ obj : masterObjs_8, appTime : 42000, rpps : 2000, distance : 200, degree : 80},{ obj : masterObjs_10, appTime : 0, rpps : 44000, distance : -200, degree : 80},{ obj : masterObjs_8, appTime : 42000, rpps : 2000, distance : 200, degree : 280},{ obj : masterObjs_10, appTime : 0, rpps : 44000, distance : -200, degree : 280},{ obj : masterObjs_8, appTime : 43000, rpps : 2000, distance : -500, degree : 90},{ obj : masterObjs_10, appTime : 0, rpps : 45000, distance : -500, degree : 80},{ obj : masterObjs_8, appTime : 43000, rpps : 2000, distance : -500, degree : 270},{ obj : masterObjs_10, appTime : 0, rpps : 45000, distance : -500, degree : 280},{ obj : masterObjs_8, appTime : 44000, rpps : 2000, distance : 200, degree : 80},{ obj : masterObjs_10, appTime : 0, rpps : 46000, distance : -200, degree : 80},{ obj : masterObjs_8, appTime : 44000, rpps : 2000, distance : 200, degree : 280},{ obj : masterObjs_10, appTime : 0, rpps : 46000, distance : -200, degree : 280},{ obj : masterObjs_8, appTime : 45000, rpps : 2000, distance : -500, degree : 90},{ obj : masterObjs_10, appTime : 0, rpps : 47000, distance : -500, degree : 80},{ obj : masterObjs_8, appTime : 45000, rpps : 2000, distance : -500, degree : 270},{ obj : masterObjs_10, appTime : 0, rpps : 47000, distance : -500, degree : 280},{ obj : masterObjs_8, appTime : 46000, rpps : 2000, distance : 200, degree : 80},{ obj : masterObjs_10, appTime : 0, rpps : 48000, distance : -200, degree : 80},{ obj : masterObjs_8, appTime : 46000, rpps : 2000, distance : 200, degree : 280},{ obj : masterObjs_10, appTime : 0, rpps : 48000, distance : -200, degree : 280},{ obj : masterObjs_8, appTime : 50000, rpps : 2000, distance : -450, degree : 0},{ obj : masterObjs_10, appTime : 0, rpps : 52000, distance : -500, degree : 0},{ obj : masterObjs_11, appTime : 50500, rpps : 2000, distance : -450, degree : 0},{ obj : masterObjs_10, appTime : 0, rpps : 52500, distance : -500, degree : 0},{ obj : masterObjs_8, appTime : 51000, rpps : 2000, distance : -450, degree : 0},{ obj : masterObjs_10, appTime : 0, rpps : 53000, distance : -500, degree : 0},{ obj : masterObjs_11, appTime : 51500, rpps : 2000, distance : -450, degree : 0},{ obj : masterObjs_10, appTime : 0, rpps : 53500, distance : -500, degree : 0},{ obj : masterObjs_8, appTime : 52000, rpps : 2000, distance : -450, degree : 0},{ obj : masterObjs_10, appTime : 0, rpps : 54000, distance : -500, degree : 0},{ obj : masterObjs_11, appTime : 52500, rpps : 2000, distance : -450, degree : 0},{ obj : masterObjs_10, appTime : 0, rpps : 54500, distance : -500, degree : 0},{ obj : masterObjs_8, appTime : 53000, rpps : 2000, distance : -450, degree : 0},{ obj : masterObjs_10, appTime : 0, rpps : 55000, distance : -500, degree : 0},{ obj : masterObjs_11, appTime : 53500, rpps : 2000, distance : -450, degree : 0},{ obj : masterObjs_10, appTime : 0, rpps : 55500, distance : -500, degree : 0},{ obj : masterObjs_8, appTime : 54000, rpps : 2000, distance : -450, degree : 0},{ obj : masterObjs_10, appTime : 0, rpps : 56000, distance : -500, degree : 0}];
		var objs_0 = { rpps : 1000, obj : masterObjs_0, appTime : 2000};
		var objs_1 = { rpps : 1000, obj : masterObjs_2, appTime : 8000, positionY : 200};
		var objs_2 = { appTime : 6000, obj : masterObjs_5, rpps : 3000};
		var objs_3 = { appTime : 4000, obj : masterObjs_1};
		var objs_4 = { appTime : 10000, obj : masterObjs_3, distance : 500, degree : 270};
		var objs_5 = { appTime : 10000, obj : masterObjs_3, distance : 500, degree : 90};
		var objs_6 = { appTime : 12000, obj : masterObjs_1, distance : 300, degree : 225};
		var objs_7 = { appTime : 12000, obj : masterObjs_1};
		var objs_8 = { appTime : 12000, obj : masterObjs_1, distance : 300, degree : 135};
		var objs_9 = { appTime : 6000, obj : masterObjs_4};
		this.setTimeObject(easy_objs);
		this.setSkyTexture(GameMgr.DIRECTORY + "images/1874-",["left","right","up","down","back","front"],null,500);
		this.stageId = 2;
		return true;
	}
	,init: function(args) {
		scene_abs_AbstractMoveRouteScene.prototype.init.call(this,args);
	}
	,update: function(time) {
		if(this.first) time = Math.min(time,1000);
		scene_abs_AbstractMoveRouteScene.prototype.update.call(this,time);
	}
	,__class__: scene_NormalGameScene
});
var scene_RandomGameScene = function(context) {
	this.masterRoutes = [];
	this.splitDistance = 5000;
	this.splitTime = 10000;
	scene_abs_AbstractMoveRouteScene.call(this,context,null,1000);
};
$hxClasses["scene.RandomGameScene"] = scene_RandomGameScene;
scene_RandomGameScene.__name__ = true;
scene_RandomGameScene.__super__ = scene_abs_AbstractMoveRouteScene;
scene_RandomGameScene.prototype = $extend(scene_abs_AbstractMoveRouteScene.prototype,{
	once: function() {
		if(!scene_abs_AbstractMoveRouteScene.prototype.once.call(this)) return false;
		this.randomMgr = new objmgr_RandomObjMgr(this.visualRange);
		this.stageId = 5;
		this.setSkyTexture(GameMgr.DIRECTORY + "images/fulllg_",["px","nx","py","ny","pz","nz"]);
		return true;
	}
	,init: function(args) {
		if(args != null && args.length > 0) this.randomMgr.again(); else {
			this.randomMgr.init();
			this.masterRoutes = [{ x : 0, y : 0},{ x : 0, y : 1}];
		}
		var ram = Std["int"](Math.random() * 3);
		switch(ram) {
		case 0:
			this.audioName = "hard";
			break;
		case 1:
			this.audioName = "normal";
			break;
		case 2:
			this.audioName = "easy";
			break;
		}
		var routes = [{ time : this.splitTime, point : new THREE.Vector3(0,0,this.splitDistance)}];
		var x = 0;
		var y = 1;
		var old = 2;
		var branch = [0,1,2,3];
		while(this.masterRoutes.length < 7) {
			var canBranch = [];
			var _g = 0;
			while(_g < branch.length) {
				var i = branch[_g];
				++_g;
				if(i != old) canBranch.push(i);
			}
			var flag = false;
			while(!flag) {
				var tmpX = x;
				var tmpY = y;
				var ram1 = Std["int"](Math.random() * canBranch.length);
				var b = canBranch[ram1];
				switch(b) {
				case 0:
					tmpY++;
					break;
				case 1:
					tmpX++;
					break;
				case 2:
					tmpY--;
					break;
				case 3:
					tmpX--;
					break;
				}
				flag = true;
				var next_x = tmpX;
				var next_y = tmpY;
				var _g1 = 0;
				var _g11 = this.masterRoutes;
				while(_g1 < _g11.length) {
					var m = _g11[_g1];
					++_g1;
					if(m.x == next_x && m.y == next_y) {
						flag = false;
						break;
					}
				}
				if(flag) {
					x = tmpX;
					y = tmpY;
					this.masterRoutes.push({ x : x, y : y});
					old = (b + 2) % 4;
				} else {
					var tmpArray = [];
					var _g2 = 0;
					while(_g2 < canBranch.length) {
						var i1 = canBranch[_g2];
						++_g2;
						if(i1 != b) tmpArray.push(i1);
					}
					canBranch = tmpArray;
				}
			}
		}
		var _g12 = 2;
		var _g3 = this.masterRoutes.length;
		while(_g12 < _g3) {
			var i2 = _g12++;
			routes.push({ time : this.splitTime, point : new THREE.Vector3(this.masterRoutes[i2].x * this.splitDistance,0,this.masterRoutes[i2].y * this.splitDistance)});
		}
		this.setRouteData(routes);
		var objs = [];
		var _g13 = 0;
		var _g4 = this.masterRoutes.length - 1;
		while(_g13 < _g4) {
			var i3 = _g13++;
			var radian = Math.atan2(this.masterRoutes[i3 + 1].y - this.masterRoutes[i3].y,this.masterRoutes[i3 + 1].x - this.masterRoutes[i3].x);
			var _g21 = 0;
			while(_g21 < 3) {
				var j = _g21++;
				var random = this.randomMgr.getRandomObjs(i3 * this.splitTime + 500 + j * 3000,radian);
				var _g31 = 0;
				while(_g31 < random.length) {
					var r = random[_g31];
					++_g31;
					objs.push(r);
				}
			}
		}
		this.setTimeObject(objs);
		scene_abs_AbstractMoveRouteScene.prototype.init.call(this,args);
		this.setDebugTime(0);
	}
	,update: function(time) {
		if(this.first) time = Math.min(time,1000);
		scene_abs_AbstractMoveRouteScene.prototype.update.call(this,time);
	}
	,__class__: scene_RandomGameScene
});
var scene_abs_AbstractResultScene = function(context) {
	this.texts = ["タイトルへ","もう一度"];
	this.againNumbers = [];
	this.againPlayer = "";
	this.cntTime = 0;
	this.waitTime = 3000;
	this.players = [];
	scene_abs_AbstractFixedScene.call(this,context);
	this.audioName = "result";
};
$hxClasses["scene.abs.AbstractResultScene"] = scene_abs_AbstractResultScene;
scene_abs_AbstractResultScene.__name__ = true;
scene_abs_AbstractResultScene.__super__ = scene_abs_AbstractFixedScene;
scene_abs_AbstractResultScene.prototype = $extend(scene_abs_AbstractFixedScene.prototype,{
	init: function(args) {
		scene_abs_AbstractFixedScene.prototype.init.call(this,args);
		this.cntTime = 0;
		this.beforeScene = js_Boot.__cast(args[1] , EScene);
		if(Type.enumEq(this.beforeScene,EScene.HardGameScene)) GameMgr.setHardClear(true);
		this.againPlayer = "";
		this.againNumbers = [];
	}
	,once: function() {
		if(!scene_abs_AbstractFixedScene.prototype.once.call(this)) return false;
		var light;
		this.scene.add(new THREE.AmbientLight(6710886));
		light = new obj3d_Lignt(14674943,1.75);
		this.scene.add(light);
		var ground = new obj3d_DefaultGround();
		this.scene.add(ground.getMesh());
		this.camera.position.set(0,500,0);
		this.camera.lookAt(this.scene.position);
		return true;
	}
	,playerShot: function(player) {
		scene_abs_AbstractFixedScene.prototype.playerShot.call(this,player);
		var x = player.getX();
		var y = player.getY();
		if(this.titleText != null && this.titleText.checkHit(x,y)) {
			GameMgr.resetReady();
			GameMgr.changeScene(EScene.TitleScene);
		}
		if(this.againText != null && this.againText.checkHit(x,y)) {
			if(this.againPlayer.indexOf("" + player.getNumber()) == -1) {
				this.againPlayer += player.getNumber();
				this.againNumbers.push(player.getNumber());
			}
		}
	}
	,update: function(time) {
		if(GameMgr.entryNum == 0 || this.players.length == this.againNumbers.length) {
			this.cntTime += time;
			if(this.waitTime <= this.cntTime) {
				if(GameMgr.entryNum == 0) {
					GameMgr.resetReady();
					GameMgr.changeScene(EScene.TitleScene);
				} else if(this.players.length == this.againNumbers.length) {
					var _g = 0;
					var _g1 = this.players;
					while(_g < _g1.length) {
						var p = _g1[_g];
						++_g;
						p.setScore(0);
					}
					GameMgr.changeScene(this.beforeScene,["again"]);
				}
			}
			var cnt = "" + ((this.waitTime - this.cntTime) / 1000 + 1 | 0);
			this.context.beginPath();
			this.context.textAlign = "center";
			this.context.fillStyle = "#000000";
			this.context.textAlign = "left";
			this.context.font = GameMgr.HEIGHT / 17 + "px bold \"Times New Roman\"";
			this.context.fillText(cnt,GameMgr.WIDTH - GameMgr.HEIGHT / 17 * 2,GameMgr.HEIGHT / 17 * 2);
			this.context.closePath();
		}
		if(this.againText != null) {
			if(this.againText.isShow()) {
				if(this.players.length != GameMgr.entryNum) this.againText.hideText();
				var _g11 = 0;
				var _g2 = this.againNumbers.length;
				while(_g11 < _g2) {
					var i = _g11++;
					this.context.beginPath();
					this.context.globalAlpha = 0.7;
					this.context.fillStyle = "#" + GameMgr.playerColor[this.againNumbers[i]];
					this.context.fillRect(this.againText.x + this.againText.getWidth() / this.players.length * i,this.againText.y,this.againText.getWidth() / this.players.length,this.againText.getHeight());
					this.context.globalAlpha = 1;
					this.context.closePath();
				}
			}
		}
		scene_abs_AbstractFixedScene.prototype.update.call(this,time);
	}
	,__class__: scene_abs_AbstractResultScene
});
var scene_ResultRankScene = function(context) {
	this.showRank = true;
	this.isGetRank = false;
	scene_abs_AbstractResultScene.call(this,context);
};
$hxClasses["scene.ResultRankScene"] = scene_ResultRankScene;
scene_ResultRankScene.__name__ = true;
scene_ResultRankScene.__super__ = scene_abs_AbstractResultScene;
scene_ResultRankScene.prototype = $extend(scene_abs_AbstractResultScene.prototype,{
	setRank: function(rank) {
		this.rank = rank;
	}
	,init: function(args) {
		scene_abs_AbstractResultScene.prototype.init.call(this,args);
		if(Type.enumEq(this.beforeScene,EScene.RandomGameScene)) this.showRank = false; else this.showRank = true;
		var player;
		player = js_Boot.__cast(args[0][0] , Player);
		this.players = [player];
		this.titleText = new obj2d_TargetRect(GameMgr.WIDTH * 0.75,GameMgr.HEIGHT / 5,this.texts[0],GameMgr.WIDTH / 30,GameMgr.WIDTH / 30 * 7);
		this.obj2d.add(this.titleText);
		this.againText = new obj2d_TargetRect(GameMgr.WIDTH * 0.75,GameMgr.HEIGHT / 5 * 2,this.texts[1],GameMgr.WIDTH / 30,GameMgr.WIDTH / 30 * 7);
		this.obj2d.add(this.againText);
		this.isGetRank = false;
		if(this.rank != null && this.showRank) {
			var rankArray = this.rank.split(",");
			if(rankArray.length > 0) {
				var myRank = new obj2d_RankResult(GameMgr.WIDTH / 6,GameMgr.HEIGHT - GameMgr.HEIGHT / 6 - GameMgr.HEIGHT / 17,rankArray[0] + " 位 " + player.getScore(),player.getName(),GameMgr.WIDTH / 3 * 2,true);
				this.obj2d.add(myRank);
				var r = 1;
				var oldScore = 0;
				var _g1 = 1;
				var _g = rankArray.length;
				while(_g1 < _g) {
					var i = _g1++;
					var dataArray = rankArray[i].split(":");
					var name = dataArray[0];
					var score = Std.parseInt(dataArray[1]);
					if(i == 1) oldScore = score; else if(score < oldScore) {
						r = i;
						oldScore = score;
					}
					var dbRank = new obj2d_RankResult(GameMgr.WIDTH / 9,GameMgr.HEIGHT / 10 + (i - 1) * (GameMgr.HEIGHT / 10 + 10),r + " 位 " + score,name);
					this.obj2d.add(dbRank);
				}
				this.isGetRank = true;
			}
		} else {
			var myRank1 = new obj2d_RankResult(GameMgr.WIDTH / 6,GameMgr.HEIGHT - GameMgr.HEIGHT / 6 - GameMgr.HEIGHT / 17,!this.showRank?"スコア " + player.getScore():"- 位 " + player.getScore(),player.getName(),GameMgr.WIDTH / 3 * 2,true);
			this.obj2d.add(myRank1);
		}
		this.rank = null;
		this.obj2d.add(player.getObj());
	}
	,update: function(time) {
		if(!this.isGetRank && this.showRank) {
			this.context.beginPath();
			this.context.textAlign = "left";
			this.context.fillStyle = "#000000";
			this.context.font = GameMgr.HEIGHT / 17 + "px bold \"Times New Roman\"";
			this.context.fillText("ランキングデータの取得に失敗しました。",GameMgr.WIDTH / 6,GameMgr.HEIGHT / 6);
			this.context.closePath();
		}
		scene_abs_AbstractResultScene.prototype.update.call(this,time);
	}
	,__class__: scene_ResultRankScene
});
var scene_ResultSingleScene = function(context) {
	scene_abs_AbstractResultScene.call(this,context);
};
$hxClasses["scene.ResultSingleScene"] = scene_ResultSingleScene;
scene_ResultSingleScene.__name__ = true;
scene_ResultSingleScene.__super__ = scene_abs_AbstractResultScene;
scene_ResultSingleScene.prototype = $extend(scene_abs_AbstractResultScene.prototype,{
	init: function(args) {
		scene_abs_AbstractResultScene.prototype.init.call(this,args);
		this.players = [];
		var array = args[0];
		array = js_Boot.__cast(array , Array);
		var _g = 0;
		while(_g < array.length) {
			var i = array[_g];
			++_g;
			this.players.push(js_Boot.__cast(i , Player));
		}
		var isDrow = true;
		var _g1 = 0;
		var _g2 = this.players.length - 1;
		while(_g1 < _g2) {
			var h = _g1++;
			var _g3 = h + 1;
			var _g21 = this.players.length;
			while(_g3 < _g21) {
				var l = _g3++;
				var high = this.players[h].getScore();
				var low = this.players[l].getScore();
				if(high != low) isDrow = false;
				if(high < low && !this.players[l].isRetire() || this.players[h].isRetire()) {
					var tm = this.players[l];
					this.players[l] = this.players[h];
					this.players[h] = tm;
				}
			}
		}
		this.endTime = 500;
		this.elapsedTime = 0;
		var num = 1;
		var rank = 1;
		var wait = 300;
		var prevScore = this.players[0].getScore();
		var maxNum = this.players.length;
		var _g4 = 0;
		var _g11 = this.players;
		while(_g4 < _g11.length) {
			var p = _g11[_g4];
			++_g4;
			var text = "リタイア";
			var score = p.getScore();
			if(!p.isRetire()) {
				if(score < prevScore) {
					rank = num;
					prevScore = score;
				}
				if(!isDrow) text = rank + "位"; else text = "drow";
			}
			this.endTime += wait;
			this.obj2d.add(new obj2d_PlayerResult(text,num,maxNum,p.getNumber(),score,p.getName(),wait * (maxNum - num)));
			num++;
		}
		this.titleText = new obj2d_TargetRect(GameMgr.WIDTH * 0.7,GameMgr.HEIGHT / 5,this.texts[0],GameMgr.WIDTH / 27,GameMgr.WIDTH / 27 * 7);
		this.titleText.hideText();
		this.obj2d.add(this.titleText);
		this.againText = new obj2d_TargetRect(GameMgr.WIDTH * 0.7,GameMgr.HEIGHT / 5 * 2,this.texts[1],GameMgr.WIDTH / 27,GameMgr.WIDTH / 27 * 7);
		this.againText.hideText();
		this.obj2d.add(this.againText);
		var $it0 = GameMgr.players.iterator();
		while( $it0.hasNext() ) {
			var p1 = $it0.next();
			this.obj2d.add(p1.getObj());
		}
	}
	,update: function(timestamp) {
		if(!this.titleText.isShow() && this.elapsedTime >= this.endTime) {
			this.titleText.showText();
			if(GameMgr.entryNum == this.players.length) this.againText.showText();
		}
		scene_abs_AbstractResultScene.prototype.update.call(this,timestamp);
		this.elapsedTime += timestamp;
	}
	,__class__: scene_ResultSingleScene
});
var scene_ResultTeamScene = function(context) {
	this.textSize = 17;
	this.redscore = 0;
	this.bluescore = 0;
	scene_abs_AbstractResultScene.call(this,context);
};
$hxClasses["scene.ResultTeamScene"] = scene_ResultTeamScene;
scene_ResultTeamScene.__name__ = true;
scene_ResultTeamScene.__super__ = scene_abs_AbstractResultScene;
scene_ResultTeamScene.prototype = $extend(scene_abs_AbstractResultScene.prototype,{
	init: function(args) {
		scene_abs_AbstractResultScene.prototype.init.call(this,args);
		this.players = [];
		var array = args[0];
		array = js_Boot.__cast(array , Array);
		var _g = 0;
		while(_g < array.length) {
			var i = array[_g];
			++_g;
			this.players.push(js_Boot.__cast(i , Player));
		}
		var redNames = [];
		var blueNames = [];
		var _g1 = 0;
		var _g11 = this.players;
		while(_g1 < _g11.length) {
			var player = _g11[_g1];
			++_g1;
			var _g2 = player.getTeamColor();
			switch(_g2[1]) {
			case 0:
				redNames[redNames.length] = player.getName();
				this.redscore += player.getScore();
				break;
			case 1:
				blueNames[blueNames.length] = player.getName();
				this.bluescore += player.getScore();
				break;
			}
		}
		var redResult = "DRAW ";
		var blueResult = "DRAW ";
		var redType = "0";
		var blueType = "0";
		var redY = GameMgr.HEIGHT;
		var blueY = GameMgr.HEIGHT;
		if(this.redscore > this.bluescore) {
			redResult = "WIN ";
			redType = "1";
			redY = -(GameMgr.HEIGHT / 6 + 20);
			blueResult = "LOSE ";
			blueType = "0";
			blueY = GameMgr.HEIGHT;
		} else if(this.bluescore > this.redscore) {
			redResult = "LOSE ";
			redType = "0";
			redY = GameMgr.HEIGHT;
			blueResult = "WIN ";
			blueType = "1";
			blueY = -(GameMgr.HEIGHT / 6 + 20);
		}
		this.obj2d.add(new obj2d_TeamResult(redResult,GameMgr.WIDTH / 6,redY,"#F78181",this.redscore,redNames,redType));
		this.obj2d.add(new obj2d_TeamResult(blueResult,GameMgr.WIDTH / 2,blueY,"#81DAF5",this.bluescore,blueNames,blueType));
		this.redscore = 0;
		this.bluescore = 0;
		this.titleText = new obj2d_TargetRect(GameMgr.WIDTH * 0.7,GameMgr.HEIGHT / 5,this.texts[0],GameMgr.WIDTH / 30,GameMgr.WIDTH / 30 * 7);
		this.obj2d.add(this.titleText);
		this.againText = new obj2d_TargetRect(GameMgr.WIDTH * 0.4,GameMgr.HEIGHT / 5,this.texts[1],GameMgr.WIDTH / 30,GameMgr.WIDTH / 30 * 7);
		this.obj2d.add(this.againText);
		var $it0 = GameMgr.players.iterator();
		while( $it0.hasNext() ) {
			var p = $it0.next();
			this.obj2d.add(p.getObj());
		}
	}
	,__class__: scene_ResultTeamScene
});
var scene_TitleScene = function(context) {
	this.col = 8;
	this.cntTime = 2000;
	this.delay = 1500;
	this.arrayMax = 24;
	this.cubeNum = 12;
	this.cubeArray = [];
	this.title = "すまっとシューター";
	this.url = "http://" + window.location.hostname + ":" + window.location.port + "/c/";
	var _g = this;
	scene_abs_AbstractFixedScene.call(this,context);
	this.audioName = "title";
	var light;
	this.scene.add(new THREE.AmbientLight(6710886));
	light = new obj3d_Lignt(14674943,1.75);
	this.scene.add(light);
	this.camera.position.set(0,300,-400);
	this.camera.lookAt(this.scene.position);
	this.audioButton = new obj2d_AudioButton(10,GameMgr.HEIGHT - GameMgr.WIDTH / 12 - 10);
	GameMgr.input.addEventListener(EScene.TitleScene,function(point) {
		_g.audioButton.checkHit(point.x,point.y);
	});
};
$hxClasses["scene.TitleScene"] = scene_TitleScene;
scene_TitleScene.__name__ = true;
scene_TitleScene.__super__ = scene_abs_AbstractFixedScene;
scene_TitleScene.prototype = $extend(scene_abs_AbstractFixedScene.prototype,{
	once: function() {
		if(!scene_abs_AbstractFixedScene.prototype.once.call(this)) return false;
		var ground = new obj3d_DefaultGround();
		this.scene.add(ground.getMesh());
		return true;
	}
	,playerShot: function(player) {
		scene_abs_AbstractFixedScene.prototype.playerShot.call(this,player);
		var x = player.getX();
		var y = player.getY();
		this.teamTypeMgr.checkType(x,y);
		this.battleModeMgr.checkHit(x,y);
		this.audioButton.checkHit(x,y);
		this.hidden.checkHit(x,y);
		var pos = new THREE.Vector3(x / GameMgr.WIDTH * 2 - 1,-(y / GameMgr.HEIGHT) * 2 + 1,1);
		pos.unproject(this.camera);
		var ray = new THREE.Raycaster(this.camera.position,pos.sub(this.camera.position).normalize());
		var target = [];
		var _g_head = this.obj3d.h;
		var _g_val = null;
		while(_g_head != null) {
			var i;
			i = (function($this) {
				var $r;
				_g_val = _g_head[0];
				_g_head = _g_head[1];
				$r = _g_val;
				return $r;
			}(this));
			target.push(i);
		}
		var objs = ray.intersectObjects(target);
		if(objs.length > 0) {
			var obj;
			obj = js_Boot.__cast(objs[0].object , obj3d_MeshEx);
			this.teamTypeMgr.checkTeam(player,obj);
			if(obj.hit() == true) {
				this.obj3d.remove(obj);
				this.scene.remove(obj);
				this.audio.playSE("break");
			}
		}
		var selectMode = this.battleModeMgr.getSelectMode(x,y);
		if(!Type.enumEq(selectMode,EBattleMode.none)) {
			GameMgr.allReady();
			switch(selectMode[1]) {
			case 2:
				GameMgr.changeScene(EScene.HardGameScene);
				break;
			case 1:
				GameMgr.changeScene(EScene.NormalGameScene);
				break;
			case 0:
				GameMgr.changeScene(EScene.EasyGameScene);
				break;
			case 3:
				GameMgr.changeScene(EScene.RandomGameScene);
				break;
			case 4:
				break;
			}
		}
	}
	,init: function(args) {
		scene_abs_AbstractFixedScene.prototype.init.call(this,args);
		this.obj2d.add(this.audioButton);
		this.playerBanner = new obj2d_PlayerBanner(GameMgr.WIDTH / 11,GameMgr.HEIGHT / 40 * 7);
		this.obj2d.add(this.playerBanner);
		this.moveTextMgr = new objmgr_MoveTextMgr(this.playerBanner.y + this.playerBanner.getHeight() + GameMgr.HEIGHT / 15,GameMgr.HEIGHT / 20);
		if(this.isFirst()) {
			this.teamTypeMgr = new objmgr_TeamTypeMgr(this.moveTextMgr.y + 10);
			this.battleModeMgr = new objmgr_BattleModeMgr(this.moveTextMgr.y + 10);
			this.url += Std.string(args[0]);
			this.qrCode = new obj2d_QrcodeObj(GameMgr.WIDTH * 0.85,GameMgr.HEIGHT / 8,-1,2,this.url,GameMgr.WIDTH / 1280);
			this.hidden = new HiddenButton(GameMgr.WIDTH * 0.85,GameMgr.HEIGHT / 8,this.qrCode.getWidth(),this.qrCode.getHeight());
		} else {
			var $it0 = GameMgr.players.iterator();
			while( $it0.hasNext() ) {
				var p = $it0.next();
				this.obj2d.add(p.getObj());
			}
		}
	}
	,appCube: function(time) {
		if(!GameMgr.isReadyAll() && !GameMgr.isTeamBattle()) {
			this.cntTime += time;
			if(this.cntTime > this.delay && this.obj3d.length < this.cubeNum) {
				this.cntTime = 0;
				var _g1 = 0;
				var _g = this.arrayMax;
				while(_g1 < _g) {
					var i = _g1++;
					if(this.cubeArray[i] != null && !this.cubeArray[i].isLife()) this.cubeArray[i] = null;
				}
				var r = Std["int"](Math.random() * this.arrayMax);
				while(this.cubeArray[r] != null) r = Std["int"](Math.random() * this.arrayMax);
				var cube = new obj3d_BreakObject(new shape_CubeShape(0,{ name : "" + r, color : Std["int"](16777215 * Math.random()), size : 40}),[new action_SetMoveAction([{ time : 500, point : new THREE.Vector3(0,40,0)}])]);
				cube.position.y -= 40;
				cube.position.x = (r % this.col - this.col / 2) * (cube.getWidth() * 1.5);
				cube.position.z = ((r / this.col | 0) + 1) * -(cube.getWidth() * 1.5);
				this.cubeArray[r] = cube;
				this.obj3d.add(cube);
				this.scene.add(cube);
			}
		} else if(this.cubeArray.length > 0) {
			var _g2 = 0;
			var _g11 = this.cubeArray;
			while(_g2 < _g11.length) {
				var c = _g11[_g2];
				++_g2;
				this.obj3d.remove(c);
				this.scene.remove(c);
			}
			this.cubeArray = [];
		}
	}
	,update: function(time) {
		this.moveTextMgr.update(this.context,time);
		this.context.beginPath();
		this.context.font = GameMgr.HEIGHT / 16 + "px \"Times New Roman\"";
		this.context.fillStyle = "#ffffff";
		this.context.textAlign = "left";
		this.context.fillText(this.title,20,GameMgr.HEIGHT / 8);
		this.context.closePath();
		this.context.beginPath();
		this.context.font = GameMgr.HEIGHT / 20 + "px \"Times New Roman\"";
		this.context.fillStyle = "#ffffff";
		this.context.textAlign = "right";
		this.context.fillText(this.url,GameMgr.WIDTH - 30,GameMgr.HEIGHT / 12);
		this.context.closePath();
		this.appCube(time);
		this.teamTypeMgr.update(this.obj3d,this.scene,this.context,time);
		this.battleModeMgr.draw(this.context,time);
		scene_abs_AbstractFixedScene.prototype.update.call(this,time);
		this.qrCode.draw(this.context,time);
	}
	,__class__: scene_TitleScene
});
var shape_AbstractShape = function(s) {
	if(s == null) s = 0;
	this.shapeHeight = 0;
	this.shapeWidth = 0;
	this.name = "abstract";
	this.material = new THREE.Material();
	this.geometry = new THREE.Geometry();
	this.score = s;
};
$hxClasses["shape.AbstractShape"] = shape_AbstractShape;
shape_AbstractShape.__name__ = true;
shape_AbstractShape.prototype = {
	getGeometry: function() {
		return this.geometry;
	}
	,getMaterial: function() {
		if(this.material != null) return this.material; else return null;
	}
	,init: function(posi,rote) {
	}
	,getName: function() {
		return this.name;
	}
	,clone: function() {
		return new shape_AbstractShape();
	}
	,getScore: function() {
		return this.score;
	}
	,getWidth: function() {
		return this.shapeWidth;
	}
	,getHeight: function() {
		return this.shapeHeight;
	}
	,__class__: shape_AbstractShape
};
var shape_ArrowShape = function(score,params) {
	if(score == null) score = 0;
	this.direction = 0;
	this.height = 25;
	this.color = 171997;
	this.size = 5;
	shape_AbstractShape.call(this,score);
	if(params == null) params = { };
	if(params.size != null) this.size = params.size;
	if(params.height != null) this.height = params.height;
	if(params.color != null) this.color = params.color;
	if(params.direction != null) this.direction = params.direction;
	var points;
	points = [new THREE.Vector3(0,0,0),new THREE.Vector3(0,this.size,0),new THREE.Vector3(this.height,0,0),new THREE.Vector3(0,0,this.size),new THREE.Vector3(0,0,-this.size),new THREE.Vector3(-this.size,0,0),new THREE.Vector3(0,-this.size,0)];
	this.geometry = new THREE.ConvexGeometry(points);
	this.material = new THREE.MeshPhongMaterial({ color : this.color, shininess : 5, specular : 11784669, metal : true, ambient : 12506589});
};
$hxClasses["shape.ArrowShape"] = shape_ArrowShape;
shape_ArrowShape.__name__ = true;
shape_ArrowShape.__super__ = shape_AbstractShape;
shape_ArrowShape.prototype = $extend(shape_AbstractShape.prototype,{
	init: function(posi,rote) {
		posi.y = this.size;
		rote.y = -Math.PI / 180 * this.direction;
	}
	,clone: function() {
		return new shape_ArrowShape(null,{ size : this.size, color : this.color, height : this.height, direction : this.direction});
	}
	,__class__: shape_ArrowShape
});
var shape_ChestShape = function(score,data) {
	if(score == null) score = 0;
	shape_AbstractShape.call(this,score);
	this.name = "chest";
	if(data == null) data = { };
	if(data.Xsize != null) this.Xsize = data.Xsize; else this.Xsize = 100;
	if(data.Ysize != null) this.Ysize = data.Ysize; else this.Ysize = 100;
	if(data.Zsize != null) this.Zsize = data.Zsize; else this.Zsize = 100;
	if(data.opacity != null) this.op = data.opacity; else this.op = 0.7;
	this.shapeWidth = this.Xsize;
	this.shapeHeight = this.Ysize;
	var chestTexture = THREE.ImageUtils.loadTexture(GameMgr.DIRECTORY + "images/Brick256.jpg");
	chestTexture.wrapS = chestTexture.wrapT = 1000;
	chestTexture.anisotropy = GameMgr.getAnisotropy();
	this.geometry = new THREE.BoxGeometry(this.Xsize,this.Ysize,this.Zsize);
	this.material = new THREE.MeshPhongMaterial({ map : chestTexture, shininess : 10});
};
$hxClasses["shape.ChestShape"] = shape_ChestShape;
shape_ChestShape.__name__ = true;
shape_ChestShape.__super__ = shape_AbstractShape;
shape_ChestShape.prototype = $extend(shape_AbstractShape.prototype,{
	init: function(posi,rote) {
		posi.y += this.Ysize / 2;
	}
	,clone: function() {
		return new shape_ChestShape(this.score,{ Xsize : this.Xsize, Ysize : this.Ysize, Zsize : this.Zsize, opacity : this.op});
	}
	,__class__: shape_ChestShape
});
var shape_CrystalShape = function(score,data) {
	if(score == null) score = 750;
	this.random = false;
	shape_AbstractShape.call(this,score);
	this.name = "crystal";
	if(data == null) data = { };
	if(data.size != null) this.size = data.size; else this.size = 50;
	if(data.ratio != null) this.ratio = data.ratio; else this.ratio = 1.5;
	if(data.color != null) this.c = data.color; else this.c = 16712028;
	if(data.randomColor == true) {
		this.c = Std["int"](16777215 * Math.random());
		this.random = true;
	}
	var group = new THREE.Geometry();
	var leftSin = Math.sin(Math.PI / 6);
	var leftCos = Math.cos(Math.PI / 6);
	var rightSin = Math.sin(Math.PI / 6 * 5);
	var rightCos = Math.cos(Math.PI / 6 * 5);
	var points = [new THREE.Vector3(0,0,0),new THREE.Vector3(0,this.size * this.ratio,this.size),new THREE.Vector3(-this.size,this.size * this.ratio,0),new THREE.Vector3(0,this.size * this.ratio,-this.size),new THREE.Vector3(this.size,this.size * this.ratio,0),new THREE.Vector3(0,this.size * this.ratio * 2,0)];
	this.shapeWidth = this.size * 2;
	this.shapeHeight = this.size * this.ratio * 2;
	group.vertices = points;
	group.faces = [];
	var _g = 1;
	while(_g < 5) {
		var i = _g++;
		group.faces.push(new THREE.Face3(0,i,i != 4?i + 1:1));
	}
	var _g1 = 1;
	while(_g1 < 5) {
		var i1 = _g1++;
		var face = new THREE.Face3(5,i1,i1 != 4?i1 + 1:1);
		face.materialIndex = 1;
		group.faces.push(face);
	}
	group.computeFaceNormals();
	this.geometry = group;
	var material1 = new THREE.MeshLambertMaterial({ color : this.c, transparent : true, opacity : 0.6, ambient : this.c, side : 0});
	var material2 = new THREE.MeshLambertMaterial({ color : this.c, transparent : true, opacity : 0.6, ambient : 10027008, side : 1});
	var materialArray = [material1,material2];
	this.material = new THREE.MeshFaceMaterial(materialArray);
};
$hxClasses["shape.CrystalShape"] = shape_CrystalShape;
shape_CrystalShape.__name__ = true;
shape_CrystalShape.__super__ = shape_AbstractShape;
shape_CrystalShape.prototype = $extend(shape_AbstractShape.prototype,{
	clone: function() {
		return new shape_CrystalShape(this.score,{ size : this.size, ratio : this.ratio, color : this.c, randomColor : this.random});
	}
	,__class__: shape_CrystalShape
});
var shape_CubeShape = function(score,data) {
	if(score == null) score = 500;
	shape_AbstractShape.call(this,score);
	if(data == null) data = { };
	if(data.name != null) this.name = data.name; else this.name = "cube";
	if(data.size != null) this.size = data.size; else this.size = 100;
	if(data.color != null) this.c = data.color; else this.c = 65280;
	if(data.opacity != null) this.op = data.opacity; else this.op = 0.7;
	this.shapeWidth = this.size;
	this.shapeHeight = this.size;
	this.geometry = new THREE.BoxGeometry(this.size,this.size,this.size);
	this.material = new THREE.MeshLambertMaterial({ color : this.c, transparent : true, opacity : this.op, ambient : 10027008});
};
$hxClasses["shape.CubeShape"] = shape_CubeShape;
shape_CubeShape.__name__ = true;
shape_CubeShape.__super__ = shape_AbstractShape;
shape_CubeShape.prototype = $extend(shape_AbstractShape.prototype,{
	init: function(posi,rote) {
		posi.y += this.size / 2;
	}
	,clone: function() {
		return new shape_CubeShape(this.score,{ size : this.size, color : this.c, opacity : this.op});
	}
	,__class__: shape_CubeShape
});
var shape_CylinderShape = function(score,params) {
	if(score == null) score = 800;
	this.defaultRotaY = 0;
	this.opacity = 0.7;
	this.color = 16766720;
	this.segments = 50;
	this.height = 20;
	this.radiusBottom = 50;
	this.radiusTop = 50;
	shape_AbstractShape.call(this,score);
	this.name = "cylinder";
	if(params == null) params = { };
	if(params.radiusTop != null) this.radiusTop = params.radiusTop;
	if(params.radiusBottom != null) this.radiusBottom = params.radiusBottom;
	if(params.height != null) this.height = params.height;
	if(params.color != null) this.color = params.color;
	if(params.opacity != null) this.opacity = params.opacity;
	if(params.defaultRotaY != null) this.defaultRotaY = params.defaultRotaY;
	this.shapeWidth = Math.max(this.radiusTop,this.radiusBottom) * 2;
	this.shapeHeight = Math.max(this.radiusTop,this.radiusBottom) * 2;
	this.geometry = new THREE.CylinderGeometry(this.radiusTop,this.radiusBottom,this.height,this.segments);
	this.material = new THREE.MeshLambertMaterial({ color : this.color, transparent : true, opacity : this.opacity, ambient : 10027008});
};
$hxClasses["shape.CylinderShape"] = shape_CylinderShape;
shape_CylinderShape.__name__ = true;
shape_CylinderShape.__super__ = shape_AbstractShape;
shape_CylinderShape.prototype = $extend(shape_AbstractShape.prototype,{
	init: function(posi,rote) {
		rote.z = -Math.PI / 2;
		if(this.radiusTop > this.radiusBottom) posi.y += this.radiusTop; else posi.y += this.radiusBottom;
		rote.y = this.defaultRotaY;
	}
	,clone: function() {
		return new shape_CylinderShape(this.score,{ radiusTop : this.radiusTop, radiusBottom : this.radiusBottom, height : this.height, color : this.color, opacity : this.opacity, defaultRotaY : this.defaultRotaY});
	}
	,__class__: shape_CylinderShape
});
var shape_DrumShape = function(s,params) {
	if(s == null) s = 0;
	this.sideColor = 10506797;
	this.height = 150;
	this.radius = 60;
	shape_AbstractShape.call(this,s);
	if(params == null) params = { };
	if(params.radius != null) this.radius = params.radius;
	if(params.height != null) this.height = params.height;
	if(params.sideColor != null) this.sideColor = params.sideColor;
	this.shapeWidth = this.radius * 2;
	this.shapeHeight = this.height;
	var convex = this.height / 50;
	var points = [new THREE.Vector3(0,0,-this.height / 2),new THREE.Vector3(this.radius + convex,0,-this.height / 2),new THREE.Vector3(this.radius + convex,0,-this.height / 2 + convex),new THREE.Vector3(this.radius,0,-this.height / 2 + convex),new THREE.Vector3(this.radius,0,-convex / 2),new THREE.Vector3(this.radius + convex,0,-convex / 2),new THREE.Vector3(this.radius + convex,0,convex / 2),new THREE.Vector3(this.radius,0,convex / 2),new THREE.Vector3(this.radius,0,this.height / 2 - convex),new THREE.Vector3(this.radius + convex,0,this.height / 2 - convex),new THREE.Vector3(this.radius + convex,0,this.height / 2),new THREE.Vector3(this.radius - convex,0,this.height / 2),new THREE.Vector3(this.radius - convex,0,this.height / 2 - convex),new THREE.Vector3(0,0,this.height / 2 - convex)];
	this.geometry = new THREE.LatheGeometry(points,16);
	this.material = new THREE.MeshLambertMaterial({ color : this.sideColor});
};
$hxClasses["shape.DrumShape"] = shape_DrumShape;
shape_DrumShape.__name__ = true;
shape_DrumShape.__super__ = shape_AbstractShape;
shape_DrumShape.prototype = $extend(shape_AbstractShape.prototype,{
	init: function(posi,rote) {
		rote.x = -Math.PI / 2;
		posi.y += this.height / 2;
	}
	,clone: function() {
		return new shape_DrumShape(this.score,{ radius : this.radius, height : this.height, sideColor : this.sideColor});
	}
	,__class__: shape_DrumShape
});
var shape_FloorShape = function(mat,points,radius,correctionValue) {
	if(correctionValue == null) correctionValue = 0.0;
	shape_AbstractShape.call(this);
	this.name = "floor";
	this.material = mat;
	this.points = points;
	this.correctionValue = correctionValue;
	this.radius = radius;
	if(points.length == 2) {
		var sp = points[0];
		var ep = points[1];
		var disX = ep.x - sp.x;
		var disZ = ep.z - sp.z;
		var distance = Math.sqrt(disX * disX + disZ * disZ);
		this.geometry = new THREE.PlaneBufferGeometry(radius * 2,distance);
	} else this.geometry = new THREE.CircleGeometry(radius,16);
};
$hxClasses["shape.FloorShape"] = shape_FloorShape;
shape_FloorShape.__name__ = true;
shape_FloorShape.__super__ = shape_AbstractShape;
shape_FloorShape.prototype = $extend(shape_AbstractShape.prototype,{
	init: function(posi,rote) {
		rote.x = -Math.PI / 2;
		if(this.points.length == 2) {
			var sp = this.points[0];
			var ep = this.points[1];
			var radian = Math.atan2(sp.z - ep.z,ep.x - sp.x);
			posi.set(sp.x + (ep.x - sp.x) / 2,0 - this.correctionValue,sp.z + (ep.z - sp.z) / 2);
			rote.z = radian - Math.PI / 2;
		} else {
			posi.copy(this.points[0]);
			posi.y = 0 - this.correctionValue;
		}
	}
	,clone: function() {
		return new shape_FloorShape(this.material,this.points,this.radius,this.correctionValue);
	}
	,__class__: shape_FloorShape
});
var shape_HexagonalShape = function(s,params) {
	if(s == null) s = 0;
	this.defaultRotaY = Math.PI / 2;
	this.opacity = 1;
	this.color = 16777215;
	this.segments = 6;
	this.radius = 75;
	this.height = 3;
	shape_AbstractShape.call(this,s);
	this.name = "Hexagonal";
	if(params == null) params = { };
	if(params.color != null) this.color = params.color;
	if(params.opacity != null) this.opacity = params.opacity;
	if(params.defaultRotaY != null) this.defaultRotaY = params.defaultRotaY;
	this.shapeWidth = this.radius * 2;
	this.shapeHeight = this.radius * 2;
	this.geometry = new THREE.CylinderGeometry(this.radius,this.radius,this.height,this.segments);
	this.material = new THREE.MeshNormalMaterial();
};
$hxClasses["shape.HexagonalShape"] = shape_HexagonalShape;
shape_HexagonalShape.__name__ = true;
shape_HexagonalShape.__super__ = shape_AbstractShape;
shape_HexagonalShape.prototype = $extend(shape_AbstractShape.prototype,{
	init: function(posi,rote) {
		rote.z = -Math.PI / 2;
		posi.y += this.radius;
		rote.y = this.defaultRotaY;
	}
	,clone: function() {
		return new shape_HexagonalShape(this.score,{ color : this.color, opacity : this.opacity, defaultRotaY : this.defaultRotaY});
	}
	,__class__: shape_HexagonalShape
});
var shape_TextShape = function(param) {
	this.opacity = 1;
	this.bevelThickness = 1;
	this.style = "normal";
	this.font = "helvetiker";
	this.weight = "bold";
	this.height = 4;
	this.size = 30;
	this.sideColor = 35071;
	this.frontColor = 65280;
	this.text = "text";
	shape_AbstractShape.call(this);
	this.name = "text";
	this.text = param.text;
	if(param.SideColor != null) this.sideColor = param.SideColor;
	if(param.frontColor != null) this.frontColor = param.frontColor;
	if(param.size != null) this.size = param.size;
	if(param.height != null) this.height = param.height;
	if(param.weight != null) this.weight = param.weight;
	if(param.style != null) this.style = param.style;
	if(param.bevelThickness != null) this.bevelThickness = param.bevelThickness;
	if(param.opacity != null) this.opacity = param.opacity;
	var materialFront = new THREE.MeshBasicMaterial({ color : this.frontColor, transparent : true, opacity : this.opacity});
	var materialSide = new THREE.MeshBasicMaterial({ color : this.sideColor, transparent : true, opacity : this.opacity});
	var materialArray = [materialFront,materialSide];
	this.geometry = new THREE.TextGeometry(this.text,{ size : this.size, height : this.height, curveSegments : 3, font : this.font, weight : this.weight, style : this.style, bevelThickness : this.bevelThickness, bevelSize : 2, bevelEnabled : true, material : 0, extrudeMaterial : 1});
	this.material = new THREE.MeshFaceMaterial(materialArray);
};
$hxClasses["shape.TextShape"] = shape_TextShape;
shape_TextShape.__name__ = true;
shape_TextShape.__super__ = shape_AbstractShape;
shape_TextShape.prototype = $extend(shape_AbstractShape.prototype,{
	clone: function() {
		return new shape_TextShape({ text : this.text, frontColor : this.frontColor, SideColor : this.sideColor, size : this.size, height : this.height, weight : this.weight, style : this.style, bevelThickness : this.bevelThickness});
	}
	,__class__: shape_TextShape
});
var shape_TreeShape = function() {
	this.trunkColor = 10120523;
	this.greenColor = 65280;
	this.height = 500;
	this.radius = 150;
	shape_AbstractShape.call(this,0);
	var bottom = this.height / 5;
	var green = this.height - bottom;
	var points = [new THREE.Vector3(this.radius / 5,0,-this.height / 2 + bottom),new THREE.Vector3(this.radius,0,-this.height / 2 + bottom - this.height / 20),new THREE.Vector3(this.radius / 3,0,-this.height / 12),new THREE.Vector3(this.radius / 5 * 4,0,-this.height / 12 - this.height / 20),new THREE.Vector3(this.radius / 3,0,this.height / 10 * 1.5),new THREE.Vector3(this.radius / 3 * 2,0,this.height / 10 * 1.5 - this.height / 20),new THREE.Vector3(0,0,this.height / 2)];
	var greenGeo = new THREE.LatheGeometry(points,6);
	var trunk = new THREE.CylinderGeometry(this.radius / 5,this.radius / 5,bottom,6);
	var _g = 0;
	var _g1 = trunk.faces;
	while(_g < _g1.length) {
		var f = _g1[_g];
		++_g;
		f.materialIndex = 1;
	}
	var trunkMesh = new THREE.Mesh(trunk);
	trunkMesh.rotation.z -= Math.PI / 2;
	trunkMesh.position.z -= this.height / 2 - bottom / 2;
	trunkMesh.rotation.y -= Math.PI / 2;
	this.shapeWidth = this.radius * 2;
	this.shapeHeight = this.height;
	greenGeo.mergeMesh(trunkMesh);
	this.geometry = greenGeo;
	var colorMaterials = [new THREE.MeshLambertMaterial({ color : this.greenColor}),new THREE.MeshLambertMaterial({ color : this.trunkColor})];
	this.material = new THREE.MeshFaceMaterial(colorMaterials);
};
$hxClasses["shape.TreeShape"] = shape_TreeShape;
shape_TreeShape.__name__ = true;
shape_TreeShape.__super__ = shape_AbstractShape;
shape_TreeShape.prototype = $extend(shape_AbstractShape.prototype,{
	init: function(posi,rote) {
		rote.x = -Math.PI / 2;
		posi.y += this.height / 2;
	}
	,__class__: shape_TreeShape
});
var three_Face = function() { };
$hxClasses["three.Face"] = three_Face;
three_Face.__name__ = true;
three_Face.prototype = {
	__class__: three_Face
};
var three_IFog = function() { };
$hxClasses["three.IFog"] = three_IFog;
three_IFog.__name__ = true;
three_IFog.prototype = {
	__class__: three_IFog
};
var three_Mapping = function() { };
$hxClasses["three.Mapping"] = three_Mapping;
three_Mapping.__name__ = true;
var three_Renderer = function() { };
$hxClasses["three.Renderer"] = three_Renderer;
three_Renderer.__name__ = true;
three_Renderer.prototype = {
	__class__: three_Renderer
};
var $_, $fid = 0;
function $bind(o,m) { if( m == null ) return null; if( m.__id__ == null ) m.__id__ = $fid++; var f; if( o.hx__closures__ == null ) o.hx__closures__ = {}; else f = o.hx__closures__[m.__id__]; if( f == null ) { f = function(){ return f.method.apply(f.scope, arguments); }; f.scope = o; f.method = m; o.hx__closures__[m.__id__] = f; } return f; }
$hxClasses.Math = Math;
String.prototype.__class__ = $hxClasses.String = String;
String.__name__ = true;
$hxClasses.Array = Array;
Array.__name__ = true;
Date.prototype.__class__ = $hxClasses.Date = Date;
Date.__name__ = ["Date"];
var Int = $hxClasses.Int = { __name__ : ["Int"]};
var Dynamic = $hxClasses.Dynamic = { __name__ : ["Dynamic"]};
var Float = $hxClasses.Float = Number;
Float.__name__ = ["Float"];
var Bool = Boolean;
Bool.__ename__ = ["Bool"];
var Class = $hxClasses.Class = { __name__ : ["Class"]};
var Enum = { };
/**
 * @author qiao / https://github.com/qiao
 * @author mrdoob / http://mrdoob.com
 * @author alteredq / http://alteredqualia.com/
 * @author WestLangley / http://github.com/WestLangley
 * @author erich666 / http://erichaines.com
 */
/*global THREE, console */

// This set of controls performs orbiting, dollying (zooming), and panning. It maintains
// the "up" direction as +Y, unlike the TrackballControls. Touch on tablet and phones is
// supported.
//
//    Orbit - left mouse / touch: one finger move
//    Zoom - middle mouse, or mousewheel / touch: two finger spread or squish
//    Pan - right mouse, or arrow keys / touch: three finter swipe

THREE.OrbitControls = function ( object, domElement ) {

	this.object = object;
	this.domElement = ( domElement !== undefined ) ? domElement : document;

	// API

	// Set to false to disable this control
	this.enabled = true;

	// "target" sets the location of focus, where the control orbits around
	// and where it pans with respect to.
	this.target = new THREE.Vector3();

	// center is old, deprecated; use "target" instead
	this.center = this.target;

	// This option actually enables dollying in and out; left as "zoom" for
	// backwards compatibility
	this.noZoom = false;
	this.zoomSpeed = 1.0;

	// Limits to how far you can dolly in and out ( PerspectiveCamera only )
	this.minDistance = 0;
	this.maxDistance = Infinity;

	// Limits to how far you can zoom in and out ( OrthographicCamera only )
	this.minZoom = 0;
	this.maxZoom = Infinity;

	// Set to true to disable this control
	this.noRotate = false;
	this.rotateSpeed = 1.0;

	// Set to true to disable this control
	this.noPan = false;
	this.keyPanSpeed = 7.0;	// pixels moved per arrow key push

	// Set to true to automatically rotate around the target
	this.autoRotate = false;
	this.autoRotateSpeed = 2.0; // 30 seconds per round when fps is 60

	// How far you can orbit vertically, upper and lower limits.
	// Range is 0 to Math.PI radians.
	this.minPolarAngle = 0; // radians
	this.maxPolarAngle = Math.PI; // radians

	// How far you can orbit horizontally, upper and lower limits.
	// If set, must be a sub-interval of the interval [ - Math.PI, Math.PI ].
	this.minAzimuthAngle = - Infinity; // radians
	this.maxAzimuthAngle = Infinity; // radians

	// Set to true to disable use of the keys
	this.noKeys = false;

	// The four arrow keys
	this.keys = { LEFT: 37, UP: 38, RIGHT: 39, BOTTOM: 40 };

	// Mouse buttons
	this.mouseButtons = { ORBIT: THREE.MOUSE.LEFT, ZOOM: THREE.MOUSE.MIDDLE, PAN: THREE.MOUSE.RIGHT };

	////////////
	// internals

	var scope = this;

	var EPS = 0.000001;

	var rotateStart = new THREE.Vector2();
	var rotateEnd = new THREE.Vector2();
	var rotateDelta = new THREE.Vector2();

	var panStart = new THREE.Vector2();
	var panEnd = new THREE.Vector2();
	var panDelta = new THREE.Vector2();
	var panOffset = new THREE.Vector3();

	var offset = new THREE.Vector3();

	var dollyStart = new THREE.Vector2();
	var dollyEnd = new THREE.Vector2();
	var dollyDelta = new THREE.Vector2();

	var theta;
	var phi;
	var phiDelta = 0;
	var thetaDelta = 0;
	var scale = 1;
	var pan = new THREE.Vector3();

	var lastPosition = new THREE.Vector3();
	var lastQuaternion = new THREE.Quaternion();

	var STATE = { NONE : -1, ROTATE : 0, DOLLY : 1, PAN : 2, TOUCH_ROTATE : 3, TOUCH_DOLLY : 4, TOUCH_PAN : 5 };

	var state = STATE.NONE;

	// for reset

	this.target0 = this.target.clone();
	this.position0 = this.object.position.clone();
	this.zoom0 = this.object.zoom;

	// so camera.up is the orbit axis

	var quat = new THREE.Quaternion().setFromUnitVectors( object.up, new THREE.Vector3( 0, 1, 0 ) );
	var quatInverse = quat.clone().inverse();

	// events

	var changeEvent = { type: 'change' };
	var startEvent = { type: 'start' };
	var endEvent = { type: 'end' };

	this.rotateLeft = function ( angle ) {

		if ( angle === undefined ) {

			angle = getAutoRotationAngle();

		}

		thetaDelta -= angle;

	};

	this.rotateUp = function ( angle ) {

		if ( angle === undefined ) {

			angle = getAutoRotationAngle();

		}

		phiDelta -= angle;

	};

	// pass in distance in world space to move left
	this.panLeft = function ( distance ) {

		var te = this.object.matrix.elements;

		// get X column of matrix
		panOffset.set( te[ 0 ], te[ 1 ], te[ 2 ] );
		panOffset.multiplyScalar( - distance );

		pan.add( panOffset );

	};

	// pass in distance in world space to move up
	this.panUp = function ( distance ) {

		var te = this.object.matrix.elements;

		// get Y column of matrix
		panOffset.set( te[ 4 ], te[ 5 ], te[ 6 ] );
		panOffset.multiplyScalar( distance );

		pan.add( panOffset );

	};

	// pass in x,y of change desired in pixel space,
	// right and down are positive
	this.pan = function ( deltaX, deltaY ) {

		var element = scope.domElement === document ? scope.domElement.body : scope.domElement;

		if ( scope.object instanceof THREE.PerspectiveCamera ) {

			// perspective
			var position = scope.object.position;
			var offset = position.clone().sub( scope.target );
			var targetDistance = offset.length();

			// half of the fov is center to top of screen
			targetDistance *= Math.tan( ( scope.object.fov / 2 ) * Math.PI / 180.0 );

			// we actually don't use screenWidth, since perspective camera is fixed to screen height
			scope.panLeft( 2 * deltaX * targetDistance / element.clientHeight );
			scope.panUp( 2 * deltaY * targetDistance / element.clientHeight );

		} else if ( scope.object instanceof THREE.OrthographicCamera ) {

			// orthographic
			scope.panLeft( deltaX * (scope.object.right - scope.object.left) / element.clientWidth );
			scope.panUp( deltaY * (scope.object.top - scope.object.bottom) / element.clientHeight );

		} else {

			// camera neither orthographic or perspective
			console.warn( 'WARNING: OrbitControls.js encountered an unknown camera type - pan disabled.' );

		}

	};

	this.dollyIn = function ( dollyScale ) {

		if ( dollyScale === undefined ) {

			dollyScale = getZoomScale();

		}

		if ( scope.object instanceof THREE.PerspectiveCamera ) {

			scale /= dollyScale;

		} else if ( scope.object instanceof THREE.OrthographicCamera ) {

			scope.object.zoom = Math.max( this.minZoom, Math.min( this.maxZoom, this.object.zoom * dollyScale ) );
			scope.object.updateProjectionMatrix();
			scope.dispatchEvent( changeEvent );

		} else {

			console.warn( 'WARNING: OrbitControls.js encountered an unknown camera type - dolly/zoom disabled.' );

		}

	};

	this.dollyOut = function ( dollyScale ) {

		if ( dollyScale === undefined ) {

			dollyScale = getZoomScale();

		}

		if ( scope.object instanceof THREE.PerspectiveCamera ) {

			scale *= dollyScale;

		} else if ( scope.object instanceof THREE.OrthographicCamera ) {

			scope.object.zoom = Math.max( this.minZoom, Math.min( this.maxZoom, this.object.zoom / dollyScale ) );
			scope.object.updateProjectionMatrix();
			scope.dispatchEvent( changeEvent );

		} else {

			console.warn( 'WARNING: OrbitControls.js encountered an unknown camera type - dolly/zoom disabled.' );

		}

	};

	this.update = function () {

		var position = this.object.position;

		offset.copy( position ).sub( this.target );

		// rotate offset to "y-axis-is-up" space
		offset.applyQuaternion( quat );

		// angle from z-axis around y-axis

		theta = Math.atan2( offset.x, offset.z );

		// angle from y-axis

		phi = Math.atan2( Math.sqrt( offset.x * offset.x + offset.z * offset.z ), offset.y );

		if ( this.autoRotate && state === STATE.NONE ) {

			this.rotateLeft( getAutoRotationAngle() );

		}

		theta += thetaDelta;
		phi += phiDelta;

		// restrict theta to be between desired limits
		theta = Math.max( this.minAzimuthAngle, Math.min( this.maxAzimuthAngle, theta ) );

		// restrict phi to be between desired limits
		phi = Math.max( this.minPolarAngle, Math.min( this.maxPolarAngle, phi ) );

		// restrict phi to be betwee EPS and PI-EPS
		phi = Math.max( EPS, Math.min( Math.PI - EPS, phi ) );

		var radius = offset.length() * scale;

		// restrict radius to be between desired limits
		radius = Math.max( this.minDistance, Math.min( this.maxDistance, radius ) );

		// move target to panned location
		this.target.add( pan );

		offset.x = radius * Math.sin( phi ) * Math.sin( theta );
		offset.y = radius * Math.cos( phi );
		offset.z = radius * Math.sin( phi ) * Math.cos( theta );

		// rotate offset back to "camera-up-vector-is-up" space
		offset.applyQuaternion( quatInverse );

		position.copy( this.target ).add( offset );

		this.object.lookAt( this.target );

		thetaDelta = 0;
		phiDelta = 0;
		scale = 1;
		pan.set( 0, 0, 0 );

		// update condition is:
		// min(camera displacement, camera rotation in radians)^2 > EPS
		// using small-angle approximation cos(x/2) = 1 - x^2 / 8

		if ( lastPosition.distanceToSquared( this.object.position ) > EPS
		    || 8 * (1 - lastQuaternion.dot(this.object.quaternion)) > EPS ) {

			this.dispatchEvent( changeEvent );

			lastPosition.copy( this.object.position );
			lastQuaternion.copy (this.object.quaternion );

		}

	};


	this.reset = function () {

		state = STATE.NONE;

		this.target.copy( this.target0 );
		this.object.position.copy( this.position0 );
		this.object.zoom = this.zoom0;

		this.object.updateProjectionMatrix();
		this.dispatchEvent( changeEvent );

		this.update();

	};

	this.getPolarAngle = function () {

		return phi;

	};

	this.getAzimuthalAngle = function () {

		return theta

	};

	function getAutoRotationAngle() {

		return 2 * Math.PI / 60 / 60 * scope.autoRotateSpeed;

	}

	function getZoomScale() {

		return Math.pow( 0.95, scope.zoomSpeed );

	}

	function onMouseDown( event ) {

		if ( scope.enabled === false ) return;
		event.preventDefault();

		if ( event.button === scope.mouseButtons.ORBIT ) {
			if ( scope.noRotate === true ) return;

			state = STATE.ROTATE;

			rotateStart.set( event.clientX, event.clientY );

		} else if ( event.button === scope.mouseButtons.ZOOM ) {
			if ( scope.noZoom === true ) return;

			state = STATE.DOLLY;

			dollyStart.set( event.clientX, event.clientY );

		} else if ( event.button === scope.mouseButtons.PAN ) {
			if ( scope.noPan === true ) return;

			state = STATE.PAN;

			panStart.set( event.clientX, event.clientY );

		}

		if ( state !== STATE.NONE ) {
			document.addEventListener( 'mousemove', onMouseMove, false );
			document.addEventListener( 'mouseup', onMouseUp, false );
			scope.dispatchEvent( startEvent );
		}

	}

	function onMouseMove( event ) {

		if ( scope.enabled === false ) return;

		event.preventDefault();

		var element = scope.domElement === document ? scope.domElement.body : scope.domElement;

		if ( state === STATE.ROTATE ) {

			if ( scope.noRotate === true ) return;

			rotateEnd.set( event.clientX, event.clientY );
			rotateDelta.subVectors( rotateEnd, rotateStart );

			// rotating across whole screen goes 360 degrees around
			scope.rotateLeft( 2 * Math.PI * rotateDelta.x / element.clientWidth * scope.rotateSpeed );

			// rotating up and down along whole screen attempts to go 360, but limited to 180
			scope.rotateUp( 2 * Math.PI * rotateDelta.y / element.clientHeight * scope.rotateSpeed );

			rotateStart.copy( rotateEnd );

		} else if ( state === STATE.DOLLY ) {

			if ( scope.noZoom === true ) return;

			dollyEnd.set( event.clientX, event.clientY );
			dollyDelta.subVectors( dollyEnd, dollyStart );

			if ( dollyDelta.y > 0 ) {

				scope.dollyIn();

			} else if ( dollyDelta.y < 0 ) {

				scope.dollyOut();

			}

			dollyStart.copy( dollyEnd );

		} else if ( state === STATE.PAN ) {

			if ( scope.noPan === true ) return;

			panEnd.set( event.clientX, event.clientY );
			panDelta.subVectors( panEnd, panStart );

			scope.pan( panDelta.x, panDelta.y );

			panStart.copy( panEnd );

		}

		if ( state !== STATE.NONE ) scope.update();

	}

	function onMouseUp( /* event */ ) {

		if ( scope.enabled === false ) return;

		document.removeEventListener( 'mousemove', onMouseMove, false );
		document.removeEventListener( 'mouseup', onMouseUp, false );
		scope.dispatchEvent( endEvent );
		state = STATE.NONE;

	}

	function onMouseWheel( event ) {

		if ( scope.enabled === false || scope.noZoom === true || state !== STATE.NONE ) return;

		event.preventDefault();
		event.stopPropagation();

		var delta = 0;

		if ( event.wheelDelta !== undefined ) { // WebKit / Opera / Explorer 9

			delta = event.wheelDelta;

		} else if ( event.detail !== undefined ) { // Firefox

			delta = - event.detail;

		}

		if ( delta > 0 ) {

			scope.dollyOut();

		} else if ( delta < 0 ) {

			scope.dollyIn();

		}

		scope.update();
		scope.dispatchEvent( startEvent );
		scope.dispatchEvent( endEvent );

	}

	function onKeyDown( event ) {

		if ( scope.enabled === false || scope.noKeys === true || scope.noPan === true ) return;

		switch ( event.keyCode ) {

			case scope.keys.UP:
				scope.pan( 0, scope.keyPanSpeed );
				scope.update();
				break;

			case scope.keys.BOTTOM:
				scope.pan( 0, - scope.keyPanSpeed );
				scope.update();
				break;

			case scope.keys.LEFT:
				scope.pan( scope.keyPanSpeed, 0 );
				scope.update();
				break;

			case scope.keys.RIGHT:
				scope.pan( - scope.keyPanSpeed, 0 );
				scope.update();
				break;

		}

	}

	function touchstart( event ) {

		if ( scope.enabled === false ) return;

		switch ( event.touches.length ) {

			case 1:	// one-fingered touch: rotate

				if ( scope.noRotate === true ) return;

				state = STATE.TOUCH_ROTATE;

				rotateStart.set( event.touches[ 0 ].pageX, event.touches[ 0 ].pageY );
				break;

			case 2:	// two-fingered touch: dolly

				if ( scope.noZoom === true ) return;

				state = STATE.TOUCH_DOLLY;

				var dx = event.touches[ 0 ].pageX - event.touches[ 1 ].pageX;
				var dy = event.touches[ 0 ].pageY - event.touches[ 1 ].pageY;
				var distance = Math.sqrt( dx * dx + dy * dy );
				dollyStart.set( 0, distance );
				break;

			case 3: // three-fingered touch: pan

				if ( scope.noPan === true ) return;

				state = STATE.TOUCH_PAN;

				panStart.set( event.touches[ 0 ].pageX, event.touches[ 0 ].pageY );
				break;

			default:

				state = STATE.NONE;

		}

		if ( state !== STATE.NONE ) scope.dispatchEvent( startEvent );

	}

	function touchmove( event ) {

		if ( scope.enabled === false ) return;

		event.preventDefault();
		event.stopPropagation();

		var element = scope.domElement === document ? scope.domElement.body : scope.domElement;

		switch ( event.touches.length ) {

			case 1: // one-fingered touch: rotate

				if ( scope.noRotate === true ) return;
				if ( state !== STATE.TOUCH_ROTATE ) return;

				rotateEnd.set( event.touches[ 0 ].pageX, event.touches[ 0 ].pageY );
				rotateDelta.subVectors( rotateEnd, rotateStart );

				// rotating across whole screen goes 360 degrees around
				scope.rotateLeft( 2 * Math.PI * rotateDelta.x / element.clientWidth * scope.rotateSpeed );
				// rotating up and down along whole screen attempts to go 360, but limited to 180
				scope.rotateUp( 2 * Math.PI * rotateDelta.y / element.clientHeight * scope.rotateSpeed );

				rotateStart.copy( rotateEnd );

				scope.update();
				break;

			case 2: // two-fingered touch: dolly

				if ( scope.noZoom === true ) return;
				if ( state !== STATE.TOUCH_DOLLY ) return;

				var dx = event.touches[ 0 ].pageX - event.touches[ 1 ].pageX;
				var dy = event.touches[ 0 ].pageY - event.touches[ 1 ].pageY;
				var distance = Math.sqrt( dx * dx + dy * dy );

				dollyEnd.set( 0, distance );
				dollyDelta.subVectors( dollyEnd, dollyStart );

				if ( dollyDelta.y > 0 ) {

					scope.dollyOut();

				} else if ( dollyDelta.y < 0 ) {

					scope.dollyIn();

				}

				dollyStart.copy( dollyEnd );

				scope.update();
				break;

			case 3: // three-fingered touch: pan

				if ( scope.noPan === true ) return;
				if ( state !== STATE.TOUCH_PAN ) return;

				panEnd.set( event.touches[ 0 ].pageX, event.touches[ 0 ].pageY );
				panDelta.subVectors( panEnd, panStart );

				scope.pan( panDelta.x, panDelta.y );

				panStart.copy( panEnd );

				scope.update();
				break;

			default:

				state = STATE.NONE;

		}

	}

	function touchend( /* event */ ) {

		if ( scope.enabled === false ) return;

		scope.dispatchEvent( endEvent );
		state = STATE.NONE;

	}

	this.domElement.addEventListener( 'contextmenu', function ( event ) { event.preventDefault(); }, false );
	this.domElement.addEventListener( 'mousedown', onMouseDown, false );
	this.domElement.addEventListener( 'mousewheel', onMouseWheel, false );
	this.domElement.addEventListener( 'DOMMouseScroll', onMouseWheel, false ); // firefox

	this.domElement.addEventListener( 'touchstart', touchstart, false );
	this.domElement.addEventListener( 'touchend', touchend, false );
	this.domElement.addEventListener( 'touchmove', touchmove, false );

	window.addEventListener( 'keydown', onKeyDown, false );

	// force an update at start
	this.update();

};

THREE.OrbitControls.prototype = Object.create( THREE.EventDispatcher.prototype );
THREE.OrbitControls.prototype.constructor = THREE.OrbitControls;
;
/**
 * @author qiao / https://github.com/qiao
 * @fileoverview This is a convex hull generator using the incremental method. 
 * The complexity is O(n^2) where n is the number of vertices.
 * O(nlogn) algorithms do exist, but they are much more complicated.
 *
 * Benchmark: 
 *
 *  Platform: CPU: P7350 @2.00GHz Engine: V8
 *
 *  Num Vertices	Time(ms)
 *
 *     10           1
 *     20           3
 *     30           19
 *     40           48
 *     50           107
 */

THREE.ConvexGeometry = function( vertices ) {

	THREE.Geometry.call( this );

	var faces = [ [ 0, 1, 2 ], [ 0, 2, 1 ] ]; 

	for ( var i = 3; i < vertices.length; i ++ ) {

		addPoint( i );

	}


	function addPoint( vertexId ) {

		var vertex = vertices[ vertexId ].clone();

		var mag = vertex.length();
		vertex.x += mag * randomOffset();
		vertex.y += mag * randomOffset();
		vertex.z += mag * randomOffset();

		var hole = [];

		for ( var f = 0; f < faces.length; ) {

			var face = faces[ f ];

			// for each face, if the vertex can see it,
			// then we try to add the face's edges into the hole.
			if ( visible( face, vertex ) ) {

				for ( var e = 0; e < 3; e ++ ) {

					var edge = [ face[ e ], face[ ( e + 1 ) % 3 ] ];
					var boundary = true;

					// remove duplicated edges.
					for ( var h = 0; h < hole.length; h ++ ) {

						if ( equalEdge( hole[ h ], edge ) ) {

							hole[ h ] = hole[ hole.length - 1 ];
							hole.pop();
							boundary = false;
							break;

						}

					}

					if ( boundary ) {

						hole.push( edge );

					}

				}

				// remove faces[ f ]
				faces[ f ] = faces[ faces.length - 1 ];
				faces.pop();

			} else { // not visible

				f ++;

			}
		}

		// construct the new faces formed by the edges of the hole and the vertex
		for ( var h = 0; h < hole.length; h ++ ) {

			faces.push( [ 
				hole[ h ][ 0 ],
				hole[ h ][ 1 ],
				vertexId
			] );

		}
	}

	/**
	 * Whether the face is visible from the vertex
	 */
	function visible( face, vertex ) {

		var va = vertices[ face[ 0 ] ];
		var vb = vertices[ face[ 1 ] ];
		var vc = vertices[ face[ 2 ] ];

		var n = normal( va, vb, vc );

		// distance from face to origin
		var dist = n.dot( va );

		return n.dot( vertex ) >= dist; 

	}

	/**
	 * Face normal
	 */
	function normal( va, vb, vc ) {

		var cb = new THREE.Vector3();
		var ab = new THREE.Vector3();

		cb.subVectors( vc, vb );
		ab.subVectors( va, vb );
		cb.cross( ab );

		cb.normalize();

		return cb;

	}

	/**
	 * Detect whether two edges are equal.
	 * Note that when constructing the convex hull, two same edges can only
	 * be of the negative direction.
	 */
	function equalEdge( ea, eb ) {

		return ea[ 0 ] === eb[ 1 ] && ea[ 1 ] === eb[ 0 ]; 

	}

	/**
	 * Create a random offset between -1e-6 and 1e-6.
	 */
	function randomOffset() {

		return ( Math.random() - 0.5 ) * 2 * 1e-6;

	}


	/**
	 * XXX: Not sure if this is the correct approach. Need someone to review.
	 */
	function vertexUv( vertex ) {

		var mag = vertex.length();
		return new THREE.Vector2( vertex.x / mag, vertex.y / mag );

	}

	// Push vertices into `this.vertices`, skipping those inside the hull
	var id = 0;
	var newId = new Array( vertices.length ); // map from old vertex id to new id

	for ( var i = 0; i < faces.length; i ++ ) {

		 var face = faces[ i ];

		 for ( var j = 0; j < 3; j ++ ) {

			if ( newId[ face[ j ] ] === undefined ) {

				newId[ face[ j ] ] = id ++;
				this.vertices.push( vertices[ face[ j ] ] );

			}

			face[ j ] = newId[ face[ j ] ];

		 }

	}

	// Convert faces into instances of THREE.Face3
	for ( var i = 0; i < faces.length; i ++ ) {

		this.faces.push( new THREE.Face3( 
				faces[ i ][ 0 ],
				faces[ i ][ 1 ],
				faces[ i ][ 2 ]
		) );

	}

	// Compute UVs
	for ( var i = 0; i < this.faces.length; i ++ ) {

		var face = this.faces[ i ];

		this.faceVertexUvs[ 0 ].push( [
			vertexUv( this.vertices[ face.a ] ),
			vertexUv( this.vertices[ face.b ] ),
			vertexUv( this.vertices[ face.c ])
		] );

	}

	this.computeFaceNormals();
	this.computeVertexNormals();

};

THREE.ConvexGeometry.prototype = Object.create( THREE.Geometry.prototype );
THREE.ConvexGeometry.prototype.constructor = THREE.ConvexGeometry;
;
if (_typeface_js && _typeface_js.loadFace) _typeface_js.loadFace({"glyphs":{"ο":{"x_min":0,"x_max":764,"ha":863,"o":"m 380 -25 q 105 87 211 -25 q 0 372 0 200 q 104 660 0 545 q 380 775 209 775 q 658 659 552 775 q 764 372 764 544 q 658 87 764 200 q 380 -25 552 -25 m 379 142 q 515 216 466 142 q 557 373 557 280 q 515 530 557 465 q 379 607 466 607 q 245 530 294 607 q 204 373 204 465 q 245 218 204 283 q 379 142 294 142 "},"S":{"x_min":0,"x_max":826,"ha":915,"o":"m 826 306 q 701 55 826 148 q 423 -29 587 -29 q 138 60 255 -29 q 0 318 13 154 l 208 318 q 288 192 216 238 q 437 152 352 152 q 559 181 506 152 q 623 282 623 217 q 466 411 623 372 q 176 487 197 478 q 18 719 18 557 q 136 958 18 869 q 399 1040 244 1040 q 670 956 561 1040 q 791 713 791 864 l 591 713 q 526 826 583 786 q 393 866 469 866 q 277 838 326 866 q 218 742 218 804 q 374 617 218 655 q 667 542 646 552 q 826 306 826 471 "},"¦":{"x_min":0,"x_max":143,"ha":240,"o":"m 143 462 l 0 462 l 0 984 l 143 984 l 143 462 m 143 -242 l 0 -242 l 0 280 l 143 280 l 143 -242 "},"/":{"x_min":196.109375,"x_max":632.5625,"ha":828,"o":"m 632 1040 l 289 -128 l 196 -128 l 538 1040 l 632 1040 "},"Τ":{"x_min":-0.609375,"x_max":808,"ha":878,"o":"m 808 831 l 508 831 l 508 0 l 298 0 l 298 831 l 0 831 l 0 1013 l 808 1013 l 808 831 "},"y":{"x_min":0,"x_max":738.890625,"ha":828,"o":"m 738 749 l 444 -107 q 361 -238 413 -199 q 213 -277 308 -277 q 156 -275 176 -277 q 120 -271 131 -271 l 120 -110 q 147 -113 134 -111 q 179 -116 161 -116 q 247 -91 226 -116 q 269 -17 269 -67 q 206 173 269 -4 q 84 515 162 301 q 0 749 41 632 l 218 749 l 376 207 l 529 749 l 738 749 "},"Π":{"x_min":0,"x_max":809,"ha":922,"o":"m 809 0 l 598 0 l 598 836 l 208 836 l 208 0 l 0 0 l 0 1012 l 809 1012 l 809 0 "},"ΐ":{"x_min":-162,"x_max":364,"ha":364,"o":"m 364 810 l 235 810 l 235 952 l 364 952 l 364 810 m 301 1064 l 86 810 l -12 810 l 123 1064 l 301 1064 m -33 810 l -162 810 l -162 952 l -33 952 l -33 810 m 200 0 l 0 0 l 0 748 l 200 748 l 200 0 "},"g":{"x_min":0,"x_max":724,"ha":839,"o":"m 724 48 q 637 -223 724 -142 q 357 -304 551 -304 q 140 -253 226 -304 q 23 -72 36 -192 l 243 -72 q 290 -127 255 -110 q 368 -144 324 -144 q 504 -82 470 -144 q 530 71 530 -38 l 530 105 q 441 25 496 51 q 319 0 386 0 q 79 115 166 0 q 0 377 0 219 q 77 647 0 534 q 317 775 166 775 q 534 656 456 775 l 534 748 l 724 748 l 724 48 m 368 167 q 492 237 447 167 q 530 382 530 297 q 490 529 530 466 q 364 603 444 603 q 240 532 284 603 q 201 386 201 471 q 240 239 201 300 q 368 167 286 167 "},"²":{"x_min":0,"x_max":463,"ha":560,"o":"m 463 791 q 365 627 463 706 q 151 483 258 555 l 455 483 l 455 382 l 0 382 q 84 565 0 488 q 244 672 97 576 q 331 784 331 727 q 299 850 331 824 q 228 876 268 876 q 159 848 187 876 q 132 762 132 820 l 10 762 q 78 924 10 866 q 228 976 137 976 q 392 925 322 976 q 463 791 463 874 "},"–":{"x_min":0,"x_max":704.171875,"ha":801,"o":"m 704 297 l 0 297 l 0 450 l 704 450 l 704 297 "},"Κ":{"x_min":0,"x_max":899.671875,"ha":969,"o":"m 899 0 l 646 0 l 316 462 l 208 355 l 208 0 l 0 0 l 0 1013 l 208 1013 l 208 596 l 603 1013 l 863 1013 l 460 603 l 899 0 "},"ƒ":{"x_min":-46,"x_max":440,"ha":525,"o":"m 440 609 l 316 609 l 149 -277 l -46 -277 l 121 609 l 14 609 l 14 749 l 121 749 q 159 949 121 894 q 344 1019 208 1019 l 440 1015 l 440 855 l 377 855 q 326 841 338 855 q 314 797 314 827 q 314 773 314 786 q 314 749 314 761 l 440 749 l 440 609 "},"e":{"x_min":0,"x_max":708,"ha":808,"o":"m 708 321 l 207 321 q 254 186 207 236 q 362 141 298 141 q 501 227 453 141 l 700 227 q 566 36 662 104 q 362 -26 477 -26 q 112 72 213 -26 q 0 369 0 182 q 95 683 0 573 q 358 793 191 793 q 619 677 531 793 q 708 321 708 561 m 501 453 q 460 571 501 531 q 353 612 420 612 q 247 570 287 612 q 207 453 207 529 l 501 453 "},"ό":{"x_min":0,"x_max":764,"ha":863,"o":"m 380 -25 q 105 87 211 -25 q 0 372 0 200 q 104 660 0 545 q 380 775 209 775 q 658 659 552 775 q 764 372 764 544 q 658 87 764 200 q 380 -25 552 -25 m 379 142 q 515 216 466 142 q 557 373 557 280 q 515 530 557 465 q 379 607 466 607 q 245 530 294 607 q 204 373 204 465 q 245 218 204 283 q 379 142 294 142 m 593 1039 l 391 823 l 293 823 l 415 1039 l 593 1039 "},"J":{"x_min":0,"x_max":649,"ha":760,"o":"m 649 294 q 573 48 649 125 q 327 -29 497 -29 q 61 82 136 -29 q 0 375 0 173 l 200 375 l 199 309 q 219 194 199 230 q 321 145 249 145 q 418 193 390 145 q 441 307 441 232 l 441 1013 l 649 1013 l 649 294 "},"»":{"x_min":-0.234375,"x_max":526,"ha":624,"o":"m 526 286 l 297 87 l 296 250 l 437 373 l 297 495 l 297 660 l 526 461 l 526 286 m 229 286 l 0 87 l 0 250 l 140 373 l 0 495 l 0 660 l 229 461 l 229 286 "},"©":{"x_min":3,"x_max":1007,"ha":1104,"o":"m 507 -6 q 129 153 269 -6 q 3 506 3 298 q 127 857 3 713 q 502 1017 266 1017 q 880 855 740 1017 q 1007 502 1007 711 q 882 152 1007 295 q 507 -6 743 -6 m 502 934 q 184 800 302 934 q 79 505 79 680 q 184 210 79 331 q 501 76 302 76 q 819 210 701 76 q 925 507 925 331 q 820 800 925 682 q 502 934 704 934 m 758 410 q 676 255 748 313 q 506 197 605 197 q 298 291 374 197 q 229 499 229 377 q 297 713 229 624 q 494 811 372 811 q 666 760 593 811 q 752 616 739 710 l 621 616 q 587 688 621 658 q 509 719 554 719 q 404 658 441 719 q 368 511 368 598 q 403 362 368 427 q 498 298 438 298 q 624 410 606 298 l 758 410 "},"ώ":{"x_min":0,"x_max":945,"ha":1051,"o":"m 566 528 l 372 528 l 372 323 q 372 298 372 311 q 373 271 372 285 q 360 183 373 211 q 292 142 342 142 q 219 222 243 142 q 203 365 203 279 q 241 565 203 461 q 334 748 273 650 l 130 748 q 36 552 68 650 q 0 337 0 444 q 69 96 0 204 q 276 -29 149 -29 q 390 0 337 -29 q 470 78 444 28 q 551 0 495 30 q 668 -29 608 -29 q 874 96 793 -29 q 945 337 945 205 q 910 547 945 444 q 814 748 876 650 l 610 748 q 703 565 671 650 q 742 365 742 462 q 718 189 742 237 q 651 142 694 142 q 577 190 597 142 q 565 289 565 221 l 565 323 l 566 528 m 718 1039 l 516 823 l 417 823 l 540 1039 l 718 1039 "},"^":{"x_min":197.21875,"x_max":630.5625,"ha":828,"o":"m 630 836 l 536 836 l 413 987 l 294 836 l 197 836 l 331 1090 l 493 1090 l 630 836 "},"«":{"x_min":0,"x_max":526.546875,"ha":624,"o":"m 526 87 l 297 286 l 297 461 l 526 660 l 526 495 l 385 373 l 526 250 l 526 87 m 229 87 l 0 286 l 0 461 l 229 660 l 229 495 l 88 373 l 229 250 l 229 87 "},"D":{"x_min":0,"x_max":864,"ha":968,"o":"m 400 1013 q 736 874 608 1013 q 864 523 864 735 q 717 146 864 293 q 340 0 570 0 l 0 0 l 0 1013 l 400 1013 m 398 837 l 206 837 l 206 182 l 372 182 q 584 276 507 182 q 657 504 657 365 q 594 727 657 632 q 398 837 522 837 "},"∙":{"x_min":0,"x_max":207,"ha":304,"o":"m 207 528 l 0 528 l 0 735 l 207 735 l 207 528 "},"ÿ":{"x_min":0,"x_max":47,"ha":125,"o":"m 47 3 q 37 -7 47 -7 q 28 0 30 -7 q 39 -4 32 -4 q 45 3 45 -1 l 37 0 q 28 9 28 0 q 39 19 28 19 l 47 16 l 47 19 l 47 3 m 37 1 q 44 8 44 1 q 37 16 44 16 q 30 8 30 16 q 37 1 30 1 m 26 1 l 23 22 l 14 0 l 3 22 l 3 3 l 0 25 l 13 1 l 22 25 l 26 1 "},"w":{"x_min":0,"x_max":1056.953125,"ha":1150,"o":"m 1056 749 l 848 0 l 647 0 l 527 536 l 412 0 l 211 0 l 0 749 l 202 749 l 325 226 l 429 748 l 633 748 l 740 229 l 864 749 l 1056 749 "},"$":{"x_min":0,"x_max":704,"ha":800,"o":"m 682 693 l 495 693 q 468 782 491 749 q 391 831 441 824 l 391 579 q 633 462 562 534 q 704 259 704 389 q 616 57 704 136 q 391 -22 528 -22 l 391 -156 l 308 -156 l 308 -22 q 76 69 152 -7 q 0 300 0 147 l 183 300 q 215 191 190 230 q 308 128 245 143 l 308 414 q 84 505 157 432 q 12 700 12 578 q 89 902 12 824 q 308 981 166 981 l 308 1069 l 391 1069 l 391 981 q 595 905 521 981 q 682 693 670 829 m 308 599 l 308 831 q 228 796 256 831 q 200 712 200 762 q 225 642 200 668 q 308 599 251 617 m 391 128 q 476 174 449 140 q 504 258 504 207 q 391 388 504 354 l 391 128 "},"\\":{"x_min":-0.03125,"x_max":434.765625,"ha":532,"o":"m 434 -128 l 341 -128 l 0 1039 l 91 1040 l 434 -128 "},"µ":{"x_min":0,"x_max":647,"ha":754,"o":"m 647 0 l 478 0 l 478 68 q 412 9 448 30 q 330 -11 375 -11 q 261 3 296 -11 q 199 43 226 18 l 199 -277 l 0 -277 l 0 749 l 199 749 l 199 358 q 216 221 199 267 q 322 151 244 151 q 435 240 410 151 q 448 401 448 283 l 448 749 l 647 749 l 647 0 "},"Ι":{"x_min":42,"x_max":250,"ha":413,"o":"m 250 0 l 42 0 l 42 1013 l 250 1013 l 250 0 "},"Ύ":{"x_min":0,"x_max":1211.15625,"ha":1289,"o":"m 1211 1012 l 907 376 l 907 0 l 697 0 l 697 376 l 374 1012 l 583 1012 l 802 576 l 1001 1012 l 1211 1012 m 313 1035 l 98 780 l 0 780 l 136 1035 l 313 1035 "},"’":{"x_min":0,"x_max":192,"ha":289,"o":"m 192 834 q 137 692 192 751 q 0 626 83 634 l 0 697 q 101 831 101 723 l 0 831 l 0 1013 l 192 1013 l 192 834 "},"Ν":{"x_min":0,"x_max":833,"ha":946,"o":"m 833 0 l 617 0 l 206 696 l 206 0 l 0 0 l 0 1013 l 216 1013 l 629 315 l 629 1013 l 833 1013 l 833 0 "},"-":{"x_min":27.78125,"x_max":413.890625,"ha":525,"o":"m 413 279 l 27 279 l 27 468 l 413 468 l 413 279 "},"Q":{"x_min":0,"x_max":995.59375,"ha":1096,"o":"m 995 49 l 885 -70 l 762 42 q 641 -12 709 4 q 497 -29 572 -29 q 135 123 271 -29 q 0 504 0 276 q 131 881 0 731 q 497 1040 270 1040 q 859 883 719 1040 q 994 506 994 731 q 966 321 994 413 q 884 152 938 229 l 995 49 m 730 299 q 767 395 755 344 q 779 504 779 446 q 713 743 779 644 q 505 857 638 857 q 284 745 366 857 q 210 501 210 644 q 279 265 210 361 q 492 157 357 157 q 615 181 557 157 l 508 287 l 620 405 l 730 299 "},"ς":{"x_min":0,"x_max":731.78125,"ha":768,"o":"m 731 448 l 547 448 q 485 571 531 533 q 369 610 440 610 q 245 537 292 610 q 204 394 204 473 q 322 186 204 238 q 540 133 430 159 q 659 -15 659 98 q 643 -141 659 -80 q 595 -278 627 -202 l 423 -278 q 458 -186 448 -215 q 474 -88 474 -133 q 352 0 474 -27 q 123 80 181 38 q 0 382 0 170 q 98 660 0 549 q 367 777 202 777 q 622 683 513 777 q 731 448 731 589 "},"M":{"x_min":0,"x_max":1019,"ha":1135,"o":"m 1019 0 l 823 0 l 823 819 l 618 0 l 402 0 l 194 818 l 194 0 l 0 0 l 0 1013 l 309 1012 l 510 241 l 707 1013 l 1019 1013 l 1019 0 "},"Ψ":{"x_min":0,"x_max":995,"ha":1085,"o":"m 995 698 q 924 340 995 437 q 590 200 841 227 l 590 0 l 404 0 l 404 200 q 70 340 152 227 q 0 698 0 437 l 0 1013 l 188 1013 l 188 694 q 212 472 188 525 q 404 383 254 383 l 404 1013 l 590 1013 l 590 383 q 781 472 740 383 q 807 694 807 525 l 807 1013 l 995 1013 l 995 698 "},"C":{"x_min":0,"x_max":970.828125,"ha":1043,"o":"m 970 345 q 802 70 933 169 q 490 -29 672 -29 q 130 130 268 -29 q 0 506 0 281 q 134 885 0 737 q 502 1040 275 1040 q 802 939 668 1040 q 965 679 936 838 l 745 679 q 649 809 716 761 q 495 857 582 857 q 283 747 361 857 q 214 508 214 648 q 282 267 214 367 q 493 154 359 154 q 651 204 584 154 q 752 345 718 255 l 970 345 "},"!":{"x_min":0,"x_max":204,"ha":307,"o":"m 204 739 q 182 515 204 686 q 152 282 167 398 l 52 282 q 13 589 27 473 q 0 739 0 704 l 0 1013 l 204 1013 l 204 739 m 204 0 l 0 0 l 0 203 l 204 203 l 204 0 "},"{":{"x_min":0,"x_max":501.390625,"ha":599,"o":"m 501 -285 q 229 -209 301 -285 q 176 -35 176 -155 q 182 47 176 -8 q 189 126 189 103 q 156 245 189 209 q 0 294 112 294 l 0 438 q 154 485 111 438 q 189 603 189 522 q 186 666 189 636 q 176 783 176 772 q 231 945 176 894 q 501 1015 306 1015 l 501 872 q 370 833 408 872 q 340 737 340 801 q 342 677 340 705 q 353 569 353 579 q 326 451 353 496 q 207 366 291 393 q 327 289 294 346 q 353 164 353 246 q 348 79 353 132 q 344 17 344 26 q 372 -95 344 -58 q 501 -141 408 -141 l 501 -285 "},"X":{"x_min":0,"x_max":894.453125,"ha":999,"o":"m 894 0 l 654 0 l 445 351 l 238 0 l 0 0 l 316 516 l 0 1013 l 238 1013 l 445 659 l 652 1013 l 894 1013 l 577 519 l 894 0 "},"#":{"x_min":0,"x_max":1019.453125,"ha":1117,"o":"m 1019 722 l 969 582 l 776 581 l 717 417 l 919 417 l 868 279 l 668 278 l 566 -6 l 413 -5 l 516 279 l 348 279 l 247 -6 l 94 -6 l 196 278 l 0 279 l 49 417 l 245 417 l 304 581 l 98 582 l 150 722 l 354 721 l 455 1006 l 606 1006 l 507 721 l 673 722 l 776 1006 l 927 1006 l 826 721 l 1019 722 m 627 581 l 454 581 l 394 417 l 567 417 l 627 581 "},"ι":{"x_min":42,"x_max":242,"ha":389,"o":"m 242 0 l 42 0 l 42 749 l 242 749 l 242 0 "},"Ά":{"x_min":0,"x_max":995.828125,"ha":1072,"o":"m 313 1035 l 98 780 l 0 780 l 136 1035 l 313 1035 m 995 0 l 776 0 l 708 208 l 315 208 l 247 0 l 29 0 l 390 1012 l 629 1012 l 995 0 m 652 376 l 509 809 l 369 376 l 652 376 "},")":{"x_min":0,"x_max":389,"ha":486,"o":"m 389 357 q 319 14 389 187 q 145 -293 259 -134 l 0 -293 q 139 22 90 -142 q 189 358 189 187 q 139 689 189 525 q 0 1013 90 853 l 145 1013 q 319 703 258 857 q 389 357 389 528 "},"ε":{"x_min":16.671875,"x_max":652.78125,"ha":742,"o":"m 652 259 q 565 49 652 123 q 340 -25 479 -25 q 102 39 188 -25 q 16 197 16 104 q 45 299 16 249 q 134 390 75 348 q 58 456 86 419 q 25 552 25 502 q 120 717 25 653 q 322 776 208 776 q 537 710 456 776 q 625 508 625 639 l 445 508 q 415 585 445 563 q 327 608 386 608 q 254 590 293 608 q 215 544 215 573 q 252 469 215 490 q 336 453 280 453 q 369 455 347 453 q 400 456 391 456 l 400 308 l 329 308 q 247 291 280 308 q 204 223 204 269 q 255 154 204 172 q 345 143 286 143 q 426 174 398 143 q 454 259 454 206 l 652 259 "},"Δ":{"x_min":0,"x_max":981.953125,"ha":1057,"o":"m 981 0 l 0 0 l 386 1013 l 594 1013 l 981 0 m 715 175 l 490 765 l 266 175 l 715 175 "},"}":{"x_min":0,"x_max":500,"ha":597,"o":"m 500 294 q 348 246 390 294 q 315 128 315 209 q 320 42 315 101 q 326 -48 326 -17 q 270 -214 326 -161 q 0 -285 196 -285 l 0 -141 q 126 -97 90 -141 q 154 8 154 -64 q 150 91 154 37 q 146 157 146 145 q 172 281 146 235 q 294 366 206 339 q 173 451 208 390 q 146 576 146 500 q 150 655 146 603 q 154 731 154 708 q 126 831 154 799 q 0 872 90 872 l 0 1015 q 270 944 196 1015 q 326 777 326 891 q 322 707 326 747 q 313 593 313 612 q 347 482 313 518 q 500 438 390 438 l 500 294 "},"‰":{"x_min":0,"x_max":1681,"ha":1775,"o":"m 861 484 q 1048 404 979 484 q 1111 228 1111 332 q 1048 51 1111 123 q 859 -29 979 -29 q 672 50 740 -29 q 610 227 610 122 q 672 403 610 331 q 861 484 741 484 m 861 120 q 939 151 911 120 q 967 226 967 183 q 942 299 967 270 q 861 333 912 333 q 783 301 811 333 q 756 226 756 269 q 783 151 756 182 q 861 120 810 120 m 904 984 l 316 -28 l 205 -29 l 793 983 l 904 984 m 250 984 q 436 904 366 984 q 499 730 499 832 q 436 552 499 626 q 248 472 366 472 q 62 552 132 472 q 0 728 0 624 q 62 903 0 831 q 250 984 132 984 m 249 835 q 169 801 198 835 q 140 725 140 768 q 167 652 140 683 q 247 621 195 621 q 327 654 298 621 q 357 730 357 687 q 329 803 357 772 q 249 835 301 835 m 1430 484 q 1618 404 1548 484 q 1681 228 1681 332 q 1618 51 1681 123 q 1429 -29 1548 -29 q 1241 50 1309 -29 q 1179 227 1179 122 q 1241 403 1179 331 q 1430 484 1311 484 m 1431 120 q 1509 151 1481 120 q 1537 226 1537 183 q 1511 299 1537 270 q 1431 333 1482 333 q 1352 301 1380 333 q 1325 226 1325 269 q 1352 151 1325 182 q 1431 120 1379 120 "},"a":{"x_min":0,"x_max":700,"ha":786,"o":"m 700 0 l 488 0 q 465 93 469 45 q 365 5 427 37 q 233 -26 303 -26 q 65 37 130 -26 q 0 205 0 101 q 120 409 0 355 q 343 452 168 431 q 465 522 465 468 q 424 588 465 565 q 337 611 384 611 q 250 581 285 611 q 215 503 215 552 l 26 503 q 113 707 26 633 q 328 775 194 775 q 538 723 444 775 q 657 554 657 659 l 657 137 q 666 73 657 101 q 700 33 675 45 l 700 0 m 465 297 l 465 367 q 299 322 358 340 q 193 217 193 287 q 223 150 193 174 q 298 127 254 127 q 417 175 370 127 q 465 297 465 224 "},"—":{"x_min":0,"x_max":941.671875,"ha":1039,"o":"m 941 297 l 0 297 l 0 450 l 941 450 l 941 297 "},"=":{"x_min":29.171875,"x_max":798.609375,"ha":828,"o":"m 798 502 l 29 502 l 29 635 l 798 635 l 798 502 m 798 204 l 29 204 l 29 339 l 798 339 l 798 204 "},"N":{"x_min":0,"x_max":833,"ha":949,"o":"m 833 0 l 617 0 l 206 695 l 206 0 l 0 0 l 0 1013 l 216 1013 l 629 315 l 629 1013 l 833 1013 l 833 0 "},"ρ":{"x_min":0,"x_max":722,"ha":810,"o":"m 364 -17 q 271 0 313 -17 q 194 48 230 16 l 194 -278 l 0 -278 l 0 370 q 87 656 0 548 q 358 775 183 775 q 626 655 524 775 q 722 372 722 541 q 621 95 722 208 q 364 -17 520 -17 m 360 607 q 237 529 280 607 q 201 377 201 463 q 234 229 201 292 q 355 147 277 147 q 467 210 419 147 q 515 374 515 273 q 471 537 515 468 q 360 607 428 607 "},"2":{"x_min":64,"x_max":764,"ha":828,"o":"m 764 685 q 675 452 764 541 q 484 325 637 415 q 307 168 357 250 l 754 168 l 754 0 l 64 0 q 193 301 64 175 q 433 480 202 311 q 564 673 564 576 q 519 780 564 737 q 416 824 475 824 q 318 780 358 824 q 262 633 270 730 l 80 633 q 184 903 80 807 q 415 988 276 988 q 654 907 552 988 q 764 685 764 819 "},"¯":{"x_min":0,"x_max":775,"ha":771,"o":"m 775 958 l 0 958 l 0 1111 l 775 1111 l 775 958 "},"Z":{"x_min":0,"x_max":804.171875,"ha":906,"o":"m 804 836 l 251 182 l 793 182 l 793 0 l 0 0 l 0 176 l 551 830 l 11 830 l 11 1013 l 804 1013 l 804 836 "},"u":{"x_min":0,"x_max":668,"ha":782,"o":"m 668 0 l 474 0 l 474 89 q 363 9 425 37 q 233 -19 301 -19 q 61 53 123 -19 q 0 239 0 126 l 0 749 l 199 749 l 199 296 q 225 193 199 233 q 316 146 257 146 q 424 193 380 146 q 469 304 469 240 l 469 749 l 668 749 l 668 0 "},"k":{"x_min":0,"x_max":688.890625,"ha":771,"o":"m 688 0 l 450 0 l 270 316 l 196 237 l 196 0 l 0 0 l 0 1013 l 196 1013 l 196 483 l 433 748 l 675 748 l 413 469 l 688 0 "},"Η":{"x_min":0,"x_max":837,"ha":950,"o":"m 837 0 l 627 0 l 627 450 l 210 450 l 210 0 l 0 0 l 0 1013 l 210 1013 l 210 635 l 627 635 l 627 1013 l 837 1013 l 837 0 "},"Α":{"x_min":0,"x_max":966.671875,"ha":1043,"o":"m 966 0 l 747 0 l 679 208 l 286 208 l 218 0 l 0 0 l 361 1013 l 600 1013 l 966 0 m 623 376 l 480 809 l 340 376 l 623 376 "},"s":{"x_min":0,"x_max":681,"ha":775,"o":"m 681 229 q 568 33 681 105 q 340 -29 471 -29 q 107 39 202 -29 q 0 245 0 114 l 201 245 q 252 155 201 189 q 358 128 295 128 q 436 144 401 128 q 482 205 482 166 q 363 284 482 255 q 143 348 181 329 q 25 533 25 408 q 129 716 25 647 q 340 778 220 778 q 554 710 465 778 q 658 522 643 643 l 463 522 q 419 596 458 570 q 327 622 380 622 q 255 606 290 622 q 221 556 221 590 q 339 473 221 506 q 561 404 528 420 q 681 229 681 344 "},"B":{"x_min":0,"x_max":835,"ha":938,"o":"m 674 547 q 791 450 747 518 q 835 304 835 383 q 718 75 835 158 q 461 0 612 0 l 0 0 l 0 1013 l 477 1013 q 697 951 609 1013 q 797 754 797 880 q 765 630 797 686 q 674 547 734 575 m 438 621 q 538 646 495 621 q 590 730 590 676 q 537 814 590 785 q 436 838 494 838 l 199 838 l 199 621 l 438 621 m 445 182 q 561 211 513 182 q 618 311 618 247 q 565 410 618 375 q 444 446 512 446 l 199 446 l 199 182 l 445 182 "},"…":{"x_min":0,"x_max":819,"ha":963,"o":"m 206 0 l 0 0 l 0 207 l 206 207 l 206 0 m 512 0 l 306 0 l 306 207 l 512 207 l 512 0 m 819 0 l 613 0 l 613 207 l 819 207 l 819 0 "},"?":{"x_min":1,"x_max":687,"ha":785,"o":"m 687 734 q 621 563 687 634 q 501 454 560 508 q 436 293 436 386 l 251 293 l 251 391 q 363 557 251 462 q 476 724 476 653 q 432 827 476 788 q 332 866 389 866 q 238 827 275 866 q 195 699 195 781 l 1 699 q 110 955 1 861 q 352 1040 210 1040 q 582 963 489 1040 q 687 734 687 878 m 446 0 l 243 0 l 243 203 l 446 203 l 446 0 "},"H":{"x_min":0,"x_max":838,"ha":953,"o":"m 838 0 l 628 0 l 628 450 l 210 450 l 210 0 l 0 0 l 0 1013 l 210 1013 l 210 635 l 628 635 l 628 1013 l 838 1013 l 838 0 "},"ν":{"x_min":0,"x_max":740.28125,"ha":828,"o":"m 740 749 l 473 0 l 266 0 l 0 749 l 222 749 l 373 211 l 529 749 l 740 749 "},"c":{"x_min":0,"x_max":751.390625,"ha":828,"o":"m 751 282 q 625 58 725 142 q 384 -26 526 -26 q 107 84 215 -26 q 0 366 0 195 q 98 651 0 536 q 370 774 204 774 q 616 700 518 774 q 751 486 715 626 l 536 486 q 477 570 516 538 q 380 607 434 607 q 248 533 298 607 q 204 378 204 466 q 242 219 204 285 q 377 139 290 139 q 483 179 438 139 q 543 282 527 220 l 751 282 "},"¶":{"x_min":0,"x_max":566.671875,"ha":678,"o":"m 21 892 l 52 892 l 98 761 l 145 892 l 176 892 l 178 741 l 157 741 l 157 867 l 108 741 l 88 741 l 40 871 l 40 741 l 21 741 l 21 892 m 308 854 l 308 731 q 252 691 308 691 q 227 691 240 691 q 207 696 213 695 l 207 712 l 253 706 q 288 733 288 706 l 288 763 q 244 741 279 741 q 193 797 193 741 q 261 860 193 860 q 287 860 273 860 q 308 854 302 855 m 288 842 l 263 843 q 213 796 213 843 q 248 756 213 756 q 288 796 288 756 l 288 842 m 566 988 l 502 988 l 502 -1 l 439 -1 l 439 988 l 317 988 l 317 -1 l 252 -1 l 252 602 q 81 653 155 602 q 0 805 0 711 q 101 989 0 918 q 309 1053 194 1053 l 566 1053 l 566 988 "},"β":{"x_min":0,"x_max":703,"ha":789,"o":"m 510 539 q 651 429 600 501 q 703 262 703 357 q 617 53 703 136 q 404 -29 532 -29 q 199 51 279 -29 l 199 -278 l 0 -278 l 0 627 q 77 911 0 812 q 343 1021 163 1021 q 551 957 464 1021 q 649 769 649 886 q 613 638 649 697 q 510 539 577 579 m 344 136 q 452 181 408 136 q 497 291 497 227 q 435 409 497 369 q 299 444 381 444 l 299 600 q 407 634 363 600 q 452 731 452 669 q 417 820 452 784 q 329 857 382 857 q 217 775 246 857 q 199 622 199 725 l 199 393 q 221 226 199 284 q 344 136 254 136 "},"Μ":{"x_min":0,"x_max":1019,"ha":1132,"o":"m 1019 0 l 823 0 l 823 818 l 617 0 l 402 0 l 194 818 l 194 0 l 0 0 l 0 1013 l 309 1013 l 509 241 l 708 1013 l 1019 1013 l 1019 0 "},"Ό":{"x_min":0.15625,"x_max":1174,"ha":1271,"o":"m 676 -29 q 312 127 451 -29 q 179 505 179 277 q 311 883 179 733 q 676 1040 449 1040 q 1040 883 901 1040 q 1174 505 1174 733 q 1041 127 1174 277 q 676 -29 903 -29 m 676 154 q 890 266 811 154 q 961 506 961 366 q 891 745 961 648 q 676 857 812 857 q 462 747 541 857 q 392 506 392 648 q 461 266 392 365 q 676 154 540 154 m 314 1034 l 98 779 l 0 779 l 136 1034 l 314 1034 "},"Ή":{"x_min":0,"x_max":1248,"ha":1361,"o":"m 1248 0 l 1038 0 l 1038 450 l 621 450 l 621 0 l 411 0 l 411 1012 l 621 1012 l 621 635 l 1038 635 l 1038 1012 l 1248 1012 l 1248 0 m 313 1035 l 98 780 l 0 780 l 136 1035 l 313 1035 "},"•":{"x_min":-27.78125,"x_max":691.671875,"ha":775,"o":"m 691 508 q 588 252 691 358 q 331 147 486 147 q 77 251 183 147 q -27 508 -27 355 q 75 761 -27 655 q 331 868 179 868 q 585 763 479 868 q 691 508 691 658 "},"¥":{"x_min":0,"x_max":836,"ha":931,"o":"m 195 625 l 0 1013 l 208 1013 l 427 576 l 626 1013 l 836 1013 l 650 625 l 777 625 l 777 472 l 578 472 l 538 389 l 777 389 l 777 236 l 532 236 l 532 0 l 322 0 l 322 236 l 79 236 l 79 389 l 315 389 l 273 472 l 79 472 l 79 625 l 195 625 "},"(":{"x_min":0,"x_max":388.890625,"ha":486,"o":"m 388 -293 l 243 -293 q 70 14 130 -134 q 0 357 0 189 q 69 703 0 526 q 243 1013 129 856 l 388 1013 q 248 695 297 860 q 200 358 200 530 q 248 24 200 187 q 388 -293 297 -138 "},"U":{"x_min":0,"x_max":813,"ha":926,"o":"m 813 362 q 697 79 813 187 q 405 -29 582 -29 q 114 78 229 -29 q 0 362 0 186 l 0 1013 l 210 1013 l 210 387 q 260 226 210 291 q 408 154 315 154 q 554 226 500 154 q 603 387 603 291 l 603 1013 l 813 1013 l 813 362 "},"γ":{"x_min":0.0625,"x_max":729.234375,"ha":815,"o":"m 729 749 l 457 37 l 457 -278 l 257 -278 l 257 37 q 218 155 243 95 q 170 275 194 215 l 0 749 l 207 749 l 363 284 l 522 749 l 729 749 "},"α":{"x_min":-1,"x_max":722,"ha":835,"o":"m 722 0 l 531 0 l 530 101 q 433 8 491 41 q 304 -25 375 -25 q 72 104 157 -25 q -1 372 -1 216 q 72 643 -1 530 q 308 775 158 775 q 433 744 375 775 q 528 656 491 713 l 528 749 l 722 749 l 722 0 m 361 601 q 233 527 277 601 q 196 375 196 464 q 232 224 196 288 q 358 144 277 144 q 487 217 441 144 q 528 370 528 281 q 489 523 528 457 q 361 601 443 601 "},"F":{"x_min":0,"x_max":706.953125,"ha":778,"o":"m 706 837 l 206 837 l 206 606 l 645 606 l 645 431 l 206 431 l 206 0 l 0 0 l 0 1013 l 706 1013 l 706 837 "},"­":{"x_min":0,"x_max":704.171875,"ha":801,"o":"m 704 297 l 0 297 l 0 450 l 704 450 l 704 297 "},":":{"x_min":0,"x_max":207,"ha":304,"o":"m 207 528 l 0 528 l 0 735 l 207 735 l 207 528 m 207 0 l 0 0 l 0 207 l 207 207 l 207 0 "},"Χ":{"x_min":0,"x_max":894.453125,"ha":978,"o":"m 894 0 l 654 0 l 445 351 l 238 0 l 0 0 l 316 516 l 0 1013 l 238 1013 l 445 660 l 652 1013 l 894 1013 l 577 519 l 894 0 "},"*":{"x_min":115,"x_max":713,"ha":828,"o":"m 713 740 l 518 688 l 651 525 l 531 438 l 412 612 l 290 439 l 173 523 l 308 688 l 115 741 l 159 880 l 342 816 l 343 1013 l 482 1013 l 481 816 l 664 880 l 713 740 "},"†":{"x_min":0,"x_max":809,"ha":894,"o":"m 509 804 l 809 804 l 809 621 l 509 621 l 509 0 l 299 0 l 299 621 l 0 621 l 0 804 l 299 804 l 299 1011 l 509 1011 l 509 804 "},"°":{"x_min":-1,"x_max":363,"ha":460,"o":"m 181 808 q 46 862 94 808 q -1 992 -1 917 q 44 1118 -1 1066 q 181 1175 96 1175 q 317 1118 265 1175 q 363 991 363 1066 q 315 862 363 917 q 181 808 267 808 m 181 908 q 240 933 218 908 q 263 992 263 958 q 242 1051 263 1027 q 181 1075 221 1075 q 120 1050 142 1075 q 99 991 99 1026 q 120 933 99 958 q 181 908 142 908 "},"V":{"x_min":0,"x_max":895.828125,"ha":997,"o":"m 895 1013 l 550 0 l 347 0 l 0 1013 l 231 1013 l 447 256 l 666 1013 l 895 1013 "},"Ξ":{"x_min":0,"x_max":751.390625,"ha":800,"o":"m 733 826 l 5 826 l 5 1012 l 733 1012 l 733 826 m 681 432 l 65 432 l 65 617 l 681 617 l 681 432 m 751 0 l 0 0 l 0 183 l 751 183 l 751 0 "}," ":{"x_min":0,"x_max":0,"ha":853},"Ϋ":{"x_min":-0.21875,"x_max":836.171875,"ha":914,"o":"m 610 1046 l 454 1046 l 454 1215 l 610 1215 l 610 1046 m 369 1046 l 212 1046 l 212 1215 l 369 1215 l 369 1046 m 836 1012 l 532 376 l 532 0 l 322 0 l 322 376 l 0 1012 l 208 1012 l 427 576 l 626 1012 l 836 1012 "},"0":{"x_min":51,"x_max":779,"ha":828,"o":"m 415 -26 q 142 129 242 -26 q 51 476 51 271 q 141 825 51 683 q 415 984 242 984 q 687 825 585 984 q 779 476 779 682 q 688 131 779 271 q 415 -26 587 -26 m 415 137 q 529 242 485 137 q 568 477 568 338 q 530 713 568 619 q 415 821 488 821 q 303 718 344 821 q 262 477 262 616 q 301 237 262 337 q 415 137 341 137 "},"”":{"x_min":0,"x_max":469,"ha":567,"o":"m 192 834 q 137 692 192 751 q 0 626 83 634 l 0 697 q 101 831 101 723 l 0 831 l 0 1013 l 192 1013 l 192 834 m 469 834 q 414 692 469 751 q 277 626 360 634 l 277 697 q 379 831 379 723 l 277 831 l 277 1013 l 469 1013 l 469 834 "},"@":{"x_min":0,"x_max":1276,"ha":1374,"o":"m 1115 -52 q 895 -170 1015 -130 q 647 -211 776 -211 q 158 -34 334 -211 q 0 360 0 123 q 179 810 0 621 q 698 1019 377 1019 q 1138 859 981 1019 q 1276 514 1276 720 q 1173 210 1276 335 q 884 75 1062 75 q 784 90 810 75 q 737 186 749 112 q 647 104 698 133 q 532 75 596 75 q 360 144 420 75 q 308 308 308 205 q 398 568 308 451 q 638 696 497 696 q 731 671 690 696 q 805 604 772 647 l 840 673 l 964 673 q 886 373 915 490 q 856 239 856 257 q 876 201 856 214 q 920 188 895 188 q 1084 284 1019 188 q 1150 511 1150 380 q 1051 779 1150 672 q 715 905 934 905 q 272 734 439 905 q 121 363 121 580 q 250 41 121 170 q 647 -103 394 -103 q 863 -67 751 -103 q 1061 26 975 -32 l 1115 -52 m 769 483 q 770 500 770 489 q 733 567 770 539 q 651 596 695 596 q 508 504 566 596 q 457 322 457 422 q 483 215 457 256 q 561 175 509 175 q 671 221 625 175 q 733 333 718 268 l 769 483 "},"Ί":{"x_min":0,"x_max":619,"ha":732,"o":"m 313 1035 l 98 780 l 0 780 l 136 1035 l 313 1035 m 619 0 l 411 0 l 411 1012 l 619 1012 l 619 0 "},"i":{"x_min":14,"x_max":214,"ha":326,"o":"m 214 830 l 14 830 l 14 1013 l 214 1013 l 214 830 m 214 0 l 14 0 l 14 748 l 214 748 l 214 0 "},"Β":{"x_min":0,"x_max":835,"ha":961,"o":"m 675 547 q 791 450 747 518 q 835 304 835 383 q 718 75 835 158 q 461 0 612 0 l 0 0 l 0 1013 l 477 1013 q 697 951 609 1013 q 797 754 797 880 q 766 630 797 686 q 675 547 734 575 m 439 621 q 539 646 496 621 q 590 730 590 676 q 537 814 590 785 q 436 838 494 838 l 199 838 l 199 621 l 439 621 m 445 182 q 561 211 513 182 q 618 311 618 247 q 565 410 618 375 q 444 446 512 446 l 199 446 l 199 182 l 445 182 "},"υ":{"x_min":0,"x_max":656,"ha":767,"o":"m 656 416 q 568 55 656 145 q 326 -25 490 -25 q 59 97 137 -25 q 0 369 0 191 l 0 749 l 200 749 l 200 369 q 216 222 200 268 q 326 142 245 142 q 440 247 411 142 q 456 422 456 304 l 456 749 l 656 749 l 656 416 "},"]":{"x_min":0,"x_max":349,"ha":446,"o":"m 349 -300 l 0 -300 l 0 -154 l 163 -154 l 163 866 l 0 866 l 0 1013 l 349 1013 l 349 -300 "},"m":{"x_min":0,"x_max":1065,"ha":1174,"o":"m 1065 0 l 866 0 l 866 483 q 836 564 866 532 q 759 596 807 596 q 663 555 700 596 q 627 454 627 514 l 627 0 l 433 0 l 433 481 q 403 563 433 531 q 323 596 374 596 q 231 554 265 596 q 197 453 197 513 l 197 0 l 0 0 l 0 748 l 189 748 l 189 665 q 279 745 226 715 q 392 775 333 775 q 509 744 455 775 q 606 659 563 713 q 695 744 640 713 q 814 775 749 775 q 992 702 920 775 q 1065 523 1065 630 l 1065 0 "},"χ":{"x_min":0,"x_max":759.71875,"ha":847,"o":"m 759 -299 l 548 -299 l 379 66 l 215 -299 l 0 -299 l 261 233 l 13 749 l 230 749 l 379 400 l 527 749 l 738 749 l 500 238 l 759 -299 "},"8":{"x_min":57,"x_max":770,"ha":828,"o":"m 625 516 q 733 416 697 477 q 770 284 770 355 q 675 69 770 161 q 415 -29 574 -29 q 145 65 244 -29 q 57 273 57 150 q 93 413 57 350 q 204 516 130 477 q 112 609 142 556 q 83 718 83 662 q 177 905 83 824 q 414 986 272 986 q 650 904 555 986 q 745 715 745 822 q 716 608 745 658 q 625 516 688 558 m 414 590 q 516 624 479 590 q 553 706 553 659 q 516 791 553 755 q 414 828 480 828 q 311 792 348 828 q 275 706 275 757 q 310 624 275 658 q 414 590 345 590 m 413 135 q 527 179 487 135 q 564 279 564 218 q 525 386 564 341 q 411 436 482 436 q 298 387 341 436 q 261 282 261 344 q 300 178 261 222 q 413 135 340 135 "},"ί":{"x_min":42,"x_max":371.171875,"ha":389,"o":"m 242 0 l 42 0 l 42 748 l 242 748 l 242 0 m 371 1039 l 169 823 l 71 823 l 193 1039 l 371 1039 "},"Ζ":{"x_min":0,"x_max":804.171875,"ha":886,"o":"m 804 835 l 251 182 l 793 182 l 793 0 l 0 0 l 0 176 l 551 829 l 11 829 l 11 1012 l 804 1012 l 804 835 "},"R":{"x_min":0,"x_max":836.109375,"ha":947,"o":"m 836 0 l 608 0 q 588 53 596 20 q 581 144 581 86 q 581 179 581 162 q 581 215 581 197 q 553 345 581 306 q 428 393 518 393 l 208 393 l 208 0 l 0 0 l 0 1013 l 491 1013 q 720 944 630 1013 q 819 734 819 869 q 778 584 819 654 q 664 485 738 513 q 757 415 727 463 q 794 231 794 358 l 794 170 q 800 84 794 116 q 836 31 806 51 l 836 0 m 462 838 l 208 838 l 208 572 l 452 572 q 562 604 517 572 q 612 704 612 640 q 568 801 612 765 q 462 838 525 838 "},"o":{"x_min":0,"x_max":764,"ha":871,"o":"m 380 -26 q 105 86 211 -26 q 0 371 0 199 q 104 660 0 545 q 380 775 209 775 q 658 659 552 775 q 764 371 764 544 q 658 86 764 199 q 380 -26 552 -26 m 379 141 q 515 216 466 141 q 557 373 557 280 q 515 530 557 465 q 379 607 466 607 q 245 530 294 607 q 204 373 204 465 q 245 217 204 282 q 379 141 294 141 "},"5":{"x_min":59,"x_max":767,"ha":828,"o":"m 767 319 q 644 59 767 158 q 382 -29 533 -29 q 158 43 247 -29 q 59 264 59 123 l 252 264 q 295 165 252 201 q 400 129 339 129 q 512 172 466 129 q 564 308 564 220 q 514 437 564 387 q 398 488 464 488 q 329 472 361 488 q 271 420 297 456 l 93 428 l 157 958 l 722 958 l 722 790 l 295 790 l 271 593 q 348 635 306 621 q 431 649 389 649 q 663 551 560 649 q 767 319 767 453 "},"7":{"x_min":65.28125,"x_max":762.5,"ha":828,"o":"m 762 808 q 521 435 604 626 q 409 0 438 244 l 205 0 q 313 422 227 234 q 548 789 387 583 l 65 789 l 65 958 l 762 958 l 762 808 "},"K":{"x_min":0,"x_max":900,"ha":996,"o":"m 900 0 l 647 0 l 316 462 l 208 355 l 208 0 l 0 0 l 0 1013 l 208 1013 l 208 595 l 604 1013 l 863 1013 l 461 603 l 900 0 "},",":{"x_min":0,"x_max":206,"ha":303,"o":"m 206 5 q 150 -151 206 -88 q 0 -238 94 -213 l 0 -159 q 84 -100 56 -137 q 111 -2 111 -62 l 0 -2 l 0 205 l 206 205 l 206 5 "},"d":{"x_min":0,"x_max":722,"ha":836,"o":"m 722 0 l 530 0 l 530 101 q 303 -26 449 -26 q 72 103 155 -26 q 0 373 0 214 q 72 642 0 528 q 305 775 156 775 q 433 743 373 775 q 530 656 492 712 l 530 1013 l 722 1013 l 722 0 m 361 600 q 234 523 280 600 q 196 372 196 458 q 233 220 196 286 q 358 143 278 143 q 489 216 442 143 q 530 369 530 280 q 491 522 530 456 q 361 600 443 600 "},"¨":{"x_min":212,"x_max":609,"ha":933,"o":"m 609 1046 l 453 1046 l 453 1216 l 609 1216 l 609 1046 m 369 1046 l 212 1046 l 212 1216 l 369 1216 l 369 1046 "},"E":{"x_min":0,"x_max":761.109375,"ha":824,"o":"m 761 0 l 0 0 l 0 1013 l 734 1013 l 734 837 l 206 837 l 206 621 l 690 621 l 690 446 l 206 446 l 206 186 l 761 186 l 761 0 "},"Y":{"x_min":0,"x_max":836,"ha":931,"o":"m 836 1013 l 532 376 l 532 0 l 322 0 l 322 376 l 0 1013 l 208 1013 l 427 576 l 626 1013 l 836 1013 "},"\"":{"x_min":0,"x_max":357,"ha":454,"o":"m 357 604 l 225 604 l 225 988 l 357 988 l 357 604 m 132 604 l 0 604 l 0 988 l 132 988 l 132 604 "},"‹":{"x_min":35.984375,"x_max":791.671875,"ha":828,"o":"m 791 17 l 36 352 l 35 487 l 791 823 l 791 672 l 229 421 l 791 168 l 791 17 "},"„":{"x_min":0,"x_max":483,"ha":588,"o":"m 206 5 q 150 -151 206 -88 q 0 -238 94 -213 l 0 -159 q 84 -100 56 -137 q 111 -2 111 -62 l 0 -2 l 0 205 l 206 205 l 206 5 m 483 5 q 427 -151 483 -88 q 277 -238 371 -213 l 277 -159 q 361 -100 334 -137 q 388 -2 388 -62 l 277 -2 l 277 205 l 483 205 l 483 5 "},"δ":{"x_min":6,"x_max":732,"ha":835,"o":"m 732 352 q 630 76 732 177 q 354 -25 529 -25 q 101 74 197 -25 q 6 333 6 174 q 89 581 6 480 q 323 690 178 690 q 66 864 201 787 l 66 1013 l 669 1013 l 669 856 l 348 856 q 532 729 461 789 q 673 566 625 651 q 732 352 732 465 m 419 551 q 259 496 321 551 q 198 344 198 441 q 238 208 198 267 q 357 140 283 140 q 484 203 437 140 q 526 344 526 260 q 499 466 526 410 q 419 551 473 521 "},"έ":{"x_min":16.671875,"x_max":652.78125,"ha":742,"o":"m 652 259 q 565 49 652 123 q 340 -25 479 -25 q 102 39 188 -25 q 16 197 16 104 q 45 299 16 250 q 134 390 75 348 q 58 456 86 419 q 25 552 25 502 q 120 717 25 653 q 322 776 208 776 q 537 710 456 776 q 625 508 625 639 l 445 508 q 415 585 445 563 q 327 608 386 608 q 254 590 293 608 q 215 544 215 573 q 252 469 215 490 q 336 453 280 453 q 369 455 347 453 q 400 456 391 456 l 400 308 l 329 308 q 247 291 280 308 q 204 223 204 269 q 255 154 204 172 q 345 143 286 143 q 426 174 398 143 q 454 259 454 206 l 652 259 m 579 1039 l 377 823 l 279 823 l 401 1039 l 579 1039 "},"ω":{"x_min":0,"x_max":945,"ha":1051,"o":"m 565 323 l 565 289 q 577 190 565 221 q 651 142 597 142 q 718 189 694 142 q 742 365 742 237 q 703 565 742 462 q 610 749 671 650 l 814 749 q 910 547 876 650 q 945 337 945 444 q 874 96 945 205 q 668 -29 793 -29 q 551 0 608 -29 q 470 78 495 30 q 390 0 444 28 q 276 -29 337 -29 q 69 96 149 -29 q 0 337 0 204 q 36 553 0 444 q 130 749 68 650 l 334 749 q 241 565 273 650 q 203 365 203 461 q 219 222 203 279 q 292 142 243 142 q 360 183 342 142 q 373 271 373 211 q 372 298 372 285 q 372 323 372 311 l 372 528 l 566 528 l 565 323 "},"´":{"x_min":0,"x_max":132,"ha":299,"o":"m 132 604 l 0 604 l 0 988 l 132 988 l 132 604 "},"±":{"x_min":29,"x_max":798,"ha":828,"o":"m 798 480 l 484 480 l 484 254 l 344 254 l 344 480 l 29 480 l 29 615 l 344 615 l 344 842 l 484 842 l 484 615 l 798 615 l 798 480 m 798 0 l 29 0 l 29 136 l 798 136 l 798 0 "},"|":{"x_min":0,"x_max":143,"ha":240,"o":"m 143 462 l 0 462 l 0 984 l 143 984 l 143 462 m 143 -242 l 0 -242 l 0 280 l 143 280 l 143 -242 "},"ϋ":{"x_min":0,"x_max":656,"ha":767,"o":"m 535 810 l 406 810 l 406 952 l 535 952 l 535 810 m 271 810 l 142 810 l 142 952 l 271 952 l 271 810 m 656 417 q 568 55 656 146 q 326 -25 490 -25 q 59 97 137 -25 q 0 369 0 192 l 0 748 l 200 748 l 200 369 q 216 222 200 268 q 326 142 245 142 q 440 247 411 142 q 456 422 456 304 l 456 748 l 656 748 l 656 417 "},"§":{"x_min":0,"x_max":633,"ha":731,"o":"m 633 469 q 601 356 633 406 q 512 274 569 305 q 570 197 548 242 q 593 105 593 152 q 501 -76 593 -5 q 301 -142 416 -142 q 122 -82 193 -142 q 43 108 43 -15 l 212 108 q 251 27 220 53 q 321 1 283 1 q 389 23 360 1 q 419 83 419 46 q 310 194 419 139 q 108 297 111 295 q 0 476 0 372 q 33 584 0 537 q 120 659 62 626 q 72 720 91 686 q 53 790 53 755 q 133 978 53 908 q 312 1042 207 1042 q 483 984 412 1042 q 574 807 562 921 l 409 807 q 379 875 409 851 q 307 900 349 900 q 244 881 270 900 q 218 829 218 862 q 324 731 218 781 q 524 636 506 647 q 633 469 633 565 m 419 334 q 473 411 473 372 q 451 459 473 436 q 390 502 430 481 l 209 595 q 167 557 182 577 q 153 520 153 537 q 187 461 153 491 q 263 413 212 440 l 419 334 "},"b":{"x_min":0,"x_max":722,"ha":822,"o":"m 416 -26 q 289 6 346 -26 q 192 101 232 39 l 192 0 l 0 0 l 0 1013 l 192 1013 l 192 656 q 286 743 226 712 q 415 775 346 775 q 649 644 564 775 q 722 374 722 533 q 649 106 722 218 q 416 -26 565 -26 m 361 600 q 232 524 279 600 q 192 371 192 459 q 229 221 192 284 q 357 145 275 145 q 487 221 441 145 q 526 374 526 285 q 488 523 526 460 q 361 600 442 600 "},"q":{"x_min":0,"x_max":722,"ha":833,"o":"m 722 -298 l 530 -298 l 530 97 q 306 -25 449 -25 q 73 104 159 -25 q 0 372 0 216 q 72 643 0 529 q 305 775 156 775 q 430 742 371 775 q 530 654 488 709 l 530 750 l 722 750 l 722 -298 m 360 601 q 234 527 278 601 q 197 378 197 466 q 233 225 197 291 q 357 144 277 144 q 488 217 441 144 q 530 370 530 282 q 491 523 530 459 q 360 601 443 601 "},"Ω":{"x_min":-0.03125,"x_max":1008.53125,"ha":1108,"o":"m 1008 0 l 589 0 l 589 199 q 717 368 670 265 q 764 580 764 471 q 698 778 764 706 q 504 855 629 855 q 311 773 380 855 q 243 563 243 691 q 289 360 243 458 q 419 199 336 262 l 419 0 l 0 0 l 0 176 l 202 176 q 77 355 123 251 q 32 569 32 459 q 165 908 32 776 q 505 1040 298 1040 q 844 912 711 1040 q 977 578 977 785 q 931 362 977 467 q 805 176 886 256 l 1008 176 l 1008 0 "},"ύ":{"x_min":0,"x_max":656,"ha":767,"o":"m 656 417 q 568 55 656 146 q 326 -25 490 -25 q 59 97 137 -25 q 0 369 0 192 l 0 748 l 200 748 l 201 369 q 218 222 201 269 q 326 142 245 142 q 440 247 411 142 q 456 422 456 304 l 456 748 l 656 748 l 656 417 m 579 1039 l 378 823 l 279 823 l 401 1039 l 579 1039 "},"z":{"x_min":0,"x_max":663.890625,"ha":753,"o":"m 663 0 l 0 0 l 0 154 l 411 591 l 25 591 l 25 749 l 650 749 l 650 584 l 245 165 l 663 165 l 663 0 "},"™":{"x_min":0,"x_max":951,"ha":1063,"o":"m 405 921 l 255 921 l 255 506 l 149 506 l 149 921 l 0 921 l 0 1013 l 405 1013 l 405 921 m 951 506 l 852 506 l 852 916 l 750 506 l 643 506 l 539 915 l 539 506 l 442 506 l 442 1013 l 595 1012 l 695 625 l 794 1013 l 951 1013 l 951 506 "},"ή":{"x_min":0,"x_max":669,"ha":779,"o":"m 669 -278 l 469 -278 l 469 390 q 448 526 469 473 q 348 606 417 606 q 244 553 288 606 q 201 441 201 501 l 201 0 l 0 0 l 0 749 l 201 749 l 201 665 q 301 744 244 715 q 423 774 359 774 q 606 685 538 774 q 669 484 669 603 l 669 -278 m 495 1039 l 293 823 l 195 823 l 317 1039 l 495 1039 "},"Θ":{"x_min":0,"x_max":993,"ha":1092,"o":"m 497 -29 q 133 127 272 -29 q 0 505 0 277 q 133 883 0 733 q 497 1040 272 1040 q 861 883 722 1040 q 993 505 993 733 q 861 127 993 277 q 497 -29 722 -29 m 497 154 q 711 266 631 154 q 782 506 782 367 q 712 746 782 648 q 497 858 634 858 q 281 746 361 858 q 211 506 211 648 q 280 266 211 365 q 497 154 359 154 m 676 430 l 316 430 l 316 593 l 676 593 l 676 430 "},"®":{"x_min":3,"x_max":1007,"ha":1104,"o":"m 507 -6 q 129 153 269 -6 q 3 506 3 298 q 127 857 3 713 q 502 1017 266 1017 q 880 855 740 1017 q 1007 502 1007 711 q 882 152 1007 295 q 507 -6 743 -6 m 502 934 q 184 800 302 934 q 79 505 79 680 q 184 210 79 331 q 501 76 302 76 q 819 210 701 76 q 925 507 925 331 q 820 800 925 682 q 502 934 704 934 m 782 190 l 639 190 q 627 225 632 202 q 623 285 623 248 l 623 326 q 603 411 623 384 q 527 439 584 439 l 388 439 l 388 190 l 257 190 l 257 829 l 566 829 q 709 787 654 829 q 772 654 772 740 q 746 559 772 604 q 675 497 720 514 q 735 451 714 483 q 756 341 756 419 l 756 299 q 760 244 756 265 q 782 212 764 223 l 782 190 m 546 718 l 388 718 l 388 552 l 541 552 q 612 572 584 552 q 641 635 641 593 q 614 695 641 672 q 546 718 587 718 "},"~":{"x_min":0,"x_max":851,"ha":949,"o":"m 851 968 q 795 750 851 831 q 599 656 730 656 q 406 744 506 656 q 259 832 305 832 q 162 775 193 832 q 139 656 139 730 l 0 656 q 58 871 0 787 q 251 968 124 968 q 442 879 341 968 q 596 791 544 791 q 691 849 663 791 q 712 968 712 892 l 851 968 "},"Ε":{"x_min":0,"x_max":761.546875,"ha":824,"o":"m 761 0 l 0 0 l 0 1012 l 735 1012 l 735 836 l 206 836 l 206 621 l 690 621 l 690 446 l 206 446 l 206 186 l 761 186 l 761 0 "},"³":{"x_min":0,"x_max":467,"ha":564,"o":"m 467 555 q 393 413 467 466 q 229 365 325 365 q 70 413 134 365 q 0 565 0 467 l 123 565 q 163 484 131 512 q 229 461 190 461 q 299 486 269 461 q 329 553 329 512 q 281 627 329 607 q 187 641 248 641 l 187 722 q 268 737 237 722 q 312 804 312 758 q 285 859 312 837 q 224 882 259 882 q 165 858 189 882 q 135 783 140 834 l 12 783 q 86 930 20 878 q 230 976 145 976 q 379 931 314 976 q 444 813 444 887 q 423 744 444 773 q 365 695 402 716 q 439 640 412 676 q 467 555 467 605 "},"[":{"x_min":0,"x_max":347.21875,"ha":444,"o":"m 347 -300 l 0 -300 l 0 1013 l 347 1013 l 347 866 l 188 866 l 188 -154 l 347 -154 l 347 -300 "},"L":{"x_min":0,"x_max":704.171875,"ha":763,"o":"m 704 0 l 0 0 l 0 1013 l 208 1013 l 208 186 l 704 186 l 704 0 "},"σ":{"x_min":0,"x_max":851.3125,"ha":940,"o":"m 851 594 l 712 594 q 761 369 761 485 q 658 83 761 191 q 379 -25 555 -25 q 104 87 208 -25 q 0 372 0 200 q 103 659 0 544 q 378 775 207 775 q 464 762 407 775 q 549 750 521 750 l 851 750 l 851 594 m 379 142 q 515 216 466 142 q 557 373 557 280 q 515 530 557 465 q 379 608 465 608 q 244 530 293 608 q 203 373 203 465 q 244 218 203 283 q 379 142 293 142 "},"ζ":{"x_min":0,"x_max":622,"ha":701,"o":"m 622 -32 q 604 -158 622 -98 q 551 -278 587 -218 l 373 -278 q 426 -180 406 -229 q 446 -80 446 -131 q 421 -22 446 -37 q 354 -8 397 -8 q 316 -9 341 -8 q 280 -11 291 -11 q 75 69 150 -11 q 0 283 0 150 q 87 596 0 437 q 291 856 162 730 l 47 856 l 47 1013 l 592 1013 l 592 904 q 317 660 422 800 q 197 318 197 497 q 306 141 197 169 q 510 123 408 131 q 622 -32 622 102 "},"θ":{"x_min":0,"x_max":714,"ha":817,"o":"m 357 1022 q 633 833 534 1022 q 714 486 714 679 q 634 148 714 288 q 354 -25 536 -25 q 79 147 175 -25 q 0 481 0 288 q 79 831 0 679 q 357 1022 177 1022 m 510 590 q 475 763 510 687 q 351 862 430 862 q 233 763 272 862 q 204 590 204 689 l 510 590 m 510 440 l 204 440 q 233 251 204 337 q 355 131 274 131 q 478 248 434 131 q 510 440 510 337 "},"Ο":{"x_min":0,"x_max":995,"ha":1092,"o":"m 497 -29 q 133 127 272 -29 q 0 505 0 277 q 132 883 0 733 q 497 1040 270 1040 q 861 883 722 1040 q 995 505 995 733 q 862 127 995 277 q 497 -29 724 -29 m 497 154 q 711 266 632 154 q 781 506 781 365 q 711 745 781 647 q 497 857 632 857 q 283 747 361 857 q 213 506 213 647 q 282 266 213 365 q 497 154 361 154 "},"Γ":{"x_min":0,"x_max":703.84375,"ha":742,"o":"m 703 836 l 208 836 l 208 0 l 0 0 l 0 1012 l 703 1012 l 703 836 "}," ":{"x_min":0,"x_max":0,"ha":375},"%":{"x_min":0,"x_max":1111,"ha":1213,"o":"m 861 484 q 1048 404 979 484 q 1111 228 1111 332 q 1048 51 1111 123 q 859 -29 979 -29 q 672 50 740 -29 q 610 227 610 122 q 672 403 610 331 q 861 484 741 484 m 861 120 q 939 151 911 120 q 967 226 967 183 q 942 299 967 270 q 861 333 912 333 q 783 301 811 333 q 756 226 756 269 q 783 151 756 182 q 861 120 810 120 m 904 984 l 316 -28 l 205 -29 l 793 983 l 904 984 m 250 984 q 436 904 366 984 q 499 730 499 832 q 436 552 499 626 q 248 472 366 472 q 62 552 132 472 q 0 728 0 624 q 62 903 0 831 q 250 984 132 984 m 249 835 q 169 801 198 835 q 140 725 140 768 q 167 652 140 683 q 247 621 195 621 q 327 654 298 621 q 357 730 357 687 q 329 803 357 772 q 249 835 301 835 "},"P":{"x_min":0,"x_max":771,"ha":838,"o":"m 208 361 l 208 0 l 0 0 l 0 1013 l 450 1013 q 682 919 593 1013 q 771 682 771 826 q 687 452 771 544 q 466 361 604 361 l 208 361 m 421 837 l 208 837 l 208 544 l 410 544 q 525 579 480 544 q 571 683 571 615 q 527 792 571 747 q 421 837 484 837 "},"Έ":{"x_min":0,"x_max":1172.546875,"ha":1235,"o":"m 1172 0 l 411 0 l 411 1012 l 1146 1012 l 1146 836 l 617 836 l 617 621 l 1101 621 l 1101 446 l 617 446 l 617 186 l 1172 186 l 1172 0 m 313 1035 l 98 780 l 0 780 l 136 1035 l 313 1035 "},"Ώ":{"x_min":0.4375,"x_max":1189.546875,"ha":1289,"o":"m 1189 0 l 770 0 l 770 199 q 897 369 849 263 q 945 580 945 474 q 879 778 945 706 q 685 855 810 855 q 492 773 561 855 q 424 563 424 691 q 470 360 424 458 q 600 199 517 262 l 600 0 l 180 0 l 180 176 l 383 176 q 258 355 304 251 q 213 569 213 459 q 346 908 213 776 q 686 1040 479 1040 q 1025 912 892 1040 q 1158 578 1158 785 q 1112 362 1158 467 q 986 176 1067 256 l 1189 176 l 1189 0 m 314 1092 l 99 837 l 0 837 l 136 1092 l 314 1092 "},"_":{"x_min":61.109375,"x_max":766.671875,"ha":828,"o":"m 766 -333 l 61 -333 l 61 -190 l 766 -190 l 766 -333 "},"Ϊ":{"x_min":-56,"x_max":342,"ha":503,"o":"m 342 1046 l 186 1046 l 186 1215 l 342 1215 l 342 1046 m 101 1046 l -56 1046 l -56 1215 l 101 1215 l 101 1046 m 249 0 l 41 0 l 41 1012 l 249 1012 l 249 0 "},"+":{"x_min":43,"x_max":784,"ha":828,"o":"m 784 353 l 483 353 l 483 0 l 343 0 l 343 353 l 43 353 l 43 489 l 343 489 l 343 840 l 483 840 l 483 489 l 784 489 l 784 353 "},"½":{"x_min":0,"x_max":1090,"ha":1188,"o":"m 1090 380 q 992 230 1090 301 q 779 101 886 165 q 822 94 784 95 q 924 93 859 93 l 951 93 l 973 93 l 992 93 l 1009 93 q 1046 93 1027 93 q 1085 93 1066 93 l 1085 0 l 650 0 l 654 38 q 815 233 665 137 q 965 376 965 330 q 936 436 965 412 q 869 461 908 461 q 806 435 831 461 q 774 354 780 409 l 659 354 q 724 505 659 451 q 870 554 783 554 q 1024 506 958 554 q 1090 380 1090 459 m 868 998 l 268 -28 l 154 -27 l 757 999 l 868 998 m 272 422 l 147 422 l 147 799 l 0 799 l 0 875 q 126 900 91 875 q 170 973 162 926 l 272 973 l 272 422 "},"Ρ":{"x_min":0,"x_max":771,"ha":838,"o":"m 208 361 l 208 0 l 0 0 l 0 1012 l 450 1012 q 682 919 593 1012 q 771 681 771 826 q 687 452 771 544 q 466 361 604 361 l 208 361 m 422 836 l 209 836 l 209 544 l 410 544 q 525 579 480 544 q 571 683 571 614 q 527 791 571 747 q 422 836 484 836 "},"'":{"x_min":0,"x_max":192,"ha":289,"o":"m 192 834 q 137 692 192 751 q 0 626 82 632 l 0 697 q 101 830 101 726 l 0 830 l 0 1013 l 192 1013 l 192 834 "},"ª":{"x_min":0,"x_max":350,"ha":393,"o":"m 350 625 l 245 625 q 237 648 241 636 q 233 672 233 661 q 117 611 192 611 q 33 643 66 611 q 0 727 0 675 q 116 846 0 828 q 233 886 233 864 q 211 919 233 907 q 168 931 190 931 q 108 877 108 931 l 14 877 q 56 977 14 942 q 165 1013 98 1013 q 270 987 224 1013 q 329 903 329 955 l 329 694 q 332 661 329 675 q 350 641 336 648 l 350 625 m 233 774 l 233 809 q 151 786 180 796 q 97 733 97 768 q 111 700 97 712 q 149 689 126 689 q 210 713 187 689 q 233 774 233 737 "},"΅":{"x_min":57,"x_max":584,"ha":753,"o":"m 584 810 l 455 810 l 455 952 l 584 952 l 584 810 m 521 1064 l 305 810 l 207 810 l 343 1064 l 521 1064 m 186 810 l 57 810 l 57 952 l 186 952 l 186 810 "},"T":{"x_min":0,"x_max":809,"ha":894,"o":"m 809 831 l 509 831 l 509 0 l 299 0 l 299 831 l 0 831 l 0 1013 l 809 1013 l 809 831 "},"Φ":{"x_min":0,"x_max":949,"ha":1032,"o":"m 566 0 l 385 0 l 385 121 q 111 230 222 121 q 0 508 0 340 q 112 775 0 669 q 385 892 219 875 l 385 1013 l 566 1013 l 566 892 q 836 776 732 875 q 949 507 949 671 q 838 231 949 341 q 566 121 728 121 l 566 0 m 566 285 q 701 352 650 285 q 753 508 753 419 q 703 658 753 597 q 566 729 653 720 l 566 285 m 385 285 l 385 729 q 245 661 297 717 q 193 516 193 604 q 246 356 193 427 q 385 285 300 285 "},"j":{"x_min":-45.828125,"x_max":242,"ha":361,"o":"m 242 830 l 42 830 l 42 1013 l 242 1013 l 242 830 m 242 -119 q 180 -267 242 -221 q 20 -308 127 -308 l -45 -308 l -45 -140 l -24 -140 q 25 -130 8 -140 q 42 -88 42 -120 l 42 748 l 242 748 l 242 -119 "},"Σ":{"x_min":0,"x_max":772.21875,"ha":849,"o":"m 772 0 l 0 0 l 0 140 l 368 526 l 18 862 l 18 1012 l 740 1012 l 740 836 l 315 836 l 619 523 l 298 175 l 772 175 l 772 0 "},"1":{"x_min":197.609375,"x_max":628,"ha":828,"o":"m 628 0 l 434 0 l 434 674 l 197 674 l 197 810 q 373 837 318 810 q 468 984 450 876 l 628 984 l 628 0 "},"›":{"x_min":36.109375,"x_max":792,"ha":828,"o":"m 792 352 l 36 17 l 36 168 l 594 420 l 36 672 l 36 823 l 792 487 l 792 352 "},"<":{"x_min":35.984375,"x_max":791.671875,"ha":828,"o":"m 791 17 l 36 352 l 35 487 l 791 823 l 791 672 l 229 421 l 791 168 l 791 17 "},"£":{"x_min":0,"x_max":716.546875,"ha":814,"o":"m 716 38 q 603 -9 658 5 q 502 -24 548 -24 q 398 -10 451 -24 q 239 25 266 25 q 161 12 200 25 q 77 -29 122 0 l 0 113 q 110 211 81 174 q 151 315 151 259 q 117 440 151 365 l 0 440 l 0 515 l 73 515 q 35 610 52 560 q 15 710 15 671 q 119 910 15 831 q 349 984 216 984 q 570 910 480 984 q 693 668 674 826 l 501 668 q 455 791 501 746 q 353 830 414 830 q 256 795 298 830 q 215 705 215 760 q 249 583 215 655 q 283 515 266 548 l 479 515 l 479 440 l 309 440 q 316 394 313 413 q 319 355 319 374 q 287 241 319 291 q 188 135 263 205 q 262 160 225 152 q 332 168 298 168 q 455 151 368 168 q 523 143 500 143 q 588 152 558 143 q 654 189 617 162 l 716 38 "},"t":{"x_min":0,"x_max":412,"ha":511,"o":"m 412 -6 q 349 -8 391 -6 q 287 -11 307 -11 q 137 38 177 -11 q 97 203 97 87 l 97 609 l 0 609 l 0 749 l 97 749 l 97 951 l 297 951 l 297 749 l 412 749 l 412 609 l 297 609 l 297 191 q 315 152 297 162 q 366 143 334 143 q 389 143 378 143 q 412 143 400 143 l 412 -6 "},"¬":{"x_min":0,"x_max":704,"ha":801,"o":"m 704 93 l 551 93 l 551 297 l 0 297 l 0 450 l 704 450 l 704 93 "},"λ":{"x_min":0,"x_max":701.390625,"ha":775,"o":"m 701 0 l 491 0 l 345 444 l 195 0 l 0 0 l 238 697 l 131 1013 l 334 1013 l 701 0 "},"W":{"x_min":0,"x_max":1291.671875,"ha":1399,"o":"m 1291 1013 l 1002 0 l 802 0 l 645 777 l 490 0 l 288 0 l 0 1013 l 215 1013 l 388 298 l 534 1012 l 757 1013 l 904 299 l 1076 1013 l 1291 1013 "},">":{"x_min":36.109375,"x_max":792,"ha":828,"o":"m 792 352 l 36 17 l 36 168 l 594 420 l 36 672 l 36 823 l 792 487 l 792 352 "},"v":{"x_min":0,"x_max":740.28125,"ha":828,"o":"m 740 749 l 473 0 l 266 0 l 0 749 l 222 749 l 373 211 l 529 749 l 740 749 "},"τ":{"x_min":0.28125,"x_max":618.734375,"ha":699,"o":"m 618 593 l 409 593 l 409 0 l 210 0 l 210 593 l 0 593 l 0 749 l 618 749 l 618 593 "},"ξ":{"x_min":0,"x_max":640,"ha":715,"o":"m 640 -14 q 619 -157 640 -84 q 563 -299 599 -230 l 399 -299 q 442 -194 433 -223 q 468 -85 468 -126 q 440 -25 468 -41 q 368 -10 412 -10 q 333 -11 355 -10 q 302 -13 311 -13 q 91 60 179 -13 q 0 259 0 138 q 56 426 0 354 q 201 530 109 493 q 106 594 144 553 q 65 699 65 642 q 94 787 65 747 q 169 856 123 828 l 22 856 l 22 1013 l 597 1013 l 597 856 l 497 857 q 345 840 398 857 q 257 736 257 812 q 366 614 257 642 q 552 602 416 602 l 552 446 l 513 446 q 313 425 379 446 q 199 284 199 389 q 312 162 199 184 q 524 136 418 148 q 640 -14 640 105 "},"&":{"x_min":-1,"x_max":910.109375,"ha":1007,"o":"m 910 -1 l 676 -1 l 607 83 q 291 -47 439 -47 q 50 100 135 -47 q -1 273 -1 190 q 51 431 -1 357 q 218 568 104 505 q 151 661 169 629 q 120 769 120 717 q 201 951 120 885 q 382 1013 276 1013 q 555 957 485 1013 q 635 789 635 894 q 584 644 635 709 q 468 539 548 597 l 615 359 q 664 527 654 440 l 844 527 q 725 223 824 359 l 910 -1 m 461 787 q 436 848 461 826 q 381 870 412 870 q 325 849 349 870 q 301 792 301 829 q 324 719 301 757 q 372 660 335 703 q 430 714 405 680 q 461 787 461 753 m 500 214 l 318 441 q 198 286 198 363 q 225 204 198 248 q 347 135 268 135 q 425 153 388 135 q 500 214 462 172 "},"Λ":{"x_min":0,"x_max":894.453125,"ha":974,"o":"m 894 0 l 666 0 l 447 757 l 225 0 l 0 0 l 344 1013 l 547 1013 l 894 0 "},"I":{"x_min":41,"x_max":249,"ha":365,"o":"m 249 0 l 41 0 l 41 1013 l 249 1013 l 249 0 "},"G":{"x_min":0,"x_max":971,"ha":1057,"o":"m 971 -1 l 829 -1 l 805 118 q 479 -29 670 -29 q 126 133 261 -29 q 0 509 0 286 q 130 884 0 737 q 493 1040 268 1040 q 790 948 659 1040 q 961 698 920 857 l 736 698 q 643 813 709 769 q 500 857 578 857 q 285 746 364 857 q 213 504 213 644 q 285 263 213 361 q 505 154 365 154 q 667 217 598 154 q 761 374 736 280 l 548 374 l 548 548 l 971 548 l 971 -1 "},"ΰ":{"x_min":0,"x_max":655,"ha":767,"o":"m 583 810 l 454 810 l 454 952 l 583 952 l 583 810 m 186 810 l 57 809 l 57 952 l 186 952 l 186 810 m 516 1039 l 315 823 l 216 823 l 338 1039 l 516 1039 m 655 417 q 567 55 655 146 q 326 -25 489 -25 q 59 97 137 -25 q 0 369 0 192 l 0 748 l 200 748 l 201 369 q 218 222 201 269 q 326 142 245 142 q 439 247 410 142 q 455 422 455 304 l 455 748 l 655 748 l 655 417 "},"`":{"x_min":0,"x_max":190,"ha":288,"o":"m 190 654 l 0 654 l 0 830 q 55 970 0 909 q 190 1040 110 1031 l 190 969 q 111 922 134 952 q 88 836 88 892 l 190 836 l 190 654 "},"·":{"x_min":0,"x_max":207,"ha":304,"o":"m 207 528 l 0 528 l 0 735 l 207 735 l 207 528 "},"Υ":{"x_min":-0.21875,"x_max":836.171875,"ha":914,"o":"m 836 1013 l 532 376 l 532 0 l 322 0 l 322 376 l 0 1013 l 208 1013 l 427 576 l 626 1013 l 836 1013 "},"r":{"x_min":0,"x_max":431.9375,"ha":513,"o":"m 431 564 q 269 536 320 564 q 200 395 200 498 l 200 0 l 0 0 l 0 748 l 183 748 l 183 618 q 285 731 224 694 q 431 768 345 768 l 431 564 "},"x":{"x_min":0,"x_max":738.890625,"ha":826,"o":"m 738 0 l 504 0 l 366 238 l 230 0 l 0 0 l 252 382 l 11 749 l 238 749 l 372 522 l 502 749 l 725 749 l 488 384 l 738 0 "},"μ":{"x_min":0,"x_max":647,"ha":754,"o":"m 647 0 l 477 0 l 477 68 q 411 9 448 30 q 330 -11 374 -11 q 261 3 295 -11 q 199 43 226 18 l 199 -278 l 0 -278 l 0 749 l 199 749 l 199 358 q 216 222 199 268 q 322 152 244 152 q 435 240 410 152 q 448 401 448 283 l 448 749 l 647 749 l 647 0 "},"h":{"x_min":0,"x_max":669,"ha":782,"o":"m 669 0 l 469 0 l 469 390 q 449 526 469 472 q 353 607 420 607 q 248 554 295 607 q 201 441 201 501 l 201 0 l 0 0 l 0 1013 l 201 1013 l 201 665 q 303 743 245 715 q 425 772 362 772 q 609 684 542 772 q 669 484 669 605 l 669 0 "},".":{"x_min":0,"x_max":206,"ha":303,"o":"m 206 0 l 0 0 l 0 207 l 206 207 l 206 0 "},"φ":{"x_min":-1,"x_max":921,"ha":990,"o":"m 542 -278 l 367 -278 l 367 -22 q 99 92 200 -22 q -1 376 -1 206 q 72 627 -1 520 q 288 769 151 742 l 288 581 q 222 495 243 550 q 202 378 202 439 q 240 228 202 291 q 367 145 285 157 l 367 776 l 515 776 q 807 667 694 776 q 921 379 921 558 q 815 93 921 209 q 542 -22 709 -22 l 542 -278 m 542 145 q 672 225 625 145 q 713 381 713 291 q 671 536 713 470 q 542 611 624 611 l 542 145 "},";":{"x_min":0,"x_max":208,"ha":306,"o":"m 208 528 l 0 528 l 0 735 l 208 735 l 208 528 m 208 6 q 152 -151 208 -89 q 0 -238 96 -212 l 0 -158 q 87 -100 61 -136 q 113 0 113 -65 l 0 0 l 0 207 l 208 207 l 208 6 "},"f":{"x_min":0,"x_max":424,"ha":525,"o":"m 424 609 l 300 609 l 300 0 l 107 0 l 107 609 l 0 609 l 0 749 l 107 749 q 145 949 107 894 q 328 1019 193 1019 l 424 1015 l 424 855 l 362 855 q 312 841 324 855 q 300 797 300 827 q 300 773 300 786 q 300 749 300 761 l 424 749 l 424 609 "},"“":{"x_min":0,"x_max":468,"ha":567,"o":"m 190 631 l 0 631 l 0 807 q 55 947 0 885 q 190 1017 110 1010 l 190 947 q 88 813 88 921 l 190 813 l 190 631 m 468 631 l 278 631 l 278 807 q 333 947 278 885 q 468 1017 388 1010 l 468 947 q 366 813 366 921 l 468 813 l 468 631 "},"A":{"x_min":0,"x_max":966.671875,"ha":1069,"o":"m 966 0 l 747 0 l 679 208 l 286 208 l 218 0 l 0 0 l 361 1013 l 600 1013 l 966 0 m 623 376 l 480 810 l 340 376 l 623 376 "},"6":{"x_min":57,"x_max":771,"ha":828,"o":"m 744 734 l 544 734 q 500 802 533 776 q 425 828 466 828 q 315 769 359 828 q 264 571 264 701 q 451 638 343 638 q 691 537 602 638 q 771 315 771 449 q 683 79 771 176 q 420 -29 586 -29 q 134 123 227 -29 q 57 455 57 250 q 184 865 57 721 q 452 988 293 988 q 657 916 570 988 q 744 734 744 845 m 426 128 q 538 178 498 128 q 578 300 578 229 q 538 422 578 372 q 415 479 493 479 q 303 430 342 479 q 264 313 264 381 q 308 184 264 240 q 426 128 352 128 "},"‘":{"x_min":0,"x_max":190,"ha":289,"o":"m 190 631 l 0 631 l 0 807 q 55 947 0 885 q 190 1017 110 1010 l 190 947 q 88 813 88 921 l 190 813 l 190 631 "},"ϊ":{"x_min":-55,"x_max":337,"ha":389,"o":"m 337 810 l 208 810 l 208 952 l 337 952 l 337 810 m 74 810 l -55 810 l -55 952 l 74 952 l 74 810 m 242 0 l 42 0 l 42 748 l 242 748 l 242 0 "},"π":{"x_min":0.5,"x_max":838.890625,"ha":938,"o":"m 838 593 l 750 593 l 750 0 l 549 0 l 549 593 l 287 593 l 287 0 l 88 0 l 88 593 l 0 593 l 0 749 l 838 749 l 838 593 "},"ά":{"x_min":-1,"x_max":722,"ha":835,"o":"m 722 0 l 531 0 l 530 101 q 433 8 491 41 q 304 -25 375 -25 q 72 104 157 -25 q -1 372 -1 216 q 72 643 -1 530 q 308 775 158 775 q 433 744 375 775 q 528 656 491 713 l 528 749 l 722 749 l 722 0 m 361 601 q 233 527 277 601 q 196 375 196 464 q 232 224 196 288 q 358 144 277 144 q 487 217 441 144 q 528 370 528 281 q 489 523 528 457 q 361 601 443 601 m 579 1039 l 377 823 l 279 823 l 401 1039 l 579 1039 "},"O":{"x_min":0,"x_max":994,"ha":1094,"o":"m 497 -29 q 133 127 272 -29 q 0 505 0 277 q 131 883 0 733 q 497 1040 270 1040 q 860 883 721 1040 q 994 505 994 733 q 862 127 994 277 q 497 -29 723 -29 m 497 154 q 710 266 631 154 q 780 506 780 365 q 710 745 780 647 q 497 857 631 857 q 283 747 361 857 q 213 506 213 647 q 282 266 213 365 q 497 154 361 154 "},"n":{"x_min":0,"x_max":669,"ha":782,"o":"m 669 0 l 469 0 l 469 452 q 442 553 469 513 q 352 601 412 601 q 245 553 290 601 q 200 441 200 505 l 200 0 l 0 0 l 0 748 l 194 748 l 194 659 q 289 744 230 713 q 416 775 349 775 q 600 700 531 775 q 669 509 669 626 l 669 0 "},"3":{"x_min":61,"x_max":767,"ha":828,"o":"m 767 290 q 653 51 767 143 q 402 -32 548 -32 q 168 48 262 -32 q 61 300 61 140 l 250 300 q 298 173 250 219 q 405 132 343 132 q 514 169 471 132 q 563 282 563 211 q 491 405 563 369 q 343 432 439 432 l 343 568 q 472 592 425 568 q 534 701 534 626 q 493 793 534 758 q 398 829 453 829 q 306 789 344 829 q 268 669 268 749 l 80 669 q 182 909 80 823 q 410 986 274 986 q 633 916 540 986 q 735 719 735 840 q 703 608 735 656 q 615 522 672 561 q 727 427 687 486 q 767 290 767 369 "},"9":{"x_min":58,"x_max":769,"ha":828,"o":"m 769 492 q 646 90 769 232 q 384 -33 539 -33 q 187 35 271 -33 q 83 224 98 107 l 282 224 q 323 154 286 182 q 404 127 359 127 q 513 182 471 127 q 563 384 563 248 q 475 335 532 355 q 372 315 418 315 q 137 416 224 315 q 58 642 58 507 q 144 877 58 781 q 407 984 239 984 q 694 827 602 984 q 769 492 769 699 m 416 476 q 525 521 488 476 q 563 632 563 566 q 521 764 563 709 q 403 826 474 826 q 297 773 337 826 q 258 649 258 720 q 295 530 258 577 q 416 476 339 476 "},"l":{"x_min":41,"x_max":240,"ha":363,"o":"m 240 0 l 41 0 l 41 1013 l 240 1013 l 240 0 "},"¤":{"x_min":40.265625,"x_max":727.203125,"ha":825,"o":"m 727 792 l 594 659 q 620 552 620 609 q 598 459 620 504 l 725 331 l 620 224 l 491 352 q 382 331 443 331 q 273 352 322 331 l 144 224 l 40 330 l 167 459 q 147 552 147 501 q 172 658 147 608 l 40 794 l 147 898 l 283 759 q 383 776 330 776 q 482 759 434 776 l 620 898 l 727 792 m 383 644 q 308 617 334 644 q 283 551 283 590 q 309 489 283 517 q 381 462 335 462 q 456 488 430 462 q 482 554 482 515 q 455 616 482 588 q 383 644 429 644 "},"κ":{"x_min":0,"x_max":691.84375,"ha":779,"o":"m 691 0 l 479 0 l 284 343 l 196 252 l 196 0 l 0 0 l 0 749 l 196 749 l 196 490 l 440 749 l 677 749 l 416 479 l 691 0 "},"4":{"x_min":53,"x_max":775.21875,"ha":828,"o":"m 775 213 l 660 213 l 660 0 l 470 0 l 470 213 l 53 213 l 53 384 l 416 958 l 660 958 l 660 370 l 775 370 l 775 213 m 474 364 l 474 786 l 204 363 l 474 364 "},"p":{"x_min":0,"x_max":722,"ha":824,"o":"m 415 -26 q 287 4 346 -26 q 192 92 228 34 l 192 -298 l 0 -298 l 0 750 l 192 750 l 192 647 q 289 740 230 706 q 416 775 347 775 q 649 645 566 775 q 722 375 722 534 q 649 106 722 218 q 415 -26 564 -26 m 363 603 q 232 529 278 603 q 192 375 192 465 q 230 222 192 286 q 360 146 276 146 q 487 221 441 146 q 526 371 526 285 q 488 523 526 458 q 363 603 443 603 "},"‡":{"x_min":0,"x_max":809,"ha":894,"o":"m 299 621 l 0 621 l 0 804 l 299 804 l 299 1011 l 509 1011 l 509 804 l 809 804 l 809 621 l 509 621 l 509 387 l 809 387 l 809 205 l 509 205 l 509 0 l 299 0 l 299 205 l 0 205 l 0 387 l 299 387 l 299 621 "},"ψ":{"x_min":0,"x_max":875,"ha":979,"o":"m 522 142 q 657 274 620 163 q 671 352 671 316 l 671 748 l 875 748 l 875 402 q 806 134 875 240 q 525 -22 719 -1 l 525 -278 l 349 -278 l 349 -22 q 65 135 152 -1 q 0 402 0 238 l 0 748 l 204 748 l 204 352 q 231 240 204 295 q 353 142 272 159 l 353 922 l 524 922 l 522 142 "},"η":{"x_min":0,"x_max":669,"ha":779,"o":"m 669 -278 l 469 -278 l 469 390 q 448 526 469 473 q 348 606 417 606 q 244 553 288 606 q 201 441 201 501 l 201 0 l 0 0 l 0 749 l 201 749 l 201 665 q 301 744 244 715 q 423 774 359 774 q 606 685 538 774 q 669 484 669 603 l 669 -278 "}},"cssFontWeight":"bold","ascender":1216,"underlinePosition":-100,"cssFontStyle":"normal","boundingBox":{"yMin":-333,"xMin":-162,"yMax":1216,"xMax":1681},"resolution":1000,"original_font_information":{"postscript_name":"Helvetiker-Bold","version_string":"Version 1.00 2004 initial release","vendor_url":"http://www.magenta.gr","full_font_name":"Helvetiker Bold","font_family_name":"Helvetiker","copyright":"Copyright (c) Magenta ltd, 2004.","description":"","trademark":"","designer":"","designer_url":"","unique_font_identifier":"Magenta ltd:Helvetiker Bold:22-10-104","license_url":"http://www.ellak.gr/fonts/MgOpen/license.html","license_description":"Copyright (c) 2004 by MAGENTA Ltd. All Rights Reserved.\r\n\r\nPermission is hereby granted, free of charge, to any person obtaining a copy of the fonts accompanying this license (\"Fonts\") and associated documentation files (the \"Font Software\"), to reproduce and distribute the Font Software, including without limitation the rights to use, copy, merge, publish, distribute, and/or sell copies of the Font Software, and to permit persons to whom the Font Software is furnished to do so, subject to the following conditions: \r\n\r\nThe above copyright and this permission notice shall be included in all copies of one or more of the Font Software typefaces.\r\n\r\nThe Font Software may be modified, altered, or added to, and in particular the designs of glyphs or characters in the Fonts may be modified and additional glyphs or characters may be added to the Fonts, only if the fonts are renamed to names not containing the word \"MgOpen\", or if the modifications are accepted for inclusion in the Font Software itself by the each appointed Administrator.\r\n\r\nThis License becomes null and void to the extent applicable to Fonts or Font Software that has been modified and is distributed under the \"MgOpen\" name.\r\n\r\nThe Font Software may be sold as part of a larger software package but no copy of one or more of the Font Software typefaces may be sold by itself. \r\n\r\nTHE FONT SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF COPYRIGHT, PATENT, TRADEMARK, OR OTHER RIGHT. IN NO EVENT SHALL MAGENTA OR PERSONS OR BODIES IN CHARGE OF ADMINISTRATION AND MAINTENANCE OF THE FONT SOFTWARE BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, INCLUDING ANY GENERAL, SPECIAL, INDIRECT, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF THE USE OR INABILITY TO USE THE FONT SOFTWARE OR FROM OTHER DEALINGS IN THE FONT SOFTWARE.","manufacturer_name":"Magenta ltd","font_sub_family_name":"Bold"},"descender":-334,"familyName":"Helvetiker","lineHeight":1549,"underlineThickness":50});;
var __map_reserved = {}
Audio.SE_ELEMENT_NUM = 4;
Audio.STATE_NOT_INITIALIZED = 0;
Audio.STATE_PLAY = 1;
Audio.STATE_STOP = 2;
Audio.STATE_PAUSE = 3;
GameMgr.DIRECTORY = Reflect.field(window,"DIRECTORY");
GameMgr.WIDTH = 960;
GameMgr.HEIGHT = 480;
GameMgr.players = new haxe_ds_StringMap();
GameMgr.entryNum = 0;
GameMgr.playerColor = ["ff0000","0000ff","009600","ff00ff","ffffff"];
GameMgr.teamColor = new haxe_ds_EnumValueMap();
GameMgr.openRandom = false;
GameMgr.hardClear = false;
GameMgr.sceneMap = new haxe_ds_EnumValueMap();
GameMgr.maxPlayer = 4;
GameMgr.playing = false;
GameMgr.teamBattle = false;
GameMgr.audio = Audio.getInstance();
GameMgr.numbers = [false,false,false,false];
js_Boot.__toStr = {}.toString;
obj2d_RadioButton.change = false;
Main.main();
})(typeof console != "undefined" ? console : {log:function(){}});
