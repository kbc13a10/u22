@echo off
setlocal

rem jar file name
set jarname="ShootingServer.jar"

:start
echo.
java -jar %jarname% --command
echo.

set loop=""
set /p loop="Restart?(Y/N) >"
if "%loop%"=="Y" (
	goto start
) else if "%loop%"=="y" (
	goto start
)

endlocal